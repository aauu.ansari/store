﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using StoreManagement.Reports.crystalReport;
using StoreManagement.Reports.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class AssignmentFORM : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        public int site_iid=0;
        public AssignmentFORM()
        {
            InitializeComponent();
        }
        bool checkSite = false;
        private void FillProjectCode()
        {
            DataTable searchs = service.siteDropdown(from.Text.ToString(), to.Text.ToString());
            DataRow dr = searchs.NewRow();
            dr["code"] = "Select One";
            dr["id"] = 0;
            searchs.Rows.InsertAt(dr, 0);
            search.DataSource = searchs;
            search.DisplayMember = "code";
            search.ValueMember = "id";
            checkSite = true;
        }
        private void AssignmentFORM_Load(object sender, EventArgs e)
        {
            FillProjectCode();
            showReport(site_iid);
        }

        private void showReport(int site_id)
        {
            try
            {
                Site site;
                site = service.siteList_rpt(site_id);
                var assignmentDS = new AssignmentDS();
                DataTable site_dt = assignmentDS.site_tbl;
                if (site != null)
                {
                    DataRow dr = site_dt.NewRow();
                    dr[0] = site.id;
                    dr[1] = site.code;
                    dr[2] = site.name;
                    dr[3] = site.client_name;

                    dr[4] = site.supervisor_name;
                    dr[5] = site.mobilization_date;
                    dr[6] = site.completion_date;
                    dr[7] = site.estimated_time;

                    dr[8] = site.contact_person;
                    dr[9] = site.task;
                    dr[10] = site.client_address;
                    dr[11] = site.purchased_order_no;

                    dr[12] = site.quotation_ref;
                    dr[13] = site.created_at;
                    dr[14] = site.updated_at;
                    dr[15] = site.site_address;
                    dr[16] = site.client_phone;
                    site_dt.Rows.Add(dr);
                }
                AssignmentCR assignmentCR = new AssignmentCR();
                assignmentCR.Database.Tables["site_tbl"].SetDataSource(site_dt);

                SiteInstruments[] siteInstruments;
                siteInstruments = service.siteInstrumentsList_rpt(site_id);
                DataTable siteInstruments_dt = assignmentDS.site_instruments_tbl;
                if (siteInstruments != null)
                {
                    foreach (var item in siteInstruments)
                    {
                        DataRow dr = siteInstruments_dt.NewRow();
                        dr[0] = item.instrument_code;
                        
                        if (item.type=="Consumable")
                        {
                            dr[1] = item.instrument_name + "  Qty  ( " + item.quantity.ToString() + " / " + item.received_quantity + " )";
                        }
                        else
                        {
                            dr[1] = item.instrument_name;
                        }
                        dr[2] = item.instrument_code;
                        dr[3] = item.status;
                        dr[4] = item.description;
                        siteInstruments_dt.Rows.Add(dr);
                    }
                    assignmentCR.Database.Tables["site_instruments_tbl"].SetDataSource(siteInstruments_dt);
                }
                SiteTeams[] siteTeams;
                siteTeams = service.siteTeamsList_rpt(site_id);
                DataTable siteTeams_dt = assignmentDS.site_teams_tbl;
                if (siteTeams != null)
                {
                    foreach (var item in siteTeams)
                    {
                        DataRow dr = siteTeams_dt.NewRow();
                        dr[1] = item.team_name;
                        dr[2] = item.role;
                        dr[3] = item.contact_number;
                        siteTeams_dt.Rows.Add(dr);
                    }
                    assignmentCR.Database.Tables["site_teams_tbl"].SetDataSource(siteTeams_dt);
                }

                SiteEquipmentIssued siteEquipmentIssued = service.siteEquipmentIssuedList_rpt(site_id);
                DataTable site_equipment_issued_tbl_dt = assignmentDS.site_equipment_issued_tbl;
                if (siteEquipmentIssued != null)
                {
                    string[] tokens = siteEquipmentIssued.name_by_to.Split('-');
                    DataRow dr = site_equipment_issued_tbl_dt.NewRow();
                    dr[0] = 0;
                    dr[1] = tokens[0];
                    dr[2] = tokens[1];
                    dr[3] = 0;
                    site_equipment_issued_tbl_dt.Rows.Add(dr);
                    assignmentCR.Database.Tables["site_equipment_issued_tbl"].SetDataSource(site_equipment_issued_tbl_dt);

                }


                assignmentReportViewer.ReportSource = null;
                assignmentReportViewer.ReportSource = assignmentCR;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }           
        }

        private void search_TextChanged(object sender, EventArgs e)
        {
            if (checkSite)
            {
                showReport(Convert.ToInt32(search.SelectedValue));
            }
        }

        private void open_Click(object sender, EventArgs e)
        {
            showReport(Convert.ToInt32(search.SelectedValue));
        }

        private void fron_to_btn_Click(object sender, EventArgs e)
        {
            FillProjectCode();
        }
    }
}
