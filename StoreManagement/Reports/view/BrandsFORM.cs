﻿using StoreManagement.Common;
using StoreManagement.Reports.crystalReport;
using StoreManagement.Reports.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class BrandsFORM : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        DataView dv;
        public BrandsFORM()
        {
            InitializeComponent();
        }

        private void BrandsFORM_Load(object sender, EventArgs e)
        {
            dv = new DataView(service.brandList_dataTable());
            showReport();
        }
        private void showReport()
        {            
            var brandsDS = new BrandsDS();
            DataTable brands_dt = brandsDS.Brands_tbl;
            brands_dt = dv.ToTable();
            BrandsCR brandsCR = new BrandsCR();
            brandsCR.Database.Tables["Brands_tbl"].SetDataSource(brands_dt);
            repaircrystalReportViewer.ReportSource = null;
            repaircrystalReportViewer.ReportSource = brandsCR;

        }

        private void search_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dv.RowFilter = "Name Like '%" + search.Text + "%'";
                showReport();
                search.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void all_Click(object sender, EventArgs e)
        {
            dv = new DataView(service.brandList_dataTable());
            showReport();
        }
    }
}
