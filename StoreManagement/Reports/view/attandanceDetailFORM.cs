﻿using CrystalDecisions.CrystalReports.Engine;
using StoreManagement.Common;
using StoreManagement.Reports.crystalReport;
using StoreManagement.Reports.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class attandanceDetailFORM : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        DataView dv_enployeesList;
        DataView dv_attendance;
        public attandanceDetailFORM()
        {
            InitializeComponent();
        }
        private int attendanceSite()
        {
            if (attendanceSite_id.Text == "Office Attendance")
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        private void attandanceDetailFORM_Load(object sender, EventArgs e)
        {            
            showReport();
        }
        private void showReport()
        {
            dv_enployeesList = new DataView(service.employee_dataTable(attendanceSite()));
            dv_attendance = new DataView(service.attendanceTBL_RPT(attendance_from.Text, attendance_to.Text));
            var attendanceDetailDS = new attandanceDetailDS();
            DataTable employees_dt = attendanceDetailDS.EmployeesTBL;
            employees_dt = dv_enployeesList.ToTable();
            DataTable Attendance_dt = attendanceDetailDS.AttendanceTBL;
            Attendance_dt = dv_attendance.ToTable();
            attandanceDetailCR attendanceDetail_CR = new attandanceDetailCR();
            TextObject title = (TextObject)attendanceDetail_CR.ReportDefinition.Sections["Section1"].ReportObjects["title"];
            title.Text = "From "+ attendance_from.Text+"              To "+ attendance_to.Text;
            TextObject report = (TextObject)attendanceDetail_CR.ReportDefinition.Sections["Section1"].ReportObjects["report"];
            if (attendanceSite_id.Text != "Office Attendance")
            {
                report.Text = "ATTENDANCE SHEET(PKLI)";
            }
            else
            {
                report.Text = "ATTENDANCE SHEET";
            }
            attendanceDetail_CR.Database.Tables["EmployeesTBL"].SetDataSource(employees_dt);
            attendanceDetail_CR.Database.Tables["AttendanceTBL"].SetDataSource(Attendance_dt);
            attendance_ReportViewer.ReportSource = null;
            attendance_ReportViewer.ReportSource = attendanceDetail_CR;

        }
        private void Search_btn_Click(object sender, EventArgs e)
        {
            showReport();
        }

        private void attendanceSite_id_TextChanged(object sender, EventArgs e)
        {
            showReport();
        }
    }
}
