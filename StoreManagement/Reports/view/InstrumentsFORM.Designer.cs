﻿namespace StoreManagement.Reports.view
{
    partial class InstrumentsFORM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.instrumentReportViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.by_life = new System.Windows.Forms.TextBox();
            this.life_btn = new MetroFramework.Controls.MetroButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.by_status = new System.Windows.Forms.ComboBox();
            this.status_btn = new MetroFramework.Controls.MetroButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.by_brand = new System.Windows.Forms.TextBox();
            this.brand_btn = new MetroFramework.Controls.MetroButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.by_code = new System.Windows.Forms.TextBox();
            this.code_btn = new MetroFramework.Controls.MetroButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.by_name = new System.Windows.Forms.TextBox();
            this.name_btn = new MetroFramework.Controls.MetroButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.by_category = new System.Windows.Forms.TextBox();
            this.category_btn = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.instrumentReportViewer);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox6);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox5);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox4);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Size = new System.Drawing.Size(974, 800);
            this.splitContainer1.SplitterDistance = 694;
            this.splitContainer1.TabIndex = 1;
            // 
            // instrumentReportViewer
            // 
            this.instrumentReportViewer.ActiveViewIndex = -1;
            this.instrumentReportViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.instrumentReportViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.instrumentReportViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.instrumentReportViewer.Location = new System.Drawing.Point(0, 0);
            this.instrumentReportViewer.Name = "instrumentReportViewer";
            this.instrumentReportViewer.Size = new System.Drawing.Size(694, 800);
            this.instrumentReportViewer.TabIndex = 0;
            this.instrumentReportViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.by_life);
            this.groupBox6.Controls.Add(this.life_btn);
            this.groupBox6.Location = new System.Drawing.Point(12, 480);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(249, 85);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "BY LIFE";
            // 
            // by_life
            // 
            this.by_life.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.by_life.Location = new System.Drawing.Point(6, 19);
            this.by_life.Name = "by_life";
            this.by_life.Size = new System.Drawing.Size(225, 25);
            this.by_life.TabIndex = 14;
            // 
            // life_btn
            // 
            this.life_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.life_btn.Location = new System.Drawing.Point(6, 48);
            this.life_btn.Name = "life_btn";
            this.life_btn.Size = new System.Drawing.Size(77, 31);
            this.life_btn.TabIndex = 81;
            this.life_btn.Text = "LIFE";
            this.life_btn.UseSelectable = true;
            this.life_btn.Click += new System.EventHandler(this.life_btn_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.by_status);
            this.groupBox5.Controls.Add(this.status_btn);
            this.groupBox5.Location = new System.Drawing.Point(12, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(249, 85);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "BY STATUS";
            // 
            // by_status
            // 
            this.by_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.by_status.FormattingEnabled = true;
            this.by_status.Items.AddRange(new object[] {
            "OK",
            "Un-Serviceable",
            "Serviceable",
            "Send To Repair",
            "On Site"});
            this.by_status.Location = new System.Drawing.Point(6, 13);
            this.by_status.Name = "by_status";
            this.by_status.Size = new System.Drawing.Size(231, 28);
            this.by_status.TabIndex = 13;
            // 
            // status_btn
            // 
            this.status_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.status_btn.Location = new System.Drawing.Point(6, 47);
            this.status_btn.Name = "status_btn";
            this.status_btn.Size = new System.Drawing.Size(77, 31);
            this.status_btn.TabIndex = 3;
            this.status_btn.Text = "STATUS";
            this.status_btn.UseSelectable = true;
            this.status_btn.Click += new System.EventHandler(this.status_btn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.by_brand);
            this.groupBox4.Controls.Add(this.brand_btn);
            this.groupBox4.Location = new System.Drawing.Point(12, 103);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(249, 89);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "BY BRAND NAME";
            // 
            // by_brand
            // 
            this.by_brand.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.by_brand.Location = new System.Drawing.Point(6, 19);
            this.by_brand.Name = "by_brand";
            this.by_brand.Size = new System.Drawing.Size(231, 25);
            this.by_brand.TabIndex = 5;
            // 
            // brand_btn
            // 
            this.brand_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.brand_btn.Location = new System.Drawing.Point(6, 50);
            this.brand_btn.Name = "brand_btn";
            this.brand_btn.Size = new System.Drawing.Size(77, 31);
            this.brand_btn.TabIndex = 6;
            this.brand_btn.Text = "BRAND";
            this.brand_btn.UseSelectable = true;
            this.brand_btn.Click += new System.EventHandler(this.brand_btn_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.by_code);
            this.groupBox3.Controls.Add(this.code_btn);
            this.groupBox3.Location = new System.Drawing.Point(12, 198);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(249, 88);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "BY INSTRUMENT CODE";
            // 
            // by_code
            // 
            this.by_code.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.by_code.Location = new System.Drawing.Point(6, 19);
            this.by_code.Name = "by_code";
            this.by_code.Size = new System.Drawing.Size(231, 25);
            this.by_code.TabIndex = 8;
            // 
            // code_btn
            // 
            this.code_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.code_btn.Location = new System.Drawing.Point(6, 50);
            this.code_btn.Name = "code_btn";
            this.code_btn.Size = new System.Drawing.Size(77, 31);
            this.code_btn.TabIndex = 9;
            this.code_btn.Text = "CODE";
            this.code_btn.UseSelectable = true;
            this.code_btn.Click += new System.EventHandler(this.code_btn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.by_name);
            this.groupBox1.Controls.Add(this.name_btn);
            this.groupBox1.Location = new System.Drawing.Point(12, 388);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(249, 85);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "BY INSTRUMENT NAME";
            // 
            // by_name
            // 
            this.by_name.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.by_name.Location = new System.Drawing.Point(6, 19);
            this.by_name.Name = "by_name";
            this.by_name.Size = new System.Drawing.Size(225, 25);
            this.by_name.TabIndex = 14;
            // 
            // name_btn
            // 
            this.name_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.name_btn.Location = new System.Drawing.Point(6, 50);
            this.name_btn.Name = "name_btn";
            this.name_btn.Size = new System.Drawing.Size(77, 31);
            this.name_btn.TabIndex = 15;
            this.name_btn.Text = "NAME";
            this.name_btn.UseSelectable = true;
            this.name_btn.Click += new System.EventHandler(this.name_btn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.by_category);
            this.groupBox2.Controls.Add(this.category_btn);
            this.groupBox2.Location = new System.Drawing.Point(12, 292);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(249, 90);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "BY CATEGORY ";
            // 
            // by_category
            // 
            this.by_category.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.by_category.Location = new System.Drawing.Point(6, 19);
            this.by_category.Name = "by_category";
            this.by_category.Size = new System.Drawing.Size(231, 25);
            this.by_category.TabIndex = 11;
            // 
            // category_btn
            // 
            this.category_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.category_btn.Location = new System.Drawing.Point(6, 50);
            this.category_btn.Name = "category_btn";
            this.category_btn.Size = new System.Drawing.Size(77, 31);
            this.category_btn.TabIndex = 12;
            this.category_btn.Text = "CATEGORY";
            this.category_btn.UseSelectable = true;
            this.category_btn.Click += new System.EventHandler(this.category_btn_Click);
            // 
            // InstrumentsFORM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(0, 800);
            this.ClientSize = new System.Drawing.Size(991, 513);
            this.Controls.Add(this.splitContainer1);
            this.Name = "InstrumentsFORM";
            this.Text = "InstrumentsFORM";
            this.Load += new System.EventHandler(this.InstrumentsFORM_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer instrumentReportViewer;
        private System.Windows.Forms.TextBox by_name;
        private MetroFramework.Controls.MetroButton life_btn;
        private MetroFramework.Controls.MetroButton name_btn;
        private MetroFramework.Controls.MetroButton brand_btn;
        private MetroFramework.Controls.MetroButton code_btn;
        private MetroFramework.Controls.MetroButton category_btn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox by_code;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox by_category;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private MetroFramework.Controls.MetroButton status_btn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox by_brand;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox by_life;
        private System.Windows.Forms.ComboBox by_status;
    }
}