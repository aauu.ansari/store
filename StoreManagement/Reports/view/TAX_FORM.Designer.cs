﻿namespace StoreManagement.Reports.view
{
    partial class TAX_FORM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TAXRV = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.client_btn = new MetroFramework.Controls.MetroButton();
            this.to = new MetroFramework.Controls.MetroDateTime();
            this.from = new MetroFramework.Controls.MetroDateTime();
            this.client_id = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TAXRV
            // 
            this.TAXRV.ActiveViewIndex = -1;
            this.TAXRV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TAXRV.Cursor = System.Windows.Forms.Cursors.Default;
            this.TAXRV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TAXRV.Location = new System.Drawing.Point(3, 74);
            this.TAXRV.Name = "TAXRV";
            this.TAXRV.Size = new System.Drawing.Size(927, 455);
            this.TAXRV.TabIndex = 1;
            this.TAXRV.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.TAXRV, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(933, 532);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.client_btn);
            this.panel1.Controls.Add(this.to);
            this.panel1.Controls.Add(this.from);
            this.panel1.Controls.Add(this.client_id);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(927, 65);
            this.panel1.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(158, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 15);
            this.label4.TabIndex = 84;
            this.label4.Text = "TO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(317, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 15);
            this.label3.TabIndex = 83;
            this.label3.Text = "SEARCH BY PROJECT CODE";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 82;
            this.label1.Text = "FROM";
            // 
            // client_btn
            // 
            this.client_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Refresh_2_icon;
            this.client_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.client_btn.Location = new System.Drawing.Point(641, 25);
            this.client_btn.Name = "client_btn";
            this.client_btn.Size = new System.Drawing.Size(30, 28);
            this.client_btn.TabIndex = 80;
            this.client_btn.UseSelectable = true;
            this.client_btn.Click += new System.EventHandler(this.client_btn_Click);
            // 
            // to
            // 
            this.to.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.to.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.to.Location = new System.Drawing.Point(161, 26);
            this.to.MinimumSize = new System.Drawing.Size(0, 25);
            this.to.Name = "to";
            this.to.Size = new System.Drawing.Size(149, 25);
            this.to.TabIndex = 81;
            // 
            // from
            // 
            this.from.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.from.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.from.Location = new System.Drawing.Point(9, 27);
            this.from.MinimumSize = new System.Drawing.Size(0, 25);
            this.from.Name = "from";
            this.from.Size = new System.Drawing.Size(146, 25);
            this.from.TabIndex = 79;
            // 
            // client_id
            // 
            this.client_id.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_id.FormattingEnabled = true;
            this.client_id.Location = new System.Drawing.Point(320, 25);
            this.client_id.Margin = new System.Windows.Forms.Padding(7);
            this.client_id.Name = "client_id";
            this.client_id.Size = new System.Drawing.Size(311, 27);
            this.client_id.TabIndex = 77;
            // 
            // TAX_FORM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 532);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "TAX_FORM";
            this.Text = "TAX_FORM";
            this.Load += new System.EventHandler(this.TAX_FORM_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer TAXRV;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroButton client_btn;
        private MetroFramework.Controls.MetroDateTime to;
        private MetroFramework.Controls.MetroDateTime from;
        private System.Windows.Forms.ComboBox client_id;
    }
}