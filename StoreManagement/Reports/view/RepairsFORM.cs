﻿using StoreManagement.Common;
using StoreManagement.Reports.crystalReport;
using StoreManagement.Reports.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class RepairsFORM : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        DataView dv;
        public RepairsFORM()
        {
            InitializeComponent();
        }

        private void RepairsFORM_Load(object sender, EventArgs e)
        {
            dv = new DataView(service.repairInstrumentsList_datatable_rpt("01-01-2000", "01-01-2050"));
        }
        private void showReport()
        {
            var repairDS = new RepairDS();
            DataTable repair_dt = repairDS.repair_tbl;
            repair_dt = dv.ToTable();
            RepairCR repairCR = new RepairCR();
            repairCR.Database.Tables["repair_tbl"].SetDataSource(repair_dt);
            repaircrystalReportViewer.ReportSource = null;
            repaircrystalReportViewer.ReportSource = repairCR;

        }

        private void fron_to_btn_Click(object sender, EventArgs e)
        {
            dv = new DataView(service.repairInstrumentsList_datatable_rpt(from.Text, to.Text));
            showReport();
        }

        private void instrument_search_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dv.RowFilter = "name Like '%" + instrument_search.Text + "%'";
                showReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void all_Click(object sender, EventArgs e)
        {
            dv = new DataView(service.repairInstrumentsList_datatable_rpt("1/1/2000", "1/1/2050"));
            showReport();
        }
    }
}
