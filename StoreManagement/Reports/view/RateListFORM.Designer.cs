﻿namespace StoreManagement.Reports.view
{
    partial class RateListFORM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RateListRV = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // RateListRV
            // 
            this.RateListRV.ActiveViewIndex = -1;
            this.RateListRV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RateListRV.Cursor = System.Windows.Forms.Cursors.Default;
            this.RateListRV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RateListRV.Location = new System.Drawing.Point(0, 0);
            this.RateListRV.Name = "RateListRV";
            this.RateListRV.Size = new System.Drawing.Size(894, 654);
            this.RateListRV.TabIndex = 1;
            this.RateListRV.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // RateListFORM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 654);
            this.Controls.Add(this.RateListRV);
            this.Name = "RateListFORM";
            this.Text = "RateListFORM";
            this.Load += new System.EventHandler(this.RateListFORM_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer RateListRV;
    }
}