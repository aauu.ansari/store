﻿using StoreManagement.BS_SERVICES_BsBill;
using StoreManagement.Common;
using StoreManagement.Reports.crystalReport;
using StoreManagement.Reports.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class SaleInvoiceForm : Form
    {
        CommonHelper common = new CommonHelper();
        Messages messages = new Messages();
        BS_SERVICES_BsBill.IbsBillClient billService = new BS_SERVICES_BsBill.IbsBillClient();
        BS_SERVICES.IbsStoreClient storeService = new BS_SERVICES.IbsStoreClient();
        public int site_id = 0;
        public SaleInvoiceForm()
        {
            InitializeComponent();
        }

        private void SaleInvoiceForm_Load(object sender, EventArgs e)
        {
            try
            {
                showReport(site_id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            invoice_type.SelectedIndex = 0;
        }
        private void showReport(int site_id)
        {
            try
            {
                var billDS = new BillDS();
                SalesTaxInvoiceCR salesTaxInvoiceCR = new SalesTaxInvoiceCR();
                Bill bill = new Bill();
                bill = billService.billRow(site_id);
                DataTable bill_dt = billDS.bill;
                if (bill != null)
                {
                    DataRow dr = bill_dt.NewRow();
                    dr[0] = bill.id;
                    dr[1] = bill.client_name;
                    dr[2] = bill.client_id;
                    dr[3] = bill.client_phone;
                    dr[4] = bill.invoice_no;
                    dr[5] = bill.lab_no;
                    dr[6] = bill.access_code;
                    dr[7] = bill.contractor;
                    dr[8] = bill.consultant;
                    dr[9] = bill.project;
                    dr[10] = bill.site_id;
                    dr[11] = bill.location;
                    dr[12] = bill.specimen;
                    dr[13] = bill.updated_at;
                    dr[14] = bill.NTN;
                    dr[15] = bill.PRA;
                    dr[16] = bill.user_id;
                    dr[17] = bill.bill_to;
                    dr[18] = bill.bill_details;
                    dr[19] = bill.order_no;
                    dr[20] = bill.order_date;
                    bill_dt.Rows.Add(dr);
                }
                if (bill != null)
                {
                    if (invoice_type.Text == "Discount Invoice")
                    {
                        salesTaxInvoiceCR.Subreports[0].ReportDefinition.Sections["ReportFooterSection4"].SectionFormat.EnableSuppress = true;
                    }
                    else
                    {
                        salesTaxInvoiceCR.Subreports[0].ReportDefinition.Sections["ReportFooterSection2"].SectionFormat.EnableSuppress = true;
                    }
                    salesTaxInvoiceCR.Database.Tables["bill"].SetDataSource(bill_dt);
                    salesTaxInvoiceCR.Database.Tables["discount_tbl"].SetDataSource(billService.billReceiptsDiscount_bill_dt(site_id));
                    salesTaxInvoiceCR.Database.Tables["balances"].SetDataSource(billService.billReceiptsBalances_bill_dt(site_id));
                    salesTaxInvoiceCR.Database.Tables["bill_details_tbl"].SetDataSource(billService.billDetailsList_dt(site_id));
                    
                    salesTaxInvoiceCR.Database.Tables["bill_receipts"].SetDataSource(billService.billReceiptsList_dt(site_id));
                    salesTaxInvoiceCR.Database.Tables["taxableAmount"].SetDataSource(billService.billIstaxable_dt(site_id));
                    SaleInvoiceRV.ReportSource = null;
                    SaleInvoiceRV.ReportSource = salesTaxInvoiceCR;
                }
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        private void invoice_type_TextChanged(object sender, EventArgs e)
        {
            showReport(site_id);
        }
    }
}
