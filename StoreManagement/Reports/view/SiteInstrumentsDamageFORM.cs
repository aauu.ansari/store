﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using StoreManagement.Reports.crystalReport;
using StoreManagement.Reports.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class SiteInstrumentsDamageFORM : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        bool checkSite = false;
        public SiteInstrumentsDamageFORM()
        {
            InitializeComponent();
        }

        private void fron_to_btn_Click(object sender, EventArgs e)
        {
            FillProjectCode();
        }

        private void FillProjectCode()
        {
            DataTable searchs = service.siteDropdown(from.Text.ToString(), to.Text.ToString());
            DataRow dr = searchs.NewRow();
            dr["code"] = "Select One";
            dr["id"] = 0;
            searchs.Rows.InsertAt(dr, 0);
            search.DataSource = searchs;
            search.DisplayMember = "code";
            search.ValueMember = "id";
            checkSite = true;
        }

        private void open_Click(object sender, EventArgs e)
        {
            showReport(Convert.ToInt32(search.SelectedValue));
        }

        private void showReport(int site_id)
        {
            try
            {
                var siteInstrumentsDamageDS = new SiteInstrumentsDamageDS();
                SiteInstrumentsDamageCR siteInstrumentsDamageCR = new SiteInstrumentsDamageCR();
                DataTable site_dt = siteInstrumentsDamageDS.site_tbl;
                OnsiteDamagedInstruments_rpt onsiteDamagedInstruments_rpt = new OnsiteDamagedInstruments_rpt();
                onsiteDamagedInstruments_rpt= service.siteForDamageIntruments_rpt(site_id);
                if (onsiteDamagedInstruments_rpt != null)
                {
                    DataRow dr = site_dt.NewRow();
                    dr[0] = onsiteDamagedInstruments_rpt.name;
                    dr[1] = onsiteDamagedInstruments_rpt.address;
                    dr[2] = onsiteDamagedInstruments_rpt.supervisor_name;
                    dr[3] = onsiteDamagedInstruments_rpt.mobilization_date;
                    dr[4] = onsiteDamagedInstruments_rpt.completion_date;
                    dr[5] = onsiteDamagedInstruments_rpt.code;
                    site_dt.Rows.Add(dr);
                    siteInstrumentsDamageCR.Database.Tables["site_tbl"].SetDataSource(site_dt);
                }

                SiteInstruments[] siteInstruments;
                siteInstruments = service.OnsiteDamageInstrumentsList_dataTable_rpt(site_id);
                DataTable siteInstruments_dt = siteInstrumentsDamageDS.instruments_tbl;
                if (siteInstruments != null)
                {
                    foreach (var item in siteInstruments)
                    {
                        DataRow dr = siteInstruments_dt.NewRow();
                        dr[0] = item.instrument_name;
                        dr[1] = item.instrument_code;
                        dr[2] = item.status;
                        dr[3] = item.description;
                        siteInstruments_dt.Rows.Add(dr);
                    }
                    siteInstrumentsDamageCR.Database.Tables["instruments_tbl"].SetDataSource(siteInstruments_dt);
                }

                SiteEquipmentIssued siteEquipmentIssued = service.siteEquipmentIssuedList_rpt(site_id);
                DataTable site_equipment_issued_tbl_dt = siteInstrumentsDamageDS.site_equipment_issued_tbl;
                if (siteEquipmentIssued != null)
                {
                    string[] tokens = new string[2];
                     tokens = siteEquipmentIssued.name_by_to.Split('-');
                    DataRow dr = site_equipment_issued_tbl_dt.NewRow();
                    dr[0] = tokens[0];
                    dr[1] = tokens[1];
                    site_equipment_issued_tbl_dt.Rows.Add(dr);
                    siteInstrumentsDamageCR.Database.Tables["site_equipment_issued_tbl"].SetDataSource(site_equipment_issued_tbl_dt);

                }


                DamageInstrumentsReportViewer.ReportSource = null;
                DamageInstrumentsReportViewer.ReportSource = siteInstrumentsDamageCR;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void search_TextChanged(object sender, EventArgs e)
        {
            if (checkSite)
            {
                showReport(Convert.ToInt32(search.SelectedValue));
            }
        }

        private void SiteInstrumentsDamageFORM_Load(object sender, EventArgs e)
        {

        }
    }
}
