﻿namespace StoreManagement.Reports.view
{
    partial class SiteInstrumentsDamageFORM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.fron_to_btn = new MetroFramework.Controls.MetroButton();
            this.to = new MetroFramework.Controls.MetroDateTime();
            this.from = new MetroFramework.Controls.MetroDateTime();
            this.search = new System.Windows.Forms.ComboBox();
            this.open = new MetroFramework.Controls.MetroButton();
            this.DamageInstrumentsReportViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.fron_to_btn);
            this.splitContainer1.Panel1.Controls.Add(this.to);
            this.splitContainer1.Panel1.Controls.Add(this.from);
            this.splitContainer1.Panel1.Controls.Add(this.search);
            this.splitContainer1.Panel1.Controls.Add(this.open);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.DamageInstrumentsReportViewer);
            this.splitContainer1.Size = new System.Drawing.Size(914, 632);
            this.splitContainer1.SplitterDistance = 90;
            this.splitContainer1.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(161, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 15);
            this.label4.TabIndex = 76;
            this.label4.Text = "TO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(385, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 15);
            this.label3.TabIndex = 75;
            this.label3.Text = "SEARCH BY PROJECT CODE";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 74;
            this.label1.Text = "FROM";
            // 
            // fron_to_btn
            // 
            this.fron_to_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Refresh_2_icon;
            this.fron_to_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fron_to_btn.Location = new System.Drawing.Point(319, 28);
            this.fron_to_btn.Name = "fron_to_btn";
            this.fron_to_btn.Size = new System.Drawing.Size(30, 28);
            this.fron_to_btn.TabIndex = 72;
            this.fron_to_btn.UseSelectable = true;
            this.fron_to_btn.Click += new System.EventHandler(this.fron_to_btn_Click);
            // 
            // to
            // 
            this.to.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.to.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.to.Location = new System.Drawing.Point(164, 30);
            this.to.MinimumSize = new System.Drawing.Size(0, 25);
            this.to.Name = "to";
            this.to.Size = new System.Drawing.Size(149, 25);
            this.to.TabIndex = 73;
            // 
            // from
            // 
            this.from.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.from.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.from.Location = new System.Drawing.Point(12, 31);
            this.from.MinimumSize = new System.Drawing.Size(0, 25);
            this.from.Name = "from";
            this.from.Size = new System.Drawing.Size(146, 25);
            this.from.TabIndex = 71;
            // 
            // search
            // 
            this.search.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search.FormattingEnabled = true;
            this.search.Location = new System.Drawing.Point(388, 28);
            this.search.Margin = new System.Windows.Forms.Padding(7);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(311, 27);
            this.search.TabIndex = 69;
            this.search.TextChanged += new System.EventHandler(this.search_TextChanged);
            // 
            // open
            // 
            this.open.BackgroundImage = global::StoreManagement.Properties.Resources.open_file_icon;
            this.open.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.open.Location = new System.Drawing.Point(705, 31);
            this.open.Name = "open";
            this.open.Size = new System.Drawing.Size(29, 21);
            this.open.TabIndex = 70;
            this.open.UseSelectable = true;
            this.open.Click += new System.EventHandler(this.open_Click);
            // 
            // DamageInstrumentsReportViewer
            // 
            this.DamageInstrumentsReportViewer.ActiveViewIndex = -1;
            this.DamageInstrumentsReportViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DamageInstrumentsReportViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.DamageInstrumentsReportViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DamageInstrumentsReportViewer.Location = new System.Drawing.Point(0, 0);
            this.DamageInstrumentsReportViewer.Name = "DamageInstrumentsReportViewer";
            this.DamageInstrumentsReportViewer.Size = new System.Drawing.Size(914, 538);
            this.DamageInstrumentsReportViewer.TabIndex = 0;
            this.DamageInstrumentsReportViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // SiteInstrumentsDamageFORM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 632);
            this.Controls.Add(this.splitContainer1);
            this.Name = "SiteInstrumentsDamageFORM";
            this.Text = "SiteInstrumentsDamageFORM";
            this.Load += new System.EventHandler(this.SiteInstrumentsDamageFORM_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroButton fron_to_btn;
        private MetroFramework.Controls.MetroDateTime to;
        private MetroFramework.Controls.MetroDateTime from;
        private System.Windows.Forms.ComboBox search;
        private MetroFramework.Controls.MetroButton open;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer DamageInstrumentsReportViewer;
    }
}