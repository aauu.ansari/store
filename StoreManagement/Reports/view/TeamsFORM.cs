﻿using StoreManagement.Common;
using StoreManagement.Reports.crystalReport;
using StoreManagement.Reports.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class TeamsFORM : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        DataView dv;
        public TeamsFORM()
        {
            InitializeComponent();
        }

        private void TeamsFORM_Load(object sender, EventArgs e)
        {
            dv = new DataView(service.teamList_dataTable());
            showReport();
        }
        private void showReport()
        {
            var teamsDS = new TeamsDS();
            DataTable teams_dt = teamsDS.Teams_tbl;
            teams_dt = dv.ToTable();
            TeamsCR teamsCR = new TeamsCR();
            teamsCR.Database.Tables["Teams_tbl"].SetDataSource(teams_dt);
            repaircrystalReportViewer.ReportSource = null;
            repaircrystalReportViewer.ReportSource = teamsCR;

        }

        private void search_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dv.RowFilter = "Name Like '%" + search.Text + "%'";
                showReport();
                search.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void all_Click(object sender, EventArgs e)
        {
            dv = new DataView(service.teamList_dataTable());
            showReport();
        }
    }
}
