﻿using StoreManagement.Reports.crystalReport;
using StoreManagement.Reports.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class RateListFORM : Form
    {
        public RateListFORM()
        {
            InitializeComponent();
        }
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        DataView dv;
        private void showReport()
        {
            var CommonDS = new CommonDS();
            DataTable rateList_dt = CommonDS.RateList_tbl;
            rateList_dt = dv.ToTable();
            RateListCR rateListCR = new RateListCR();
            rateListCR.Database.Tables["RateList_tbl"].SetDataSource(rateList_dt);
            RateListRV.ReportSource = null;
            RateListRV.ReportSource = rateListCR;

        }

        private void RateListFORM_Load(object sender, EventArgs e)
        {
            dv = new DataView(service.rateList_dataTable());
            showReport();
        }
    }
}
