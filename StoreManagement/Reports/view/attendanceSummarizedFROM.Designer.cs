﻿namespace StoreManagement.Reports.view
{
    partial class attendanceSummarizedFROM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.attendance_summary_ReportViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.attendance_from = new System.Windows.Forms.DateTimePicker();
            this.Search_btn = new System.Windows.Forms.Button();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.attendance_to = new System.Windows.Forms.DateTimePicker();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.attendanceSite_id = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // attendance_summary_ReportViewer
            // 
            this.attendance_summary_ReportViewer.ActiveViewIndex = -1;
            this.attendance_summary_ReportViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.attendance_summary_ReportViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.attendance_summary_ReportViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attendance_summary_ReportViewer.Location = new System.Drawing.Point(3, 61);
            this.attendance_summary_ReportViewer.Name = "attendance_summary_ReportViewer";
            this.attendance_summary_ReportViewer.Size = new System.Drawing.Size(848, 641);
            this.attendance_summary_ReportViewer.TabIndex = 1;
            this.attendance_summary_ReportViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.attendance_summary_ReportViewer, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.226951F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.77305F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(854, 705);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.attendanceSite_id);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.attendance_from);
            this.panel1.Controls.Add(this.Search_btn);
            this.panel1.Controls.Add(this.metroLabel2);
            this.panel1.Controls.Add(this.attendance_to);
            this.panel1.Controls.Add(this.metroLabel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(848, 52);
            this.panel1.TabIndex = 2;
            // 
            // attendance_from
            // 
            this.attendance_from.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.attendance_from.Location = new System.Drawing.Point(63, 19);
            this.attendance_from.Name = "attendance_from";
            this.attendance_from.Size = new System.Drawing.Size(107, 20);
            this.attendance_from.TabIndex = 14;
            // 
            // Search_btn
            // 
            this.Search_btn.Location = new System.Drawing.Point(345, 18);
            this.Search_btn.Name = "Search_btn";
            this.Search_btn.Size = new System.Drawing.Size(75, 23);
            this.Search_btn.TabIndex = 17;
            this.Search_btn.Text = "Search";
            this.Search_btn.UseVisualStyleBackColor = true;
            this.Search_btn.Click += new System.EventHandler(this.Search_btn_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(9, 20);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(48, 19);
            this.metroLabel2.TabIndex = 13;
            this.metroLabel2.Text = "From :";
            // 
            // attendance_to
            // 
            this.attendance_to.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.attendance_to.Location = new System.Drawing.Point(232, 19);
            this.attendance_to.Name = "attendance_to";
            this.attendance_to.Size = new System.Drawing.Size(107, 20);
            this.attendance_to.TabIndex = 16;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(197, 20);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(29, 19);
            this.metroLabel1.TabIndex = 15;
            this.metroLabel1.Text = "To :";
            // 
            // attendanceSite_id
            // 
            this.attendanceSite_id.FormattingEnabled = true;
            this.attendanceSite_id.Items.AddRange(new object[] {
            "Office Attendance",
            "PKLI Site Attendance"});
            this.attendanceSite_id.Location = new System.Drawing.Point(584, 18);
            this.attendanceSite_id.Name = "attendanceSite_id";
            this.attendanceSite_id.Size = new System.Drawing.Size(255, 21);
            this.attendanceSite_id.TabIndex = 59;
            this.attendanceSite_id.TextChanged += new System.EventHandler(this.attendanceSite_id_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(531, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 14);
            this.label7.TabIndex = 58;
            this.label7.Text = "Type : ";
            // 
            // attendanceSummarizedFROM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 705);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "attendanceSummarizedFROM";
            this.Text = "attendanceSummarizedFROM";
            this.Load += new System.EventHandler(this.attendanceSummarizedFROM_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer attendance_summary_ReportViewer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker attendance_from;
        private System.Windows.Forms.Button Search_btn;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.DateTimePicker attendance_to;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.ComboBox attendanceSite_id;
        private System.Windows.Forms.Label label7;
    }
}