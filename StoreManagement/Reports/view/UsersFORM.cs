﻿using StoreManagement.Common;
using StoreManagement.Reports.crystalReport;
using StoreManagement.Reports.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class UsersFORM : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        DataView dv;
        public UsersFORM()
        {
            InitializeComponent();
        }

        private void UsersFORM_Load(object sender, EventArgs e)
        {
            dv = new DataView(service.userList_dt());
            showReport();
        }
        private void showReport()
        {
            var usersDS = new UsersDS();
            DataTable teams_dt = usersDS.Users_tbl;
            teams_dt = dv.ToTable();
            UsersCR usersCR = new UsersCR();
            usersCR.Database.Tables["Users_tbl"].SetDataSource(teams_dt);
            repaircrystalReportViewer.ReportSource = null;
            repaircrystalReportViewer.ReportSource = usersCR;

        }

        private void search_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dv.RowFilter = "username Like '%" + search.Text + "%'";
                showReport();
                search.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void all_Click(object sender, EventArgs e)
        {
            dv = new DataView(service.userList_dt());
            showReport();
        }
    }
}
