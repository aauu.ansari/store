﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using StoreManagement.Common;
using StoreManagement.Reports.crystalReport;
using StoreManagement.Reports.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class InstrumentsFORM : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        DataView dv;
        string report_title = "INTRUMENTS LIST";
        public InstrumentsFORM()
        {
            InitializeComponent();
        }
        private void showReport(string report_title)
        {
            var instrumentsDS = new InstrumentsDS();
            DataTable instruments_dt = instrumentsDS.instrument_tbl;
            instruments_dt = dv.ToTable();
            InstrumentsCR instrumentsCR = new InstrumentsCR();
            TextObject title = (TextObject)instrumentsCR.ReportDefinition.Sections["Section1"].ReportObjects["title"];
            title.Text = report_title;
            instrumentsCR.Database.Tables["instrument_tbl"].SetDataSource(instruments_dt);
            instrumentReportViewer.ReportSource = null;
            instrumentReportViewer.ReportSource = instrumentsCR;

        }
        private void report(string tb, string column, string title)
        {
            try
            {
                if (tb != "")
                {
                    report_title = title;
                }
                if (column != "Life")
                {
                    dv.RowFilter = "" + column + " Like '%" + tb + "%'";
                }
                else
                {
                    dv.RowFilter = "" + column + " <=" + tb + "";
                }

                showReport(report_title);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void status_btn_Click(object sender, EventArgs e)
        {
            string status = "";
            if (by_status.Text == "Un-Serviceable")
            {
                status = "UN-SERVICEABLE - INTRUMENTS LIST";
            }
            else if (by_status.Text == "Send To Repair")
            {
                status = "SEND TO REPAIR - INTRUMENTS LIST";
            }
            else if (by_status.Text == "On Site")
            {
                status = "INTRUMENTS LIST FOR SITE";
            }
            else if (by_status.Text == "Serviceable")
            {
                status = "SERVICEABLE - INTRUMENTS LIST";
            }
            else if (by_status.Text == "OK")
            {
                status = "OK - INTRUMENTS LIST";
            }
            report(by_status.Text, "status", status);
        }

        private void InstrumentsFORM_Load(object sender, EventArgs e)
        {
            dv = new DataView(service.instrumentsList_dataTable());
        }

        private void brand_btn_Click(object sender, EventArgs e)
        {
            report(by_brand.Text, "Brand", "INTRUMENTS LIST( Brandswise )");
        }

        private void code_btn_Click(object sender, EventArgs e)
        {
            report(by_code.Text, "Code", "INTRUMENTS LIST( Codeswise )");
        }

        private void category_btn_Click(object sender, EventArgs e)
        {
            report(by_category.Text, "Category", "INTRUMENTS LIST( Categorywise )");
        }

        private void name_btn_Click(object sender, EventArgs e)
        {
            report(by_name.Text, "Name", "INTRUMENTS LIST( Namewise )");
        }

        private void life_btn_Click(object sender, EventArgs e)
        {
            report(by_life.Text, "Life", "INTRUMENTS LIST( Lifewise )");
        }
    }
}
