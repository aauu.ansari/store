﻿using CrystalDecisions.CrystalReports.Engine;
using StoreManagement.BS_SERVICES_BsBill;
using StoreManagement.Common;
using StoreManagement.Reports.crystalReport;
using StoreManagement.Reports.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class BillFORM : Form
    {
        CommonHelper common = new CommonHelper();
        Messages messages = new Messages();
        BS_SERVICES_BsBill.IbsBillClient billService = new BS_SERVICES_BsBill.IbsBillClient();
        BS_SERVICES.IbsStoreClient storeService = new BS_SERVICES.IbsStoreClient();
        public int site_id = 0;
        public BillFORM()
        {
            InitializeComponent();
        }        
        private void BillFORM_Load(object sender, EventArgs e)
        {
            showReport(site_id);
            invoice_type.SelectedIndex = 0;
        }

        private void showReport(int site_id)
        {
            try
            {
                var billDS = new BillDS();
                BillCR billCR = new BillCR();
                Bill bill = new Bill();
                bill = billService.billRow(site_id);
                DataTable bill_dt = billDS.bill;
                if (bill != null)
                {
                    DataRow dr = bill_dt.NewRow();
                    dr[0] = bill.id;
                    dr[1] = bill.client_name;
                    dr[2] = bill.client_id;
                    dr[3] = bill.client_phone;
                    dr[4] = bill.invoice_no;
                    dr[5] = bill.lab_no;
                    dr[6] = bill.access_code;
                    dr[7] = bill.contractor;
                    dr[8] = bill.consultant;
                    dr[9] = bill.project;
                    dr[10] = bill.site_id;
                    dr[11] = bill.location;
                    dr[12] = bill.specimen;
                    dr[13] = bill.updated_at;
                    dr[14] = bill.NTN;
                    dr[15] = bill.PRA;
                    dr[16] = bill.user_id;
                    dr[17] = bill.bill_to;
                    dr[18] = bill.bill_details;
                    dr[19] = bill.order_no;
                    bill_dt.Rows.Add(dr);
                }
                //TextObject disc_txt = (TextObject)billCR.Subreports[1].ReportDefinition.Sections["ReportFooterSection2"].ReportObjects["disc_txt"];
                //disc_txt.Color = Color.Red;
                //disc_txt.Text = "false";
                //doc.ReportDefinition.Sections["sectionnameOrIndex"].SectionFormat.EnableSuppress = true;
                if (invoice_type.Text == "Discount Invoice")
                {
                    billCR.Subreports[1].ReportDefinition.Sections["ReportFooterSection5"].SectionFormat.EnableSuppress = true;
                }
                else
                {
                    billCR.Subreports[1].ReportDefinition.Sections["ReportFooterSection4"].SectionFormat.EnableSuppress = true;
                }

                billCR.Database.Tables["bill"].SetDataSource(bill_dt);
                billCR.Database.Tables["discount_tbl"].SetDataSource(billService.billReceiptsDiscount_bill_dt(site_id));
                billCR.Database.Tables["balances"].SetDataSource(billService.billReceiptsBalances_bill_dt(site_id));
                billCR.Database.Tables["bill_details_tbl"].SetDataSource(billService.billDetailsList_dt(site_id));
                billCR.Database.Tables["bill_receipts"].SetDataSource(billService.billReceiptsList_dt(site_id));
                billCR.Database.Tables["taxableAmount"].SetDataSource(billService.billIstaxable_dt(site_id));
                BillRV.ReportSource = null;
                BillRV.ReportSource = billCR;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }            
        }

        private void invoice_type_TextChanged(object sender, EventArgs e)
        {
            showReport(site_id);
        }
    }
}
