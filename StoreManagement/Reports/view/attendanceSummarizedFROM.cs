﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StoreManagement.Reports.data;
using StoreManagement.Reports.crystalReport;
using CrystalDecisions.CrystalReports.Engine;

namespace StoreManagement.Reports.view
{
    public partial class attendanceSummarizedFROM : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        DataView dv_attendance;
        public attendanceSummarizedFROM()
        {
            InitializeComponent();
        }

        private void attendanceSummarizedFROM_Load(object sender, EventArgs e)
        {
            showReport();
        }
        private void showReport()
        {
            dv_attendance = new DataView(service.attendanceTBL_statics_RPT(attendance_from.Text,attendance_to.Text, attendanceSite()));
            var attendanceSummarizedDS = new attendanceSummarizedDS();
            DataTable AttendanceTBL = attendanceSummarizedDS.AttendanceTBL;
            AttendanceTBL = dv_attendance.ToTable();
            attendanceSummarizedCR attendanceSummarized_CR = new attendanceSummarizedCR();
            TextObject title = (TextObject)attendanceSummarized_CR.ReportDefinition.Sections["Section1"].ReportObjects["title"];
            title.Text = "From " + attendance_from.Text + "              To " + attendance_to.Text;
            TextObject report = (TextObject)attendanceSummarized_CR.ReportDefinition.Sections["Section1"].ReportObjects["report"];
            if (attendanceSite_id.Text != "Office Attendance")
            {                
                report.Text = "ATTENDANCE SHEET(PKLI)";
            }
            else
            {
                report.Text = "ATTENDANCE SHEET";
            }            
            attendanceSummarized_CR.Database.Tables["AttendanceTBL"].SetDataSource(AttendanceTBL);
            attendance_summary_ReportViewer.ReportSource = null;
            attendance_summary_ReportViewer.ReportSource = attendanceSummarized_CR;
        }

        private int attendanceSite()
        {
            if (attendanceSite_id.Text == "Office Attendance")
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        private void Search_btn_Click(object sender, EventArgs e)
        {
            showReport();
        }

        private void attendanceSite_id_TextChanged(object sender, EventArgs e)
        {
            showReport();
        }
    }
}
