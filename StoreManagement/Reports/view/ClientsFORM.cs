﻿using StoreManagement.Common;
using StoreManagement.Reports.crystalReport;
using StoreManagement.Reports.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class ClientsFORM : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        DataView dv;
        public ClientsFORM()
        {
            InitializeComponent();
        }

        private void ClientsFORM_Load(object sender, EventArgs e)
        {
            dv = new DataView(service.clientsList_dataTable());
            showReport();
        }
        private void showReport()
        {
            var clientsDS = new ClientsDS();
            DataTable clients_dt = clientsDS.Clients_tbl;
            clients_dt = dv.ToTable();
            ClientsCR clientsCR = new ClientsCR();
            clientsCR.Database.Tables["clients_tbl"].SetDataSource(clients_dt);
            repaircrystalReportViewer.ReportSource = null;
            repaircrystalReportViewer.ReportSource = clientsCR;

        }

        private void search_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dv.RowFilter = "Name Like '%" + search.Text + "%'";
                showReport();
                search.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void all_Click(object sender, EventArgs e)
        {
            dv = new DataView(service.clientsList_dataTable());
            showReport();
        }
    }
}
