﻿using StoreManagement.Reports.crystalReport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Reports.view
{
    public partial class TAX_FORM : Form
    {
        public TAX_FORM()
        {
            InitializeComponent();
        }
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        
        private void TAX_FORM_Load(object sender, EventArgs e)
        {
            FillClients();
            from.Format = DateTimePickerFormat.Custom;
            from.CustomFormat = "yyyy-MM-dd";
            to.Format = DateTimePickerFormat.Custom;
            to.CustomFormat = "yyyy-MM-dd";
        }
        private void FillClients()
        {
            DataTable clients = service.dropdown("clients",0);
            client_id.DataSource = clients;
            client_id.DisplayMember = "name";
            client_id.ValueMember = "id";
        }

        private void client_btn_Click(object sender, EventArgs e)
        {
            showReport();
        }

        private void showReport()
        {
            DataTable dt = service.dt_tax_received_prt(Convert.ToInt32(client_id.SelectedValue), from.Text, to.Text);
            TAXCR tAXCR = new TAXCR();
            tAXCR.Database.Tables["tax_payable_tbl"].SetDataSource(service.dt_tax_payable_rpt(Convert.ToInt32(client_id.SelectedValue), from.Text, to.Text));
            tAXCR.Database.Tables["tax_Deducted_tbl"].SetDataSource(service.dt_tax_received_prt(Convert.ToInt32(client_id.SelectedValue),from.Text,to.Text));
            TAXRV.ReportSource = null;
            
            TAXRV.ReportSource = tAXCR;
        }
    }
}
