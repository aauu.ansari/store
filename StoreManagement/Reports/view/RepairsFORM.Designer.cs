﻿namespace StoreManagement.Reports.view
{
    partial class RepairsFORM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.repaircrystalReportViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.instrument_search = new MetroFramework.Controls.MetroTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.fron_to_btn = new MetroFramework.Controls.MetroButton();
            this.to = new MetroFramework.Controls.MetroDateTime();
            this.from = new MetroFramework.Controls.MetroDateTime();
            this.all = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.repaircrystalReportViewer, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 117F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(991, 513);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // repaircrystalReportViewer
            // 
            this.repaircrystalReportViewer.ActiveViewIndex = -1;
            this.repaircrystalReportViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.repaircrystalReportViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.repaircrystalReportViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.repaircrystalReportViewer.Location = new System.Drawing.Point(3, 120);
            this.repaircrystalReportViewer.Name = "repaircrystalReportViewer";
            this.repaircrystalReportViewer.Size = new System.Drawing.Size(985, 390);
            this.repaircrystalReportViewer.TabIndex = 0;
            this.repaircrystalReportViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.all);
            this.panel1.Controls.Add(this.metroLabel8);
            this.panel1.Controls.Add(this.instrument_search);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.fron_to_btn);
            this.panel1.Controls.Add(this.to);
            this.panel1.Controls.Add(this.from);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(985, 111);
            this.panel1.TabIndex = 1;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(9, 58);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(106, 19);
            this.metroLabel8.TabIndex = 83;
            this.metroLabel8.Text = "Search By Name";
            // 
            // instrument_search
            // 
            // 
            // 
            // 
            this.instrument_search.CustomButton.Image = null;
            this.instrument_search.CustomButton.Location = new System.Drawing.Point(252, 1);
            this.instrument_search.CustomButton.Name = "";
            this.instrument_search.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.instrument_search.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.instrument_search.CustomButton.TabIndex = 1;
            this.instrument_search.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.instrument_search.CustomButton.UseSelectable = true;
            this.instrument_search.CustomButton.Visible = false;
            this.instrument_search.Lines = new string[0];
            this.instrument_search.Location = new System.Drawing.Point(9, 80);
            this.instrument_search.MaxLength = 32767;
            this.instrument_search.Name = "instrument_search";
            this.instrument_search.PasswordChar = '\0';
            this.instrument_search.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.instrument_search.SelectedText = "";
            this.instrument_search.SelectionLength = 0;
            this.instrument_search.SelectionStart = 0;
            this.instrument_search.ShortcutsEnabled = true;
            this.instrument_search.Size = new System.Drawing.Size(274, 23);
            this.instrument_search.TabIndex = 82;
            this.instrument_search.UseSelectable = true;
            this.instrument_search.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.instrument_search.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.instrument_search.TextChanged += new System.EventHandler(this.instrument_search_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(158, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 15);
            this.label4.TabIndex = 81;
            this.label4.Text = "TO";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 80;
            this.label1.Text = "FROM";
            // 
            // fron_to_btn
            // 
            this.fron_to_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Refresh_2_icon;
            this.fron_to_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fron_to_btn.Location = new System.Drawing.Point(292, 25);
            this.fron_to_btn.Name = "fron_to_btn";
            this.fron_to_btn.Size = new System.Drawing.Size(30, 28);
            this.fron_to_btn.TabIndex = 78;
            this.fron_to_btn.UseSelectable = true;
            this.fron_to_btn.Click += new System.EventHandler(this.fron_to_btn_Click);
            // 
            // to
            // 
            this.to.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.to.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.to.Location = new System.Drawing.Point(161, 27);
            this.to.MinimumSize = new System.Drawing.Size(0, 25);
            this.to.Name = "to";
            this.to.Size = new System.Drawing.Size(122, 25);
            this.to.TabIndex = 79;
            // 
            // from
            // 
            this.from.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.from.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.from.Location = new System.Drawing.Point(9, 28);
            this.from.MinimumSize = new System.Drawing.Size(0, 25);
            this.from.Name = "from";
            this.from.Size = new System.Drawing.Size(123, 25);
            this.from.TabIndex = 77;
            // 
            // all
            // 
            this.all.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.all.Location = new System.Drawing.Point(292, 80);
            this.all.Name = "all";
            this.all.Size = new System.Drawing.Size(30, 23);
            this.all.TabIndex = 84;
            this.all.Text = "ALL";
            this.all.UseSelectable = true;
            this.all.Click += new System.EventHandler(this.all_Click);
            // 
            // RepairsFORM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 513);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "RepairsFORM";
            this.Text = "RepairsFORM";
            this.Load += new System.EventHandler(this.RepairsFORM_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer repaircrystalReportViewer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroButton fron_to_btn;
        private MetroFramework.Controls.MetroDateTime to;
        private MetroFramework.Controls.MetroDateTime from;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox instrument_search;
        private MetroFramework.Controls.MetroButton all;
    }
}