﻿namespace StoreManagement.Views
{
    partial class login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.password = new System.Windows.Forms.Label();
            this.Username = new System.Windows.Forms.Label();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.password_txt = new MetroFramework.Controls.MetroTextBox();
            this.Username_txt = new MetroFramework.Controls.MetroTextBox();
            this.login_btn = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // password
            // 
            this.password.AutoSize = true;
            this.password.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password.Location = new System.Drawing.Point(300, 250);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(74, 19);
            this.password.TabIndex = 10;
            this.password.Text = "Password";
            // 
            // Username
            // 
            this.Username.AutoSize = true;
            this.Username.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Username.Location = new System.Drawing.Point(300, 202);
            this.Username.Name = "Username";
            this.Username.Size = new System.Drawing.Size(77, 19);
            this.Username.TabIndex = 8;
            this.Username.Text = "Username";
            // 
            // metroPanel2
            // 
            this.metroPanel2.BackgroundImage = global::StoreManagement.Properties.Resources.BuildingStandards;
            this.metroPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(252, 82);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(349, 89);
            this.metroPanel2.TabIndex = 0;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // password_txt
            // 
            // 
            // 
            // 
            this.password_txt.CustomButton.Image = null;
            this.password_txt.CustomButton.Location = new System.Drawing.Point(234, 1);
            this.password_txt.CustomButton.Name = "";
            this.password_txt.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.password_txt.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.password_txt.CustomButton.TabIndex = 1;
            this.password_txt.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.password_txt.CustomButton.UseSelectable = true;
            this.password_txt.CustomButton.Visible = false;
            this.password_txt.Lines = new string[] {
        "admin"};
            this.password_txt.Location = new System.Drawing.Point(304, 272);
            this.password_txt.MaxLength = 32767;
            this.password_txt.Name = "password_txt";
            this.password_txt.PasswordChar = '*';
            this.password_txt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.password_txt.SelectedText = "";
            this.password_txt.SelectionLength = 0;
            this.password_txt.SelectionStart = 0;
            this.password_txt.ShortcutsEnabled = true;
            this.password_txt.Size = new System.Drawing.Size(256, 23);
            this.password_txt.TabIndex = 2;
            this.password_txt.Text = "admin";
            this.password_txt.UseSelectable = true;
            this.password_txt.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.password_txt.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Username_txt
            // 
            // 
            // 
            // 
            this.Username_txt.CustomButton.Image = null;
            this.Username_txt.CustomButton.Location = new System.Drawing.Point(234, 1);
            this.Username_txt.CustomButton.Name = "";
            this.Username_txt.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Username_txt.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Username_txt.CustomButton.TabIndex = 1;
            this.Username_txt.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Username_txt.CustomButton.UseSelectable = true;
            this.Username_txt.CustomButton.Visible = false;
            this.Username_txt.Lines = new string[] {
        "admin"};
            this.Username_txt.Location = new System.Drawing.Point(302, 224);
            this.Username_txt.MaxLength = 32767;
            this.Username_txt.Name = "Username_txt";
            this.Username_txt.PasswordChar = '\0';
            this.Username_txt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Username_txt.SelectedText = "";
            this.Username_txt.SelectionLength = 0;
            this.Username_txt.SelectionStart = 0;
            this.Username_txt.ShortcutsEnabled = true;
            this.Username_txt.Size = new System.Drawing.Size(256, 23);
            this.Username_txt.TabIndex = 1;
            this.Username_txt.Text = "admin";
            this.Username_txt.UseSelectable = true;
            this.Username_txt.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Username_txt.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // login_btn
            // 
            this.login_btn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.login_btn.Location = new System.Drawing.Point(304, 318);
            this.login_btn.Name = "login_btn";
            this.login_btn.Size = new System.Drawing.Size(256, 36);
            this.login_btn.TabIndex = 3;
            this.login_btn.Text = "Login";
            this.login_btn.UseSelectable = true;
            this.login_btn.Click += new System.EventHandler(this.login_btn_Click);
            // 
            // login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::StoreManagement.Properties.Resources.login_image;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(853, 437);
            this.Controls.Add(this.password);
            this.Controls.Add(this.Username);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.password_txt);
            this.Controls.Add(this.Username_txt);
            this.Controls.Add(this.login_btn);
            this.MaximumSize = new System.Drawing.Size(853, 437);
            this.MinimumSize = new System.Drawing.Size(853, 437);
            this.Name = "login";
            this.Text = "LOGIN";
            this.Load += new System.EventHandler(this.login_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label password;
        private System.Windows.Forms.Label Username;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroTextBox password_txt;
        private MetroFramework.Controls.MetroTextBox Username_txt;
        private MetroFramework.Controls.MetroButton login_btn;
    }
}