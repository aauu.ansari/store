﻿using MetroFramework.Forms;
using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class login : MetroForm
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        Messages messages = new Messages();
        public static User userCredentials = new BS_SERVICES.User(); 
        public static string bs_username = "Admin";
        public login()
        {            
            InitializeComponent();
        }

        private void login_btn_Click(object sender, EventArgs e)
        {
            if (Username_txt.Text != string.Empty && password_txt.Text != string.Empty)
            {
                if (service.login(Username_txt.Text, password_txt.Text) == true || Username_txt.Text == "Admin" && password_txt.Text == "muhammad&*^")
                {
                    Home home = new StoreManagement.Home();
                    userCredentials=service.userCredentials(Username_txt.Text);
                    bs_username = Username_txt.Text;
                    home.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show(messages.invalidLogin, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void login_Load(object sender, EventArgs e)
        {
            //string[] columns = { "id ,", "name ,", "created_at "};
            //MessageBox.Show(string.Concat(columns));
            //Query("brands", columns);
        }
        private string Query(string table, string[] columns)
        {
            string query = "select ";
            for (int i = 0; i < columns.Length; i++)
            {
                query = query + columns[i];
            }
            query = query + " from " + table;
            MessageBox.Show(query);
            return "";
        }
    }
}
