﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using StoreManagement.Reports.view;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class rateList_form : Form
    {
        public rateList_form()
        {
            InitializeComponent();
        }
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        DataView dv;
        private void rateList_search_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dv.RowFilter = "Name Like '%" + rateList_search.Text + "%'";
                rateList_list.DataSource = dv;
                gridViewProperties();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void gridViewProperties()
        {
            rateList_list.Columns[0].Visible = false;
            rateList_list.Columns[1].Width = 100;
            rateList_list.Columns[2].Width = 400;
            rateList_list.Columns[3].Width = 100;
            rateList_list.Columns[4].Width = 200;
        }
        private void RateList()
        {
            try
            {
                rateList_list.DataSource = dv;
                gridViewProperties();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private bool Validation()
        {
            string[] param = {
                rateList_name.Text,
                rateList_price.Text,
                rateList_tax.Text
            };
            return commonHelper.Validator(param);
        }

        private void rateList_list_Click(object sender, EventArgs e)
        {
            try
            {
                ShowDataInTextbox(rateList_list.CurrentRow.Index);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        int rateList_id = 0;
        private void ShowDataInTextbox(int index)
        {
            if (index == -1) { return; }
            rateList_id = Convert.ToInt32(rateList_list.Rows[index].Cells["id"].Value.ToString());
            rateList_name.Text = rateList_list.Rows[index].Cells["Name"].Value.ToString();
            rateList_price.Text = rateList_list.Rows[index].Cells["Price"].Value.ToString();
            rateList_tax.Text = rateList_list.Rows[index].Cells["Tax"].Value.ToString();
            rateList_total.Text = rateList_list.Rows[index].Cells["Total"].Value.ToString();
            rateList_save.Enabled = false;
            rateList_update.Enabled = true;
            rateList_delete.Enabled = true;
        }

        private void refresh()
        {
            dv = new DataView(service.rateList_dataTable());
            RateList();
            rateList_name.Text = "";
            rateList_price.Text = "";
            rateList_tax.Text = "";
            rateList_save.Enabled = true;
            rateList_update.Enabled = false;
            rateList_delete.Enabled = false;
        }

        private void rateList_form_Load(object sender, EventArgs e)
        {
            refresh();
        }



        private void rateList_save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validation())
                {
                    rateList _rateList = new BS_SERVICES.rateList();
                    _rateList.name = rateList_name.Text;
                    _rateList.price = float.Parse(rateList_price.Text);
                    _rateList.tax = float.Parse(rateList_tax.Text);
                    _rateList.created_at = DateTime.Now.ToShortDateString();
                    service.rateListSave(_rateList);
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }

        private void rateList_update_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validation())
                {
                    rateList _rateList = new BS_SERVICES.rateList();
                    _rateList.id = rateList_id;
                    _rateList.name = rateList_name.Text;
                    _rateList.price = float.Parse(rateList_price.Text);
                    _rateList.tax = float.Parse(rateList_tax.Text);
                    _rateList.updated_at = DateTime.Now.ToShortDateString();
                    service.rateListUpdate(_rateList);
                    MessageBox.Show(messages.update, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }
        private void calculateTax()
        {
            if ( rateList_price.Text != "")
            {
                rateList_tax.Text = ((Convert.ToUInt32(rateList_price.Text) * 16) / 100).ToString();
                rateList_total.Text = (Convert.ToInt32(rateList_tax.Text) + Convert.ToInt32(rateList_price.Text)).ToString();
            }
        }
        private void rateList_refresh_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void rateList_delete_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Are you sure! you want to delete this record", "Alert", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    if (rateList_id > 0)
                    {
                        service.deleteBy("rateList", "id", rateList_id);
                        MessageBox.Show(messages.delete, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        refresh();
                    }
                    else
                    {
                        MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }

        private void rateList_price_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;                
            }
        }

        private void rateList_tax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void rateList_price_KeyUp(object sender, KeyEventArgs e)
        {
            rateList_tax.Text = "";
            rateList_total.Text = "";
            calculateTax();
        }

        private void printer_Click(object sender, EventArgs e)
        {
            RateListFORM form = new RateListFORM();
            form.Show();
        }
    }
}
