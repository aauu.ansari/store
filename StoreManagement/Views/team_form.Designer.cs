﻿namespace StoreManagement.Views
{
    partial class team_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(team_form));
            this.team_go_btn = new MetroFramework.Controls.MetroButton();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.team_search = new MetroFramework.Controls.MetroTextBox();
            this.team_list = new System.Windows.Forms.DataGridView();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.team_name = new MetroFramework.Controls.MetroTextBox();
            this.team_update = new MetroFramework.Controls.MetroButton();
            this.team_save = new MetroFramework.Controls.MetroButton();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.team_role = new MetroFramework.Controls.MetroComboBox();
            this.team_de_active = new MetroFramework.Controls.MetroRadioButton();
            this.team_active = new MetroFramework.Controls.MetroRadioButton();
            this.team_delete = new MetroFramework.Controls.MetroButton();
            this.team_add_new = new MetroFramework.Controls.MetroButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.team_list)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // team_go_btn
            // 
            this.team_go_btn.Location = new System.Drawing.Point(284, 31);
            this.team_go_btn.Name = "team_go_btn";
            this.team_go_btn.Size = new System.Drawing.Size(38, 23);
            this.team_go_btn.TabIndex = 4;
            this.team_go_btn.Text = "GO";
            this.team_go_btn.UseSelectable = true;
            this.team_go_btn.Click += new System.EventHandler(this.team_search_TextChanged);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(9, 9);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(80, 19);
            this.metroLabel2.TabIndex = 12;
            this.metroLabel2.Text = "Team Name";
            // 
            // team_search
            // 
            // 
            // 
            // 
            this.team_search.CustomButton.Image = null;
            this.team_search.CustomButton.Location = new System.Drawing.Point(247, 1);
            this.team_search.CustomButton.Name = "";
            this.team_search.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.team_search.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.team_search.CustomButton.TabIndex = 1;
            this.team_search.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.team_search.CustomButton.UseSelectable = true;
            this.team_search.CustomButton.Visible = false;
            this.team_search.Lines = new string[0];
            this.team_search.Location = new System.Drawing.Point(9, 31);
            this.team_search.MaxLength = 32767;
            this.team_search.Name = "team_search";
            this.team_search.PasswordChar = '\0';
            this.team_search.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.team_search.SelectedText = "";
            this.team_search.SelectionLength = 0;
            this.team_search.SelectionStart = 0;
            this.team_search.ShortcutsEnabled = true;
            this.team_search.Size = new System.Drawing.Size(269, 23);
            this.team_search.TabIndex = 3;
            this.team_search.UseSelectable = true;
            this.team_search.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.team_search.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.team_search.TextChanged += new System.EventHandler(this.team_search_TextChanged);
            // 
            // team_list
            // 
            this.team_list.BackgroundColor = System.Drawing.Color.White;
            this.team_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.team_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.team_list.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.team_list.Location = new System.Drawing.Point(3, 72);
            this.team_list.Name = "team_list";
            this.team_list.Size = new System.Drawing.Size(850, 324);
            this.team_list.TabIndex = 5;
            this.team_list.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.brand_list_CellClick);
            this.team_list.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.brand_list_KeyPress);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(16, 55);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(45, 19);
            this.metroLabel1.TabIndex = 17;
            this.metroLabel1.Text = "Name";
            // 
            // team_name
            // 
            // 
            // 
            // 
            this.team_name.CustomButton.Image = null;
            this.team_name.CustomButton.Location = new System.Drawing.Point(232, 1);
            this.team_name.CustomButton.Name = "";
            this.team_name.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.team_name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.team_name.CustomButton.TabIndex = 1;
            this.team_name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.team_name.CustomButton.UseSelectable = true;
            this.team_name.CustomButton.Visible = false;
            this.team_name.Lines = new string[0];
            this.team_name.Location = new System.Drawing.Point(15, 77);
            this.team_name.MaxLength = 32767;
            this.team_name.Name = "team_name";
            this.team_name.PasswordChar = '\0';
            this.team_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.team_name.SelectedText = "";
            this.team_name.SelectionLength = 0;
            this.team_name.SelectionStart = 0;
            this.team_name.ShortcutsEnabled = true;
            this.team_name.Size = new System.Drawing.Size(254, 23);
            this.team_name.TabIndex = 7;
            this.team_name.UseSelectable = true;
            this.team_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.team_name.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // team_update
            // 
            this.team_update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.team_update.Location = new System.Drawing.Point(66, 3);
            this.team_update.Name = "team_update";
            this.team_update.Size = new System.Drawing.Size(57, 27);
            this.team_update.TabIndex = 13;
            this.team_update.Text = "UPDATE";
            this.team_update.UseSelectable = true;
            this.team_update.Click += new System.EventHandler(this.team_update_Click);
            // 
            // team_save
            // 
            this.team_save.Dock = System.Windows.Forms.DockStyle.Fill;
            this.team_save.Location = new System.Drawing.Point(3, 3);
            this.team_save.Name = "team_save";
            this.team_save.Size = new System.Drawing.Size(57, 27);
            this.team_save.TabIndex = 12;
            this.team_save.Text = "SAVE";
            this.team_save.UseSelectable = true;
            this.team_save.Click += new System.EventHandler(this.team_save_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(16, 103);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(40, 19);
            this.metroLabel3.TabIndex = 19;
            this.metroLabel3.Text = "Roles";
            // 
            // team_role
            // 
            this.team_role.FormattingEnabled = true;
            this.team_role.ItemHeight = 23;
            this.team_role.Items.AddRange(new object[] {
            "Select One",
            "Site Incharge",
            "Site Supervisor",
            "Technician",
            "Jr. Technician",
            "Helper",
            "Store Incharge"});
            this.team_role.Location = new System.Drawing.Point(16, 125);
            this.team_role.Name = "team_role";
            this.team_role.Size = new System.Drawing.Size(252, 29);
            this.team_role.TabIndex = 8;
            this.team_role.UseSelectable = true;
            // 
            // team_de_active
            // 
            this.team_de_active.AutoSize = true;
            this.team_de_active.Location = new System.Drawing.Point(78, 160);
            this.team_de_active.Name = "team_de_active";
            this.team_de_active.Size = new System.Drawing.Size(75, 15);
            this.team_de_active.TabIndex = 10;
            this.team_de_active.Text = "De-Active";
            this.team_de_active.UseSelectable = true;
            // 
            // team_active
            // 
            this.team_active.AutoSize = true;
            this.team_active.Checked = true;
            this.team_active.Location = new System.Drawing.Point(16, 160);
            this.team_active.Name = "team_active";
            this.team_active.Size = new System.Drawing.Size(56, 15);
            this.team_active.TabIndex = 9;
            this.team_active.TabStop = true;
            this.team_active.Text = "Active";
            this.team_active.UseSelectable = true;
            // 
            // team_delete
            // 
            this.team_delete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.team_delete.Location = new System.Drawing.Point(129, 3);
            this.team_delete.Name = "team_delete";
            this.team_delete.Size = new System.Drawing.Size(57, 27);
            this.team_delete.TabIndex = 14;
            this.team_delete.Text = "DELETE";
            this.team_delete.UseSelectable = true;
            this.team_delete.Click += new System.EventHandler(this.team_delete_Click);
            // 
            // team_add_new
            // 
            this.team_add_new.Dock = System.Windows.Forms.DockStyle.Fill;
            this.team_add_new.Location = new System.Drawing.Point(192, 3);
            this.team_add_new.Name = "team_add_new";
            this.team_add_new.Size = new System.Drawing.Size(57, 27);
            this.team_add_new.TabIndex = 15;
            this.team_add_new.Text = "Refresh";
            this.team_add_new.UseSelectable = true;
            this.team_add_new.Click += new System.EventHandler(this.team_add_new_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.LightGray;
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1143, 64);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = resources.GetString("groupBox6.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(3845, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = resources.GetString("label2.Text");
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.team_save, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.team_update, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.team_add_new, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.team_delete, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(16, 181);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(252, 33);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 64);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.FloralWhite;
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.metroLabel1);
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Panel2.Controls.Add(this.team_name);
            this.splitContainer1.Panel2.Controls.Add(this.metroLabel3);
            this.splitContainer1.Panel2.Controls.Add(this.team_de_active);
            this.splitContainer1.Panel2.Controls.Add(this.team_role);
            this.splitContainer1.Panel2.Controls.Add(this.team_active);
            this.splitContainer1.Size = new System.Drawing.Size(1143, 399);
            this.splitContainer1.SplitterDistance = 856;
            this.splitContainer1.TabIndex = 75;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.team_list, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(856, 399);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.metroLabel2);
            this.panel1.Controls.Add(this.team_go_btn);
            this.panel1.Controls.Add(this.team_search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(850, 63);
            this.panel1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(2, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 21);
            this.label1.TabIndex = 6;
            this.label1.Text = "Add / Update / Delete Team";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // team_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1143, 463);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.groupBox6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "team_form";
            this.Text = "team_form";
            this.Load += new System.EventHandler(this.team_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.team_list)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroButton team_go_btn;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox team_search;
        private System.Windows.Forms.DataGridView team_list;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox team_name;
        private MetroFramework.Controls.MetroButton team_update;
        private MetroFramework.Controls.MetroButton team_save;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroComboBox team_role;
        private MetroFramework.Controls.MetroRadioButton team_de_active;
        private MetroFramework.Controls.MetroRadioButton team_active;
        private MetroFramework.Controls.MetroButton team_delete;
        private MetroFramework.Controls.MetroButton team_add_new;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
    }
}