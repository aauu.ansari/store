﻿namespace StoreManagement.Views
{
    partial class attendance_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.attendanceSite_id = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.refresh_btn = new System.Windows.Forms.Button();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.attendance_from = new System.Windows.Forms.DateTimePicker();
            this.summrized_report_btn = new System.Windows.Forms.Button();
            this.attendance_to = new System.Windows.Forms.DateTimePicker();
            this.attendance_time = new System.Windows.Forms.DateTimePicker();
            this.Search_btn = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.detailed_report_btn = new System.Windows.Forms.Button();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.info_lbl = new System.Windows.Forms.Label();
            this.attendance_list = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emp_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.department = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.edit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.attendance_list)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.LightGray;
            this.groupBox6.Controls.Add(this.tableLayoutPanel2);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1144, 46);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.86292F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 88.13708F));
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1138, 27);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Impact", 15F);
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 27);
            this.label2.TabIndex = 1;
            this.label2.Text = "ATTENDANCE";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.attendanceSite_id);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(138, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(997, 21);
            this.panel5.TabIndex = 2;
            // 
            // attendanceSite_id
            // 
            this.attendanceSite_id.FormattingEnabled = true;
            this.attendanceSite_id.Items.AddRange(new object[] {
            "Office Attendance",
            "PKLI Site Attendance"});
            this.attendanceSite_id.Location = new System.Drawing.Point(56, 0);
            this.attendanceSite_id.Name = "attendanceSite_id";
            this.attendanceSite_id.Size = new System.Drawing.Size(255, 21);
            this.attendanceSite_id.TabIndex = 57;
            this.attendanceSite_id.TextChanged += new System.EventHandler(this.attendanceSite_id_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 14);
            this.label7.TabIndex = 56;
            this.label7.Text = "Type : ";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 46);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.FloralWhite;
            this.splitContainer1.Size = new System.Drawing.Size(1144, 417);
            this.splitContainer1.SplitterDistance = 1100;
            this.splitContainer1.TabIndex = 66;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.30072F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 393F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1100, 417);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1094, 411);
            this.panel1.TabIndex = 2;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 387F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1094, 411);
            this.tableLayoutPanel3.TabIndex = 13;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1088, 405);
            this.panel2.TabIndex = 6;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.attendance_list, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1088, 405);
            this.tableLayoutPanel4.TabIndex = 15;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1082, 49);
            this.panel4.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tableLayoutPanel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1082, 49);
            this.panel3.TabIndex = 14;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 6;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.99815F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.92237F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.872458F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.75046F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.Controls.Add(this.refresh_btn, 5, 1);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel2, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel1, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.attendance_from, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.summrized_report_btn, 4, 1);
            this.tableLayoutPanel5.Controls.Add(this.attendance_to, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.attendance_time, 5, 0);
            this.tableLayoutPanel5.Controls.Add(this.Search_btn, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.panel6, 3, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1082, 49);
            this.tableLayoutPanel5.TabIndex = 19;
            // 
            // refresh_btn
            // 
            this.refresh_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.refresh_btn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.refresh_btn.Location = new System.Drawing.Point(903, 27);
            this.refresh_btn.Name = "refresh_btn";
            this.refresh_btn.Size = new System.Drawing.Size(176, 19);
            this.refresh_btn.TabIndex = 13;
            this.refresh_btn.Text = "Refresh";
            this.refresh_btn.UseVisualStyleBackColor = true;
            this.refresh_btn.Click += new System.EventHandler(this.refresh_btn_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(3, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(48, 19);
            this.metroLabel2.TabIndex = 8;
            this.metroLabel2.Text = "From :";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(122, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(29, 19);
            this.metroLabel1.TabIndex = 10;
            this.metroLabel1.Text = "To :";
            // 
            // attendance_from
            // 
            this.attendance_from.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attendance_from.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.attendance_from.Location = new System.Drawing.Point(3, 27);
            this.attendance_from.Name = "attendance_from";
            this.attendance_from.Size = new System.Drawing.Size(113, 20);
            this.attendance_from.TabIndex = 9;
            this.attendance_from.ValueChanged += new System.EventHandler(this.attendance_search_ValueChanged);
            // 
            // summrized_report_btn
            // 
            this.summrized_report_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.summrized_report_btn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.summrized_report_btn.Location = new System.Drawing.Point(723, 27);
            this.summrized_report_btn.Name = "summrized_report_btn";
            this.summrized_report_btn.Size = new System.Drawing.Size(174, 19);
            this.summrized_report_btn.TabIndex = 18;
            this.summrized_report_btn.Text = "Summarized Report";
            this.summrized_report_btn.UseVisualStyleBackColor = true;
            this.summrized_report_btn.Click += new System.EventHandler(this.summrized_report_btn_Click);
            // 
            // attendance_to
            // 
            this.attendance_to.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attendance_to.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.attendance_to.Location = new System.Drawing.Point(122, 27);
            this.attendance_to.Name = "attendance_to";
            this.attendance_to.Size = new System.Drawing.Size(123, 20);
            this.attendance_to.TabIndex = 11;
            // 
            // attendance_time
            // 
            this.attendance_time.Dock = System.Windows.Forms.DockStyle.Right;
            this.attendance_time.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.attendance_time.Location = new System.Drawing.Point(967, 3);
            this.attendance_time.Name = "attendance_time";
            this.attendance_time.Size = new System.Drawing.Size(112, 20);
            this.attendance_time.TabIndex = 12;
            // 
            // Search_btn
            // 
            this.Search_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Search_btn.Location = new System.Drawing.Point(251, 27);
            this.Search_btn.Name = "Search_btn";
            this.Search_btn.Size = new System.Drawing.Size(90, 19);
            this.Search_btn.TabIndex = 12;
            this.Search_btn.Text = "Search";
            this.Search_btn.UseVisualStyleBackColor = true;
            this.Search_btn.Click += new System.EventHandler(this.Search_btn_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.detailed_report_btn);
            this.panel6.Controls.Add(this.metroLabel3);
            this.panel6.Controls.Add(this.info_lbl);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(347, 27);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(370, 19);
            this.panel6.TabIndex = 19;
            // 
            // detailed_report_btn
            // 
            this.detailed_report_btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.detailed_report_btn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.detailed_report_btn.Location = new System.Drawing.Point(238, 0);
            this.detailed_report_btn.Name = "detailed_report_btn";
            this.detailed_report_btn.Size = new System.Drawing.Size(132, 19);
            this.detailed_report_btn.TabIndex = 17;
            this.detailed_report_btn.Text = "Detailed Report";
            this.detailed_report_btn.UseVisualStyleBackColor = true;
            this.detailed_report_btn.Click += new System.EventHandler(this.detailed_report_btn_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(3, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(42, 19);
            this.metroLabel3.TabIndex = 15;
            this.metroLabel3.Text = "Info : ";
            // 
            // info_lbl
            // 
            this.info_lbl.AutoSize = true;
            this.info_lbl.Location = new System.Drawing.Point(51, 3);
            this.info_lbl.Name = "info_lbl";
            this.info_lbl.Size = new System.Drawing.Size(10, 13);
            this.info_lbl.TabIndex = 16;
            this.info_lbl.Text = ":";
            // 
            // attendance_list
            // 
            this.attendance_list.BackgroundColor = System.Drawing.Color.White;
            this.attendance_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.attendance_list.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.emp_id,
            this.name,
            this.status,
            this.description,
            this.department,
            this.edit});
            this.attendance_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attendance_list.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.attendance_list.Location = new System.Drawing.Point(3, 58);
            this.attendance_list.Name = "attendance_list";
            this.attendance_list.Size = new System.Drawing.Size(1082, 344);
            this.attendance_list.TabIndex = 5;
            this.attendance_list.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.attendance_list_CellContentClick);
            // 
            // id
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Red;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            this.id.DefaultCellStyle = dataGridViewCellStyle1;
            this.id.HeaderText = "Id";
            this.id.Name = "id";
            this.id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.id.Visible = false;
            // 
            // emp_id
            // 
            this.emp_id.HeaderText = "empid";
            this.emp_id.Name = "emp_id";
            this.emp_id.Visible = false;
            // 
            // name
            // 
            this.name.FillWeight = 250F;
            this.name.HeaderText = "Employee";
            this.name.Name = "name";
            this.name.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.name.Width = 250;
            // 
            // status
            // 
            this.status.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.status.HeaderText = "Status";
            this.status.Items.AddRange(new object[] {
            "A",
            "P",
            "L",
            "Holiday"});
            this.status.Name = "status";
            this.status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // description
            // 
            this.description.HeaderText = "Decription";
            this.description.Name = "description";
            this.description.Width = 400;
            // 
            // department
            // 
            this.department.HeaderText = "Department";
            this.department.Items.AddRange(new object[] {
            "NDEC",
            "Plumbing",
            "Fire Fighting"});
            this.department.Name = "department";
            this.department.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.department.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // edit
            // 
            this.edit.FillWeight = 70F;
            this.edit.HeaderText = "Actions";
            this.edit.Name = "edit";
            this.edit.Text = "Edit";
            this.edit.Width = 89;
            // 
            // attendance_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1144, 463);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.groupBox6);
            this.Name = "attendance_form";
            this.Text = "Building Standards Attendance Sheet";
            this.Load += new System.EventHandler(this.attendance_form_Load);
            this.groupBox6.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.attendance_list)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DateTimePicker attendance_from;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DateTimePicker attendance_to;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.Button Search_btn;
        private System.Windows.Forms.DataGridView attendance_list;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DateTimePicker attendance_time;
        private System.Windows.Forms.Button refresh_btn;
        private System.Windows.Forms.Label info_lbl;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button detailed_report_btn;
        private System.Windows.Forms.Button summrized_report_btn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox attendanceSite_id;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn emp_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewComboBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
        private System.Windows.Forms.DataGridViewComboBoxColumn department;
        private System.Windows.Forms.DataGridViewButtonColumn edit;
    }
}