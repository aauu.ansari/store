﻿namespace StoreManagement.Views
{
    partial class store_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(store_form));
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.instrument_name = new MetroFramework.Controls.MetroTextBox();
            this.instrument_update = new MetroFramework.Controls.MetroButton();
            this.instrument_save = new MetroFramework.Controls.MetroButton();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.instrument_list = new System.Windows.Forms.DataGridView();
            this.instrument_category = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.instrument_brand_id = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.instrument_image = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.instrument_code = new MetroFramework.Controls.MetroTextBox();
            this.brand_refresh = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.instrument_status = new System.Windows.Forms.ComboBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.search_type = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.search_brand = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.search_lifelesser = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.search_life_greater = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.barCodeCatchar = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.search_category = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.instrument_search = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.instrument_quantity = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.instrument_type = new System.Windows.Forms.ComboBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.servicable_instrument_description = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.instrument_life = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.instrument_image_box = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.instrument_delete = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.instrument_list)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.instrument_image_box)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(111, 94);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(55, 19);
            this.metroLabel1.TabIndex = 28;
            this.metroLabel1.Text = "Name *";
            // 
            // instrument_name
            // 
            // 
            // 
            // 
            this.instrument_name.CustomButton.Image = null;
            this.instrument_name.CustomButton.Location = new System.Drawing.Point(185, 1);
            this.instrument_name.CustomButton.Name = "";
            this.instrument_name.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.instrument_name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.instrument_name.CustomButton.TabIndex = 1;
            this.instrument_name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.instrument_name.CustomButton.UseSelectable = true;
            this.instrument_name.CustomButton.Visible = false;
            this.instrument_name.Lines = new string[0];
            this.instrument_name.Location = new System.Drawing.Point(110, 116);
            this.instrument_name.MaxLength = 50;
            this.instrument_name.Name = "instrument_name";
            this.instrument_name.PasswordChar = '\0';
            this.instrument_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.instrument_name.SelectedText = "";
            this.instrument_name.SelectionLength = 0;
            this.instrument_name.SelectionStart = 0;
            this.instrument_name.ShortcutsEnabled = true;
            this.instrument_name.Size = new System.Drawing.Size(207, 23);
            this.instrument_name.TabIndex = 10;
            this.instrument_name.UseSelectable = true;
            this.instrument_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.instrument_name.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // instrument_update
            // 
            this.instrument_update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.instrument_update.Location = new System.Drawing.Point(80, 3);
            this.instrument_update.Name = "instrument_update";
            this.instrument_update.Size = new System.Drawing.Size(71, 32);
            this.instrument_update.TabIndex = 21;
            this.instrument_update.Text = "UPDATE";
            this.instrument_update.UseSelectable = true;
            this.instrument_update.Click += new System.EventHandler(this.instrument_update_Click);
            // 
            // instrument_save
            // 
            this.instrument_save.Dock = System.Windows.Forms.DockStyle.Fill;
            this.instrument_save.Location = new System.Drawing.Point(3, 3);
            this.instrument_save.Name = "instrument_save";
            this.instrument_save.Size = new System.Drawing.Size(71, 32);
            this.instrument_save.TabIndex = 20;
            this.instrument_save.Text = "SAVE";
            this.instrument_save.UseSelectable = true;
            this.instrument_save.Click += new System.EventHandler(this.instrument_save_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(12, 45);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(45, 19);
            this.metroLabel2.TabIndex = 23;
            this.metroLabel2.Text = "Name";
            // 
            // instrument_list
            // 
            this.instrument_list.BackgroundColor = System.Drawing.Color.White;
            this.instrument_list.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.instrument_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.instrument_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.instrument_list.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.instrument_list.Location = new System.Drawing.Point(3, 72);
            this.instrument_list.Name = "instrument_list";
            this.instrument_list.Size = new System.Drawing.Size(825, 695);
            this.instrument_list.TabIndex = 4;
            this.instrument_list.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.instrument_list_CellClick);
            // 
            // instrument_category
            // 
            this.instrument_category.FormattingEnabled = true;
            this.instrument_category.ItemHeight = 23;
            this.instrument_category.Items.AddRange(new object[] {
            "Parent"});
            this.instrument_category.Location = new System.Drawing.Point(5, 64);
            this.instrument_category.Name = "instrument_category";
            this.instrument_category.Size = new System.Drawing.Size(196, 29);
            this.instrument_category.TabIndex = 7;
            this.instrument_category.UseSelectable = true;
            this.instrument_category.TextChanged += new System.EventHandler(this.instrument_category_TextChanged);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(820, 45);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(74, 19);
            this.metroLabel4.TabIndex = 31;
            this.metroLabel4.Text = "Category *";
            // 
            // instrument_brand_id
            // 
            this.instrument_brand_id.Cursor = System.Windows.Forms.Cursors.Default;
            this.instrument_brand_id.DropDownWidth = 220;
            this.instrument_brand_id.FormattingEnabled = true;
            this.instrument_brand_id.ItemHeight = 23;
            this.instrument_brand_id.Items.AddRange(new object[] {
            "Parent"});
            this.instrument_brand_id.Location = new System.Drawing.Point(6, 163);
            this.instrument_brand_id.Name = "instrument_brand_id";
            this.instrument_brand_id.Size = new System.Drawing.Size(179, 29);
            this.instrument_brand_id.TabIndex = 11;
            this.instrument_brand_id.UseSelectable = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(7, 141);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(94, 19);
            this.metroLabel6.TabIndex = 37;
            this.metroLabel6.Text = "Brand Name *";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(6, 246);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(117, 19);
            this.metroLabel3.TabIndex = 44;
            this.metroLabel3.Text = "Image ( Optional )";
            // 
            // instrument_image
            // 
            // 
            // 
            // 
            this.instrument_image.CustomButton.Image = null;
            this.instrument_image.CustomButton.Location = new System.Drawing.Point(227, 1);
            this.instrument_image.CustomButton.Name = "";
            this.instrument_image.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.instrument_image.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.instrument_image.CustomButton.TabIndex = 1;
            this.instrument_image.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.instrument_image.CustomButton.UseSelectable = true;
            this.instrument_image.CustomButton.Visible = false;
            this.instrument_image.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.instrument_image.Lines = new string[] {
        "15"};
            this.instrument_image.Location = new System.Drawing.Point(6, 268);
            this.instrument_image.MaxLength = 32767;
            this.instrument_image.Name = "instrument_image";
            this.instrument_image.PasswordChar = '\0';
            this.instrument_image.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.instrument_image.SelectedText = "";
            this.instrument_image.SelectionLength = 0;
            this.instrument_image.SelectionStart = 0;
            this.instrument_image.ShortcutsEnabled = true;
            this.instrument_image.Size = new System.Drawing.Size(249, 23);
            this.instrument_image.TabIndex = 13;
            this.instrument_image.Text = "15";
            this.instrument_image.UseSelectable = true;
            this.instrument_image.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.instrument_image.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(6, 94);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(51, 19);
            this.metroLabel5.TabIndex = 42;
            this.metroLabel5.Text = "Code *";
            // 
            // instrument_code
            // 
            // 
            // 
            // 
            this.instrument_code.CustomButton.Image = null;
            this.instrument_code.CustomButton.Location = new System.Drawing.Point(77, 1);
            this.instrument_code.CustomButton.Name = "";
            this.instrument_code.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.instrument_code.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.instrument_code.CustomButton.TabIndex = 1;
            this.instrument_code.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.instrument_code.CustomButton.UseSelectable = true;
            this.instrument_code.CustomButton.Visible = false;
            this.instrument_code.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.instrument_code.Lines = new string[0];
            this.instrument_code.Location = new System.Drawing.Point(6, 116);
            this.instrument_code.MaxLength = 15;
            this.instrument_code.Name = "instrument_code";
            this.instrument_code.PasswordChar = '\0';
            this.instrument_code.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.instrument_code.SelectedText = "";
            this.instrument_code.SelectionLength = 0;
            this.instrument_code.SelectionStart = 0;
            this.instrument_code.ShortcutsEnabled = true;
            this.instrument_code.Size = new System.Drawing.Size(99, 23);
            this.instrument_code.TabIndex = 9;
            this.instrument_code.UseSelectable = true;
            this.instrument_code.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.instrument_code.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // brand_refresh
            // 
            this.brand_refresh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.brand_refresh.Location = new System.Drawing.Point(234, 3);
            this.brand_refresh.Name = "brand_refresh";
            this.brand_refresh.Size = new System.Drawing.Size(73, 32);
            this.brand_refresh.TabIndex = 23;
            this.brand_refresh.Text = "Refresh";
            this.brand_refresh.UseSelectable = true;
            this.brand_refresh.Click += new System.EventHandler(this.brand_refresh_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(263, 268);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(53, 23);
            this.metroButton1.TabIndex = 16;
            this.metroButton1.Text = "Upload";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.instrument_image_Click);
            // 
            // instrument_status
            // 
            this.instrument_status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.instrument_status.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.instrument_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instrument_status.FormattingEnabled = true;
            this.instrument_status.Items.AddRange(new object[] {
            "OK",
            "Un-Serviceable",
            "Serviceable",
            "Send To Repair",
            "On Site"});
            this.instrument_status.Location = new System.Drawing.Point(6, 216);
            this.instrument_status.Name = "instrument_status";
            this.instrument_status.Size = new System.Drawing.Size(179, 28);
            this.instrument_status.TabIndex = 13;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(6, 194);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(43, 19);
            this.metroLabel7.TabIndex = 71;
            this.metroLabel7.Text = "Status";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.LightGray;
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1177, 64);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = resources.GetString("groupBox6.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(4097, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = resources.GetString("label2.Text");
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 64);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(1177, 770);
            this.splitContainer1.SplitterDistance = 831;
            this.splitContainer1.TabIndex = 73;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.instrument_list, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(831, 770);
            this.tableLayoutPanel1.TabIndex = 75;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Silver;
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.search_type);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.search_brand);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.search_lifelesser);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.search_life_greater);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.barCodeCatchar);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.search_category);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.instrument_search);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(825, 63);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(727, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 89;
            this.label9.Text = "TYPE";
            // 
            // search_type
            // 
            this.search_type.Location = new System.Drawing.Point(727, 34);
            this.search_type.Name = "search_type";
            this.search_type.Size = new System.Drawing.Size(114, 20);
            this.search_type.TabIndex = 88;
            this.search_type.TextChanged += new System.EventHandler(this.search_type_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(607, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 87;
            this.label8.Text = "BRAND";
            // 
            // search_brand
            // 
            this.search_brand.Location = new System.Drawing.Point(607, 34);
            this.search_brand.Name = "search_brand";
            this.search_brand.Size = new System.Drawing.Size(114, 20);
            this.search_brand.TabIndex = 86;
            this.search_brand.TextChanged += new System.EventHandler(this.search_brand_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(487, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 85;
            this.label7.Text = "< LIFE";
            // 
            // search_lifelesser
            // 
            this.search_lifelesser.Location = new System.Drawing.Point(487, 34);
            this.search_lifelesser.Name = "search_lifelesser";
            this.search_lifelesser.Size = new System.Drawing.Size(114, 20);
            this.search_lifelesser.TabIndex = 84;
            this.search_lifelesser.TextChanged += new System.EventHandler(this.search_lifelesser_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(367, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 83;
            this.label6.Text = "LIFE >";
            // 
            // search_life_greater
            // 
            this.search_life_greater.Location = new System.Drawing.Point(367, 34);
            this.search_life_greater.Name = "search_life_greater";
            this.search_life_greater.Size = new System.Drawing.Size(114, 20);
            this.search_life_greater.TabIndex = 82;
            this.search_life_greater.TextChanged += new System.EventHandler(this.search_life_greater_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(247, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 81;
            this.label5.Text = "CODE";
            // 
            // barCodeCatchar
            // 
            this.barCodeCatchar.Location = new System.Drawing.Point(247, 34);
            this.barCodeCatchar.Name = "barCodeCatchar";
            this.barCodeCatchar.Size = new System.Drawing.Size(114, 20);
            this.barCodeCatchar.TabIndex = 80;
            this.barCodeCatchar.TextChanged += new System.EventHandler(this.barCodeCatchar_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 79;
            this.label4.Text = "CATEGORY";
            // 
            // search_category
            // 
            this.search_category.Location = new System.Drawing.Point(6, 34);
            this.search_category.Name = "search_category";
            this.search_category.Size = new System.Drawing.Size(114, 20);
            this.search_category.TabIndex = 78;
            this.search_category.TextChanged += new System.EventHandler(this.search_category_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(126, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 77;
            this.label3.Text = "NAME";
            // 
            // instrument_search
            // 
            this.instrument_search.Location = new System.Drawing.Point(126, 34);
            this.instrument_search.Name = "instrument_search";
            this.instrument_search.Size = new System.Drawing.Size(114, 20);
            this.instrument_search.TabIndex = 76;
            this.instrument_search.TextChanged += new System.EventHandler(this.instrument_search_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FloralWhite;
            this.groupBox1.Controls.Add(this.instrument_quantity);
            this.groupBox1.Controls.Add(this.metroLabel13);
            this.groupBox1.Controls.Add(this.metroLabel12);
            this.groupBox1.Controls.Add(this.instrument_type);
            this.groupBox1.Controls.Add(this.metroLabel11);
            this.groupBox1.Controls.Add(this.servicable_instrument_description);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.instrument_life);
            this.groupBox1.Controls.Add(this.metroLabel10);
            this.groupBox1.Controls.Add(this.metroLabel9);
            this.groupBox1.Controls.Add(this.instrument_category);
            this.groupBox1.Controls.Add(this.instrument_image_box);
            this.groupBox1.Controls.Add(this.instrument_name);
            this.groupBox1.Controls.Add(this.tableLayoutPanel2);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel7);
            this.groupBox1.Controls.Add(this.instrument_code);
            this.groupBox1.Controls.Add(this.instrument_status);
            this.groupBox1.Controls.Add(this.metroButton1);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Controls.Add(this.instrument_image);
            this.groupBox1.Controls.Add(this.metroLabel6);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.instrument_brand_id);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 770);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // instrument_quantity
            // 
            // 
            // 
            // 
            this.instrument_quantity.CustomButton.Image = null;
            this.instrument_quantity.CustomButton.Location = new System.Drawing.Point(105, 1);
            this.instrument_quantity.CustomButton.Name = "";
            this.instrument_quantity.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.instrument_quantity.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.instrument_quantity.CustomButton.TabIndex = 1;
            this.instrument_quantity.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.instrument_quantity.CustomButton.UseSelectable = true;
            this.instrument_quantity.CustomButton.Visible = false;
            this.instrument_quantity.Enabled = false;
            this.instrument_quantity.Lines = new string[] {
        "14"};
            this.instrument_quantity.Location = new System.Drawing.Point(189, 221);
            this.instrument_quantity.MaxLength = 50;
            this.instrument_quantity.Name = "instrument_quantity";
            this.instrument_quantity.PasswordChar = '\0';
            this.instrument_quantity.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.instrument_quantity.SelectedText = "";
            this.instrument_quantity.SelectionLength = 0;
            this.instrument_quantity.SelectionStart = 0;
            this.instrument_quantity.ShortcutsEnabled = true;
            this.instrument_quantity.Size = new System.Drawing.Size(127, 23);
            this.instrument_quantity.TabIndex = 81;
            this.instrument_quantity.Text = "14";
            this.instrument_quantity.UseSelectable = true;
            this.instrument_quantity.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.instrument_quantity.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(190, 199);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(68, 19);
            this.metroLabel13.TabIndex = 82;
            this.metroLabel13.Text = "Quantity *";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(189, 142);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(36, 19);
            this.metroLabel12.TabIndex = 80;
            this.metroLabel12.Text = "Type";
            // 
            // instrument_type
            // 
            this.instrument_type.DropDownHeight = 150;
            this.instrument_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.instrument_type.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.instrument_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instrument_type.FormattingEnabled = true;
            this.instrument_type.IntegralHeight = false;
            this.instrument_type.Items.AddRange(new object[] {
            "Non-Consumable",
            "Consumable"});
            this.instrument_type.Location = new System.Drawing.Point(190, 164);
            this.instrument_type.Name = "instrument_type";
            this.instrument_type.Size = new System.Drawing.Size(127, 28);
            this.instrument_type.TabIndex = 12;
            this.instrument_type.TextChanged += new System.EventHandler(this.instrument_type_TextChanged);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(9, 293);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(210, 19);
            this.metroLabel11.TabIndex = 78;
            this.metroLabel11.Text = "Serviceable Instrument Description";
            // 
            // servicable_instrument_description
            // 
            this.servicable_instrument_description.Enabled = false;
            this.servicable_instrument_description.Location = new System.Drawing.Point(7, 315);
            this.servicable_instrument_description.Name = "servicable_instrument_description";
            this.servicable_instrument_description.Size = new System.Drawing.Size(307, 64);
            this.servicable_instrument_description.TabIndex = 17;
            this.servicable_instrument_description.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(7, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(304, 21);
            this.label1.TabIndex = 6;
            this.label1.Text = "Add / Update / Delete Instruments";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // instrument_life
            // 
            // 
            // 
            // 
            this.instrument_life.CustomButton.Image = null;
            this.instrument_life.CustomButton.Location = new System.Drawing.Point(87, 1);
            this.instrument_life.CustomButton.Name = "";
            this.instrument_life.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.instrument_life.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.instrument_life.CustomButton.TabIndex = 1;
            this.instrument_life.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.instrument_life.CustomButton.UseSelectable = true;
            this.instrument_life.CustomButton.Visible = false;
            this.instrument_life.Lines = new string[0];
            this.instrument_life.Location = new System.Drawing.Point(207, 70);
            this.instrument_life.MaxLength = 50;
            this.instrument_life.Name = "instrument_life";
            this.instrument_life.PasswordChar = '\0';
            this.instrument_life.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.instrument_life.SelectedText = "";
            this.instrument_life.SelectionLength = 0;
            this.instrument_life.SelectionStart = 0;
            this.instrument_life.ShortcutsEnabled = true;
            this.instrument_life.Size = new System.Drawing.Size(109, 23);
            this.instrument_life.TabIndex = 8;
            this.instrument_life.UseSelectable = true;
            this.instrument_life.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.instrument_life.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel10.Location = new System.Drawing.Point(207, 41);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(29, 19);
            this.metroLabel10.TabIndex = 76;
            this.metroLabel10.Text = "Life";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(6, 42);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(64, 19);
            this.metroLabel9.TabIndex = 74;
            this.metroLabel9.Text = "Category";
            // 
            // instrument_image_box
            // 
            this.instrument_image_box.Location = new System.Drawing.Point(6, 425);
            this.instrument_image_box.Name = "instrument_image_box";
            this.instrument_image_box.Size = new System.Drawing.Size(310, 192);
            this.instrument_image_box.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.instrument_image_box.TabIndex = 50;
            this.instrument_image_box.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.instrument_save, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.brand_refresh, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.instrument_update, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.instrument_delete, 2, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(7, 384);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(310, 38);
            this.tableLayoutPanel2.TabIndex = 18;
            // 
            // instrument_delete
            // 
            this.instrument_delete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.instrument_delete.Location = new System.Drawing.Point(157, 3);
            this.instrument_delete.Name = "instrument_delete";
            this.instrument_delete.Size = new System.Drawing.Size(71, 32);
            this.instrument_delete.TabIndex = 22;
            this.instrument_delete.Text = "DELETE";
            this.instrument_delete.UseSelectable = true;
            this.instrument_delete.Click += new System.EventHandler(this.instrument_delete_Click);
            // 
            // store_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(0, 770);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1194, 637);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "store_form";
            this.Text = "store";
            this.Load += new System.EventHandler(this.store_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.instrument_list)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.instrument_image_box)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox instrument_name;
        private MetroFramework.Controls.MetroButton instrument_update;
        private MetroFramework.Controls.MetroButton instrument_save;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.DataGridView instrument_list;
        private MetroFramework.Controls.MetroComboBox instrument_category;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroComboBox instrument_brand_id;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox instrument_image;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox instrument_code;
        private MetroFramework.Controls.MetroButton brand_refresh;
        private System.Windows.Forms.PictureBox instrument_image_box;
        private MetroFramework.Controls.MetroButton metroButton1;
        private System.Windows.Forms.ComboBox instrument_status;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroButton instrument_delete;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroTextBox instrument_life;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox servicable_instrument_description;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private System.Windows.Forms.ComboBox instrument_type;
        private MetroFramework.Controls.MetroTextBox instrument_quantity;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox search_life_greater;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox barCodeCatchar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox search_category;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox instrument_search;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox search_type;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox search_brand;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox search_lifelesser;
    }
}