﻿namespace StoreManagement.Views
{
    partial class rateList_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rateList_total = new System.Windows.Forms.Label();
            this.rateList_name = new MetroFramework.Controls.MetroTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.rateList_tax = new MetroFramework.Controls.MetroTextBox();
            this.rateList_price = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.rateList_save = new MetroFramework.Controls.MetroButton();
            this.rateList_update = new MetroFramework.Controls.MetroButton();
            this.rateList_delete = new MetroFramework.Controls.MetroButton();
            this.rateList_refresh = new MetroFramework.Controls.MetroButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.rateList_search = new MetroFramework.Controls.MetroTextBox();
            this.rateList_list = new System.Windows.Forms.DataGridView();
            this.printer = new System.Windows.Forms.Button();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rateList_list)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Impact", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Rate List";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.rateList_list, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1144, 628);
            this.tableLayoutPanel2.TabIndex = 59;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rateList_total);
            this.panel2.Controls.Add(this.rateList_name);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.metroLabel4);
            this.panel2.Controls.Add(this.rateList_tax);
            this.panel2.Controls.Add(this.rateList_price);
            this.panel2.Controls.Add(this.metroLabel6);
            this.panel2.Controls.Add(this.metroLabel2);
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1138, 70);
            this.panel2.TabIndex = 38;
            // 
            // rateList_total
            // 
            this.rateList_total.AutoSize = true;
            this.rateList_total.Font = new System.Drawing.Font("Adobe Arabic", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rateList_total.Location = new System.Drawing.Point(757, 22);
            this.rateList_total.Name = "rateList_total";
            this.rateList_total.Size = new System.Drawing.Size(37, 29);
            this.rateList_total.TabIndex = 37;
            this.rateList_total.Text = "Rs.";
            // 
            // rateList_name
            // 
            // 
            // 
            // 
            this.rateList_name.CustomButton.Image = null;
            this.rateList_name.CustomButton.Location = new System.Drawing.Point(202, 1);
            this.rateList_name.CustomButton.Name = "";
            this.rateList_name.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.rateList_name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.rateList_name.CustomButton.TabIndex = 1;
            this.rateList_name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.rateList_name.CustomButton.UseSelectable = true;
            this.rateList_name.CustomButton.Visible = false;
            this.rateList_name.Lines = new string[0];
            this.rateList_name.Location = new System.Drawing.Point(8, 28);
            this.rateList_name.MaxLength = 32767;
            this.rateList_name.Name = "rateList_name";
            this.rateList_name.PasswordChar = '\0';
            this.rateList_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.rateList_name.SelectedText = "";
            this.rateList_name.SelectionLength = 0;
            this.rateList_name.SelectionStart = 0;
            this.rateList_name.ShortcutsEnabled = true;
            this.rateList_name.Size = new System.Drawing.Size(224, 23);
            this.rateList_name.TabIndex = 7;
            this.rateList_name.UseSelectable = true;
            this.rateList_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.rateList_name.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Adobe Arabic", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(698, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 29);
            this.label3.TabIndex = 36;
            this.label3.Text = "Total : ";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(238, 6);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(38, 19);
            this.metroLabel4.TabIndex = 19;
            this.metroLabel4.Text = "Price";
            // 
            // rateList_tax
            // 
            // 
            // 
            // 
            this.rateList_tax.CustomButton.Image = null;
            this.rateList_tax.CustomButton.Location = new System.Drawing.Point(202, 1);
            this.rateList_tax.CustomButton.Name = "";
            this.rateList_tax.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.rateList_tax.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.rateList_tax.CustomButton.TabIndex = 1;
            this.rateList_tax.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.rateList_tax.CustomButton.UseSelectable = true;
            this.rateList_tax.CustomButton.Visible = false;
            this.rateList_tax.Lines = new string[0];
            this.rateList_tax.Location = new System.Drawing.Point(468, 28);
            this.rateList_tax.MaxLength = 32767;
            this.rateList_tax.Name = "rateList_tax";
            this.rateList_tax.PasswordChar = '\0';
            this.rateList_tax.ReadOnly = true;
            this.rateList_tax.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.rateList_tax.SelectedText = "";
            this.rateList_tax.SelectionLength = 0;
            this.rateList_tax.SelectionStart = 0;
            this.rateList_tax.ShortcutsEnabled = true;
            this.rateList_tax.Size = new System.Drawing.Size(224, 23);
            this.rateList_tax.TabIndex = 9;
            this.rateList_tax.UseSelectable = true;
            this.rateList_tax.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.rateList_tax.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.rateList_tax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rateList_tax_KeyPress);
            // 
            // rateList_price
            // 
            // 
            // 
            // 
            this.rateList_price.CustomButton.Image = null;
            this.rateList_price.CustomButton.Location = new System.Drawing.Point(202, 1);
            this.rateList_price.CustomButton.Name = "";
            this.rateList_price.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.rateList_price.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.rateList_price.CustomButton.TabIndex = 1;
            this.rateList_price.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.rateList_price.CustomButton.UseSelectable = true;
            this.rateList_price.CustomButton.Visible = false;
            this.rateList_price.Lines = new string[0];
            this.rateList_price.Location = new System.Drawing.Point(238, 28);
            this.rateList_price.MaxLength = 32767;
            this.rateList_price.Name = "rateList_price";
            this.rateList_price.PasswordChar = '\0';
            this.rateList_price.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.rateList_price.SelectedText = "";
            this.rateList_price.SelectionLength = 0;
            this.rateList_price.SelectionStart = 0;
            this.rateList_price.ShortcutsEnabled = true;
            this.rateList_price.Size = new System.Drawing.Size(224, 23);
            this.rateList_price.TabIndex = 8;
            this.rateList_price.UseSelectable = true;
            this.rateList_price.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.rateList_price.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.rateList_price.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rateList_price_KeyPress);
            this.rateList_price.KeyUp += new System.Windows.Forms.KeyEventHandler(this.rateList_price_KeyUp);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(468, 6);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(27, 19);
            this.metroLabel6.TabIndex = 35;
            this.metroLabel6.Text = "Tax";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(8, 6);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(45, 19);
            this.metroLabel2.TabIndex = 15;
            this.metroLabel2.Text = "Name";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.rateList_save, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.rateList_update, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.rateList_delete, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.rateList_refresh, 3, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(905, 22);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(224, 29);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // rateList_save
            // 
            this.rateList_save.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rateList_save.Location = new System.Drawing.Point(3, 3);
            this.rateList_save.Name = "rateList_save";
            this.rateList_save.Size = new System.Drawing.Size(50, 23);
            this.rateList_save.TabIndex = 18;
            this.rateList_save.Text = "SAVE";
            this.rateList_save.UseSelectable = true;
            this.rateList_save.Click += new System.EventHandler(this.rateList_save_Click);
            // 
            // rateList_update
            // 
            this.rateList_update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rateList_update.Location = new System.Drawing.Point(59, 3);
            this.rateList_update.Name = "rateList_update";
            this.rateList_update.Size = new System.Drawing.Size(50, 23);
            this.rateList_update.TabIndex = 19;
            this.rateList_update.Text = "UPDATE";
            this.rateList_update.UseSelectable = true;
            this.rateList_update.Click += new System.EventHandler(this.rateList_update_Click);
            // 
            // rateList_delete
            // 
            this.rateList_delete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rateList_delete.Location = new System.Drawing.Point(115, 3);
            this.rateList_delete.Name = "rateList_delete";
            this.rateList_delete.Size = new System.Drawing.Size(50, 23);
            this.rateList_delete.TabIndex = 20;
            this.rateList_delete.Text = "DELETE";
            this.rateList_delete.UseSelectable = true;
            this.rateList_delete.Click += new System.EventHandler(this.rateList_delete_Click);
            // 
            // rateList_refresh
            // 
            this.rateList_refresh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rateList_refresh.Location = new System.Drawing.Point(171, 3);
            this.rateList_refresh.Name = "rateList_refresh";
            this.rateList_refresh.Size = new System.Drawing.Size(50, 23);
            this.rateList_refresh.TabIndex = 21;
            this.rateList_refresh.Text = "Refresh";
            this.rateList_refresh.UseSelectable = true;
            this.rateList_refresh.Click += new System.EventHandler(this.rateList_refresh_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1138, 30);
            this.panel1.TabIndex = 2;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.00527F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.72584F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.26889F));
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.rateList_search, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.printer, 2, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1138, 30);
            this.tableLayoutPanel3.TabIndex = 60;
            // 
            // rateList_search
            // 
            // 
            // 
            // 
            this.rateList_search.CustomButton.Image = null;
            this.rateList_search.CustomButton.Location = new System.Drawing.Point(811, 2);
            this.rateList_search.CustomButton.Name = "";
            this.rateList_search.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.rateList_search.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.rateList_search.CustomButton.TabIndex = 1;
            this.rateList_search.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.rateList_search.CustomButton.UseSelectable = true;
            this.rateList_search.CustomButton.Visible = false;
            this.rateList_search.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rateList_search.Lines = new string[0];
            this.rateList_search.Location = new System.Drawing.Point(151, 3);
            this.rateList_search.MaxLength = 32767;
            this.rateList_search.Name = "rateList_search";
            this.rateList_search.PasswordChar = '\0';
            this.rateList_search.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.rateList_search.SelectedText = "";
            this.rateList_search.SelectionLength = 0;
            this.rateList_search.SelectionStart = 0;
            this.rateList_search.ShortcutsEnabled = true;
            this.rateList_search.Size = new System.Drawing.Size(833, 24);
            this.rateList_search.TabIndex = 3;
            this.rateList_search.UseSelectable = true;
            this.rateList_search.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.rateList_search.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.rateList_search.TextChanged += new System.EventHandler(this.rateList_search_TextChanged);
            // 
            // rateList_list
            // 
            this.rateList_list.BackgroundColor = System.Drawing.Color.White;
            this.rateList_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rateList_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rateList_list.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.rateList_list.Location = new System.Drawing.Point(3, 115);
            this.rateList_list.Name = "rateList_list";
            this.rateList_list.Size = new System.Drawing.Size(1138, 510);
            this.rateList_list.TabIndex = 5;
            this.rateList_list.Click += new System.EventHandler(this.rateList_list_Click);
            // 
            // printer
            // 
            this.printer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.printer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.printer.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printer.Location = new System.Drawing.Point(990, 3);
            this.printer.Name = "printer";
            this.printer.Size = new System.Drawing.Size(145, 24);
            this.printer.TabIndex = 4;
            this.printer.Text = "Print";
            this.printer.UseVisualStyleBackColor = true;
            this.printer.Click += new System.EventHandler(this.printer_Click);
            // 
            // rateList_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1144, 628);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "rateList_form";
            this.Text = "RATE LIST";
            this.Load += new System.EventHandler(this.rateList_form_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rateList_list)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroTextBox rateList_search;
        private System.Windows.Forms.DataGridView rateList_list;
        private MetroFramework.Controls.MetroTextBox rateList_tax;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroButton rateList_save;
        private MetroFramework.Controls.MetroButton rateList_update;
        private MetroFramework.Controls.MetroButton rateList_delete;
        private MetroFramework.Controls.MetroButton rateList_refresh;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox rateList_price;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox rateList_name;
        private System.Windows.Forms.Label rateList_total;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button printer;
    }
}