﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using StoreManagement.Reports.view;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class assigment_slip_form : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        Roles userRoles = new Roles();
        public assigment_slip_form()
        {
            InitializeComponent();
        }

        private void assigment_slip_form_Load(object sender, EventArgs e)
        {
            try
            {
                if (service.rolesUser(login.userCredentials.id, "assigment_slip_form") != null)
                {
                    userRoles = service.rolesUser(login.userCredentials.id, "assigment_slip_form");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            refresh();
        }
        private void refresh()
        {
            FillLists();
            clear();
            displayAndValueMembers();
            clear();
        }
        private void displayAndValueMembers()
        {
            team_selection_list.DisplayMember = "name";
            team_selection_list.ValueMember = "id";

            equipment_selection_list.DisplayMember = "name";
            equipment_selection_list.ValueMember = "id";

            consumable_equipment_selection_list.DisplayMember = "name";
            consumable_equipment_selection_list.ValueMember = "id";

        }

        private void FillClients()
        {
            DataTable clients = service.dropdown("clients",0);
            commonHelper.addNewRow(clients);
            client_id.DataSource = clients;
            client_id.DisplayMember = "name";
            client_id.ValueMember = "id";
        }
        bool checkSite = false;
        private void FillProjectCode()
        {
            DataTable searchs = service.siteDropdown(from.Text.ToString(), to.Text.ToString());
            DataRow dr = searchs.NewRow();
            dr["code"] = "Select One";
            dr["id"] = 0;
            searchs.Rows.InsertAt(dr, 0);
            search.DataSource = searchs;
            search.DisplayMember = "code";
            search.ValueMember = "id";
            checkSite = true;
        }

        private void FillLists()
        {
            try
            {
                FillInstruments();
                FillProjectCode();
                fillTeamList();
                FillClients();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void FillInstruments()
        {
            DataTable instruments = service.dropdown("instruments",0);
            commonHelper.addNewRow(instruments);
            equipment_selection.DataSource = instruments;
            equipment_selection.DisplayMember = "name";
            equipment_selection.ValueMember = "id";

            DataTable instruments_consumable = service.dropdown("instruments_consumable",0);
            commonHelper.addNewRow(instruments_consumable);
            consumable_equipment_selection.DataSource = instruments_consumable;
            consumable_equipment_selection.DisplayMember = "name";
            consumable_equipment_selection.ValueMember = "id";
            consumable_equipment_selection_list.IntegralHeight = true;
            consumable_equipment_selection_list.HorizontalScrollbar = true;
        }

        private void fillTeamList()
        {
            DataTable team_1 = service.dropdown("teams_with_Role",0);
            commonHelper.addNewRow(team_1);
            supervisor_id.DataSource = team_1;
            supervisor_id.SelectedIndex = 0;
            supervisor_id.DisplayMember = "name";
            supervisor_id.ValueMember = "id";

            DataTable team_2 = service.dropdown("teams_with_Role",0);
            commonHelper.addNewRow(team_2);
            issue_by.DataSource = team_2;
            issue_by.SelectedIndex = 0;
            issue_by.DisplayMember = "name";
            issue_by.ValueMember = "id";

            DataTable team_3 = service.dropdown("teams_with_Role",0);
            commonHelper.addNewRow(team_3);
            issue_to.DataSource = team_3;
            issue_to.SelectedIndex = 0;
            issue_to.DisplayMember = "name";
            issue_to.ValueMember = "id";

            DataTable team_4 = service.dropdown("Supervisors",0);
            commonHelper.addNewRow(team_4);
            team_selection.DataSource = team_4;
            team_selection.SelectedIndex = 0;
            team_selection.DisplayMember = "name";
            team_selection.ValueMember = "id";
        }

        private bool ValidationUpdate()
        {
            string[] param = {
                name.Text,
                client_id.Text,
                address.Text,
                contact_person.Text,
                purchased_order_no.Text,
                quotation_ref.Text,
                supervisor_id.Text,
                task.Text,
                mobilization_date.Text,
                issue_by.Text,
                issue_to.Text
            };
            return commonHelper.Validator(param);
        }
        private bool Validation()
        {
            string list = "";
            if (equipment_selection_list.Items.Count != 0 && team_selection_list.Items.Count != 0)
            {
                list = "fill";
            }
            string[] param = {
               // code.Text,
                name.Text,
                client_id.Text,
                address.Text,
                contact_person.Text,
                purchased_order_no.Text,
                quotation_ref.Text,
                supervisor_id.Text,
                task.Text,
                mobilization_date.Text,
                issue_by.Text,
                issue_to.Text,
                equipment_selection.Text,
                team_selection.Text,
                list
            };
            return commonHelper.Validator(param);
        }
        private void clear()
        {
            name.Text = "";
            client_id.Text = "";
            address.Text = "";
            contact_person.Text = "";
            purchased_order_no.Text = "";
            quotation_ref.Text = "";
            supervisor_id.Text = "";
            task.Text = "";
            mobilization_date.Text = "";
            issue_by.Text = "";
            issue_to.Text = "";
            equipment_selection.Text = "";
            team_selection.Text = "";
            team_selection_update.Enabled = false;
            equipment_issued_update.Enabled = false;
            instrumnets_selection_update.Enabled = false;
            consumable_instrumnets_selection_update.Enabled = false;
            site_update.Enabled = false;
            site_save.Enabled = true;
            equipment_selection_list.Items.Clear();
            team_selection_list.Items.Clear();
        }
        private void client_new_btn_Click(object sender, EventArgs e)
        {
            client_form cf = new Views.client_form();
            cf.ShowDialog();
        }
        private void instrument_plus_btn_Click(object sender, EventArgs e)
        {
            commonHelper.addRowInListBox(equipment_selection_list, equipment_selection);
        }
        private void instrument_minus_btn_Click(object sender, EventArgs e)
        {
            if (site_id > 0)
            {
                string[] tokens = equipment_selection_list.SelectedItem.ToString().Split();
                int count = tokens.Count();
                int instrument_id = Convert.ToInt32(tokens[count - 2]);
                instrumentsUpdateStatus(instrument_id, 0, false);
                Instruments instrument = new Instruments();
                service.deleteByInstrumentAndSiteId(instrument_id, site_id, instrument,false);
            }
            equipment_selection_list.Items.Remove(equipment_selection_list.SelectedItem);
        }

        private void instrument_remove_all_btn_Click(object sender, EventArgs e)
        {
            equipment_selection_list.Items.Clear();
        }

        private void team_remove_all_btn_Click(object sender, EventArgs e)
        {
            team_selection_list.Items.Clear();
        }

        private void team_remove_btn_Click(object sender, EventArgs e)
        {
            team_selection_list.Items.Remove(team_selection_list.SelectedItem);
        }

        private void team_plus_btn_Click(object sender, EventArgs e)
        {
            commonHelper.addRowInListBox(team_selection_list, team_selection);
        }
        int site_id = 0;
        private void site_save_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_add == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    site_id = siteCreate();
                    siteEquipmentIssuedCreate(site_id);
                    saveInstrumentSelections(site_id);
                    saveConsumableInstrumentSelections(site_id);
                    saveTeamSelections(site_id);
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //MessageBox.Show(service.SendMail("( Site id= " + site_id + " ) A New Project (" + name.Text + ")'s Assignment Slip", "Assignment Slip Created By " + login.bs_username, ""), messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    printReport();
                    refresh();
                    
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private bool siteEquipmentIssuedCreate(int site_id)
        {
            SiteEquipmentIssued siteEquipmentIssued = new BS_SERVICES.SiteEquipmentIssued();
            siteEquipmentIssued.issue_by = Convert.ToInt32(issue_by.SelectedValue);
            siteEquipmentIssued.issue_to = Convert.ToInt32(issue_to.SelectedValue);
            siteEquipmentIssued.site_id = site_id;
            if (service.siteEquipmentIssuedSave(siteEquipmentIssued) != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private int siteCreate()
        {
            Site site = new BS_SERVICES.Site();
            site.user_id = login.userCredentials.id;
            site.name = name.Text;
            site.client_id = Convert.ToInt32(client_id.SelectedValue);
            site.site_address = address.Text;
            site.contact_person = contact_person.Text;
            site.purchased_order_no = purchased_order_no.Text;
            site.quotation_ref = quotation_ref.Text;
            site.supervisor_id = Convert.ToInt32(supervisor_id.SelectedValue);
            site.task = task.Text;
            site.mobilization_date = mobilization_date.Text;
            site.completion_date = completion_date.Text;
            site.created_at = DateTime.Now.ToShortDateString();
            return service.siteSave(site);
        }
        private int siteUpdate()
        {
            Site site = new BS_SERVICES.Site();
            site.id = Convert.ToInt32(search.SelectedValue);
            site.user_id = login.userCredentials.id;
            site.name = name.Text;
            site.client_id = Convert.ToInt32(client_id.SelectedValue);
            site.site_address = address.Text;
            site.contact_person = contact_person.Text;
            site.purchased_order_no = purchased_order_no.Text;
            site.quotation_ref = quotation_ref.Text;
            site.supervisor_id = Convert.ToInt32(supervisor_id.SelectedValue);
            site.task = task.Text;
            site.mobilization_date = mobilization_date.Text;
            site.completion_date = completion_date.Text;
            site.created_at = DateTime.Now.ToShortDateString();
            return service.siteUpdate(site);
        }
        private void saveInstrumentSelections(int site_id, bool isUpdate = false)
        {
            SiteInstruments siteInstruments = new BS_SERVICES.SiteInstruments();
            foreach (var item in equipment_selection_list.Items)
            {
                string[] tokens = item.ToString().Split();
                int count = tokens.Count();
                int instrument_id = Convert.ToInt32(tokens[count - 2]);
                siteInstruments.site_id = site_id;
                siteInstruments.instrument_id = instrument_id;
                siteInstruments.quantity = 0;
                siteInstruments.user_id = login.userCredentials.id;
                if (isUpdate)
                {
                    if (service.siteInstrumentsIfNotExist(siteInstruments) > 0)
                    {
                        instrumentsUpdateStatus(instrument_id,0);
                    }
                }
                else
                {
                    service.siteInstrumentsSave(siteInstruments);
                    instrumentsUpdateStatus(instrument_id,0);
                }


            }
        }
        private void instrumentsUpdateStatus(int instrument_id,int quantity=0,bool isconsumable=false)
        {
            Instruments instruments = new BS_SERVICES.Instruments();
            instruments.status = "On Site";
            instruments.updated_at = DateTime.Now.ToShortDateString();
            instruments.id = instrument_id;
            if (isconsumable)
            {
                instruments.quantity = quantity;
                service.consumableInstrumentupdateStatus(instruments,false);
            }
            else
            {
                instruments.quantity = 0;
                service.instrumentsUpdateStatus(instruments, true);
            }
            
        }
        private void saveConsumableInstrumentSelections(int site_id, bool isUpdate = false)
        {
            SiteInstruments siteInstruments = new BS_SERVICES.SiteInstruments();
            foreach (var item in consumable_equipment_selection_list.Items)
            {
                string[] tokens = item.ToString().Split();
                int count = tokens.Count();
                int instrument_id = Convert.ToInt32(tokens[count - 2]);
                int quantity = Convert.ToInt32(tokens[count - 6]);
                siteInstruments.site_id = site_id;
                siteInstruments.quantity = quantity;
                siteInstruments.instrument_id = instrument_id;
                siteInstruments.user_id = login.userCredentials.id;
                if (isUpdate)
                {
                    if (service.siteInstrumentsIfNotExist(siteInstruments) > 0)
                    {
                        instrumentsUpdateStatus(instrument_id,quantity,true);
                    }
                }
                else
                {
                    service.siteInstrumentsSave(siteInstruments);
                    instrumentsUpdateStatus(instrument_id, quantity, true);
                }

            }
        }
        private void saveTeamSelections(int site_id)
        {
            SiteTeams siteTeams = new BS_SERVICES.SiteTeams();
            foreach (var item in team_selection_list.Items)
            {
                string[] tokens = item.ToString().Split();
                int count = tokens.Count();
                int team_id = Convert.ToInt32(tokens[count - 2]);
                siteTeams.site_id = site_id;
                siteTeams.team_id = team_id;
                service.siteTeamsSave(siteTeams);
            }
        }

        private void refresh_client_Click(object sender, EventArgs e)
        {
            FillClients();
        }

        private void refresh_form_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void open_Click(object sender, EventArgs e)
        {

        }

        private void fron_to_btn_Click(object sender, EventArgs e)
        {
            FillProjectCode();
        }

        private void search_TextChanged(object sender, EventArgs e)
        {
            if (checkSite)
            {
                Site site;
                SiteEquipmentIssued siteEquipmentIssued;
                SiteTeams[] siteTeams;
                SiteInstruments[] siteInstruments;
                site_id = Convert.ToInt32(search.SelectedValue);
                string site_str = search.Text.ToString();
                site = service.siteList(Convert.ToInt32(search.SelectedValue));
                siteEquipmentIssued = service.siteEquipmentIssuedList(Convert.ToInt32(search.SelectedValue));
                siteTeams = service.siteTeamsListForTeamSelection(Convert.ToInt32(search.SelectedValue));
                siteInstruments = service.siteInstrumentsListForSelection(Convert.ToInt32(search.SelectedValue));
                displayDataInSite(site, siteEquipmentIssued, siteInstruments, siteTeams);
                siteInstrumentsList();
                team_selection_update.Enabled = true;
                equipment_issued_update.Enabled = true;
                instrumnets_selection_update.Enabled = true;
                consumable_instrumnets_selection_update.Enabled = true;
                site_update.Enabled = true;
                site_save.Enabled = false;
            }


        }
        private void gridViewProperties()
        {
            instrument_list.Columns[0].Visible = false;
            instrument_list.Columns[1].Visible = false;
            instrument_list.Columns[2].Visible = false;
            instrument_list.Columns[3].Visible = true;
            instrument_list.Columns[4].Visible = false;
            instrument_list.Columns[5].Visible = true;
            instrument_list.Columns[6].Visible = true;
            instrument_list.Columns[7].Visible = true;
            instrument_list.Columns[8].Visible = false;
            instrument_list.Columns[9].Visible = true;
            instrument_list.Columns[10].Visible = true;
            instrument_list.Columns[11].Visible = false;
            instrument_list.Columns[12].Visible = false;
        }
        int siteEquipmentIssued_id = 0;
        private void displayDataInSite(Site site = null, SiteEquipmentIssued siteEquipmentIssued = null, SiteInstruments[] siteInstruments = null, SiteTeams[] siteTeams = null)
        {
            if (site != null)
            {
                name.Text = site.name;
                client_id.SelectedValue = site.client_id;
                address.Text = site.site_address;
                contact_person.Text = site.contact_person;
                purchased_order_no.Text = site.purchased_order_no;
                quotation_ref.Text = site.quotation_ref;
                supervisor_id.SelectedValue = site.supervisor_id;
                task.Text = site.task;
                mobilization_date.Text = site.mobilization_date.ToString();
                completion_date.Text = site.completion_date.ToString();
            }
            if (siteEquipmentIssued != null)
            {
                siteEquipmentIssued_id = siteEquipmentIssued.id;
                issue_by.SelectedValue = siteEquipmentIssued.issue_by;
                issue_to.SelectedValue = siteEquipmentIssued.issue_to;
            }
            else
            {
                siteEquipmentIssued_id = 0;
            }
            if (siteInstruments != null)
            {
                equipment_selection_list.Items.Clear();
                consumable_equipment_selection_list.Items.Clear();
                foreach (var item in siteInstruments)
                {
                    if (item.type != "Consumable")
                    {
                        equipment_selection_list.Items.Add(new { name = item.instrument_name, id = item.instrument_id });
                    }
                    else
                    {
                        string name = item.instrument_name + " | Quantity - " + item.quantity + " |";
                        consumable_equipment_selection_list.Items.Add(new { name = name, id = item.instrument_id });
                    }

                }
                equipment_selection.Text = "Select One";
                consumable_equipment_selection.Text = "Select One";
            }
            else
            {
                equipment_selection_list.Items.Clear();
                consumable_equipment_selection_list.Items.Clear();
            }
            if (siteTeams != null)
            {
                team_selection_list.Items.Clear();
                foreach (var item in siteTeams)
                {
                    team_selection_list.Items.Add(new { name = item.team_name, id = item.team_id });
                }
                team_selection.Text = "Select One";
            }
            else
            {
                team_selection_list.Items.Clear();
            }


        }
        SiteInstruments[] siteInstruments;

        private void siteInstrumentsList()
        {
            siteInstruments = service.siteInstrumentsList(Convert.ToInt32(search.SelectedValue), "On Site");
            if (siteInstruments != null)
            {

                instrument_list.DataSource = siteInstruments;
                gridViewProperties();
            }
            else
            {
                instrument_list.DataSource = null;
                instrument_list.Refresh();
            }
        }
        private void instrument_refresh_Click(object sender, EventArgs e)
        {
            siteInstrumentsList();
        }
        private void barCodeGridCatcher_btn_Click(object sender, EventArgs e)
        {
            string[] instrument = service.GetIdByBarcode(barCodeGridCatcher.Text);
            if (instrument[0] != null)
            {
                foreach (var item in siteInstruments)
                {
                    if (item.instrument_id == Convert.ToInt32(instrument[0]))
                    {
                        siteinstrument_id = Convert.ToInt32(item.id);
                        instrument_id = Convert.ToInt32(item.instrument_id);
                        instrument_name.Text = item.instrument_name;
                        instrument_status.Text = item.status;
                        instrument_quantity.Text= item.quantity.ToString();
                        receivedQuantity = item.quantity;
                        instrument_type.Text = item.type;
                        instrument_life.Text = item.instrument_life;
                    }
                }
                barCodeGridCatcher.Focus();
                barCodeGridCatcher.SelectionStart = 0;
                barCodeGridCatcher.SelectionLength = barCodeGridCatcher.Text.Length;
            }
            else
            {
                //MessageBox.Show("code not found");
            }

        }
        private void instrument_list_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ShowDataInTextbox(instrument_list.CurrentRow.Index);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        int instrument_id = 0;
        int siteinstrument_id = 0;
        int receivedQuantity = 0;
        private void ShowDataInTextbox(int index)
        {
            if (index == -1) { return; }
            siteinstrument_id = Convert.ToInt32(instrument_list.Rows[index].Cells["id"].Value.ToString());
            instrument_id = Convert.ToInt32(instrument_list.Rows[index].Cells["instrument_id"].Value.ToString());
            instrument_name.Text = instrument_list.Rows[index].Cells["instrument_name"].Value.ToString();
            instrument_status.Text = instrument_list.Rows[index].Cells["status"].Value.ToString();
            instrument_quantity.Text = instrument_list.Rows[index].Cells["quantity"].Value.ToString();
            receivedQuantity = Convert.ToInt32(instrument_quantity.Text);
            instrument_type.Text = instrument_list.Rows[index].Cells["type"].Value.ToString();
            instrument_life.Text = instrument_list.Rows[index].Cells["instrument_life"].Value.ToString();
        }
        private bool saveRepairs()
        {
            if (damage_instrument_description.Text != "")
            {
                Repairs repairs = new Repairs();
                repairs.instrument_id = instrument_id;
                repairs.shop = "";
                repairs.person = "";
                repairs.description = damage_instrument_description.Text;
                repairs.site_id = Convert.ToInt32(search.SelectedValue);
                service.repairsSave(repairs);
                return true;
            }
            else
            {
                MessageBox.Show("Please Fill Damaged Instrument Description Field", messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private void instrument_save_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_add == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (instrument_name.Text != null && instrument_status.Text != null && instrument_life.Text != null)
                {
                    bool isconsumableInstrument = false;
                    if (isConsumable())
                    {
                        isconsumableInstrument = true;
                        if (Convert.ToInt32(instrument_quantity.Text) <= 0 || instrument_quantity.Text == "")
                        {
                            MessageBox.Show("Please enter quantity", messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        else if (Convert.ToInt32(instrument_quantity.Text) > receivedQuantity)
                        {
                            MessageBox.Show("You have only '"+receivedQuantity+"' quantity", messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }else
                    {
                        isconsumableInstrument = false;
                    }
                    if (instrument_status.Text == "On Site")
                    {
                        MessageBox.Show("Please change status", messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (instrument_status.Text == "Serviceable" || instrument_status.Text == "Un-Serviceable")
                    {
                        if (saveRepairs() == false)
                        {
                            return;
                        }
                    }
                    if (isconsumableInstrument)
                    {
                        Instruments instruments = new BS_SERVICES.Instruments();
                        instruments.id = instrument_id;
                        instruments.status = instrument_status.Text;
                        instruments.life = Convert.ToInt32(instrument_life.Text);
                        instruments.quantity = Convert.ToInt32(instrument_quantity.Text);
                        service.consumableInstrumentupdateStatus(instruments, true);
                        SiteInstruments siteInstruments = new BS_SERVICES.SiteInstruments();
                        siteInstruments.id = siteinstrument_id;
                        siteInstruments.user_id = login.userCredentials.id;
                        siteInstruments.status = instrument_status.Text;
                        siteInstruments.received_quantity = Convert.ToInt32(instrument_quantity.Text);
                        siteInstruments.description = damage_instrument_description.Text;
                        service.siteInstrumentsUpdateStatus(siteInstruments);
                    }
                    else
                    {
                        Instruments instruments = new BS_SERVICES.Instruments();
                        instruments.id = instrument_id;
                        instruments.status = instrument_status.Text;
                        instruments.life = Convert.ToInt32(instrument_life.Text);
                        service.instrumentsUpdateStatus(instruments, false);
                        SiteInstruments siteInstruments = new BS_SERVICES.SiteInstruments();
                        siteInstruments.id = siteinstrument_id;
                        siteInstruments.user_id = login.userCredentials.id;
                        siteInstruments.status = instrument_status.Text;
                        siteInstruments.description = damage_instrument_description.Text;
                        siteInstruments.received_quantity = 0;
                        service.siteInstrumentsUpdateStatus(siteInstruments);
                    }
                    
                    FillInstruments();
                    siteInstrumentsList();
                    SiteInstrumentclear();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool isConsumable()
        {
            if (instrument_type.Text=="Consumable")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SiteInstrumentclear()
        {
            damage_instrument_description.Text = null;
            instrument_status.Text = null;
            instrument_life.Text = null;
            instrument_name.Text = null;
        }

        private void team_selection_update_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (team_selection_list.Items.Count != 0)
                {
                    service.deleteBy("site_teams", "site_id", Convert.ToInt32(search.SelectedValue));
                    saveTeamSelections(Convert.ToInt32(search.SelectedValue));
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    // refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void instrumnets_selection_update_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (equipment_selection_list.Items.Count != 0)
                {
                    //service.deleteBy("site_instruments", "site_id", Convert.ToInt32(search.SelectedValue));
                    saveInstrumentSelections(Convert.ToInt32(search.SelectedValue), true);
                    //saveConsumableInstrumentSelections(Convert.ToInt32(search.SelectedValue));
                    MessageBox.Show(messages.update, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //refresh();
                    FillInstruments();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void equipment_issued_update_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (issue_to.Text != "Select One" && issue_to.Text != "" && issue_by.Text != "Select One" && issue_by.Text != "")
                {
                    SiteEquipmentIssued siteEquipmentIssued = new SiteEquipmentIssued();
                    siteEquipmentIssued.id = siteEquipmentIssued_id;
                    siteEquipmentIssued.issue_by = Convert.ToInt32(issue_by.SelectedValue);
                    siteEquipmentIssued.issue_to = Convert.ToInt32(issue_to.SelectedValue);
                    siteEquipmentIssued.updated_at = DateTime.Now.ToShortDateString();
                    if (siteEquipmentIssued_id == 0)
                    {
                        siteEquipmentIssuedCreate(Convert.ToInt32(search.SelectedValue));
                    }
                    else
                    {
                        service.siteEquipmentIssuedUpdate(siteEquipmentIssued);
                    }
                    MessageBox.Show(messages.update, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void printReport()
        {
            AssignmentFORM assignmentFORM = new AssignmentFORM();
            assignmentFORM.site_iid = site_id;
            assignmentFORM.Show();
        }
        private void print_Click(object sender, EventArgs e)
        {
            printReport();
        }

        private void site_update_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                if (ValidationUpdate())
                {
                    site_id = siteUpdate();
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void barCodeCatcher_btn_Click(object sender, EventArgs e)
        {
            try
            {
                string[] instrument = service.GetIdByBarcode(barCodeCatcher.Text);
                if (instrument[0] != null)
                {
                    int index = equipment_selection.FindString(instrument[1]);
                    equipment_selection.SelectedIndex = index;
                    commonHelper.addRowInListBox(equipment_selection_list, equipment_selection);
                    barCodeCatcher.Focus();
                    barCodeCatcher.SelectionStart = 0;
                    barCodeCatcher.SelectionLength = barCodeCatcher.Text.Length;
                }
                else
                {
                    //MessageBox.Show("code not found");
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void consumable_barCodeCatcher_btn_Click(object sender, EventArgs e)
        {
            try
            {
                if (quantity.Text != "")
                {
                    string[] instrument = service.GetIdByBarcode(consumable_barCodeCatcher.Text);
                    if (instrument[0] != null)
                    {
                        int index = consumable_equipment_selection.FindString(instrument[1]);
                        consumable_equipment_selection.SelectedIndex = index;
                        commonHelper.addRowInListBox(consumable_equipment_selection_list, consumable_equipment_selection, Convert.ToInt32(quantity.Text));
                        consumable_barCodeCatcher.Focus();
                        consumable_barCodeCatcher.SelectionStart = 0;
                        consumable_barCodeCatcher.SelectionLength = barCodeCatcher.Text.Length;
                    }
                    else
                    {
                        //MessageBox.Show("code not found");
                    }
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void consumable_instrument_plus_btn_Click(object sender, EventArgs e)
        {
            try
            {
                if (quantity.Text != "")
                {
                    commonHelper.addRowInListBox(consumable_equipment_selection_list, consumable_equipment_selection, Convert.ToInt32(quantity.Text));

                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void consumable_instrument_remove_btn_Click(object sender, EventArgs e)
        {
            if (site_id > 0)
            {
                string[] tokens = consumable_equipment_selection_list.SelectedItem.ToString().Split();
                int count = tokens.Count();
                int instrument_id = Convert.ToInt32(tokens[count - 2]);
                int quantity = Convert.ToInt32(tokens[count - 6]);
                Instruments instruments = new BS_SERVICES.Instruments();
                instruments.id = instrument_id;
                instruments.status = "On Site";
                instruments.quantity = quantity;
                instruments.updated_at = DateTime.Now.ToShortDateString();
                service.deleteByInstrumentAndSiteId(instrument_id, site_id,instruments,true);
            }
            
            consumable_equipment_selection_list.Items.Remove(consumable_equipment_selection_list.SelectedItem);
        }

        private void consumable_instrument_remove_all_btn_Click(object sender, EventArgs e)
        {
            consumable_equipment_selection_list.Items.Clear();
        }

        private void consumable_instrumnets_selection_update_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (equipment_selection_list.Items.Count != 0)
                {
                    saveConsumableInstrumentSelections(Convert.ToInt32(search.SelectedValue), true);
                    MessageBox.Show(messages.update, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //refresh();
                    FillInstruments();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void quantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) /*&& (e.KeyChar != '.')*/)
            {
                e.Handled = true;
            }
        }

        private void instrument_life_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) /*&& (e.KeyChar != '.')*/)
            {
                e.Handled = true;
            }
        }

        private void instrument_delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure! you want to delete this record", "Alert", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                if (siteinstrument_id>0)
                {
                    service.deleteBy("site_instruments", "id", siteinstrument_id);
                    MessageBox.Show(messages.delete, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FillInstruments();
                    siteInstrumentsList();
                    SiteInstrumentclear();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
               
            }
        }
    }
}
