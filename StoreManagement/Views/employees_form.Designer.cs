﻿namespace StoreManagement.Views
{
    partial class employees_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(employees_form));
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.employees_refresh = new System.Windows.Forms.Button();
            this.employees_update = new System.Windows.Forms.Button();
            this.employees_save = new System.Windows.Forms.Button();
            this.attendanceSite_id = new System.Windows.Forms.ComboBox();
            this.department = new System.Windows.Forms.ComboBox();
            this.employees_address = new System.Windows.Forms.TextBox();
            this.employees_email = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.employees_status = new System.Windows.Forms.ComboBox();
            this.employees_roles = new System.Windows.Forms.TextBox();
            this.employees_cnic = new System.Windows.Forms.TextBox();
            this.employees_phone = new System.Windows.Forms.TextBox();
            this.employees_name = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.employees_list = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employees_list)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Impact", 15F);
            this.label2.Location = new System.Drawing.Point(19, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "EMPLOYEES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.employees_refresh);
            this.panel2.Controls.Add(this.employees_update);
            this.panel2.Controls.Add(this.employees_save);
            this.panel2.Controls.Add(this.attendanceSite_id);
            this.panel2.Controls.Add(this.department);
            this.panel2.Controls.Add(this.employees_address);
            this.panel2.Controls.Add(this.employees_email);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.employees_status);
            this.panel2.Controls.Add(this.employees_roles);
            this.panel2.Controls.Add(this.employees_cnic);
            this.panel2.Controls.Add(this.employees_phone);
            this.panel2.Controls.Add(this.employees_name);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1309, 286);
            this.panel2.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(70, 224);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(755, 9);
            this.label4.TabIndex = 80;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // employees_refresh
            // 
            this.employees_refresh.BackColor = System.Drawing.Color.LightSlateGray;
            this.employees_refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.employees_refresh.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.employees_refresh.ForeColor = System.Drawing.Color.White;
            this.employees_refresh.Location = new System.Drawing.Point(757, 184);
            this.employees_refresh.Name = "employees_refresh";
            this.employees_refresh.Size = new System.Drawing.Size(63, 23);
            this.employees_refresh.TabIndex = 79;
            this.employees_refresh.Text = "REFRESH";
            this.employees_refresh.UseVisualStyleBackColor = false;
            this.employees_refresh.Click += new System.EventHandler(this.employees_refresh_Click);
            // 
            // employees_update
            // 
            this.employees_update.BackColor = System.Drawing.Color.LightSlateGray;
            this.employees_update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.employees_update.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.employees_update.ForeColor = System.Drawing.Color.White;
            this.employees_update.Location = new System.Drawing.Point(688, 184);
            this.employees_update.Name = "employees_update";
            this.employees_update.Size = new System.Drawing.Size(63, 23);
            this.employees_update.TabIndex = 77;
            this.employees_update.Text = "UPDATE";
            this.employees_update.UseVisualStyleBackColor = false;
            this.employees_update.Click += new System.EventHandler(this.employees_update_Click);
            // 
            // employees_save
            // 
            this.employees_save.BackColor = System.Drawing.Color.LightSlateGray;
            this.employees_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.employees_save.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.employees_save.ForeColor = System.Drawing.Color.White;
            this.employees_save.Location = new System.Drawing.Point(619, 184);
            this.employees_save.Name = "employees_save";
            this.employees_save.Size = new System.Drawing.Size(63, 23);
            this.employees_save.TabIndex = 76;
            this.employees_save.Text = "SAVE";
            this.employees_save.UseVisualStyleBackColor = false;
            this.employees_save.Click += new System.EventHandler(this.employees_save_Click);
            // 
            // attendanceSite_id
            // 
            this.attendanceSite_id.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.attendanceSite_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendanceSite_id.FormattingEnabled = true;
            this.attendanceSite_id.Items.AddRange(new object[] {
            "Office Attendance",
            "PKLI Site Attendance"});
            this.attendanceSite_id.Location = new System.Drawing.Point(130, 246);
            this.attendanceSite_id.Name = "attendanceSite_id";
            this.attendanceSite_id.Size = new System.Drawing.Size(270, 26);
            this.attendanceSite_id.TabIndex = 73;
            this.attendanceSite_id.TextChanged += new System.EventHandler(this.attendanceSite_id_TextChanged);
            // 
            // department
            // 
            this.department.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.department.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.department.FormattingEnabled = true;
            this.department.Items.AddRange(new object[] {
            "BS Office",
            "NDEC",
            "Plumbing",
            "Fire Fighting"});
            this.department.Location = new System.Drawing.Point(550, 88);
            this.department.Name = "department";
            this.department.Size = new System.Drawing.Size(164, 26);
            this.department.TabIndex = 72;
            // 
            // employees_address
            // 
            this.employees_address.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.employees_address.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employees_address.Location = new System.Drawing.Point(550, 119);
            this.employees_address.Multiline = true;
            this.employees_address.Name = "employees_address";
            this.employees_address.Size = new System.Drawing.Size(270, 56);
            this.employees_address.TabIndex = 71;
            // 
            // employees_email
            // 
            this.employees_email.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.employees_email.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employees_email.Location = new System.Drawing.Point(550, 57);
            this.employees_email.Name = "employees_email";
            this.employees_email.Size = new System.Drawing.Size(270, 25);
            this.employees_email.TabIndex = 69;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F);
            this.label1.Location = new System.Drawing.Point(458, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 19);
            this.label1.TabIndex = 68;
            this.label1.Text = "Department :";
            // 
            // employees_status
            // 
            this.employees_status.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.employees_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employees_status.FormattingEnabled = true;
            this.employees_status.Items.AddRange(new object[] {
            "1",
            "0"});
            this.employees_status.Location = new System.Drawing.Point(130, 181);
            this.employees_status.Name = "employees_status";
            this.employees_status.Size = new System.Drawing.Size(134, 26);
            this.employees_status.TabIndex = 67;
            // 
            // employees_roles
            // 
            this.employees_roles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.employees_roles.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employees_roles.Location = new System.Drawing.Point(130, 150);
            this.employees_roles.Name = "employees_roles";
            this.employees_roles.Size = new System.Drawing.Size(270, 25);
            this.employees_roles.TabIndex = 66;
            // 
            // employees_cnic
            // 
            this.employees_cnic.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.employees_cnic.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employees_cnic.Location = new System.Drawing.Point(130, 119);
            this.employees_cnic.Name = "employees_cnic";
            this.employees_cnic.Size = new System.Drawing.Size(270, 25);
            this.employees_cnic.TabIndex = 65;
            // 
            // employees_phone
            // 
            this.employees_phone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.employees_phone.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employees_phone.Location = new System.Drawing.Point(130, 88);
            this.employees_phone.Name = "employees_phone";
            this.employees_phone.Size = new System.Drawing.Size(270, 25);
            this.employees_phone.TabIndex = 64;
            // 
            // employees_name
            // 
            this.employees_name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.employees_name.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employees_name.Location = new System.Drawing.Point(130, 57);
            this.employees_name.Name = "employees_name";
            this.employees_name.Size = new System.Drawing.Size(270, 25);
            this.employees_name.TabIndex = 63;
            this.employees_name.TextChanged += new System.EventHandler(this.employees_name_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(73, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 19);
            this.label7.TabIndex = 56;
            this.label7.Text = "Type : ";
            this.label7.TextChanged += new System.EventHandler(this.attendanceSite_id_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F);
            this.label3.Location = new System.Drawing.Point(73, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 19);
            this.label3.TabIndex = 38;
            this.label3.Text = "Name : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F);
            this.label5.Location = new System.Drawing.Point(73, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 19);
            this.label5.TabIndex = 40;
            this.label5.Text = "Phone : ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12F);
            this.label9.Location = new System.Drawing.Point(73, 121);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 19);
            this.label9.TabIndex = 42;
            this.label9.Text = "CNIC :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12F);
            this.label17.Location = new System.Drawing.Point(73, 153);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 19);
            this.label17.TabIndex = 46;
            this.label17.Text = "Role :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 12F);
            this.label15.Location = new System.Drawing.Point(73, 183);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 19);
            this.label15.TabIndex = 48;
            this.label15.Text = "Status :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12F);
            this.label11.Location = new System.Drawing.Point(458, 121);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 19);
            this.label11.TabIndex = 52;
            this.label11.Text = "Address :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 12F);
            this.label13.Location = new System.Drawing.Point(458, 59);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 19);
            this.label13.TabIndex = 50;
            this.label13.Text = "Email :";
            // 
            // employees_list
            // 
            this.employees_list.BackgroundColor = System.Drawing.Color.White;
            this.employees_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.employees_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.employees_list.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.employees_list.Location = new System.Drawing.Point(3, 295);
            this.employees_list.Name = "employees_list";
            this.employees_list.Size = new System.Drawing.Size(1309, 384);
            this.employees_list.TabIndex = 5;
            this.employees_list.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.employees_list_CellClick);
            this.employees_list.Click += new System.EventHandler(this.employees_list_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.employees_list, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1315, 682);
            this.tableLayoutPanel3.TabIndex = 68;
            // 
            // employees_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1315, 682);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Name = "employees_form";
            this.Text = "EMPLOYEES";
            this.Load += new System.EventHandler(this.employees_form_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employees_list)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView employees_list;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox employees_roles;
        private System.Windows.Forms.TextBox employees_cnic;
        private System.Windows.Forms.TextBox employees_phone;
        private System.Windows.Forms.TextBox employees_name;
        private System.Windows.Forms.ComboBox employees_status;
        private System.Windows.Forms.ComboBox department;
        private System.Windows.Forms.TextBox employees_address;
        private System.Windows.Forms.TextBox employees_email;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox attendanceSite_id;
        private System.Windows.Forms.Button employees_refresh;
        private System.Windows.Forms.Button employees_update;
        private System.Windows.Forms.Button employees_save;
        private System.Windows.Forms.Label label4;
    }
}