﻿namespace StoreManagement
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.header_panel = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.username_txt = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.metroTile2 = new MetroFramework.Controls.MetroTile();
            this.employee_btn = new MetroFramework.Controls.MetroTile();
            this.rating_btn = new MetroFramework.Controls.MetroTile();
            this.attendance_btn = new MetroFramework.Controls.MetroTile();
            this.setting_btn = new MetroFramework.Controls.MetroTile();
            this.billing_btn = new MetroFramework.Controls.MetroTile();
            this.PERMISSION = new MetroFramework.Controls.MetroTile();
            this.user = new MetroFramework.Controls.MetroTile();
            this.logout = new MetroFramework.Controls.MetroTile();
            this.SEND_TO_REPAIR = new MetroFramework.Controls.MetroTile();
            this.Title_ASSIGNMENT_CLIENT = new MetroFramework.Controls.MetroTile();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.Title_ASSIGNMENT_SLIP = new MetroFramework.Controls.MetroTile();
            this.Title_VIEW_SLIPS = new MetroFramework.Controls.MetroTile();
            this.Title_TEAM = new MetroFramework.Controls.MetroTile();
            this.Title_BRANDS = new MetroFramework.Controls.MetroTile();
            this.Title_INSTRUMENT = new MetroFramework.Controls.MetroTile();
            this.footer_panel = new System.Windows.Forms.Panel();
            this.content_panel = new System.Windows.Forms.Panel();
            this.header_panel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // header_panel
            // 
            this.header_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.header_panel.Controls.Add(this.panel2);
            this.header_panel.Controls.Add(this.panel1);
            resources.ApplyResources(this.header_panel, "header_panel");
            this.header_panel.Name = "header_panel";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.username_txt);
            this.panel2.Controls.Add(this.label2);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // username_txt
            // 
            resources.ApplyResources(this.username_txt, "username_txt");
            this.username_txt.ForeColor = System.Drawing.Color.White;
            this.username_txt.Name = "username_txt";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Name = "label2";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.metroTile2);
            this.panel1.Controls.Add(this.employee_btn);
            this.panel1.Controls.Add(this.rating_btn);
            this.panel1.Controls.Add(this.attendance_btn);
            this.panel1.Controls.Add(this.setting_btn);
            this.panel1.Controls.Add(this.billing_btn);
            this.panel1.Controls.Add(this.PERMISSION);
            this.panel1.Controls.Add(this.user);
            this.panel1.Controls.Add(this.logout);
            this.panel1.Controls.Add(this.SEND_TO_REPAIR);
            this.panel1.Controls.Add(this.Title_ASSIGNMENT_CLIENT);
            this.panel1.Controls.Add(this.metroTile1);
            this.panel1.Controls.Add(this.Title_ASSIGNMENT_SLIP);
            this.panel1.Controls.Add(this.Title_VIEW_SLIPS);
            this.panel1.Controls.Add(this.Title_TEAM);
            this.panel1.Controls.Add(this.Title_BRANDS);
            this.panel1.Controls.Add(this.Title_INSTRUMENT);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // metroTile2
            // 
            this.metroTile2.ActiveControl = null;
            resources.ApplyResources(this.metroTile2, "metroTile2");
            this.metroTile2.Name = "metroTile2";
            this.metroTile2.UseSelectable = true;
            this.metroTile2.Click += new System.EventHandler(this.metroTile2_Click);
            // 
            // employee_btn
            // 
            this.employee_btn.ActiveControl = null;
            resources.ApplyResources(this.employee_btn, "employee_btn");
            this.employee_btn.Name = "employee_btn";
            this.employee_btn.UseSelectable = true;
            this.employee_btn.Click += new System.EventHandler(this.employee_btn_Click);
            // 
            // rating_btn
            // 
            this.rating_btn.ActiveControl = null;
            resources.ApplyResources(this.rating_btn, "rating_btn");
            this.rating_btn.Name = "rating_btn";
            this.rating_btn.UseSelectable = true;
            this.rating_btn.Click += new System.EventHandler(this.rating_btn_Click);
            // 
            // attendance_btn
            // 
            this.attendance_btn.ActiveControl = null;
            resources.ApplyResources(this.attendance_btn, "attendance_btn");
            this.attendance_btn.Name = "attendance_btn";
            this.attendance_btn.UseSelectable = true;
            this.attendance_btn.Click += new System.EventHandler(this.attendance_btn_Click);
            // 
            // setting_btn
            // 
            this.setting_btn.ActiveControl = null;
            resources.ApplyResources(this.setting_btn, "setting_btn");
            this.setting_btn.Name = "setting_btn";
            this.setting_btn.UseSelectable = true;
            this.setting_btn.Click += new System.EventHandler(this.setting_btn_Click);
            // 
            // billing_btn
            // 
            this.billing_btn.ActiveControl = null;
            resources.ApplyResources(this.billing_btn, "billing_btn");
            this.billing_btn.Name = "billing_btn";
            this.billing_btn.UseSelectable = true;
            this.billing_btn.Click += new System.EventHandler(this.billing_btn_Click);
            // 
            // PERMISSION
            // 
            this.PERMISSION.ActiveControl = null;
            resources.ApplyResources(this.PERMISSION, "PERMISSION");
            this.PERMISSION.Name = "PERMISSION";
            this.PERMISSION.UseSelectable = true;
            this.PERMISSION.Click += new System.EventHandler(this.PERMISSION_Click);
            // 
            // user
            // 
            this.user.ActiveControl = null;
            resources.ApplyResources(this.user, "user");
            this.user.Name = "user";
            this.user.UseSelectable = true;
            this.user.Click += new System.EventHandler(this.user_Click);
            // 
            // logout
            // 
            this.logout.ActiveControl = null;
            resources.ApplyResources(this.logout, "logout");
            this.logout.Name = "logout";
            this.logout.UseSelectable = true;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // SEND_TO_REPAIR
            // 
            this.SEND_TO_REPAIR.ActiveControl = null;
            resources.ApplyResources(this.SEND_TO_REPAIR, "SEND_TO_REPAIR");
            this.SEND_TO_REPAIR.Name = "SEND_TO_REPAIR";
            this.SEND_TO_REPAIR.UseSelectable = true;
            this.SEND_TO_REPAIR.Click += new System.EventHandler(this.SEND_TO_REPAIR_Click);
            // 
            // Title_ASSIGNMENT_CLIENT
            // 
            this.Title_ASSIGNMENT_CLIENT.ActiveControl = null;
            resources.ApplyResources(this.Title_ASSIGNMENT_CLIENT, "Title_ASSIGNMENT_CLIENT");
            this.Title_ASSIGNMENT_CLIENT.Name = "Title_ASSIGNMENT_CLIENT";
            this.Title_ASSIGNMENT_CLIENT.UseSelectable = true;
            this.Title_ASSIGNMENT_CLIENT.Click += new System.EventHandler(this.Title_ASSIGNMENT_CLIENT_Click);
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            resources.ApplyResources(this.metroTile1, "metroTile1");
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.UseSelectable = true;
            this.metroTile1.Click += new System.EventHandler(this.metroTile1_Click);
            // 
            // Title_ASSIGNMENT_SLIP
            // 
            this.Title_ASSIGNMENT_SLIP.ActiveControl = null;
            resources.ApplyResources(this.Title_ASSIGNMENT_SLIP, "Title_ASSIGNMENT_SLIP");
            this.Title_ASSIGNMENT_SLIP.Name = "Title_ASSIGNMENT_SLIP";
            this.Title_ASSIGNMENT_SLIP.UseSelectable = true;
            this.Title_ASSIGNMENT_SLIP.Click += new System.EventHandler(this.Title_ASSIGNMENT_SLIP_Click);
            // 
            // Title_VIEW_SLIPS
            // 
            this.Title_VIEW_SLIPS.ActiveControl = null;
            resources.ApplyResources(this.Title_VIEW_SLIPS, "Title_VIEW_SLIPS");
            this.Title_VIEW_SLIPS.Name = "Title_VIEW_SLIPS";
            this.Title_VIEW_SLIPS.UseSelectable = true;
            this.Title_VIEW_SLIPS.Click += new System.EventHandler(this.Title_VIEW_SLIPS_Click);
            // 
            // Title_TEAM
            // 
            this.Title_TEAM.ActiveControl = null;
            resources.ApplyResources(this.Title_TEAM, "Title_TEAM");
            this.Title_TEAM.Name = "Title_TEAM";
            this.Title_TEAM.UseSelectable = true;
            this.Title_TEAM.Click += new System.EventHandler(this.Title_TEAM_Click);
            // 
            // Title_BRANDS
            // 
            this.Title_BRANDS.ActiveControl = null;
            resources.ApplyResources(this.Title_BRANDS, "Title_BRANDS");
            this.Title_BRANDS.Name = "Title_BRANDS";
            this.Title_BRANDS.UseSelectable = true;
            this.Title_BRANDS.Click += new System.EventHandler(this.Title_BRANDS_Click);
            // 
            // Title_INSTRUMENT
            // 
            this.Title_INSTRUMENT.ActiveControl = null;
            resources.ApplyResources(this.Title_INSTRUMENT, "Title_INSTRUMENT");
            this.Title_INSTRUMENT.Name = "Title_INSTRUMENT";
            this.Title_INSTRUMENT.UseSelectable = true;
            this.Title_INSTRUMENT.Click += new System.EventHandler(this.Title_STORE_Click);
            // 
            // footer_panel
            // 
            this.footer_panel.BackColor = System.Drawing.Color.Gray;
            resources.ApplyResources(this.footer_panel, "footer_panel");
            this.footer_panel.Name = "footer_panel";
            // 
            // content_panel
            // 
            resources.ApplyResources(this.content_panel, "content_panel");
            this.content_panel.BackColor = System.Drawing.Color.White;
            this.content_panel.Name = "content_panel";
            // 
            // Home
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.content_panel);
            this.Controls.Add(this.footer_panel);
            this.Controls.Add(this.header_panel);
            this.Name = "Home";
            this.Load += new System.EventHandler(this.Home_Load);
            this.header_panel.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel header_panel;
        private System.Windows.Forms.Panel footer_panel;
        private MetroFramework.Controls.MetroTile Title_TEAM;
        private MetroFramework.Controls.MetroTile Title_BRANDS;
        private MetroFramework.Controls.MetroTile Title_VIEW_SLIPS;
        private MetroFramework.Controls.MetroTile Title_INSTRUMENT;
        private MetroFramework.Controls.MetroTile Title_ASSIGNMENT_SLIP;
        private System.Windows.Forms.Panel content_panel;
        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroTile metroTile1;
        private MetroFramework.Controls.MetroTile Title_ASSIGNMENT_CLIENT;
        private MetroFramework.Controls.MetroTile SEND_TO_REPAIR;
        private MetroFramework.Controls.MetroTile logout;
        private MetroFramework.Controls.MetroTile user;
        private System.Windows.Forms.Label username_txt;
        private MetroFramework.Controls.MetroTile PERMISSION;
        private MetroFramework.Controls.MetroTile billing_btn;
        private MetroFramework.Controls.MetroTile setting_btn;
        private MetroFramework.Controls.MetroTile attendance_btn;
        private MetroFramework.Controls.MetroTile rating_btn;
        private MetroFramework.Controls.MetroTile employee_btn;
        private MetroFramework.Controls.MetroTile metroTile2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
    }
}

