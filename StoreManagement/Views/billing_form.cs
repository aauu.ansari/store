﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using StoreManagement.BS_SERVICES;
using StoreManagement.BS_SERVICES_BsBill;
using StoreManagement.Common;
using StoreManagement.Reports.data;
using StoreManagement.Reports.view;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class billing_form : Form
    {
        CommonHelper common = new CommonHelper();
        Messages messages = new Messages();
        BS_SERVICES_BsBill.IbsBillClient billService = new BS_SERVICES_BsBill.IbsBillClient();
        BS_SERVICES.IbsStoreClient storeService = new BS_SERVICES.IbsStoreClient();
        Roles userRoles = new Roles();
        int savedBillId = 0;
        int billDetails_id = 0;
        int billReceipts_id = 0;
        int bill_id = 0;
        bool searchBoxFilled = false;
        bool searchClientBoxFilled = false;
        bool ExistingProject = false;
        public billing_form()
        {
            InitializeComponent();
        }
        private void billing_form_Load(object sender, EventArgs e)
        {
            if (storeService.rolesUser(login.userCredentials.id, "billing_form") != null)
            {
                userRoles = storeService.rolesUser(login.userCredentials.id, "billing_form");
            }
            refresh();
            fillTests();

        }
        private void refresh()
        {
            cleare();
            FillClients();
            fillSites();
            newProject();
            isBillSaved = false;
            paymentMode.SelectedIndex = 0;
            specimen.SelectedIndex = 0;
        }
        private bool ValidationBill()
        {
            string[] param = {
                client_id.Text,
                lab_no.Text,
                project.Text,
                location.Text,
                specimen.Text,
        };
            return common.Validator(param);
        }
        private bool ValidationBillReceipts()
        {
            string[] param = {
                paymentMode.Text,
                amount.Text,
        };
            return common.Validator(param);
        }
        private bool ValidationBillDetails()
        {
            string[] param = {
                txt_test_description.Text,
                test_status.Text,
            txt_quantity.Text,
            txt_rate.Text,
        };
            return common.Validator(param);

        }
        private void cleare()
        {
            client_id.SelectedValue = 0;
            project.Text = "";
            project.SelectedValue = 0;
            location.Text = "";
            specimen.Text = "";
            lab_no.Text = "";
            txt_test_description.Text = "";
            txt_quantity.Text = "";
            txt_rate.Text = "";
            txt_pgst.Text = "";
            txt_amount.Text = "";
            billing_save.Enabled = true;
            billing_update.Enabled = false;
            edit_btn.Enabled = false;
            Delete_btn.Enabled = false;
            plus_btn.Enabled = true;
            clearReceipts();
            bilingList.Refresh();
            bilingList.DataSource = null;
            BillReceiptsList.Refresh();
            BillReceiptsList.DataSource = null;

        }
        private void clearReceipts()
        {
            paymentMode.Text = "";
            cheque.Text = "";
            bank.Text = "";
            details.Text = "";
            amount.Text = "";
            bill_save.Enabled = true;
            bill_update.Enabled = false;
        }
        bool isBillSaved = false;
        private void saveBill()
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_add == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (ValidationBill())
            {
                Bill bill = new Bill();
                bill.user_id = login.userCredentials.id;
                bill.client_id = Convert.ToInt32(client_id.SelectedValue);
                bill.contractor = "";
                bill.consultant = "";
                bill.invoice_no = common.getInvoiceNumber();
                bill.access_code = common.getAccessCode();
                bill.lab_no = lab_no.Text;
                bill.project = project.Text;
                bill.site_id = siteCreate();
                bill.bill_to = bill_to.Text;
                bill.bill_details = bill_details.Text;
                bill.order_date = order_date.Text;
                bill.location = location.Text;
                bill.specimen = specimen.Text;
                bill.created_at = DateTime.Now.ToShortDateString();
                savedBillId = billService.billSave(bill);
                billing_save.Enabled = false;
                billing_update.Enabled = true;
                isBillSaved = true;
            }
            else
            {
                MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        private void billing_save_Click(object sender, EventArgs e)
        {
            try
            {
                saveBill();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private int siteCreate()
        {
            if (!ExistingProject)
            {
                Site site = new BS_SERVICES.Site();
                site.user_id = login.userCredentials.id;
                site.name = project.Text;
                site.client_id = Convert.ToInt32(client_id.SelectedValue);
                site.site_address = location.Text;
                site.contact_person = "";
                site.purchased_order_no = "";
                site.quotation_ref = "";
                site.supervisor_id = 0;
                site.task = "";
                site.type = "Billing";
                site.mobilization_date = DateTime.Now.ToShortDateString();
                site.completion_date = DateTime.Now.ToShortDateString();
                site.created_at = DateTime.Now.ToShortDateString();
                return storeService.siteSave(site);
            }
            return Convert.ToInt32(project.SelectedValue);
        }
        private void billing_update_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (ValidationBill())
                {
                    Bill bill = new Bill();
                    bill.user_id = login.userCredentials.id;
                    bill.client_id = Convert.ToInt32(client_id.SelectedValue);
                    bill.contractor = "";// contractor.Text;
                    bill.consultant = "";//consultant.Text;
                    bill.project = project.Text;
                    bill.site_id = siteCreate();
                    bill.lab_no = lab_no.Text;
                    bill.bill_to = bill_to.Text;
                    bill.bill_details = bill_details.Text;
                    bill.order_no = order_no.Text;
                    bill.order_date = order_date.Text;
                    bill.location = location.Text;
                    bill.specimen = specimen.Text;
                    bill.updated_at = DateTime.Now.ToShortDateString();
                    bill.id = savedBillId;
                    billService.billUpdate(bill);
                    MessageBox.Show(messages.update, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void plus_btn_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_add == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (ValidationBillDetails() == true && ValidationBill() == true)
                {
                    if (isBillSaved == false)
                    {
                        saveBill();
                    }
                    BillDetails billDetails = new BillDetails();
                    billDetails.bill_id = savedBillId;
                    billDetails.description = txt_test_description.Text;
                    billDetails.test_status = test_status.Text;
                    billDetails.quantity = float.Parse(txt_quantity.Text);
                    billDetails.rate = float.Parse(txt_rate.Text);
                    billDetails.pgst = float.Parse(txt_pgst.Text);
                    billDetails.pgstRate = float.Parse(txt_pgstRate.Text);
                    billDetails.report_sending_date = report_sending_date.Text;
                    billService.billDetailsSave(billDetails);
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txt_test_description.Text = "";
                    txt_quantity.Text = "";
                    txt_rate.Text = "";
                    txt_amount.Text = "";
                    showBillDetails(savedBillId);
                    txt_quantity.Focus();

                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        private void bilingList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                ShowBillDetailsDataInTextbox(e.RowIndex);
                billing_save.Enabled = false;
                billing_update.Enabled = true;
                edit_btn.Enabled = true;
                Delete_btn.Enabled = true;
                plus_btn.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ShowBillDetailsDataInTextbox(int rowIndex)
        {
            if (rowIndex == -1) { return; }
            billDetails_id = Convert.ToInt32(bilingList.Rows[rowIndex].Cells["id"].Value.ToString());
            bill_id = Convert.ToInt32(bilingList.Rows[rowIndex].Cells["Bill_No"].Value.ToString());
            txt_test_description.Text = bilingList.Rows[rowIndex].Cells["Description"].Value.ToString();
            txt_quantity.Text = bilingList.Rows[rowIndex].Cells["Quantity"].Value.ToString();
            txt_rate.Text = bilingList.Rows[rowIndex].Cells["Rate"].Value.ToString();
            test_status.Text = bilingList.Rows[rowIndex].Cells["Status"].Value.ToString();
            txt_pgst.Text = bilingList.Rows[rowIndex].Cells["PGST"].Value.ToString();
            txt_amount.Text = bilingList.Rows[rowIndex].Cells["Amount"].Value.ToString();
            amount.Text = bilingList.Rows[rowIndex].Cells["Amount"].Value.ToString();
            report_sending_date.Text= bilingList.Rows[rowIndex].Cells["Report_Date"].Value.ToString();
            // showBill(bill_id);

        }
        private void ShowBillReceiptsDataInTextbox(int rowIndex)
        {
            if (rowIndex == -1) { return; }
            billReceipts_id = Convert.ToInt32(BillReceiptsList.Rows[rowIndex].Cells["receipt_id"].Value.ToString());
            paymentMode.Text = BillReceiptsList.Rows[rowIndex].Cells["Mode"].Value.ToString();
            cheque.Text = BillReceiptsList.Rows[rowIndex].Cells["cheque"].Value.ToString();
            bank.Text = BillReceiptsList.Rows[rowIndex].Cells["bank"].Value.ToString();
            details.Text = BillReceiptsList.Rows[rowIndex].Cells["details"].Value.ToString();
            tax_deducted.Text = BillReceiptsList.Rows[rowIndex].Cells["tax_deducted"].Value.ToString();
            amount.Text = BillReceiptsList.Rows[rowIndex].Cells["receipt_Amount"].Value.ToString();
            cashReceiveFrom.Text = BillReceiptsList.Rows[rowIndex].Cells["Person"].Value.ToString();
            txt_discount.Text = BillReceiptsList.Rows[rowIndex].Cells["discount"].Value.ToString();

        }
        private void showBill(int bill_id)
        {
            try
            {
                if (bill_id != 0)
                {
                    Bill bill = new BS_SERVICES_BsBill.Bill();
                    bill = billService.billRow(bill_id);
                    client_id.SelectedValue = bill.client_id;
                    lab_no.Text = bill.lab_no;
                    project.SelectedValue = bill.site_id;
                    location.Text = bill.location;
                    specimen.Text = bill.specimen;
                    bill_to.Text = bill.bill_to;
                    bill_details.Text = bill.bill_details;
                    order_no.Text = bill.order_no;
                    order_date.Text = bill.order_date;
                }
                else
                {
                    // MessageBox.Show("Please Select A Project Name", messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void showBillDetails(int bill_id)
        {
            try
            {
                if (bill_id != 0)
                {
                    bilingList.DataSource = null;
                    bilingList.DataSource = billService.billDetailsList_dt(bill_id);
                    int sum = 0;
                    for (int i = 0; i < bilingList.Rows.Count; ++i)
                    {
                        sum += Convert.ToInt32(bilingList.Rows[i].Cells[8].Value);
                    }
                    g_total.Text = sum.ToString();
                    bilingList.Columns[0].Visible = false;
                    bilingList.Columns[1].Visible = false;
                    receiptsBill(bill_id);
                }
                else
                {
                    return;
                }

            }
            catch (FaultException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void receiptsBill(int bill_id)
        {
            BillReceiptsList.DataSource = null;
            BillReceiptsList.DataSource = billService.billReceiptsList_dt(bill_id);
            BillReceiptsListProperties();
        }
        private void BillReceiptsListProperties()
        {
            BillReceiptsList.Columns[0].Visible = false;
            BillReceiptsList.Columns[1].Visible = false;
            BillReceiptsList.Columns[2].Width = 120;
            BillReceiptsList.Columns[3].Visible = false;
            BillReceiptsList.Columns[4].Visible = false;
            BillReceiptsList.Columns[5].Visible = false;
            BillReceiptsList.Columns[6].Width = 100;
            BillReceiptsList.Columns[7].Visible = false;
            BillReceiptsList.Columns[8].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            BillReceiptsList.Columns[9].Visible = false;
            BillReceiptsList.Columns[10].Visible = false;
        }
        private void fillSites()
        {
            DataTable site = storeService.dropdown("site",0);
            common.addNewRow(site);
            project.DataSource = site;
            project.DisplayMember = "name";
            project.ValueMember = "id";
        }
        private void fillBillingSites(int client_id=0)
        {
            DataTable site = storeService.dropdown("billing", client_id);
            common.addNewRow(site);
            search.DataSource = site;
            search.DisplayMember = "name";
            search.ValueMember = "id";
            searchBoxFilled = true;
        }
        private void fillTests()
        {            
            DataTable rateList = storeService.dropdown("rateList",0);
            DataRow dr = rateList.NewRow();
            dr["name"] = "Select Test";
            dr["price"] = 0;
            rateList.Rows.InsertAt(dr, 0);
            StringsText.DataSource = rateList; 
            StringsText.DisplayMember = "name";
            StringsText.ValueMember = "price";
        }
        private void FillClients()
        {
            DataTable clients_search = storeService.dropdown("clients", 0);
            common.addNewRow(clients_search);
            client_search.DataSource = clients_search;
            client_search.DisplayMember = "name";
            client_search.ValueMember = "id";

            searchClientBoxFilled = true;

            DataTable clients = storeService.dropdown("clients",0);
            common.addNewRow(clients);//
            client_id.DataSource = clients;
            client_id.DisplayMember = "name";
            client_id.ValueMember = "id";

           
        }
        private void lbl_new_Click(object sender, EventArgs e)
        {
            newProject();
        }
        private void lbl_existing_Click(object sender, EventArgs e)
        {
            existingProject();
        }
        private void existingProject()
        {
            ExistingProject = true;
            lbl_existing.ForeColor = System.Drawing.Color.Green;
            lbl_existing.Font = new Font(lbl_existing.Font, FontStyle.Bold);
            lbl_new.ForeColor = System.Drawing.Color.Black;
            lbl_new.Font = new Font(lbl_new.Font, FontStyle.Regular);
            project.DropDownStyle = ComboBoxStyle.DropDownList;
            project.DataSource = null;
            fillSites();
        }
        private void newProject()
        {
            ExistingProject = false;
            lbl_new.ForeColor = System.Drawing.Color.Green;
            lbl_new.Font = new Font(lbl_new.Font, FontStyle.Bold);
            lbl_existing.ForeColor = System.Drawing.Color.Black;
            lbl_existing.Font = new Font(lbl_existing.Font, FontStyle.Regular);
            project.DropDownStyle = ComboBoxStyle.DropDown;
            project.DataSource = null;
        }
        private void search_TextChanged(object sender, EventArgs e)
        {
            if (searchBoxFilled)
            {
                existingProject();
                showBill(Convert.ToInt32(search.SelectedValue));
                savedBillId = Convert.ToInt32(search.SelectedValue);
                isBillSaved = true;
                clearReceipts();
                showBillDetails(savedBillId);
                billing_save.Enabled = false;
                billing_update.Enabled = true;
                edit_btn.Enabled = false;
                Delete_btn.Enabled = false;
                plus_btn.Enabled = true;


            }

        }

        private void txt_quantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
        private void txt_rate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

        }
        private void Total()
        {
            if (txt_rate.Text != "" && txt_quantity.Text != "")
            {
                PGSTAmount();
                float amt = (float.Parse(txt_rate.Text) + ((float.Parse(txt_rate.Text) * float.Parse(txt_pgstRate.Text) / 100)));
                amt = amt * float.Parse(txt_quantity.Text);
                txt_amount.Text = amt.ToString();
            }
        }
        private void PGSTAmount()
        {
            if (txt_rate.Text != "" && txt_pgstRate.Text != "" && txt_quantity.Text != "")
            {
                float amt = (float.Parse(txt_rate.Text) * float.Parse(txt_pgstRate.Text) / 100);
                txt_pgst.Text = (amt * float.Parse(txt_quantity.Text)).ToString();
            }
        }
        private void txt_pgstRate_KeyUp(object sender, KeyEventArgs e)
        {

        }
        private void txt_quantity_KeyUp(object sender, KeyEventArgs e)
        {
            txt_amount.Text = "";
            Total();
        }
        private void txt_rate_KeyUp(object sender, KeyEventArgs e)
        {
            txt_amount.Text = "";
            Total();
        }
        private void txt_pgst_KeyUp(object sender, KeyEventArgs e)
        {
            txt_amount.Text = "";
            Total();
        }

        private void Delete_btn_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (MessageBox.Show("Are you sure! you want to delete this record", "Alert", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    if (ValidationBillDetails() && savedBillId != 0 && billDetails_id != 0)
                    {
                        billService.billDetailsDelete(billDetails_id);
                        MessageBox.Show(messages.delete, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txt_test_description.Text = "";
                        txt_quantity.Text = "";
                        txt_rate.Text = "";
                        txt_amount.Text = "";
                        showBillDetails(savedBillId);
                        txt_quantity.Focus();
                    }
                    else
                    {
                        MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void edit_btn_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (ValidationBillDetails() && savedBillId != 0 && billDetails_id != 0)
                {
                    BillDetails billDetails = new BillDetails();
                    billDetails.description = txt_test_description.Text;
                    billDetails.quantity = float.Parse(txt_quantity.Text);
                    billDetails.rate = float.Parse(txt_rate.Text);
                    billDetails.pgst = float.Parse(txt_pgst.Text);
                    billDetails.test_status = test_status.Text;
                    billDetails.pgstRate = float.Parse(txt_pgstRate.Text);
                    billDetails.id = billDetails_id;
                    billDetails.report_sending_date = report_sending_date.Text;
                    billService.billDetailsUpdate(billDetails);
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txt_test_description.Text = "";
                    txt_quantity.Text = "";
                    txt_rate.Text = "";
                    txt_amount.Text = "";
                    showBillDetails(savedBillId);
                    txt_quantity.Focus();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void refresh_btn_Click(object sender, EventArgs e)
        {
            refresh();
        }
        private void txt_pgst_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
        private void bill_save_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_add == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (ValidationBillReceipts() && savedBillId != 0)
                {
                    if (taxDeductionValidation()==true && tax_deducted.Text=="")
                    {
                        MessageBox.Show("Please Fill Tax Deducted Amount");
                        return;
                    }
                    BillReceipts billReceipts = new BillReceipts();
                    billReceipts.paymentMode = paymentMode.Text;
                    billReceipts.user_id = login.userCredentials.id;
                    billReceipts.bill_id = savedBillId;
                    billReceipts.cheque = cheque.Text;
                    billReceipts.details = details.Text;
                    billReceipts.tax_deducted =float.Parse(tax_deducted.Text);
                    billReceipts.discount= float.Parse(txt_discount.Text);
                    billReceipts.bank = bank.Text;
                    billReceipts.amount = float.Parse(amount.Text);
                    billReceipts.cashReceiveFrom = cashReceiveFrom.Text;
                    billReceipts.created_at = DateTime.Now.ToShortDateString();
                    billService.billReceiptsSave(billReceipts);
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    mailSending();
                    paymentMode.Text = "";
                    cheque.Text = "";
                    bank.Text = "";
                    details.Text = "";
                    amount.Text = "";
                    paymentMode.Focus();
                    ReportView();
                    receiptsBill(savedBillId);
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private bool mailSending(bool isReceipts = true)
        {
            try
            {
                using (var client = new System.Net.WebClient())
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                    if (isReceipts)
                    {
                        MessageBox.Show(storeService.SendMail(project.Text + "'s Amount Receipt", paymentMode.Text + " ( " + amount.Text + " ) Collected By " + login.bs_username, attetchmentPath), messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    return true;
                }
            }
            catch
            {
                MessageBox.Show(messages.InternetException, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        private void BillReceiptsList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                ShowBillReceiptsDataInTextbox(e.RowIndex);
                bill_save.Enabled = false;
                bill_update.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bill_update_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_add == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (ValidationBillReceipts() && savedBillId != 0 && billReceipts_id != 0)
                {
                    BillReceipts billReceipts = new BillReceipts();
                    billReceipts.user_id = login.userCredentials.id;
                    billReceipts.paymentMode = paymentMode.Text;
                    billReceipts.bill_id = savedBillId;
                    billReceipts.cheque = cheque.Text;
                    billReceipts.details = details.Text;
                    billReceipts.tax_deducted = float.Parse(tax_deducted.Text);
                    billReceipts.discount = float.Parse(txt_discount.Text);
                    billReceipts.bank = bank.Text;
                    billReceipts.amount = float.Parse(amount.Text);
                    billReceipts.id = billReceipts_id;
                    billReceipts.cashReceiveFrom = cashReceiveFrom.Text;
                    billReceipts.updated_at = DateTime.Now.ToShortDateString();
                    billService.billReceiptsUpdate(billReceipts);
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    paymentMode.Text = "";
                    cheque.Text = "";
                    bank.Text = "";
                    details.Text = "";
                    amount.Text = "";
                    paymentMode.Focus();
                    ReportView();
                    receiptsBill(savedBillId);
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
        }
        private void ReportView()
        {
            BillFORM bf = new BillFORM();
            if (savedBillId != 0)
            {
                bf.site_id = (savedBillId);
            }
            else
            {
                bf.site_id = (Convert.ToInt32(search.SelectedValue));
            }

            bf.Show();
        }
        private void receipts_btn_Click(object sender, EventArgs e)
        {
            ReportView();
        }
        private bool taxDeductionValidation()
        {
            if (paymentMode.Text == "Cash (Tax deducted)" ||
                paymentMode.Text == "Cheque (Tax deducted)" ||
                paymentMode.Text == "Online (Tax deducted)" ||
                paymentMode.Text == "Cash (Income tax deducted)" ||
                paymentMode.Text == "Cheque (Income tax deducted)" ||
                paymentMode.Text == "Online (Income tax deducted")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void paymentMode_TextChanged(object sender, EventArgs e)
        {
            if (taxDeductionValidation())
            {
                tax_deducted.Visible = true;
            }
            else
            {
               // tax_deducted.Visible = false;
            }
        }
        public string folderName = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\BS-Files";
        public void createDirectory(string p)
        {
            if (!Directory.Exists(p))
            {
                Directory.CreateDirectory(p);
            }

        }
        string attetchmentPath = "";
        private void Attetchments()
        {
            try
            {
                createDirectory(folderName);
                int site_id = Convert.ToInt32(search.SelectedValue);
                ReportDocument rdReport = new ReportDocument();
                var projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                var pth = Application.StartupPath + @"\Reports\BillCR.rpt";
                string filePath = Path.Combine(projectPath, @"Reports\crystalReport\BillCR.rpt");
                string fileName = "BillCR.rpt";
                FileInfo f = new FileInfo(fileName);
                string fullname = f.FullName;
                MessageBox.Show(fullname);
                rdReport.Load(pth);
                var billDS = new BillDS();
                Bill bill = new Bill();
                bill = billService.billRow(site_id);
                DataTable bill_dt = billDS.bill;
                if (bill != null)
                {
                    DataRow dr = bill_dt.NewRow();
                    dr[0] = bill.id;
                    dr[1] = bill.client_name;
                    dr[2] = bill.client_id;
                    dr[3] = bill.client_phone;
                    dr[4] = bill.invoice_no;
                    dr[5] = bill.lab_no;
                    dr[6] = bill.access_code;
                    dr[7] = bill.contractor;
                    dr[8] = bill.consultant;
                    dr[9] = bill.project;
                    dr[10] = bill.site_id;
                    dr[11] = bill.location;
                    dr[12] = bill.specimen;
                    dr[13] = bill.updated_at;
                    bill_dt.Rows.Add(dr);
                }
                rdReport.Database.Tables["bill"].SetDataSource(bill_dt);
                rdReport.Database.Tables["dicount_tbl"].SetDataSource(billService.billReceiptsDiscount_bill_dt(site_id));
                rdReport.Database.Tables["balances"].SetDataSource(billService.billReceiptsBalances_bill_dt(site_id));
                rdReport.Database.Tables["bill_details_tbl"].SetDataSource(billService.billDetailsList_dt(site_id));
                rdReport.Database.Tables["bill_receipts"].SetDataSource(billService.billReceiptsList_dt(site_id));
                ExportOptions exportOption;
                DiskFileDestinationOptions diskFileDestinationOptions = new DiskFileDestinationOptions();
                string filename = DateTime.Now.Ticks.ToString();
                diskFileDestinationOptions.DiskFileName = folderName + "\\" + search.Text + filename + ".pdf";
                attetchmentPath = diskFileDestinationOptions.DiskFileName;


                string[] result = Regex.Split(attetchmentPath, "C:");
                string str = System.Environment.MachineName + result[1];
                attetchmentPath = str;

                exportOption = rdReport.ExportOptions;
                {
                    exportOption.ExportDestinationType = ExportDestinationType.DiskFile;
                    exportOption.ExportFormatType = ExportFormatType.PortableDocFormat;
                    exportOption.ExportDestinationOptions = diskFileDestinationOptions;
                    exportOption.FormatOptions = new PdfRtfWordFormatOptions();

                }
                rdReport.Export();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        private void send_mail_btn_Click(object sender, EventArgs e)
        {

        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show(storeService.SendMail(project.Text + "'s Samples Are Collected", "Samples Are Collected By " + login.bs_username, attetchmentPath), messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void StringsText_KeyDown(object sender, KeyEventArgs e)
        {
            test_status.Text = "";
            if (e.KeyValue == 13)
            {
                txt_rate.Text = StringsText.SelectedValue != null ? StringsText.SelectedValue.ToString() : "0";
                txt_test_description.Text = StringsText.Text;
                StringsText.Text.Split(' ').ToList().ForEach(i => test_status.Text+=(i[0] + " "));
                txt_quantity.Text = "1";
                Total();
            }
        }

        private void tax_deducted_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txt_pgstRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txt_amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void client_search_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (searchClientBoxFilled) {
                fillBillingSites(Convert.ToInt32(client_search.SelectedValue));
            }
            
        }

        private void metroButton1_Click_1(object sender, EventArgs e)
        {
            SaleInvoiceForm bf = new SaleInvoiceForm();
            if (savedBillId != 0)
            {
                bf.site_id = (savedBillId);
            }
            else
            {
                bf.site_id = (Convert.ToInt32(search.SelectedValue));
            }

            bf.Show();
        }
    }
}
