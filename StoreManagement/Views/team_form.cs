﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class team_form : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        DataView dv;
        Roles userRoles = new Roles();
        public team_form()
        {
            InitializeComponent();
        }

        private void team_form_Load(object sender, EventArgs e)
        {
            if (service.rolesUser(login.userCredentials.id, "team_form") != null)
            {
                userRoles = service.rolesUser(login.userCredentials.id, "team_form");
            }
            refresh();
        }

        private void refresh()
        {
            clear();           
            dv = new DataView(service.teamList_dataTable());
            TeamList();
        }

        private void TeamList()
        {
            try
            {
                team_list.DataSource = dv;
                gridViewProperties();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridViewProperties()
        {
            if (dv != null)
            {
                team_list.Columns[0].Visible = false;
                team_list.Columns[1].Width = 100;
                team_list.Columns[2].Width = 100;
                team_list.Columns[3].Width = 250;
                team_list.Columns[4].Width = 200;
                team_list.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            
        }
        private void clear()
        {
            team_active.Checked = true;
            team_de_active.Checked = false;
            team_name.Text = null;
            team_role.Text = "Select One";
            team_search.Text = null;
            team_save.Enabled = true;
            team_update.Enabled = false;
            team_delete.Enabled = false;
        }

        private void team_add_new_Click(object sender, EventArgs e)
        {
            refresh();
        }
        private bool Validation()
        {
            string[] param = { team_name.Text, team_role.Text };
            return commonHelper.Validator(param);
        }
        private void team_save_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_add == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    Teams team = new Teams();
                    team.name = team_name.Text;
                    team.role = team_role.Text;
                    team.status = commonHelper.status(team_active.Checked, team_de_active.Checked);
                    team.created_at = DateTime.Now.ToShortDateString();
                    team.updated_at = DateTime.Now.ToShortDateString();
                    service.teamSave(team);
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }
        int brand_id = 0;
        private void brand_list_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ShowDataInTextbox(e.RowIndex);
            team_update.Enabled = true;
            team_delete.Enabled = true;
            team_save.Enabled = false;
        }

        private void brand_list_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        private void ShowDataInTextbox(int index)
        {
            try
            {
                if (index == -1) { return; }
                brand_id = Convert.ToInt32(team_list.Rows[index].Cells["id"].Value.ToString());
                team_name.Text = team_list.Rows[index].Cells["Name"].Value.ToString();
                team_role.Text = team_list.Rows[index].Cells["Role"].Value.ToString();
                if (team_list.Rows[index].Cells["Status"].Value.ToString() != "0")
                {
                    team_active.Checked = true;
                }
                else
                {
                    team_de_active.Checked = true;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void team_search_TextChanged(object sender, EventArgs e)
        {
            dv.RowFilter = "Name Like '%" + team_search.Text + "%'";
            team_list.DataSource = dv;
            gridViewProperties();
        }

        private void team_update_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    Teams team = new BS_SERVICES.Teams();
                    team.id = brand_id;
                    team.name = team_name.Text;
                    team.role = team_role.Text;
                    team.status = commonHelper.status(team_active.Checked, team_de_active.Checked);
                    team.updated_at = DateTime.Now.ToShortDateString();
                    service.teamUpdate(team);
                    MessageBox.Show(messages.update, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }

        private void team_delete_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_delete == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    Teams team = new BS_SERVICES.Teams();
                    team.id = brand_id;
                    team.status = 0;
                    team.updated_at = DateTime.Now.ToShortDateString();
                    service.teamDelete(team);
                    MessageBox.Show(messages.delete, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }
    }
}
