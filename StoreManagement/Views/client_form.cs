﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class client_form : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        string path = Application.StartupPath + "\\images\\Clients";
        string oldpath = "";
        string pathWithFileName = "";
        Roles userRoles = new Roles();
        DataView dv;       
        public client_form()
        {
            InitializeComponent();
        }
        private void client_form_Load(object sender, EventArgs e)
        {
            if (service.rolesUser(login.userCredentials.id, "client_form") != null)
            {
                userRoles = service.rolesUser(login.userCredentials.id, "client_form");
            }
            refresh();
        }
        private void refresh()
        {
            dv = new DataView(service.clientsList_dataTable());
            ClientsList();
            client_name.Text = "";
            client_contact.Text = "";
            client_address.Text = "";
            client_logo.Text = "";
            client_save.Enabled = true;
            client_update.Enabled = false;
            client_delete.Enabled = false;
            client_logo_box.ImageLocation = "";
            client_email.Text = "";
            client_type.SelectedIndex = 0;
        }

        private void ClientsList()
        {
            try
            {
                client_list.DataSource = dv;
                gridViewProperties();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool Validation()
        {
            string[] param = {
                client_name.Text,
                client_contact.Text,
                client_address.Text,
                client_email.Text
                //client_logo.Text
            };
            return commonHelper.Validator(param);
        }
        private void openFile(object sender, EventArgs e)
        {
            OpenFileDialog op1 = new OpenFileDialog();
            op1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            op1.ShowDialog();
            client_logo.Text = op1.FileName;
            pathWithFileName = op1.SafeFileName;
            client_logo_box.ImageLocation = op1.FileName;
            commonHelper.isFolderExist(path);
        }
        

        private void gridViewProperties()
        {
            client_list.Columns[0].Visible = false;
            client_list.Columns[1].Width = 100;
            client_list.Columns[2].Width = 200;
            client_list.Columns[3].Width = 100;
            client_list.Columns[4].Width = 200;
            client_list.Columns[5].Width = 200;
            client_list.Columns[6].Visible = false;
            client_list.Columns[7].Width = 70;
            client_list.Columns[8].Width = 120;
            client_list.Columns[9].Width = 120;
        }

        private void client_list_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ShowDataInTextbox(client_list.CurrentRow.Index);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        int client_id = 0;
        private void ShowDataInTextbox(int index)
        {
            if (index == -1) { return; }
            client_id = Convert.ToInt32(client_list.Rows[index].Cells["id"].Value.ToString());
            client_name.Text = client_list.Rows[index].Cells["Name"].Value.ToString();
            client_contact.Text = client_list.Rows[index].Cells["Contact"].Value.ToString();
            client_email.Text = client_list.Rows[index].Cells["Email"].Value.ToString();
            client_address.Text = client_list.Rows[index].Cells["Address"].Value.ToString();
            client_logo.Text = client_list.Rows[index].Cells["logo"].Value.ToString();
            oldpath= client_list.Rows[index].Cells["logo"].Value.ToString();
            client_ntn.Text = client_list.Rows[index].Cells["NTN"].Value.ToString();
            client_pra.Text = client_list.Rows[index].Cells["PRA"].Value.ToString();
            client_type.Text = client_list.Rows[index].Cells["Type"].Value.ToString();
            client_logo_box.ImageLocation = client_list.Rows[index].Cells["logo"].Value.ToString();
            if (client_list.Rows[index].Cells["Status"].Value.ToString() != "0")
            {
                client_active.Checked = true;
            }
            else
            {
                client_de_active.Checked = true;
            }
            client_save.Enabled = false;
            client_update.Enabled = true;
            client_delete.Enabled = true;
        }
        private void client_save_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_add == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    string distination = path + "\\" + DateTime.Now.Ticks.ToString() + "_" + pathWithFileName;
                    Clients clients = new BS_SERVICES.Clients();
                    clients.name = client_name.Text;
                    clients.contact = client_contact.Text;
                    clients.email = client_email.Text;
                    clients.NTN = client_ntn.Text;
                    clients.PRA = client_pra.Text;
                    clients.address = client_address.Text;
                    clients.logo = distination;
                    clients.created_at = DateTime.Now.ToShortDateString();
                    clients.type = client_type.Text;
                    service.clientsSave(clients);
                    if (pathWithFileName != "")
                    {
                        File.Copy(client_logo.Text, distination);
                    }
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }
        private void client_update_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    string distination = path + "\\" + DateTime.Now.Ticks.ToString() + "_" + pathWithFileName;
                    Clients clients = new BS_SERVICES.Clients();
                    clients.id = client_id;
                    clients.name = client_name.Text;
                    clients.email = client_email.Text;
                    clients.contact = client_contact.Text;
                    clients.NTN = client_ntn.Text;
                    clients.PRA = client_pra.Text;
                    clients.address = client_address.Text;                    
                    clients.updated_at = DateTime.Now.ToShortDateString();
                    clients.status = commonHelper.status(client_active.Checked, client_de_active.Checked);
                    clients.type = client_type.Text;
                    if (client_logo.Text != oldpath)
                    {
                        File.Copy(client_logo.Text, distination);
                        File.Delete(oldpath);
                        clients.logo = distination;
                    }
                    else
                    {
                        clients.logo = client_logo.Text;
                    }
                    service.clientsUpdate(clients);
                    MessageBox.Show(messages.update, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }

        private void client_delete_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_delete == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    string distination = path + "\\" + DateTime.Now.Ticks.ToString() + "_" + pathWithFileName;
                    Clients clients = new BS_SERVICES.Clients();
                    clients.updated_at = DateTime.Now.ToShortDateString();
                    clients.id = client_id;
                    service.clientsDelete(clients);
                    MessageBox.Show(messages.delete, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }

        private void client_name_TextChanged(object sender, EventArgs e)
        {
            searchGrid();            
        }

        private void searchGrid()
        {
            try
            {
                string search = "";
                if (client_name.Focused == true)
                {
                    search = "Name Like '%" + client_name.Text + "%'";
                }
                else if (client_contact.Focused == true)
                {
                    search = "Contact Like '%" + client_contact.Text + "%'";
                }
                else if (client_email.Focused == true)
                {
                    search = "Email Like '%" + client_email.Text + "%'";
                }
                else if (client_ntn.Focused == true)
                {
                    search = "NTN Like '%" + client_ntn.Text + "%'";
                }
                else if (client_pra.Focused == true)
                {
                    search = "PRA Like '%" + client_pra.Text + "%'";
                }
                else if (client_address.Focused == true)
                {
                    search = "Address Like '%" + client_address.Text + "%'";
                }
                dv.RowFilter = search;
                client_list.DataSource = dv;
                gridViewProperties();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }
    }
}
