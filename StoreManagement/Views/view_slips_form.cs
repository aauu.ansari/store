﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using StoreManagement.Reports.view;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class view_slips_form : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper common = new CommonHelper();
        Roles userRoles = new Roles();
        public view_slips_form()
        {
            InitializeComponent();
        }

        private void all_slips_Click(object sender, EventArgs e)
        {
            common.tabControler(splitContainer1.Panel2, "AssignmentFORM");
        }

        private void Instruments_Click(object sender, EventArgs e)
        {
            common.tabControler(splitContainer1.Panel2, "InstrumentsFORM");
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            common.tabControler(splitContainer1.Panel2, "SiteInstrumentsDamageFORM");
        }

        private void send_to_repair_instruments_Click(object sender, EventArgs e)
        {
            common.tabControler(splitContainer1.Panel2, "RepairsFORM");
        }

        private void view_slips_form_Load(object sender, EventArgs e)
        {
            if (service.rolesUser(login.userCredentials.id, "view_slips_form") != null)
            {
                userRoles = service.rolesUser(login.userCredentials.id, "view_slips_form");
            }
        }

        private void clients_btn_Click(object sender, EventArgs e)
        {
            common.tabControler(splitContainer1.Panel2, "ClientsFORM");
        }

        private void Teams_btn_Click(object sender, EventArgs e)
        {
            common.tabControler(splitContainer1.Panel2, "TeamsFORM");
        }

        private void Users_btn_Click(object sender, EventArgs e)
        {
            common.tabControler(splitContainer1.Panel2, "UsersFORM");
        }

        private void Brands_btn_Click(object sender, EventArgs e) 
        {
            common.tabControler(splitContainer1.Panel2, "BrandsFORM");
        }

        private void client_tax_Click(object sender, EventArgs e)
        {
            common.tabControler(splitContainer1.Panel2, "TAX_FORM");
        }
    }
}
