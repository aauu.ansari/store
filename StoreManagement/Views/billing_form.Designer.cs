﻿namespace StoreManagement.Views
{
    partial class billing_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.search = new System.Windows.Forms.ComboBox();
            this.client_search = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.receipts_btn = new MetroFramework.Controls.MetroButton();
            this.label29 = new System.Windows.Forms.Label();
            this.g_total = new System.Windows.Forms.Label();
            this.refresh_btn = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.location = new MetroFramework.Controls.MetroTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.bilingList = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.order_date = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.order_no = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbl_new = new System.Windows.Forms.Label();
            this.txt_test_description = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.billing_update = new MetroFramework.Controls.MetroButton();
            this.billing_save = new MetroFramework.Controls.MetroButton();
            this.txt_discount = new MetroFramework.Controls.MetroTextBox();
            this.amount = new MetroFramework.Controls.MetroTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.bank = new MetroFramework.Controls.MetroTextBox();
            this.test_status = new MetroFramework.Controls.MetroTextBox();
            this.StringsText = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_pgstRate = new MetroFramework.Controls.MetroTextBox();
            this.details = new MetroFramework.Controls.MetroTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txt_amount = new MetroFramework.Controls.MetroTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.specimen = new System.Windows.Forms.ComboBox();
            this.lbl_existing = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.client_id = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.paymentMode = new System.Windows.Forms.ComboBox();
            this.txt_pgst = new MetroFramework.Controls.MetroTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tax_deducted = new MetroFramework.Controls.MetroTextBox();
            this.cheque = new MetroFramework.Controls.MetroTextBox();
            this.txt_rate = new MetroFramework.Controls.MetroTextBox();
            this.project = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.lab_no = new MetroFramework.Controls.MetroTextBox();
            this.txt_quantity = new MetroFramework.Controls.MetroTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.report_sending_date = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.bill_details = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.bill_update = new MetroFramework.Controls.MetroButton();
            this.bill_save = new MetroFramework.Controls.MetroButton();
            this.cashReceiveFrom = new MetroFramework.Controls.MetroTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.bill_to = new System.Windows.Forms.RichTextBox();
            this.BillReceiptsList = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.Delete_btn = new MetroFramework.Controls.MetroButton();
            this.plus_btn = new MetroFramework.Controls.MetroButton();
            this.edit_btn = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bilingList)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BillReceiptsList)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // search
            // 
            this.search.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search.FormattingEnabled = true;
            this.search.Location = new System.Drawing.Point(516, 39);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(309, 23);
            this.search.TabIndex = 1;
            this.search.TextChanged += new System.EventHandler(this.search_TextChanged);
            // 
            // client_search
            // 
            this.client_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_search.FormattingEnabled = true;
            this.client_search.Location = new System.Drawing.Point(219, 39);
            this.client_search.Name = "client_search";
            this.client_search.Size = new System.Drawing.Size(291, 23);
            this.client_search.TabIndex = 0;
            this.client_search.SelectedIndexChanged += new System.EventHandler(this.client_search_SelectedIndexChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(216, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(87, 15);
            this.label27.TabIndex = 2;
            this.label27.Text = "SEARCH CLIENT";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(513, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(103, 15);
            this.label14.TabIndex = 0;
            this.label14.Text = "SEARCH PROJECTS";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Impact", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "BILLING && RECEIPTS ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // receipts_btn
            // 
            this.receipts_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Receipt;
            this.receipts_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.receipts_btn.Location = new System.Drawing.Point(831, 39);
            this.receipts_btn.Name = "receipts_btn";
            this.receipts_btn.Size = new System.Drawing.Size(22, 26);
            this.receipts_btn.TabIndex = 2;
            this.receipts_btn.UseSelectable = true;
            this.receipts_btn.Click += new System.EventHandler(this.receipts_btn_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(949, 41);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(68, 23);
            this.label29.TabIndex = 5;
            this.label29.Text = "TOTAL :";
            // 
            // g_total
            // 
            this.g_total.AutoSize = true;
            this.g_total.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.g_total.Location = new System.Drawing.Point(1019, 41);
            this.g_total.Name = "g_total";
            this.g_total.Size = new System.Drawing.Size(16, 23);
            this.g_total.TabIndex = 6;
            this.g_total.Text = "-";
            // 
            // refresh_btn
            // 
            this.refresh_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Refresh_2_icon;
            this.refresh_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.refresh_btn.Location = new System.Drawing.Point(859, 39);
            this.refresh_btn.Name = "refresh_btn";
            this.refresh_btn.Size = new System.Drawing.Size(22, 26);
            this.refresh_btn.TabIndex = 3;
            this.refresh_btn.UseSelectable = true;
            this.refresh_btn.Click += new System.EventHandler(this.refresh_btn_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.BackgroundImage = global::StoreManagement.Properties.Resources.Receipt;
            this.metroButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.metroButton1.Location = new System.Drawing.Point(887, 39);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(22, 26);
            this.metroButton1.TabIndex = 4;
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(256, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "CLIENT";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 15);
            this.label5.TabIndex = 5;
            this.label5.Text = "PROJECT";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(250, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "LOCATION";
            // 
            // location
            // 
            // 
            // 
            // 
            this.location.CustomButton.Image = null;
            this.location.CustomButton.Location = new System.Drawing.Point(180, 1);
            this.location.CustomButton.Name = "";
            this.location.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.location.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.location.CustomButton.TabIndex = 1;
            this.location.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.location.CustomButton.UseSelectable = true;
            this.location.CustomButton.Visible = false;
            this.location.Lines = new string[0];
            this.location.Location = new System.Drawing.Point(253, 189);
            this.location.MaxLength = 32767;
            this.location.Name = "location";
            this.location.PasswordChar = '\0';
            this.location.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.location.SelectedText = "";
            this.location.SelectionLength = 0;
            this.location.SelectionStart = 0;
            this.location.ShortcutsEnabled = true;
            this.location.Size = new System.Drawing.Size(202, 23);
            this.location.TabIndex = 6;
            this.location.UseSelectable = true;
            this.location.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.location.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(457, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "SPECIMEN";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.bilingList, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 519F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1423, 807);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // bilingList
            // 
            this.bilingList.BackgroundColor = System.Drawing.Color.White;
            this.bilingList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.bilingList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bilingList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bilingList.Location = new System.Drawing.Point(3, 522);
            this.bilingList.Name = "bilingList";
            this.bilingList.Size = new System.Drawing.Size(1417, 282);
            this.bilingList.TabIndex = 1;
            this.bilingList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.bilingList_CellClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.order_date);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.order_no);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.lbl_new);
            this.panel1.Controls.Add(this.txt_test_description);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.txt_discount);
            this.panel1.Controls.Add(this.amount);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.bank);
            this.panel1.Controls.Add(this.test_status);
            this.panel1.Controls.Add(this.StringsText);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txt_pgstRate);
            this.panel1.Controls.Add(this.details);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.txt_amount);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.specimen);
            this.panel1.Controls.Add(this.lbl_existing);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.client_id);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.paymentMode);
            this.panel1.Controls.Add(this.txt_pgst);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.tax_deducted);
            this.panel1.Controls.Add(this.cheque);
            this.panel1.Controls.Add(this.txt_rate);
            this.panel1.Controls.Add(this.project);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.lab_no);
            this.panel1.Controls.Add(this.txt_quantity);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.report_sending_date);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.bill_details);
            this.panel1.Controls.Add(this.tableLayoutPanel5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.location);
            this.panel1.Controls.Add(this.cashReceiveFrom);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.bill_to);
            this.panel1.Controls.Add(this.BillReceiptsList);
            this.panel1.Controls.Add(this.tableLayoutPanel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1417, 513);
            this.panel1.TabIndex = 0;
            // 
            // order_date
            // 
            this.order_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.order_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.order_date.Location = new System.Drawing.Point(593, 253);
            this.order_date.Name = "order_date";
            this.order_date.Size = new System.Drawing.Size(93, 23);
            this.order_date.TabIndex = 10;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(592, 218);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(124, 30);
            this.label16.TabIndex = 52;
            this.label16.Text = "ORDER DATE \r\n(For Sales Tax Invoice)";
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(20, 82);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1376, 2);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.receipts_btn);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.metroButton1);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.search);
            this.panel2.Controls.Add(this.g_total);
            this.panel2.Controls.Add(this.refresh_btn);
            this.panel2.Controls.Add(this.client_search);
            this.panel2.Location = new System.Drawing.Point(9, 9);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1387, 67);
            this.panel2.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Stencil", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(18, 91);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(331, 26);
            this.label17.TabIndex = 50;
            this.label17.Text = "PROJECT && TEST DESCRIPTION";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Stencil", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(717, 91);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(123, 26);
            this.label31.TabIndex = 21;
            this.label31.Text = "PAYMENTS";
            // 
            // order_no
            // 
            this.order_no.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.order_no.Location = new System.Drawing.Point(460, 253);
            this.order_no.Name = "order_no";
            this.order_no.Size = new System.Drawing.Size(126, 24);
            this.order_no.TabIndex = 9;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(321, 359);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(47, 15);
            this.label24.TabIndex = 20;
            this.label24.Text = "STATUS:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(848, 289);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(63, 15);
            this.label26.TabIndex = 47;
            this.label26.Text = "DISCOUNT";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(719, 248);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(48, 15);
            this.label19.TabIndex = 34;
            this.label19.Text = "DETAILS";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(719, 210);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 15);
            this.label18.TabIndex = 33;
            this.label18.Text = "BANK";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(457, 215);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(124, 30);
            this.label15.TabIndex = 40;
            this.label15.Text = "ORDER NO. \r\n(For Sales Tax Invoice)";
            // 
            // lbl_new
            // 
            this.lbl_new.AutoSize = true;
            this.lbl_new.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_new.Location = new System.Drawing.Point(174, 129);
            this.lbl_new.Name = "lbl_new";
            this.lbl_new.Size = new System.Drawing.Size(30, 15);
            this.lbl_new.TabIndex = 23;
            this.lbl_new.Text = "New";
            // 
            // txt_test_description
            // 
            this.txt_test_description.Location = new System.Drawing.Point(269, 436);
            this.txt_test_description.MaxLength = 500;
            this.txt_test_description.Name = "txt_test_description";
            this.txt_test_description.Size = new System.Drawing.Size(264, 49);
            this.txt_test_description.TabIndex = 20;
            this.txt_test_description.Text = "";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.billing_update, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.billing_save, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(519, 181);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(167, 31);
            this.tableLayoutPanel2.TabIndex = 8;
            // 
            // billing_update
            // 
            this.billing_update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.billing_update.Enabled = false;
            this.billing_update.Location = new System.Drawing.Point(83, 0);
            this.billing_update.Margin = new System.Windows.Forms.Padding(0);
            this.billing_update.Name = "billing_update";
            this.billing_update.Size = new System.Drawing.Size(84, 31);
            this.billing_update.TabIndex = 1;
            this.billing_update.Text = "Edit";
            this.billing_update.UseSelectable = true;
            this.billing_update.Click += new System.EventHandler(this.billing_update_Click);
            // 
            // billing_save
            // 
            this.billing_save.Dock = System.Windows.Forms.DockStyle.Fill;
            this.billing_save.Location = new System.Drawing.Point(0, 0);
            this.billing_save.Margin = new System.Windows.Forms.Padding(0);
            this.billing_save.Name = "billing_save";
            this.billing_save.Size = new System.Drawing.Size(83, 31);
            this.billing_save.TabIndex = 0;
            this.billing_save.Text = "Add";
            this.billing_save.UseSelectable = true;
            this.billing_save.Visible = false;
            this.billing_save.Click += new System.EventHandler(this.billing_save_Click);
            // 
            // txt_discount
            // 
            // 
            // 
            // 
            this.txt_discount.CustomButton.Image = null;
            this.txt_discount.CustomButton.Location = new System.Drawing.Point(73, 1);
            this.txt_discount.CustomButton.Name = "";
            this.txt_discount.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txt_discount.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_discount.CustomButton.TabIndex = 1;
            this.txt_discount.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_discount.CustomButton.UseSelectable = true;
            this.txt_discount.CustomButton.Visible = false;
            this.txt_discount.Lines = new string[] {
        "0"};
            this.txt_discount.Location = new System.Drawing.Point(851, 304);
            this.txt_discount.MaxLength = 32767;
            this.txt_discount.Name = "txt_discount";
            this.txt_discount.PasswordChar = '\0';
            this.txt_discount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_discount.SelectedText = "";
            this.txt_discount.SelectionLength = 0;
            this.txt_discount.SelectionStart = 0;
            this.txt_discount.ShortcutsEnabled = true;
            this.txt_discount.Size = new System.Drawing.Size(95, 23);
            this.txt_discount.TabIndex = 27;
            this.txt_discount.Text = "0";
            this.txt_discount.UseSelectable = true;
            this.txt_discount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_discount.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // amount
            // 
            // 
            // 
            // 
            this.amount.CustomButton.Image = null;
            this.amount.CustomButton.Location = new System.Drawing.Point(202, 1);
            this.amount.CustomButton.Name = "";
            this.amount.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.amount.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.amount.CustomButton.TabIndex = 1;
            this.amount.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.amount.CustomButton.UseSelectable = true;
            this.amount.CustomButton.Visible = false;
            this.amount.Lines = new string[0];
            this.amount.Location = new System.Drawing.Point(722, 349);
            this.amount.MaxLength = 32767;
            this.amount.Name = "amount";
            this.amount.PasswordChar = '\0';
            this.amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.amount.SelectedText = "";
            this.amount.SelectionLength = 0;
            this.amount.SelectionStart = 0;
            this.amount.ShortcutsEnabled = true;
            this.amount.Size = new System.Drawing.Size(224, 23);
            this.amount.TabIndex = 28;
            this.amount.UseSelectable = true;
            this.amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.amount.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.amount_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 329);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 15);
            this.label9.TabIndex = 10;
            this.label9.Text = "TEST:";
            // 
            // bank
            // 
            // 
            // 
            // 
            this.bank.CustomButton.Image = null;
            this.bank.CustomButton.Location = new System.Drawing.Point(202, 1);
            this.bank.CustomButton.Name = "";
            this.bank.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.bank.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.bank.CustomButton.TabIndex = 1;
            this.bank.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.bank.CustomButton.UseSelectable = true;
            this.bank.CustomButton.Visible = false;
            this.bank.Lines = new string[0];
            this.bank.Location = new System.Drawing.Point(722, 225);
            this.bank.MaxLength = 32767;
            this.bank.Name = "bank";
            this.bank.PasswordChar = '\0';
            this.bank.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.bank.SelectedText = "";
            this.bank.SelectionLength = 0;
            this.bank.SelectionStart = 0;
            this.bank.ShortcutsEnabled = true;
            this.bank.Size = new System.Drawing.Size(224, 23);
            this.bank.TabIndex = 24;
            this.bank.UseSelectable = true;
            this.bank.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.bank.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // test_status
            // 
            // 
            // 
            // 
            this.test_status.CustomButton.Image = null;
            this.test_status.CustomButton.Location = new System.Drawing.Point(100, 1);
            this.test_status.CustomButton.Name = "";
            this.test_status.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.test_status.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.test_status.CustomButton.TabIndex = 1;
            this.test_status.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.test_status.CustomButton.UseSelectable = true;
            this.test_status.CustomButton.Visible = false;
            this.test_status.Lines = new string[0];
            this.test_status.Location = new System.Drawing.Point(411, 358);
            this.test_status.MaxLength = 32767;
            this.test_status.Name = "test_status";
            this.test_status.PasswordChar = '\0';
            this.test_status.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.test_status.SelectedText = "";
            this.test_status.SelectionLength = 0;
            this.test_status.SelectionStart = 0;
            this.test_status.ShortcutsEnabled = true;
            this.test_status.Size = new System.Drawing.Size(122, 23);
            this.test_status.TabIndex = 18;
            this.test_status.UseSelectable = true;
            this.test_status.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.test_status.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // StringsText
            // 
            this.StringsText.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StringsText.FormattingEnabled = true;
            this.StringsText.Location = new System.Drawing.Point(111, 329);
            this.StringsText.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.StringsText.Name = "StringsText";
            this.StringsText.Size = new System.Drawing.Size(422, 27);
            this.StringsText.TabIndex = 12;
            this.StringsText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StringsText_KeyDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(20, 462);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 15);
            this.label13.TabIndex = 18;
            this.label13.Text = "TOTAL:";
            // 
            // txt_pgstRate
            // 
            // 
            // 
            // 
            this.txt_pgstRate.CustomButton.Image = null;
            this.txt_pgstRate.CustomButton.Location = new System.Drawing.Point(37, 1);
            this.txt_pgstRate.CustomButton.Name = "";
            this.txt_pgstRate.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txt_pgstRate.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_pgstRate.CustomButton.TabIndex = 1;
            this.txt_pgstRate.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_pgstRate.CustomButton.UseSelectable = true;
            this.txt_pgstRate.CustomButton.Visible = false;
            this.txt_pgstRate.Lines = new string[] {
        "16"};
            this.txt_pgstRate.Location = new System.Drawing.Point(111, 358);
            this.txt_pgstRate.MaxLength = 32767;
            this.txt_pgstRate.Name = "txt_pgstRate";
            this.txt_pgstRate.PasswordChar = '\0';
            this.txt_pgstRate.ReadOnly = true;
            this.txt_pgstRate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_pgstRate.SelectedText = "";
            this.txt_pgstRate.SelectionLength = 0;
            this.txt_pgstRate.SelectionStart = 0;
            this.txt_pgstRate.ShortcutsEnabled = true;
            this.txt_pgstRate.Size = new System.Drawing.Size(59, 23);
            this.txt_pgstRate.TabIndex = 13;
            this.txt_pgstRate.Text = "16";
            this.txt_pgstRate.UseSelectable = true;
            this.txt_pgstRate.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_pgstRate.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txt_pgstRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_pgstRate_KeyPress);
            this.txt_pgstRate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_pgstRate_KeyUp);
            // 
            // details
            // 
            // 
            // 
            // 
            this.details.CustomButton.Image = null;
            this.details.CustomButton.Location = new System.Drawing.Point(202, 1);
            this.details.CustomButton.Name = "";
            this.details.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.details.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.details.CustomButton.TabIndex = 1;
            this.details.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.details.CustomButton.UseSelectable = true;
            this.details.CustomButton.Visible = false;
            this.details.Lines = new string[0];
            this.details.Location = new System.Drawing.Point(722, 263);
            this.details.MaxLength = 32767;
            this.details.Name = "details";
            this.details.PasswordChar = '\0';
            this.details.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.details.SelectedText = "";
            this.details.SelectionLength = 0;
            this.details.SelectionStart = 0;
            this.details.ShortcutsEnabled = true;
            this.details.Size = new System.Drawing.Size(224, 23);
            this.details.TabIndex = 25;
            this.details.UseSelectable = true;
            this.details.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.details.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(719, 333);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(56, 15);
            this.label20.TabIndex = 36;
            this.label20.Text = "AMOUNT";
            // 
            // txt_amount
            // 
            // 
            // 
            // 
            this.txt_amount.CustomButton.Image = null;
            this.txt_amount.CustomButton.Location = new System.Drawing.Point(117, 1);
            this.txt_amount.CustomButton.Name = "";
            this.txt_amount.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txt_amount.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_amount.CustomButton.TabIndex = 1;
            this.txt_amount.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_amount.CustomButton.UseSelectable = true;
            this.txt_amount.CustomButton.Visible = false;
            this.txt_amount.Enabled = false;
            this.txt_amount.Lines = new string[0];
            this.txt_amount.Location = new System.Drawing.Point(111, 462);
            this.txt_amount.MaxLength = 32767;
            this.txt_amount.Name = "txt_amount";
            this.txt_amount.PasswordChar = '\0';
            this.txt_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_amount.SelectedText = "";
            this.txt_amount.SelectionLength = 0;
            this.txt_amount.SelectionStart = 0;
            this.txt_amount.ShortcutsEnabled = true;
            this.txt_amount.Size = new System.Drawing.Size(139, 23);
            this.txt_amount.TabIndex = 17;
            this.txt_amount.UseSelectable = true;
            this.txt_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_amount.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txt_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(719, 129);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 15);
            this.label8.TabIndex = 31;
            this.label8.Text = "PAYMENT METHOD";
            // 
            // specimen
            // 
            this.specimen.DropDownWidth = 170;
            this.specimen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.specimen.FormattingEnabled = true;
            this.specimen.Items.AddRange(new object[] {
            "Concrete Cylinder",
            "Concrete Cubes",
            "Concrete Cores",
            "Bricks",
            "Tuff Tiles",
            "Bricks / Tuff Tiles",
            "Fine Aggregate",
            "Ferro Scanning of RCC Struccture",
            "Aggregate Base Course",
            "Geotechnical Investigation",
            "RCC Structure",
            "Asphalt Sampling",
            "Concrete Mix Design",
            "Soil Investigation",
            "Solid Concrete Block",
            "Extraction Of Concrete Block",
            "Extraction Of Concrete Cores",
            "Sub Base & Base Course",
            "Sub Base Sample",
            "Structural Analysis",
            "Non-Distructive Testing",
            "Compaction Test",
            "Ferro Scanning",
            "Rebound Hammer",
            "Ultrasonic Pulse Velosity Test"});
            this.specimen.Location = new System.Drawing.Point(460, 145);
            this.specimen.Name = "specimen";
            this.specimen.Size = new System.Drawing.Size(226, 23);
            this.specimen.TabIndex = 4;
            // 
            // lbl_existing
            // 
            this.lbl_existing.AutoSize = true;
            this.lbl_existing.Enabled = false;
            this.lbl_existing.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_existing.Location = new System.Drawing.Point(204, 129);
            this.lbl_existing.Name = "lbl_existing";
            this.lbl_existing.Size = new System.Drawing.Size(49, 15);
            this.lbl_existing.TabIndex = 24;
            this.lbl_existing.Text = "Existing";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 436);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 15);
            this.label12.TabIndex = 16;
            this.label12.Text = "PGST AMOUNT:";
            // 
            // client_id
            // 
            this.client_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.client_id.DropDownWidth = 270;
            this.client_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_id.FormattingEnabled = true;
            this.client_id.Location = new System.Drawing.Point(253, 145);
            this.client_id.Name = "client_id";
            this.client_id.Size = new System.Drawing.Size(202, 23);
            this.client_id.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(719, 289);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(113, 15);
            this.label25.TabIndex = 44;
            this.label25.Text = "TAX DEDUCTED AMT";
            // 
            // paymentMode
            // 
            this.paymentMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.paymentMode.DropDownWidth = 200;
            this.paymentMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paymentMode.FormattingEnabled = true;
            this.paymentMode.Items.AddRange(new object[] {
            "Cash",
            "Cheque",
            "Online",
            "Cash (Tax deducted)",
            "Cheque (Tax deducted)",
            "Online (Tax deducted)",
            "Cash (Income tax deducted)",
            "Cheque (Income tax deducted)",
            "Online (Income tax deducted)",
            "Rebate",
            "Baddebts"});
            this.paymentMode.Location = new System.Drawing.Point(722, 145);
            this.paymentMode.Name = "paymentMode";
            this.paymentMode.Size = new System.Drawing.Size(224, 23);
            this.paymentMode.TabIndex = 22;
            this.paymentMode.TextChanged += new System.EventHandler(this.paymentMode_TextChanged);
            // 
            // txt_pgst
            // 
            // 
            // 
            // 
            this.txt_pgst.CustomButton.Image = null;
            this.txt_pgst.CustomButton.Location = new System.Drawing.Point(117, 1);
            this.txt_pgst.CustomButton.Name = "";
            this.txt_pgst.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txt_pgst.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_pgst.CustomButton.TabIndex = 1;
            this.txt_pgst.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_pgst.CustomButton.UseSelectable = true;
            this.txt_pgst.CustomButton.Visible = false;
            this.txt_pgst.Lines = new string[0];
            this.txt_pgst.Location = new System.Drawing.Point(111, 436);
            this.txt_pgst.MaxLength = 32767;
            this.txt_pgst.Name = "txt_pgst";
            this.txt_pgst.PasswordChar = '\0';
            this.txt_pgst.ReadOnly = true;
            this.txt_pgst.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_pgst.SelectedText = "";
            this.txt_pgst.SelectionLength = 0;
            this.txt_pgst.SelectionStart = 0;
            this.txt_pgst.ShortcutsEnabled = true;
            this.txt_pgst.Size = new System.Drawing.Size(139, 23);
            this.txt_pgst.TabIndex = 16;
            this.txt_pgst.UseSelectable = true;
            this.txt_pgst.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_pgst.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txt_pgst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_pgst_KeyPress);
            this.txt_pgst.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_pgst_KeyUp);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(250, 215);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(192, 15);
            this.label30.TabIndex = 39;
            this.label30.Text = "JOB DETAILS (For Sales Tax Invoice)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 173);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 15);
            this.label3.TabIndex = 25;
            this.label3.Text = "LAB NUMBER";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 410);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 15);
            this.label11.TabIndex = 14;
            this.label11.Text = "RATE:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(20, 357);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 15);
            this.label23.TabIndex = 31;
            this.label23.Text = "PGST %:";
            // 
            // tax_deducted
            // 
            // 
            // 
            // 
            this.tax_deducted.CustomButton.Image = null;
            this.tax_deducted.CustomButton.Location = new System.Drawing.Point(73, 1);
            this.tax_deducted.CustomButton.Name = "";
            this.tax_deducted.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tax_deducted.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tax_deducted.CustomButton.TabIndex = 1;
            this.tax_deducted.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tax_deducted.CustomButton.UseSelectable = true;
            this.tax_deducted.CustomButton.Visible = false;
            this.tax_deducted.Lines = new string[] {
        "0"};
            this.tax_deducted.Location = new System.Drawing.Point(722, 305);
            this.tax_deducted.MaxLength = 32767;
            this.tax_deducted.Name = "tax_deducted";
            this.tax_deducted.PasswordChar = '\0';
            this.tax_deducted.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tax_deducted.SelectedText = "";
            this.tax_deducted.SelectionLength = 0;
            this.tax_deducted.SelectionStart = 0;
            this.tax_deducted.ShortcutsEnabled = true;
            this.tax_deducted.Size = new System.Drawing.Size(95, 23);
            this.tax_deducted.TabIndex = 26;
            this.tax_deducted.Text = "0";
            this.tax_deducted.UseSelectable = true;
            this.tax_deducted.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tax_deducted.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tax_deducted.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tax_deducted_KeyPress);
            // 
            // cheque
            // 
            // 
            // 
            // 
            this.cheque.CustomButton.Image = null;
            this.cheque.CustomButton.Location = new System.Drawing.Point(202, 1);
            this.cheque.CustomButton.Name = "";
            this.cheque.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.cheque.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.cheque.CustomButton.TabIndex = 1;
            this.cheque.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.cheque.CustomButton.UseSelectable = true;
            this.cheque.CustomButton.Visible = false;
            this.cheque.Lines = new string[0];
            this.cheque.Location = new System.Drawing.Point(722, 186);
            this.cheque.MaxLength = 32767;
            this.cheque.Name = "cheque";
            this.cheque.PasswordChar = '\0';
            this.cheque.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.cheque.SelectedText = "";
            this.cheque.SelectionLength = 0;
            this.cheque.SelectionStart = 0;
            this.cheque.ShortcutsEnabled = true;
            this.cheque.Size = new System.Drawing.Size(224, 23);
            this.cheque.TabIndex = 23;
            this.cheque.UseSelectable = true;
            this.cheque.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.cheque.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txt_rate
            // 
            // 
            // 
            // 
            this.txt_rate.CustomButton.Image = null;
            this.txt_rate.CustomButton.Location = new System.Drawing.Point(117, 1);
            this.txt_rate.CustomButton.Name = "";
            this.txt_rate.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txt_rate.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_rate.CustomButton.TabIndex = 1;
            this.txt_rate.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_rate.CustomButton.UseSelectable = true;
            this.txt_rate.CustomButton.Visible = false;
            this.txt_rate.Lines = new string[0];
            this.txt_rate.Location = new System.Drawing.Point(111, 410);
            this.txt_rate.MaxLength = 32767;
            this.txt_rate.Name = "txt_rate";
            this.txt_rate.PasswordChar = '\0';
            this.txt_rate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_rate.SelectedText = "";
            this.txt_rate.SelectionLength = 0;
            this.txt_rate.SelectionStart = 0;
            this.txt_rate.ShortcutsEnabled = true;
            this.txt_rate.Size = new System.Drawing.Size(139, 23);
            this.txt_rate.TabIndex = 15;
            this.txt_rate.UseSelectable = true;
            this.txt_rate.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_rate.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txt_rate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_rate_KeyPress);
            this.txt_rate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_rate_KeyUp);
            // 
            // project
            // 
            this.project.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.project.FormattingEnabled = true;
            this.project.Location = new System.Drawing.Point(23, 145);
            this.project.Name = "project";
            this.project.Size = new System.Drawing.Size(224, 23);
            this.project.TabIndex = 2;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(20, 215);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(145, 15);
            this.label28.TabIndex = 38;
            this.label28.Text = "TO, (For Sales Tax Invoice)";
            // 
            // lab_no
            // 
            // 
            // 
            // 
            this.lab_no.CustomButton.Image = null;
            this.lab_no.CustomButton.Location = new System.Drawing.Point(202, 1);
            this.lab_no.CustomButton.Name = "";
            this.lab_no.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.lab_no.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lab_no.CustomButton.TabIndex = 1;
            this.lab_no.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lab_no.CustomButton.UseSelectable = true;
            this.lab_no.CustomButton.Visible = false;
            this.lab_no.Lines = new string[0];
            this.lab_no.Location = new System.Drawing.Point(23, 189);
            this.lab_no.MaxLength = 32767;
            this.lab_no.Name = "lab_no";
            this.lab_no.PasswordChar = '\0';
            this.lab_no.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lab_no.SelectedText = "";
            this.lab_no.SelectionLength = 0;
            this.lab_no.SelectionStart = 0;
            this.lab_no.ShortcutsEnabled = true;
            this.lab_no.Size = new System.Drawing.Size(224, 23);
            this.lab_no.TabIndex = 5;
            this.lab_no.UseSelectable = true;
            this.lab_no.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lab_no.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txt_quantity
            // 
            // 
            // 
            // 
            this.txt_quantity.CustomButton.Image = null;
            this.txt_quantity.CustomButton.Location = new System.Drawing.Point(117, 1);
            this.txt_quantity.CustomButton.Name = "";
            this.txt_quantity.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txt_quantity.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txt_quantity.CustomButton.TabIndex = 1;
            this.txt_quantity.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txt_quantity.CustomButton.UseSelectable = true;
            this.txt_quantity.CustomButton.Visible = false;
            this.txt_quantity.Lines = new string[0];
            this.txt_quantity.Location = new System.Drawing.Point(111, 384);
            this.txt_quantity.MaxLength = 32767;
            this.txt_quantity.Name = "txt_quantity";
            this.txt_quantity.PasswordChar = '\0';
            this.txt_quantity.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_quantity.SelectedText = "";
            this.txt_quantity.SelectionLength = 0;
            this.txt_quantity.SelectionStart = 0;
            this.txt_quantity.ShortcutsEnabled = true;
            this.txt_quantity.Size = new System.Drawing.Size(139, 23);
            this.txt_quantity.TabIndex = 14;
            this.txt_quantity.UseSelectable = true;
            this.txt_quantity.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txt_quantity.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txt_quantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_quantity_KeyPress);
            this.txt_quantity.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_quantity_KeyUp);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(719, 375);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(117, 15);
            this.label22.TabIndex = 42;
            this.label22.Text = "CASH RECEIVE FROM";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(719, 171);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(114, 15);
            this.label21.TabIndex = 40;
            this.label21.Text = "CHEQUE / ACCOUNT";
            // 
            // report_sending_date
            // 
            this.report_sending_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.report_sending_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.report_sending_date.Location = new System.Drawing.Point(411, 386);
            this.report_sending_date.Name = "report_sending_date";
            this.report_sending_date.Size = new System.Drawing.Size(122, 23);
            this.report_sending_date.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(321, 390);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 15);
            this.label4.TabIndex = 22;
            this.label4.Text = "REPORT DATE:";
            // 
            // bill_details
            // 
            this.bill_details.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bill_details.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bill_details.Location = new System.Drawing.Point(253, 231);
            this.bill_details.MaxLength = 500;
            this.bill_details.Name = "bill_details";
            this.bill_details.Size = new System.Drawing.Size(202, 46);
            this.bill_details.TabIndex = 8;
            this.bill_details.Text = "";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.bill_update, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.bill_save, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(722, 416);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(224, 33);
            this.tableLayoutPanel5.TabIndex = 30;
            // 
            // bill_update
            // 
            this.bill_update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bill_update.Enabled = false;
            this.bill_update.Location = new System.Drawing.Point(115, 3);
            this.bill_update.Name = "bill_update";
            this.bill_update.Size = new System.Drawing.Size(106, 27);
            this.bill_update.TabIndex = 1;
            this.bill_update.Text = "Edit";
            this.bill_update.UseSelectable = true;
            this.bill_update.Click += new System.EventHandler(this.bill_update_Click);
            // 
            // bill_save
            // 
            this.bill_save.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bill_save.Location = new System.Drawing.Point(3, 3);
            this.bill_save.Name = "bill_save";
            this.bill_save.Size = new System.Drawing.Size(106, 27);
            this.bill_save.TabIndex = 0;
            this.bill_save.Text = "Add";
            this.bill_save.UseSelectable = true;
            this.bill_save.Click += new System.EventHandler(this.bill_save_Click);
            // 
            // cashReceiveFrom
            // 
            // 
            // 
            // 
            this.cashReceiveFrom.CustomButton.Image = null;
            this.cashReceiveFrom.CustomButton.Location = new System.Drawing.Point(202, 1);
            this.cashReceiveFrom.CustomButton.Name = "";
            this.cashReceiveFrom.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.cashReceiveFrom.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.cashReceiveFrom.CustomButton.TabIndex = 1;
            this.cashReceiveFrom.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.cashReceiveFrom.CustomButton.UseSelectable = true;
            this.cashReceiveFrom.CustomButton.Visible = false;
            this.cashReceiveFrom.Lines = new string[0];
            this.cashReceiveFrom.Location = new System.Drawing.Point(722, 391);
            this.cashReceiveFrom.MaxLength = 32767;
            this.cashReceiveFrom.Name = "cashReceiveFrom";
            this.cashReceiveFrom.PasswordChar = '\0';
            this.cashReceiveFrom.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.cashReceiveFrom.SelectedText = "";
            this.cashReceiveFrom.SelectionLength = 0;
            this.cashReceiveFrom.SelectionStart = 0;
            this.cashReceiveFrom.ShortcutsEnabled = true;
            this.cashReceiveFrom.Size = new System.Drawing.Size(224, 23);
            this.cashReceiveFrom.TabIndex = 29;
            this.cashReceiveFrom.UseSelectable = true;
            this.cashReceiveFrom.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.cashReceiveFrom.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(20, 300);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(665, 2);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(20, 384);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 15);
            this.label10.TabIndex = 12;
            this.label10.Text = "QUANTITY:";
            // 
            // bill_to
            // 
            this.bill_to.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bill_to.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bill_to.Location = new System.Drawing.Point(22, 231);
            this.bill_to.MaxLength = 500;
            this.bill_to.Name = "bill_to";
            this.bill_to.Size = new System.Drawing.Size(225, 46);
            this.bill_to.TabIndex = 7;
            this.bill_to.Text = "";
            // 
            // BillReceiptsList
            // 
            this.BillReceiptsList.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.BillReceiptsList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BillReceiptsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BillReceiptsList.Location = new System.Drawing.Point(963, 140);
            this.BillReceiptsList.Name = "BillReceiptsList";
            this.BillReceiptsList.Size = new System.Drawing.Size(349, 307);
            this.BillReceiptsList.TabIndex = 31;
            this.BillReceiptsList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.BillReceiptsList_CellClick);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.Delete_btn, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.plus_btn, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.edit_btn, 0, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(564, 329);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(122, 156);
            this.tableLayoutPanel3.TabIndex = 24;
            // 
            // Delete_btn
            // 
            this.Delete_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Delete_btn.Location = new System.Drawing.Point(0, 104);
            this.Delete_btn.Margin = new System.Windows.Forms.Padding(0);
            this.Delete_btn.Name = "Delete_btn";
            this.Delete_btn.Size = new System.Drawing.Size(122, 52);
            this.Delete_btn.TabIndex = 2;
            this.Delete_btn.Text = "Delete";
            this.Delete_btn.UseSelectable = true;
            this.Delete_btn.Click += new System.EventHandler(this.Delete_btn_Click);
            // 
            // plus_btn
            // 
            this.plus_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.plus_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plus_btn.Location = new System.Drawing.Point(0, 0);
            this.plus_btn.Margin = new System.Windows.Forms.Padding(0);
            this.plus_btn.Name = "plus_btn";
            this.plus_btn.Size = new System.Drawing.Size(122, 52);
            this.plus_btn.TabIndex = 0;
            this.plus_btn.Text = "Add";
            this.plus_btn.UseSelectable = true;
            this.plus_btn.Click += new System.EventHandler(this.plus_btn_Click);
            // 
            // edit_btn
            // 
            this.edit_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.edit_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.edit_btn.Enabled = false;
            this.edit_btn.Location = new System.Drawing.Point(0, 52);
            this.edit_btn.Margin = new System.Windows.Forms.Padding(0);
            this.edit_btn.Name = "edit_btn";
            this.edit_btn.Size = new System.Drawing.Size(122, 52);
            this.edit_btn.TabIndex = 1;
            this.edit_btn.Text = "Edit";
            this.edit_btn.UseSelectable = true;
            this.edit_btn.Click += new System.EventHandler(this.edit_btn_Click);
            // 
            // billing_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(0, 650);
            this.AutoScrollMinSize = new System.Drawing.Size(1300, 0);
            this.ClientSize = new System.Drawing.Size(1423, 807);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "billing_form";
            this.Text = "billing_form";
            this.Load += new System.EventHandler(this.billing_form_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bilingList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BillReceiptsList)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private MetroFramework.Controls.MetroTextBox location;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label13;
        private MetroFramework.Controls.MetroTextBox txt_amount;
        private System.Windows.Forms.Label label12;
        private MetroFramework.Controls.MetroTextBox txt_pgst;
        private System.Windows.Forms.Label label11;
        private MetroFramework.Controls.MetroTextBox txt_rate;
        private System.Windows.Forms.Label label10;
        private MetroFramework.Controls.MetroTextBox txt_quantity;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox txt_test_description;
        private MetroFramework.Controls.MetroButton plus_btn;
        private MetroFramework.Controls.MetroButton edit_btn;
        private System.Windows.Forms.ComboBox project;
        private System.Windows.Forms.ComboBox client_id;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroButton billing_update;
        private MetroFramework.Controls.MetroButton billing_save;
        private System.Windows.Forms.Label lbl_existing;
        private System.Windows.Forms.Label lbl_new;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MetroFramework.Controls.MetroButton refresh_btn;
        private MetroFramework.Controls.MetroButton receipts_btn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private MetroFramework.Controls.MetroButton bill_update;
        private MetroFramework.Controls.MetroButton bill_save;
        private System.Windows.Forms.Label label21;
        private MetroFramework.Controls.MetroTextBox cheque;
        private System.Windows.Forms.ComboBox paymentMode;
        private System.Windows.Forms.Label label20;
        private MetroFramework.Controls.MetroTextBox amount;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private MetroFramework.Controls.MetroTextBox bank;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView BillReceiptsList;
        private System.Windows.Forms.Label label22;
        private MetroFramework.Controls.MetroTextBox cashReceiveFrom;
        private MetroFramework.Controls.MetroTextBox txt_pgstRate;
        private System.Windows.Forms.Label label23;
        private MetroFramework.Controls.MetroTextBox lab_no;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker report_sending_date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox StringsText;
        private System.Windows.Forms.DataGridView bilingList;
        private System.Windows.Forms.GroupBox groupBox3;
        private MetroFramework.Controls.MetroButton Delete_btn;
        private System.Windows.Forms.Label label25;
        private MetroFramework.Controls.MetroTextBox tax_deducted;
        private MetroFramework.Controls.MetroTextBox details;
        private System.Windows.Forms.Label label26;
        private MetroFramework.Controls.MetroTextBox txt_discount;
        private MetroFramework.Controls.MetroButton metroButton1;
        private System.Windows.Forms.Label g_total;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.RichTextBox bill_details;
        private System.Windows.Forms.RichTextBox bill_to;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox client_search;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox search;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox specimen;
        private MetroFramework.Controls.MetroTextBox test_status;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox order_no;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker order_date;
    }
}