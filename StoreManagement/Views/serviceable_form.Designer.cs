﻿namespace StoreManagement.Views
{
    partial class serviceable_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(serviceable_form));
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.instrument_search = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.repair_instrument_id = new MetroFramework.Controls.MetroTextBox();
            this.repair_save = new MetroFramework.Controls.MetroButton();
            this.instrument_list = new System.Windows.Forms.DataGridView();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.repair_description = new System.Windows.Forms.RichTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.repair_shop = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.repair_person = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.repair_code = new MetroFramework.Controls.MetroTextBox();
            this.return_date = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.instrument_list)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(9, 13);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(104, 19);
            this.metroLabel2.TabIndex = 16;
            this.metroLabel2.Text = "Category Name";
            // 
            // instrument_search
            // 
            // 
            // 
            // 
            this.instrument_search.CustomButton.Image = null;
            this.instrument_search.CustomButton.Location = new System.Drawing.Point(291, 1);
            this.instrument_search.CustomButton.Name = "";
            this.instrument_search.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.instrument_search.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.instrument_search.CustomButton.TabIndex = 1;
            this.instrument_search.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.instrument_search.CustomButton.UseSelectable = true;
            this.instrument_search.CustomButton.Visible = false;
            this.instrument_search.Lines = new string[0];
            this.instrument_search.Location = new System.Drawing.Point(9, 35);
            this.instrument_search.MaxLength = 32767;
            this.instrument_search.Name = "instrument_search";
            this.instrument_search.PasswordChar = '\0';
            this.instrument_search.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.instrument_search.SelectedText = "";
            this.instrument_search.SelectionLength = 0;
            this.instrument_search.SelectionStart = 0;
            this.instrument_search.ShortcutsEnabled = true;
            this.instrument_search.Size = new System.Drawing.Size(313, 23);
            this.instrument_search.TabIndex = 3;
            this.instrument_search.UseSelectable = true;
            this.instrument_search.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.instrument_search.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.instrument_search.TextChanged += new System.EventHandler(this.category_search_TextChanged);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(11, 101);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(45, 19);
            this.metroLabel1.TabIndex = 14;
            this.metroLabel1.Text = "Name";
            // 
            // repair_instrument_id
            // 
            // 
            // 
            // 
            this.repair_instrument_id.CustomButton.Image = null;
            this.repair_instrument_id.CustomButton.Location = new System.Drawing.Point(231, 1);
            this.repair_instrument_id.CustomButton.Name = "";
            this.repair_instrument_id.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.repair_instrument_id.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.repair_instrument_id.CustomButton.TabIndex = 1;
            this.repair_instrument_id.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.repair_instrument_id.CustomButton.UseSelectable = true;
            this.repair_instrument_id.CustomButton.Visible = false;
            this.repair_instrument_id.Lines = new string[0];
            this.repair_instrument_id.Location = new System.Drawing.Point(10, 123);
            this.repair_instrument_id.MaxLength = 32767;
            this.repair_instrument_id.Name = "repair_instrument_id";
            this.repair_instrument_id.PasswordChar = '\0';
            this.repair_instrument_id.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.repair_instrument_id.SelectedText = "";
            this.repair_instrument_id.SelectionLength = 0;
            this.repair_instrument_id.SelectionStart = 0;
            this.repair_instrument_id.ShortcutsEnabled = true;
            this.repair_instrument_id.Size = new System.Drawing.Size(253, 23);
            this.repair_instrument_id.TabIndex = 7;
            this.repair_instrument_id.UseSelectable = true;
            this.repair_instrument_id.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.repair_instrument_id.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // repair_save
            // 
            this.repair_save.Dock = System.Windows.Forms.DockStyle.Fill;
            this.repair_save.Location = new System.Drawing.Point(3, 3);
            this.repair_save.Name = "repair_save";
            this.repair_save.Size = new System.Drawing.Size(245, 35);
            this.repair_save.TabIndex = 12;
            this.repair_save.Text = "SAVE";
            this.repair_save.UseSelectable = true;
            this.repair_save.Click += new System.EventHandler(this.repair_save_Click);
            // 
            // instrument_list
            // 
            this.instrument_list.BackgroundColor = System.Drawing.Color.White;
            this.instrument_list.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.instrument_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.instrument_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.instrument_list.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.instrument_list.Location = new System.Drawing.Point(3, 87);
            this.instrument_list.Name = "instrument_list";
            this.instrument_list.Size = new System.Drawing.Size(749, 379);
            this.instrument_list.TabIndex = 4;
            this.instrument_list.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.category_list_CellClick);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.LightGray;
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1143, 64);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = resources.GetString("groupBox6.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(4305, 31);
            this.label2.TabIndex = 0;
            this.label2.Text = resources.GetString("label2.Text");
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 64);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Size = new System.Drawing.Size(1143, 469);
            this.splitContainer1.SplitterDistance = 755;
            this.splitContainer1.TabIndex = 74;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.instrument_list, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(755, 469);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.instrument_search);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(749, 78);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FloralWhite;
            this.groupBox2.Controls.Add(this.return_date);
            this.groupBox2.Controls.Add(this.metroLabel7);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Controls.Add(this.repair_description);
            this.groupBox2.Controls.Add(this.metroLabel6);
            this.groupBox2.Controls.Add(this.repair_shop);
            this.groupBox2.Controls.Add(this.metroLabel5);
            this.groupBox2.Controls.Add(this.repair_person);
            this.groupBox2.Controls.Add(this.metroLabel4);
            this.groupBox2.Controls.Add(this.metroLabel3);
            this.groupBox2.Controls.Add(this.repair_instrument_id);
            this.groupBox2.Controls.Add(this.repair_code);
            this.groupBox2.Controls.Add(this.metroLabel1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(384, 469);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(11, 245);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(135, 19);
            this.metroLabel7.TabIndex = 85;
            this.metroLabel7.Text = "Expected Return Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(358, 21);
            this.label1.TabIndex = 5;
            this.label1.Text = "Send Serviceable Instruments To Repair";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.repair_save, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(11, 419);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(251, 41);
            this.tableLayoutPanel2.TabIndex = 83;
            // 
            // repair_description
            // 
            this.repair_description.Location = new System.Drawing.Point(10, 316);
            this.repair_description.Name = "repair_description";
            this.repair_description.Size = new System.Drawing.Size(252, 96);
            this.repair_description.TabIndex = 11;
            this.repair_description.Text = "";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(10, 293);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(74, 19);
            this.metroLabel6.TabIndex = 81;
            this.metroLabel6.Text = "Description";
            // 
            // repair_shop
            // 
            // 
            // 
            // 
            this.repair_shop.CustomButton.Image = null;
            this.repair_shop.CustomButton.Location = new System.Drawing.Point(231, 1);
            this.repair_shop.CustomButton.Name = "";
            this.repair_shop.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.repair_shop.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.repair_shop.CustomButton.TabIndex = 1;
            this.repair_shop.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.repair_shop.CustomButton.UseSelectable = true;
            this.repair_shop.CustomButton.Visible = false;
            this.repair_shop.Lines = new string[0];
            this.repair_shop.Location = new System.Drawing.Point(9, 219);
            this.repair_shop.MaxLength = 32767;
            this.repair_shop.Name = "repair_shop";
            this.repair_shop.PasswordChar = '\0';
            this.repair_shop.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.repair_shop.SelectedText = "";
            this.repair_shop.SelectionLength = 0;
            this.repair_shop.SelectionStart = 0;
            this.repair_shop.ShortcutsEnabled = true;
            this.repair_shop.Size = new System.Drawing.Size(253, 23);
            this.repair_shop.TabIndex = 9;
            this.repair_shop.UseSelectable = true;
            this.repair_shop.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.repair_shop.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(10, 197);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(39, 19);
            this.metroLabel5.TabIndex = 79;
            this.metroLabel5.Text = "Shop";
            // 
            // repair_person
            // 
            // 
            // 
            // 
            this.repair_person.CustomButton.Image = null;
            this.repair_person.CustomButton.Location = new System.Drawing.Point(231, 1);
            this.repair_person.CustomButton.Name = "";
            this.repair_person.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.repair_person.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.repair_person.CustomButton.TabIndex = 1;
            this.repair_person.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.repair_person.CustomButton.UseSelectable = true;
            this.repair_person.CustomButton.Visible = false;
            this.repair_person.Lines = new string[0];
            this.repair_person.Location = new System.Drawing.Point(10, 171);
            this.repair_person.MaxLength = 32767;
            this.repair_person.Name = "repair_person";
            this.repair_person.PasswordChar = '\0';
            this.repair_person.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.repair_person.SelectedText = "";
            this.repair_person.SelectionLength = 0;
            this.repair_person.SelectionStart = 0;
            this.repair_person.ShortcutsEnabled = true;
            this.repair_person.Size = new System.Drawing.Size(253, 23);
            this.repair_person.TabIndex = 8;
            this.repair_person.UseSelectable = true;
            this.repair_person.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.repair_person.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(11, 149);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(48, 19);
            this.metroLabel4.TabIndex = 77;
            this.metroLabel4.Text = "Person";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(11, 53);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(41, 19);
            this.metroLabel3.TabIndex = 75;
            this.metroLabel3.Text = "Code";
            // 
            // repair_code
            // 
            // 
            // 
            // 
            this.repair_code.CustomButton.Image = null;
            this.repair_code.CustomButton.Location = new System.Drawing.Point(104, 1);
            this.repair_code.CustomButton.Name = "";
            this.repair_code.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.repair_code.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.repair_code.CustomButton.TabIndex = 1;
            this.repair_code.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.repair_code.CustomButton.UseSelectable = true;
            this.repair_code.CustomButton.Visible = false;
            this.repair_code.Lines = new string[0];
            this.repair_code.Location = new System.Drawing.Point(10, 75);
            this.repair_code.MaxLength = 32767;
            this.repair_code.Name = "repair_code";
            this.repair_code.PasswordChar = '\0';
            this.repair_code.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.repair_code.SelectedText = "";
            this.repair_code.SelectionLength = 0;
            this.repair_code.SelectionStart = 0;
            this.repair_code.ShortcutsEnabled = true;
            this.repair_code.Size = new System.Drawing.Size(126, 23);
            this.repair_code.TabIndex = 6;
            this.repair_code.UseSelectable = true;
            this.repair_code.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.repair_code.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // return_date
            // 
            this.return_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.return_date.Location = new System.Drawing.Point(10, 267);
            this.return_date.Name = "return_date";
            this.return_date.Size = new System.Drawing.Size(253, 20);
            this.return_date.TabIndex = 10;
            // 
            // serviceable_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 533);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.groupBox6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "serviceable_form";
            this.Text = "category_form";
            this.Load += new System.EventHandler(this.serviceable_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.instrument_list)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox instrument_search;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox repair_instrument_id;
        private MetroFramework.Controls.MetroButton repair_save;
        private System.Windows.Forms.DataGridView instrument_list;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox repair_code;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox repair_description;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox repair_shop;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox repair_person;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private System.Windows.Forms.DateTimePicker return_date;
    }
}