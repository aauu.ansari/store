﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class settings_form : Form
    {
        CommonHelper common = new CommonHelper();
        Messages messages = new Messages();
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        EmailAddress emailAddresss = new EmailAddress();
        public settings_form()
        {
            InitializeComponent();
        }

        private void settings_form_Load(object sender, EventArgs e)
        {
            emailAddresss = service.emailAddressRow();
            txt_email.Text = emailAddresss.email;
            txt_password.Text = emailAddresss.password;
        }

        private void email_btn_Click(object sender, EventArgs e)
        {
            try
            {
                EmailAddress emailAddress = new EmailAddress();
                emailAddress.email = txt_email.Text;
                emailAddress.password = txt_password.Text;
                service.save(emailAddress);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

    }
}
