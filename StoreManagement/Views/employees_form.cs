﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class employees_form : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        DataView dv;
        public employees_form()
        {
            InitializeComponent();
        }
        private void employees_form_Load(object sender, EventArgs e)
        {
            refresh();
        }
        private void gridViewProperties()
        {
            employees_list.Columns[0].Visible = false;
            employees_list.Columns[1].Width = 100;
            employees_list.Columns[2].Width = 100;
            employees_list.Columns[3].Width = 100;
            employees_list.Columns[4].Width = 100;
            employees_list.Columns[5].Width = 100;
            employees_list.Columns[6].Width = 100;
            employees_list.Columns[7].Width = 100;
            employees_list.Columns[8].Width = 100;
        }
        private void employeesList()
        {
            try
            {
                dv = new DataView(service.employee_dataTable(attendanceSite()));
                employees_list.DataSource = dv;
                gridViewProperties();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private int attendanceSite()
        {
            if (attendanceSite_id.Text== "Office Attendance") {
                return 0;
            }
            else{
                return 1;
            }
        }
        private void refresh()
        {
           
            attendanceSite_id.SelectedIndex = 0;
            employeesList();
            employees_name.Text = "";
            employees_phone.Text = "";
            employees_cnic.Text = "";
            employees_address.Text = "";
            employees_roles.Text = "";
            employees_email.Text = "";
            employees_status.Text = "";
            employees_save.Enabled = true;
            employees_update.Enabled = false;
        }
        private bool Validation()
        {
            string[] param = {
                employees_name.Text,
                employees_phone.Text,
                employees_cnic.Text,
                employees_address.Text,
                employees_roles.Text,
                employees_email.Text,
                employees_status.Text,
            };
            return commonHelper.Validator(param);
        }
        private void employees_save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validation())
                {
                    Employee employee = new BS_SERVICES.Employee();
                    employee.name = employees_name.Text;
                    employee.phone = employees_phone.Text;
                    employee.cnic = employees_cnic.Text;
                    employee.address = employees_address.Text;
                    employee.roles = employees_roles.Text;
                    employee.email = employees_email.Text;
                    employee.status = Convert.ToInt32(employees_status.SelectedValue);
                    employee.created_at = DateTime.Now.ToShortDateString();
                    employee.updated_at = DateTime.Now.ToShortDateString();
                    employee.attendanceSite_id = attendanceSite();
                    employee.department = department.Text;
                    service.employeeSave(employee);
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }
        
        private void employees_list_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void employees_list_Click(object sender, EventArgs e)
        {
            try
            {
                ShowDataInTextbox(employees_list.CurrentRow.Index);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        int emp_id = 0;
        private void ShowDataInTextbox(int index)
        {
            if (index == -1) { return; }
            emp_id = Convert.ToInt32(employees_list.Rows[index].Cells["id"].Value.ToString());
            employees_name.Text = employees_list.Rows[index].Cells["Name"].Value.ToString();
            employees_phone.Text = employees_list.Rows[index].Cells["Phone"].Value.ToString();
            employees_email.Text = employees_list.Rows[index].Cells["Email"].Value.ToString();
            employees_cnic.Text = employees_list.Rows[index].Cells["CNIC"].Value.ToString();
            employees_roles.Text = employees_list.Rows[index].Cells["Roles"].Value.ToString();
            employees_status.Text = employees_list.Rows[index].Cells["Status"].Value.ToString();
            employees_address.Text = employees_list.Rows[index].Cells["Address"].Value.ToString();
            department.Text = employees_list.Rows[index].Cells["department"].Value.ToString();
            employees_save.Enabled = false;
            employees_update.Enabled = true;
        }

        private void employees_update_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validation())
                {
                    Employee employee = new BS_SERVICES.Employee();
                    employee.id = emp_id;
                    employee.name = employees_name.Text;
                    employee.phone = employees_phone.Text;
                    employee.cnic = employees_cnic.Text;
                    employee.address = employees_address.Text;
                    employee.roles = employees_roles.Text;
                    employee.email = employees_email.Text;
                    employee.status = Convert.ToInt32(employees_status.Text);
                    employee.created_at = DateTime.Now.ToShortDateString();
                    employee.updated_at = DateTime.Now.ToShortDateString();
                    employee.department = department.Text;
                    employee.attendanceSite_id = attendanceSite();
                    service.employeeUpdate(employee);
                    MessageBox.Show(messages.update, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }

        private void employees_refresh_Click(object sender, EventArgs e)
        {
            refresh();
        }
        
        private void attendanceSite_id_TextChanged(object sender, EventArgs e)
        {
            employeesList();
        }

        private void employees_name_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dv.RowFilter = "Name Like '%" + employees_name.Text + "%'";
                employees_list.DataSource = dv;
                gridViewProperties();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
