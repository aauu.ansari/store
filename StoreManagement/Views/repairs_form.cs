﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class repairs_form : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        Roles userRoles = new Roles();
        DataView dv;
        public repairs_form()
        {
            InitializeComponent();
        }
        private bool Validation()
        {
            string[] param = {
                repair_receive_description.Text,
                instrument_status.Text
            };
            return commonHelper.Validator(param);
        }
        int instrument_id = 0;
        int repair_id = 0;
        private void repair_save_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_add == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    Repairs repairs = new Repairs();
                    repairs.id= repair_id;
                    repairs.person = repair_person.Text;
                    repairs.receive_description = repair_receive_description.Text;
                    repairs.updated_at= DateTime.Now.ToShortDateString();
                    service.repairsUpdateStatus(repairs);
                    Instruments instruments = new BS_SERVICES.Instruments();
                    instruments.id = instrument_id;
                    instruments.status = instrument_status.Text;
                    instruments.updated_at = DateTime.Now.ToShortDateString();
                    service.instrumentsUpdateStatus(instruments, true);
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }
        private void refresh()
        {
            dv = new DataView(service.repairsList_dt());
            InstrumentList();
            repair_person.Text = "";
            repair_description.Text = "";
            repair_shop.Text = "";
        }
        private void InstrumentList()
        {
            try
            {
                dv.RowFilter = "Name Like '%" + instrument_search.Text + "%'";
                instrument_list.DataSource = dv;
                gridViewProperties();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void gridViewProperties()
        {
            instrument_list.Columns[0].Visible = false;
            instrument_list.Columns[1].Visible = false;
            instrument_list.Columns[2].Width = 100;
            instrument_list.Columns[3].Width = 200;
            instrument_list.Columns[4].Width = 120;
            instrument_list.Columns[5].Width = 120;
            instrument_list.Columns[6].Width = 200;
            instrument_list.Columns[7].Width = 200;
            instrument_list.Columns[8].Width = 120;
        }

        private void repairs_form_Load(object sender, EventArgs e)
        {
            if (service.rolesUser(login.userCredentials.id, "repairs_form") != null)
            {
                userRoles = service.rolesUser(login.userCredentials.id, "repairs_form");
            }
            refresh();
        }

        private void instrument_search_TextChanged(object sender, EventArgs e)
        {
            InstrumentList();
        }

        private void instrument_list_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ShowDataInTextbox(instrument_list.CurrentRow.Index);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void ShowDataInTextbox(int index)
        {
            repair_id = Convert.ToInt32(instrument_list.Rows[index].Cells["id"].Value.ToString());
            instrument_id = Convert.ToInt32(instrument_list.Rows[index].Cells["instrument_id"].Value.ToString());
            repair_instrument_id.Text = instrument_list.Rows[index].Cells["Name"].Value.ToString();
            repair_description.Text = instrument_list.Rows[index].Cells["Description"].Value.ToString();
            repair_shop.Text = instrument_list.Rows[index].Cells["Shop"].Value.ToString();
            repair_person.Text = instrument_list.Rows[index].Cells["Person"].Value.ToString();
            repair_code.Text = instrument_list.Rows[index].Cells["Code"].Value.ToString();
        }
    }
}
