﻿namespace StoreManagement.Views
{
    partial class contractor_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bin_location_list = new System.Windows.Forms.DataGridView();
            this.sr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.project = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.client_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consultant_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.type = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.print_report = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.print_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.email_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.payments = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.payment_advance = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.payment_balance = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.payment_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.delivery = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.delivery_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.report = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.save = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bin_location_list)).BeginInit();
            this.SuspendLayout();
            // 
            // bin_location_list
            // 
            this.bin_location_list.BackgroundColor = System.Drawing.Color.White;
            this.bin_location_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bin_location_list.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sr,
            this.Id,
            this.project,
            this.client_id,
            this.cell,
            this.name,
            this.consultant_id,
            this.type,
            this.year,
            this.folder,
            this.print_report,
            this.print_date,
            this.email,
            this.email_date,
            this.payments,
            this.payment_advance,
            this.payment_balance,
            this.payment_date,
            this.delivery,
            this.delivery_date,
            this.report,
            this.save});
            this.bin_location_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bin_location_list.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.bin_location_list.Location = new System.Drawing.Point(0, 0);
            this.bin_location_list.Name = "bin_location_list";
            this.bin_location_list.Size = new System.Drawing.Size(1144, 463);
            this.bin_location_list.TabIndex = 6;
            // 
            // sr
            // 
            this.sr.HeaderText = "Sr";
            this.sr.Name = "sr";
            // 
            // Id
            // 
            this.Id.HeaderText = "id";
            this.Id.Name = "Id";
            // 
            // project
            // 
            this.project.FillWeight = 250F;
            this.project.HeaderText = "Project";
            this.project.Name = "project";
            this.project.ToolTipText = "Project";
            this.project.Width = 250;
            // 
            // client_id
            // 
            this.client_id.FillWeight = 250F;
            this.client_id.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.client_id.HeaderText = "Client";
            this.client_id.Name = "client_id";
            this.client_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.client_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.client_id.Width = 250;
            // 
            // cell
            // 
            this.cell.HeaderText = "Cell";
            this.cell.Name = "cell";
            // 
            // name
            // 
            this.name.HeaderText = "Name";
            this.name.Name = "name";
            // 
            // consultant_id
            // 
            this.consultant_id.FillWeight = 250F;
            this.consultant_id.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.consultant_id.HeaderText = "Consultant";
            this.consultant_id.Name = "consultant_id";
            this.consultant_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.consultant_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.consultant_id.Width = 250;
            // 
            // type
            // 
            this.type.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.type.HeaderText = "Type";
            this.type.Items.AddRange(new object[] {
            "BS",
            "CP",
            "EIA",
            "EOI",
            "FS",
            "GT",
            "MD",
            "POC",
            "Propo",
            "RFP",
            "RH",
            "Tender",
            "Testing",
            "TG",
            "TP"});
            this.type.Name = "type";
            this.type.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // year
            // 
            this.year.HeaderText = "Year";
            this.year.Name = "year";
            // 
            // folder
            // 
            this.folder.HeaderText = "Folder";
            this.folder.Name = "folder";
            // 
            // print_report
            // 
            this.print_report.HeaderText = "Print";
            this.print_report.Name = "print_report";
            this.print_report.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.print_report.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // print_date
            // 
            this.print_date.HeaderText = "Print Date";
            this.print_date.Name = "print_date";
            // 
            // email
            // 
            this.email.HeaderText = "Email";
            this.email.Name = "email";
            // 
            // email_date
            // 
            this.email_date.HeaderText = "Email Date";
            this.email_date.Name = "email_date";
            // 
            // payments
            // 
            this.payments.HeaderText = "Payments";
            this.payments.Name = "payments";
            this.payments.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.payments.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // payment_advance
            // 
            this.payment_advance.HeaderText = "Payment Advance";
            this.payment_advance.Name = "payment_advance";
            this.payment_advance.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.payment_advance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // payment_balance
            // 
            this.payment_balance.HeaderText = "Payment Balance";
            this.payment_balance.Name = "payment_balance";
            this.payment_balance.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.payment_balance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // payment_date
            // 
            this.payment_date.HeaderText = "Payment Date";
            this.payment_date.Name = "payment_date";
            // 
            // delivery
            // 
            this.delivery.HeaderText = "Delivery";
            this.delivery.Name = "delivery";
            this.delivery.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.delivery.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // delivery_date
            // 
            this.delivery_date.HeaderText = "Delivery Date";
            this.delivery_date.Name = "delivery_date";
            // 
            // report
            // 
            this.report.HeaderText = "Report View";
            this.report.Name = "report";
            // 
            // save
            // 
            this.save.HeaderText = "Save";
            this.save.Name = "save";
            this.save.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.save.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.save.Text = "Save";
            // 
            // contractor_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1144, 463);
            this.Controls.Add(this.bin_location_list);
            this.Name = "contractor_form";
            this.Text = "contractor_form";
            ((System.ComponentModel.ISupportInitialize)(this.bin_location_list)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView bin_location_list;
        private System.Windows.Forms.DataGridViewTextBoxColumn sr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn project;
        private System.Windows.Forms.DataGridViewComboBoxColumn client_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn cell;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewComboBoxColumn consultant_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn type;
        private System.Windows.Forms.DataGridViewTextBoxColumn year;
        private System.Windows.Forms.DataGridViewTextBoxColumn folder;
        private System.Windows.Forms.DataGridViewCheckBoxColumn print_report;
        private System.Windows.Forms.DataGridViewTextBoxColumn print_date;
        private System.Windows.Forms.DataGridViewCheckBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn email_date;
        private System.Windows.Forms.DataGridViewCheckBoxColumn payments;
        private System.Windows.Forms.DataGridViewCheckBoxColumn payment_advance;
        private System.Windows.Forms.DataGridViewCheckBoxColumn payment_balance;
        private System.Windows.Forms.DataGridViewTextBoxColumn payment_date;
        private System.Windows.Forms.DataGridViewCheckBoxColumn delivery;
        private System.Windows.Forms.DataGridViewTextBoxColumn delivery_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn report;
        private System.Windows.Forms.DataGridViewButtonColumn save;
    }
}