﻿namespace StoreManagement.Views
{
    partial class user
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(user));
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.user_List = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.user_search = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.roles_txt = new System.Windows.Forms.ComboBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.Username_txt = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.user_update = new MetroFramework.Controls.MetroButton();
            this.user_refresh = new MetroFramework.Controls.MetroButton();
            this.user_save = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.Password_txt = new MetroFramework.Controls.MetroTextBox();
            this.user_de_active = new MetroFramework.Controls.MetroRadioButton();
            this.user_active = new MetroFramework.Controls.MetroRadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.user_List)).BeginInit();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.LightGray;
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1160, 70);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = resources.GetString("groupBox6.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(3865, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = resources.GetString("label2.Text");
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 70);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.FloralWhite;
            this.splitContainer1.Panel2.Controls.Add(this.metroLabel4);
            this.splitContainer1.Panel2.Controls.Add(this.roles_txt);
            this.splitContainer1.Panel2.Controls.Add(this.metroLabel2);
            this.splitContainer1.Panel2.Controls.Add(this.Username_txt);
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel2);
            this.splitContainer1.Panel2.Controls.Add(this.metroLabel1);
            this.splitContainer1.Panel2.Controls.Add(this.Password_txt);
            this.splitContainer1.Panel2.Controls.Add(this.user_de_active);
            this.splitContainer1.Panel2.Controls.Add(this.user_active);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Size = new System.Drawing.Size(1160, 432);
            this.splitContainer1.SplitterDistance = 832;
            this.splitContainer1.TabIndex = 66;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.user_List, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(832, 432);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // user_List
            // 
            this.user_List.BackgroundColor = System.Drawing.Color.White;
            this.user_List.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.user_List.Dock = System.Windows.Forms.DockStyle.Fill;
            this.user_List.Location = new System.Drawing.Point(3, 75);
            this.user_List.Name = "user_List";
            this.user_List.Size = new System.Drawing.Size(826, 354);
            this.user_List.TabIndex = 4;
            this.user_List.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.user_List_CellClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.user_search);
            this.panel1.Controls.Add(this.metroLabel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(826, 66);
            this.panel1.TabIndex = 2;
            // 
            // user_search
            // 
            // 
            // 
            // 
            this.user_search.CustomButton.Image = null;
            this.user_search.CustomButton.Location = new System.Drawing.Point(342, 1);
            this.user_search.CustomButton.Name = "";
            this.user_search.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.user_search.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.user_search.CustomButton.TabIndex = 1;
            this.user_search.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.user_search.CustomButton.UseSelectable = true;
            this.user_search.CustomButton.Visible = false;
            this.user_search.Lines = new string[0];
            this.user_search.Location = new System.Drawing.Point(9, 33);
            this.user_search.MaxLength = 32767;
            this.user_search.Name = "user_search";
            this.user_search.PasswordChar = '\0';
            this.user_search.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.user_search.SelectedText = "";
            this.user_search.SelectionLength = 0;
            this.user_search.SelectionStart = 0;
            this.user_search.ShortcutsEnabled = true;
            this.user_search.Size = new System.Drawing.Size(364, 23);
            this.user_search.TabIndex = 3;
            this.user_search.UseSelectable = true;
            this.user_search.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.user_search.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.user_search.TextChanged += new System.EventHandler(this.user_search_TextChanged);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(9, 9);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(68, 19);
            this.metroLabel3.TabIndex = 11;
            this.metroLabel3.Text = "Username";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(19, 155);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(35, 19);
            this.metroLabel4.TabIndex = 71;
            this.metroLabel4.Text = "Role";
            // 
            // roles_txt
            // 
            this.roles_txt.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roles_txt.FormattingEnabled = true;
            this.roles_txt.Items.AddRange(new object[] {
            "Admin",
            "User",
            "Billing User"});
            this.roles_txt.Location = new System.Drawing.Point(18, 181);
            this.roles_txt.Margin = new System.Windows.Forms.Padding(7);
            this.roles_txt.Name = "roles_txt";
            this.roles_txt.Size = new System.Drawing.Size(126, 27);
            this.roles_txt.TabIndex = 70;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(19, 59);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(68, 19);
            this.metroLabel2.TabIndex = 23;
            this.metroLabel2.Text = "Username";
            // 
            // Username_txt
            // 
            // 
            // 
            // 
            this.Username_txt.CustomButton.Image = null;
            this.Username_txt.CustomButton.Location = new System.Drawing.Point(255, 1);
            this.Username_txt.CustomButton.Name = "";
            this.Username_txt.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Username_txt.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Username_txt.CustomButton.TabIndex = 1;
            this.Username_txt.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Username_txt.CustomButton.UseSelectable = true;
            this.Username_txt.CustomButton.Visible = false;
            this.Username_txt.Lines = new string[0];
            this.Username_txt.Location = new System.Drawing.Point(18, 81);
            this.Username_txt.MaxLength = 32767;
            this.Username_txt.Name = "Username_txt";
            this.Username_txt.PasswordChar = '\0';
            this.Username_txt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Username_txt.SelectedText = "";
            this.Username_txt.SelectionLength = 0;
            this.Username_txt.SelectionStart = 0;
            this.Username_txt.ShortcutsEnabled = true;
            this.Username_txt.Size = new System.Drawing.Size(277, 23);
            this.Username_txt.TabIndex = 6;
            this.Username_txt.UseSelectable = true;
            this.Username_txt.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Username_txt.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.user_update, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.user_refresh, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.user_save, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(18, 239);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(277, 32);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // user_update
            // 
            this.user_update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.user_update.Location = new System.Drawing.Point(95, 3);
            this.user_update.Name = "user_update";
            this.user_update.Size = new System.Drawing.Size(86, 26);
            this.user_update.TabIndex = 12;
            this.user_update.Text = "UPDATE";
            this.user_update.UseSelectable = true;
            this.user_update.Click += new System.EventHandler(this.user_update_Click);
            // 
            // user_refresh
            // 
            this.user_refresh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.user_refresh.Location = new System.Drawing.Point(187, 3);
            this.user_refresh.Name = "user_refresh";
            this.user_refresh.Size = new System.Drawing.Size(87, 26);
            this.user_refresh.TabIndex = 13;
            this.user_refresh.Text = "Refresh";
            this.user_refresh.UseSelectable = true;
            this.user_refresh.Click += new System.EventHandler(this.user_refresh_Click);
            // 
            // user_save
            // 
            this.user_save.Dock = System.Windows.Forms.DockStyle.Fill;
            this.user_save.Location = new System.Drawing.Point(3, 3);
            this.user_save.Name = "user_save";
            this.user_save.Size = new System.Drawing.Size(86, 26);
            this.user_save.TabIndex = 11;
            this.user_save.Text = "SAVE";
            this.user_save.UseSelectable = true;
            this.user_save.Click += new System.EventHandler(this.user_save_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(19, 107);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(63, 19);
            this.metroLabel1.TabIndex = 18;
            this.metroLabel1.Text = "Password";
            // 
            // Password_txt
            // 
            // 
            // 
            // 
            this.Password_txt.CustomButton.Image = null;
            this.Password_txt.CustomButton.Location = new System.Drawing.Point(255, 1);
            this.Password_txt.CustomButton.Name = "";
            this.Password_txt.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Password_txt.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Password_txt.CustomButton.TabIndex = 1;
            this.Password_txt.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Password_txt.CustomButton.UseSelectable = true;
            this.Password_txt.CustomButton.Visible = false;
            this.Password_txt.Lines = new string[0];
            this.Password_txt.Location = new System.Drawing.Point(18, 129);
            this.Password_txt.MaxLength = 32767;
            this.Password_txt.Name = "Password_txt";
            this.Password_txt.PasswordChar = '@';
            this.Password_txt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Password_txt.SelectedText = "";
            this.Password_txt.SelectionLength = 0;
            this.Password_txt.SelectionStart = 0;
            this.Password_txt.ShortcutsEnabled = true;
            this.Password_txt.Size = new System.Drawing.Size(277, 23);
            this.Password_txt.TabIndex = 7;
            this.Password_txt.UseSelectable = true;
            this.Password_txt.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Password_txt.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // user_de_active
            // 
            this.user_de_active.AutoSize = true;
            this.user_de_active.Location = new System.Drawing.Point(80, 218);
            this.user_de_active.Name = "user_de_active";
            this.user_de_active.Size = new System.Drawing.Size(75, 15);
            this.user_de_active.TabIndex = 9;
            this.user_de_active.Text = "De-Active";
            this.user_de_active.UseSelectable = true;
            // 
            // user_active
            // 
            this.user_active.AutoSize = true;
            this.user_active.Checked = true;
            this.user_active.Location = new System.Drawing.Point(18, 218);
            this.user_active.Name = "user_active";
            this.user_active.Size = new System.Drawing.Size(56, 15);
            this.user_active.TabIndex = 8;
            this.user_active.TabStop = true;
            this.user_active.Text = "Active";
            this.user_active.UseSelectable = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(14, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(239, 21);
            this.label1.TabIndex = 5;
            this.label1.Text = "Add / Update / Delete User";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // user
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 502);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.groupBox6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "user";
            this.Text = "user";
            this.Load += new System.EventHandler(this.user_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.user_List)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView user_List;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox Username_txt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroButton user_update;
        private MetroFramework.Controls.MetroButton user_refresh;
        private MetroFramework.Controls.MetroButton user_save;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox Password_txt;
        private MetroFramework.Controls.MetroRadioButton user_de_active;
        private MetroFramework.Controls.MetroRadioButton user_active;
        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroTextBox user_search;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.ComboBox roles_txt;
    }
}