﻿namespace StoreManagement.Views
{
    partial class brands_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.brand_list = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.brand_name = new System.Windows.Forms.TextBox();
            this.brand_active = new System.Windows.Forms.RadioButton();
            this.brand_de_active = new System.Windows.Forms.RadioButton();
            this.brand_save = new System.Windows.Forms.Button();
            this.brand_update = new System.Windows.Forms.Button();
            this.brand_delete = new System.Windows.Forms.Button();
            this.brand_add_new = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.brand_list)).BeginInit();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // brand_list
            // 
            this.brand_list.BackgroundColor = System.Drawing.Color.White;
            this.brand_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.brand_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.brand_list.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.brand_list.Location = new System.Drawing.Point(3, 185);
            this.brand_list.Name = "brand_list";
            this.brand_list.Size = new System.Drawing.Size(1154, 409);
            this.brand_list.TabIndex = 1;
            this.brand_list.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.UpdateGridView);
            this.brand_list.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.brand_list_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 33);
            this.label2.TabIndex = 0;
            this.label2.Text = "BRANDS";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(35, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 19);
            this.label1.TabIndex = 66;
            this.label1.Text = "Brand Name";
            // 
            // brand_name
            // 
            this.brand_name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.brand_name.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brand_name.Location = new System.Drawing.Point(130, 57);
            this.brand_name.Name = "brand_name";
            this.brand_name.Size = new System.Drawing.Size(270, 25);
            this.brand_name.TabIndex = 1;
            this.brand_name.TextChanged += new System.EventHandler(this.brand_name_TextChanged);
            // 
            // brand_active
            // 
            this.brand_active.AutoSize = true;
            this.brand_active.Checked = true;
            this.brand_active.Location = new System.Drawing.Point(130, 88);
            this.brand_active.Name = "brand_active";
            this.brand_active.Size = new System.Drawing.Size(55, 17);
            this.brand_active.TabIndex = 2;
            this.brand_active.TabStop = true;
            this.brand_active.Text = "Active";
            this.brand_active.UseVisualStyleBackColor = true;
            // 
            // brand_de_active
            // 
            this.brand_de_active.AutoSize = true;
            this.brand_de_active.Location = new System.Drawing.Point(193, 88);
            this.brand_de_active.Name = "brand_de_active";
            this.brand_de_active.Size = new System.Drawing.Size(72, 17);
            this.brand_de_active.TabIndex = 3;
            this.brand_de_active.Text = "De-Active";
            this.brand_de_active.UseVisualStyleBackColor = true;
            // 
            // brand_save
            // 
            this.brand_save.BackColor = System.Drawing.Color.LightSlateGray;
            this.brand_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.brand_save.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.brand_save.ForeColor = System.Drawing.Color.White;
            this.brand_save.Location = new System.Drawing.Point(130, 132);
            this.brand_save.Name = "brand_save";
            this.brand_save.Size = new System.Drawing.Size(63, 23);
            this.brand_save.TabIndex = 4;
            this.brand_save.Text = "SAVE";
            this.brand_save.UseVisualStyleBackColor = false;
            this.brand_save.Click += new System.EventHandler(this.brand_save_Click);
            // 
            // brand_update
            // 
            this.brand_update.BackColor = System.Drawing.Color.LightSlateGray;
            this.brand_update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.brand_update.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.brand_update.ForeColor = System.Drawing.Color.White;
            this.brand_update.Location = new System.Drawing.Point(199, 132);
            this.brand_update.Name = "brand_update";
            this.brand_update.Size = new System.Drawing.Size(63, 23);
            this.brand_update.TabIndex = 5;
            this.brand_update.Text = "UPDATE";
            this.brand_update.UseVisualStyleBackColor = false;
            this.brand_update.Click += new System.EventHandler(this.brand_update_Click);
            // 
            // brand_delete
            // 
            this.brand_delete.BackColor = System.Drawing.Color.LightSlateGray;
            this.brand_delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.brand_delete.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.brand_delete.ForeColor = System.Drawing.Color.White;
            this.brand_delete.Location = new System.Drawing.Point(268, 131);
            this.brand_delete.Name = "brand_delete";
            this.brand_delete.Size = new System.Drawing.Size(63, 23);
            this.brand_delete.TabIndex = 6;
            this.brand_delete.Text = "DELETE";
            this.brand_delete.UseVisualStyleBackColor = false;
            this.brand_delete.Click += new System.EventHandler(this.brand_delete_Click);
            // 
            // brand_add_new
            // 
            this.brand_add_new.BackColor = System.Drawing.Color.LightSlateGray;
            this.brand_add_new.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.brand_add_new.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.brand_add_new.ForeColor = System.Drawing.Color.White;
            this.brand_add_new.Location = new System.Drawing.Point(337, 131);
            this.brand_add_new.Name = "brand_add_new";
            this.brand_add_new.Size = new System.Drawing.Size(63, 23);
            this.brand_add_new.TabIndex = 7;
            this.brand_add_new.Text = "REFRESH";
            this.brand_add_new.UseVisualStyleBackColor = false;
            this.brand_add_new.Click += new System.EventHandler(this.brand_add_new_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.brand_add_new);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.brand_delete);
            this.panel1.Controls.Add(this.brand_update);
            this.panel1.Controls.Add(this.brand_save);
            this.panel1.Controls.Add(this.brand_de_active);
            this.panel1.Controls.Add(this.brand_active);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.brand_name);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1154, 176);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.brand_list, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 182F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1160, 597);
            this.tableLayoutPanel1.TabIndex = 74;
            // 
            // brands_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1160, 597);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "brands_form";
            this.Text = "brands_form";
            this.Load += new System.EventHandler(this.brands_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.brand_list)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView brand_list;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox brand_name;
        private System.Windows.Forms.RadioButton brand_active;
        private System.Windows.Forms.RadioButton brand_de_active;
        private System.Windows.Forms.Button brand_save;
        private System.Windows.Forms.Button brand_update;
        private System.Windows.Forms.Button brand_delete;
        private System.Windows.Forms.Button brand_add_new;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}