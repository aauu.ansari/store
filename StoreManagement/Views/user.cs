﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class user : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        FormsNames formsNames = new FormsNames();
        Roles userRoles = new Roles();
        DataView dv;
        public user()
        {
            InitializeComponent();
        }
        private void user_Load(object sender, EventArgs e)
        {
            if (service.rolesUser(login.userCredentials.id, "user") != null)
            {
                userRoles = service.rolesUser(login.userCredentials.id, "user");
            }
            refresh();
        }

        private void user_save_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_add == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    User user = new BS_SERVICES.User();
                    user.username = Username_txt.Text;
                    user.password = Password_txt.Text;
                    user.roles = roles_txt.Text;
                    user.status = commonHelper.status(user_active.Checked, user_de_active.Checked);
                    int user_id = service.userSave(user);
                    if (roles_txt.Text == "Admin")
                    {
                        addRoles(user_id);
                    }

                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (FaultException fex)
            {
                MessageBox.Show(fex.Message);
            }
        }

        private void addRoles(int user_id)
        {
            foreach (string item in formsNames.formsList)
            {
                Roles roles = new BS_SERVICES.Roles();
                roles.login_id = user_id;
                roles.roles_add = 1;
                roles.roles_update = 1;
                roles.roles_delete = 1;
                roles.roles_view = 1;
                roles.form = item;
                roles.created_at = DateTime.Now.ToShortDateString();
                service.rolesSave(roles);
            }

        }

        private void refresh()
        {
            dv = new DataView(service.userList_dt());
            UserList();
            Username_txt.Text = "";
            Password_txt.Text = "";
            user_save.Enabled = true;
            user_update.Enabled = false;

        }

        private void UserList()
        {
            try
            {
                user_List.DataSource = dv;
                gridViewProperties();
            }
            catch (FaultException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridViewProperties()
        {
            user_List.Columns[0].Visible = false;
            user_List.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private bool Validation()
        {
            string[] param = { Username_txt.Text, Password_txt.Text };
            return commonHelper.Validator(param);
        }

        private void user_search_TextChanged(object sender, EventArgs e)
        {
            dv.RowFilter = "Username Like '%" + user_search.Text + "%'";
            user_List.DataSource = dv;
            gridViewProperties();
        }

        private void user_List_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ShowDataInTextbox(e.RowIndex);
            user_update.Enabled = true;
            user_save.Enabled = false;
        }
        int user_id = 0;
        private void ShowDataInTextbox(int index)
        {
            try
            {
                if (index == -1) { return; }
                user_id = Convert.ToInt32(user_List.Rows[index].Cells["id"].Value.ToString());
                Username_txt.Text = user_List.Rows[index].Cells["Username"].Value.ToString();
                if (user_List.Rows[index].Cells["Status"].Value.ToString() != "0")
                {
                    user_active.Checked = true;
                }
                else
                {
                    user_de_active.Checked = true;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void user_update_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0 )
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Username_txt.Text != "")
                {
                    User user = new BS_SERVICES.User();
                    user.id = user_id;
                    user.username = Username_txt.Text;
                    user.password = Password_txt.Text;
                    user.status = commonHelper.status(user_active.Checked, user_de_active.Checked);
                    user.updated_at = DateTime.Now.ToShortDateString();
                    service.userUpdate(user);
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (FaultException fex)
            {
                MessageBox.Show(fex.Message);
            }
        }

        private void user_refresh_Click(object sender, EventArgs e)
        {
            refresh();
        }
        private void PERMISSION_Click(object sender, EventArgs e)
        {
        }
    }
}
