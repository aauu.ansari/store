﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class brands_form : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        DataView dv;
        Roles userRoles = new Roles();
        public brands_form()
        {
            InitializeComponent();            
        }
        private void brands_form_Load(object sender, EventArgs e)
        {
            if (service.rolesUser(login.userCredentials.id, "brands_form") != null)
            {
                userRoles = service.rolesUser(login.userCredentials.id, "brands_form");
            }
            refresh();            
        }
        private bool Validation()
        {
            string[] param = { brand_name.Text};
            return commonHelper.Validator(param);
        }
        private void refresh()
        {
            try
            {
                dv = new DataView(service.brandList_dataTable());
                BrandList();
                brand_name.Text = null;
                brand_save.Enabled = true;
                brand_update.Enabled = false;
                brand_delete.Enabled = false;
                brand_active.Checked = true;
                brand_de_active.Checked = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }

        private void brand_save_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_add == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    Brands brand = new BS_SERVICES.Brands();
                    brand.name = brand_name.Text;
                    brand.status = commonHelper.status(brand_active.Checked, brand_de_active.Checked);
                    brand.created_at = DateTime.Now.ToShortDateString();
                    brand.updated_at = DateTime.Now.ToShortDateString();
                    service.brandSave(brand);
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
            }
            catch (FaultException fex)
            {
                MessageBox.Show(fex.Message);
            }

        }

        private void brand_update_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    Brands brand = new BS_SERVICES.Brands();
                    brand.id = brand_id;
                    brand.name = brand_name.Text;
                    brand.status = commonHelper.status(brand_active.Checked, brand_de_active.Checked);
                    brand.updated_at = DateTime.Now.ToShortDateString();
                    service.brandUpdate(brand);
                    MessageBox.Show(messages.update, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
            }
            catch (FaultException fex)
            {
                MessageBox.Show(fex.ToString());
            }
        }  
        
        protected void BrandList()
        {
            try
            {
                brand_list.DataSource = dv;
                gridViewProperties();
            }
            catch (FaultException ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void gridViewProperties()
        {
            brand_list.Columns[0].Visible = false;
            brand_list.Columns[1].Width = 150;
            brand_list.Columns[2].Width = 150;
            brand_list.Columns[3].Width = 350;
            brand_list.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        int brand_id = 0;
        private void UpdateGridView(object sender, DataGridViewCellEventArgs e)
        {
            ShowDataInTextbox(e.RowIndex);
            brand_update.Enabled = true;
            brand_delete.Enabled = true;
            brand_save.Enabled = false;
        }

        private void brand_list_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                ShowDataInTextbox(brand_list.CurrentRow.Index);
            }
        }

        private void ShowDataInTextbox(int index)
        {
            try
            {
                if (index == -1) { return; }
                brand_id = Convert.ToInt32(brand_list.Rows[index].Cells["id"].Value.ToString());                
                if (Convert.ToInt32(brand_list.Rows[index].Cells["Status"].Value.ToString()) == 1)
                {
                    brand_active.Checked = true;
                }
                else
                {
                    brand_de_active.Checked = true;
                }
                brand_name.Text = brand_list.Rows[index].Cells["Name"].Value.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void brand_add_new_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void brand_delete_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_delete == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (MessageBox.Show("Are you sure! you want to delete this record", "Alert", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    if (Validation())
                    {
                        Brands brand = new BS_SERVICES.Brands();
                        brand.id = brand_id;
                        brand.status = 0;
                        brand.updated_at = DateTime.Now.ToShortDateString();
                        service.brandDelete(brand);
                        MessageBox.Show(messages.delete, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        refresh();
                    }
                    else
                    {
                        MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            catch (FaultException fex)
            {
                MessageBox.Show(fex.ToString());
            }
        }

        private void brand_name_TextChanged(object sender, EventArgs e)
        {
            dv.RowFilter = "Name Like '%" + brand_name.Text + "%'";//"Name LIKE '%,del,%'";//LIKE '%,1,%'//"Column1 Like '%" + textBox1.Text + "%'"
            brand_list.DataSource = dv;
            gridViewProperties();
        }
    }
}
