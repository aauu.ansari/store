﻿namespace StoreManagement.Views
{
    partial class client_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.client_list = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.client_type = new System.Windows.Forms.ComboBox();
            this.client_de_active = new System.Windows.Forms.RadioButton();
            this.client_active = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.client_name = new System.Windows.Forms.TextBox();
            this.client_contact = new System.Windows.Forms.TextBox();
            this.client_email = new System.Windows.Forms.TextBox();
            this.client_address = new System.Windows.Forms.TextBox();
            this.client_ntn = new System.Windows.Forms.TextBox();
            this.client_pra = new System.Windows.Forms.TextBox();
            this.client_logo_box = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.client_refresh = new System.Windows.Forms.Button();
            this.client_delete = new System.Windows.Forms.Button();
            this.client_update = new System.Windows.Forms.Button();
            this.client_save = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.client_logo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.client_list)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.client_logo_box)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // client_list
            // 
            this.client_list.BackgroundColor = System.Drawing.Color.White;
            this.client_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.client_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.client_list.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.client_list.Location = new System.Drawing.Point(3, 254);
            this.client_list.Name = "client_list";
            this.client_list.Size = new System.Drawing.Size(1295, 389);
            this.client_list.TabIndex = 1;
            this.client_list.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.client_list_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 31);
            this.label1.TabIndex = 1;
            this.label1.Text = "Clients";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // client_type
            // 
            this.client_type.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.client_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_type.FormattingEnabled = true;
            this.client_type.Items.AddRange(new object[] {
            "client",
            "consultant"});
            this.client_type.Location = new System.Drawing.Point(130, 149);
            this.client_type.Name = "client_type";
            this.client_type.Size = new System.Drawing.Size(134, 26);
            this.client_type.TabIndex = 3;
            // 
            // client_de_active
            // 
            this.client_de_active.AutoSize = true;
            this.client_de_active.Location = new System.Drawing.Point(191, 181);
            this.client_de_active.Name = "client_de_active";
            this.client_de_active.Size = new System.Drawing.Size(72, 17);
            this.client_de_active.TabIndex = 5;
            this.client_de_active.Text = "De-Active";
            this.client_de_active.UseVisualStyleBackColor = true;
            // 
            // client_active
            // 
            this.client_active.AutoSize = true;
            this.client_active.Checked = true;
            this.client_active.Location = new System.Drawing.Point(130, 181);
            this.client_active.Name = "client_active";
            this.client_active.Size = new System.Drawing.Size(55, 17);
            this.client_active.TabIndex = 4;
            this.client_active.TabStop = true;
            this.client_active.Text = "Active";
            this.client_active.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 19);
            this.label2.TabIndex = 72;
            this.label2.Text = "Company Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(477, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 19);
            this.label4.TabIndex = 74;
            this.label4.Text = "PRA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(475, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 19);
            this.label5.TabIndex = 75;
            this.label5.Text = "NTN";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(451, 123);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 19);
            this.label6.TabIndex = 76;
            this.label6.Text = "Address";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(79, 123);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 19);
            this.label7.TabIndex = 77;
            this.label7.Text = "Email";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(2, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 19);
            this.label8.TabIndex = 78;
            this.label8.Text = "Name - Tell / Cell";
            // 
            // client_name
            // 
            this.client_name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.client_name.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_name.Location = new System.Drawing.Point(130, 57);
            this.client_name.Name = "client_name";
            this.client_name.Size = new System.Drawing.Size(270, 25);
            this.client_name.TabIndex = 0;
            this.client_name.TextChanged += new System.EventHandler(this.client_name_TextChanged);
            // 
            // client_contact
            // 
            this.client_contact.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.client_contact.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_contact.Location = new System.Drawing.Point(130, 88);
            this.client_contact.Name = "client_contact";
            this.client_contact.Size = new System.Drawing.Size(270, 25);
            this.client_contact.TabIndex = 1;
            this.client_contact.TextChanged += new System.EventHandler(this.client_name_TextChanged);
            // 
            // client_email
            // 
            this.client_email.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.client_email.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_email.Location = new System.Drawing.Point(130, 119);
            this.client_email.Name = "client_email";
            this.client_email.Size = new System.Drawing.Size(270, 25);
            this.client_email.TabIndex = 2;
            this.client_email.TextChanged += new System.EventHandler(this.client_name_TextChanged);
            // 
            // client_address
            // 
            this.client_address.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.client_address.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_address.Location = new System.Drawing.Point(518, 119);
            this.client_address.Name = "client_address";
            this.client_address.Size = new System.Drawing.Size(270, 25);
            this.client_address.TabIndex = 8;
            this.client_address.TextChanged += new System.EventHandler(this.client_name_TextChanged);
            // 
            // client_ntn
            // 
            this.client_ntn.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.client_ntn.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_ntn.Location = new System.Drawing.Point(518, 55);
            this.client_ntn.Name = "client_ntn";
            this.client_ntn.Size = new System.Drawing.Size(145, 25);
            this.client_ntn.TabIndex = 6;
            this.client_ntn.TextChanged += new System.EventHandler(this.client_name_TextChanged);
            // 
            // client_pra
            // 
            this.client_pra.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.client_pra.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_pra.Location = new System.Drawing.Point(518, 86);
            this.client_pra.Name = "client_pra";
            this.client_pra.Size = new System.Drawing.Size(145, 25);
            this.client_pra.TabIndex = 7;
            this.client_pra.TextChanged += new System.EventHandler(this.client_name_TextChanged);
            // 
            // client_logo_box
            // 
            this.client_logo_box.Location = new System.Drawing.Point(824, 28);
            this.client_logo_box.Name = "client_logo_box";
            this.client_logo_box.Size = new System.Drawing.Size(103, 143);
            this.client_logo_box.TabIndex = 85;
            this.client_logo_box.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(85, 152);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 19);
            this.label9.TabIndex = 86;
            this.label9.Text = "Type";
            // 
            // client_refresh
            // 
            this.client_refresh.BackColor = System.Drawing.Color.LightSlateGray;
            this.client_refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.client_refresh.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.client_refresh.ForeColor = System.Drawing.Color.White;
            this.client_refresh.Location = new System.Drawing.Point(725, 174);
            this.client_refresh.Name = "client_refresh";
            this.client_refresh.Size = new System.Drawing.Size(63, 23);
            this.client_refresh.TabIndex = 16;
            this.client_refresh.Text = "REFRESH";
            this.client_refresh.UseVisualStyleBackColor = false;
            // 
            // client_delete
            // 
            this.client_delete.BackColor = System.Drawing.Color.LightSlateGray;
            this.client_delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.client_delete.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.client_delete.ForeColor = System.Drawing.Color.White;
            this.client_delete.Location = new System.Drawing.Point(656, 174);
            this.client_delete.Name = "client_delete";
            this.client_delete.Size = new System.Drawing.Size(63, 23);
            this.client_delete.TabIndex = 15;
            this.client_delete.Text = "DELETE";
            this.client_delete.UseVisualStyleBackColor = false;
            this.client_delete.Click += new System.EventHandler(this.client_delete_Click);
            // 
            // client_update
            // 
            this.client_update.BackColor = System.Drawing.Color.LightSlateGray;
            this.client_update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.client_update.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.client_update.ForeColor = System.Drawing.Color.White;
            this.client_update.Location = new System.Drawing.Point(587, 175);
            this.client_update.Name = "client_update";
            this.client_update.Size = new System.Drawing.Size(63, 23);
            this.client_update.TabIndex = 14;
            this.client_update.Text = "UPDATE";
            this.client_update.UseVisualStyleBackColor = false;
            this.client_update.Click += new System.EventHandler(this.client_update_Click);
            // 
            // client_save
            // 
            this.client_save.BackColor = System.Drawing.Color.LightSlateGray;
            this.client_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.client_save.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.client_save.ForeColor = System.Drawing.Color.White;
            this.client_save.Location = new System.Drawing.Point(518, 175);
            this.client_save.Name = "client_save";
            this.client_save.Size = new System.Drawing.Size(63, 23);
            this.client_save.TabIndex = 13;
            this.client_save.Text = "SAVE";
            this.client_save.UseVisualStyleBackColor = false;
            this.client_save.Click += new System.EventHandler(this.client_save_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightSlateGray;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(824, 172);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 25);
            this.button1.TabIndex = 11;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.openFile);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.client_list, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 251F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1301, 646);
            this.tableLayoutPanel1.TabIndex = 93;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.client_logo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.client_type);
            this.panel1.Controls.Add(this.client_refresh);
            this.panel1.Controls.Add(this.client_active);
            this.panel1.Controls.Add(this.client_delete);
            this.panel1.Controls.Add(this.client_de_active);
            this.panel1.Controls.Add(this.client_update);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.client_save);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.client_logo_box);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.client_pra);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.client_ntn);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.client_address);
            this.panel1.Controls.Add(this.client_name);
            this.panel1.Controls.Add(this.client_email);
            this.panel1.Controls.Add(this.client_contact);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1295, 245);
            this.panel1.TabIndex = 0;
            // 
            // client_logo
            // 
            this.client_logo.AutoSize = true;
            this.client_logo.Location = new System.Drawing.Point(821, 39);
            this.client_logo.Name = "client_logo";
            this.client_logo.Size = new System.Drawing.Size(0, 13);
            this.client_logo.TabIndex = 87;
            // 
            // client_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1301, 646);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "client_form";
            this.Text = "New Client";
            this.Load += new System.EventHandler(this.client_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.client_list)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.client_logo_box)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView client_list;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox client_type;
        private System.Windows.Forms.RadioButton client_de_active;
        private System.Windows.Forms.RadioButton client_active;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox client_name;
        private System.Windows.Forms.TextBox client_contact;
        private System.Windows.Forms.TextBox client_email;
        private System.Windows.Forms.TextBox client_address;
        private System.Windows.Forms.TextBox client_ntn;
        private System.Windows.Forms.TextBox client_pra;
        private System.Windows.Forms.PictureBox client_logo_box;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button client_refresh;
        private System.Windows.Forms.Button client_delete;
        private System.Windows.Forms.Button client_update;
        private System.Windows.Forms.Button client_save;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label client_logo;
    }
}