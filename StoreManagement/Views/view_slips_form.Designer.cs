﻿namespace StoreManagement.Views
{
    partial class view_slips_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(view_slips_form));
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.all_slips = new MetroFramework.Controls.MetroButton();
            this.Instruments = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.send_to_repair_instruments = new MetroFramework.Controls.MetroButton();
            this.clients_btn = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.metroButton5 = new MetroFramework.Controls.MetroButton();
            this.client_tax = new MetroFramework.Controls.MetroButton();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.LightGray;
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1198, 70);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = resources.GetString("groupBox6.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(3907, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = resources.GetString("label2.Text");
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 70);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.FloralWhite;
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Size = new System.Drawing.Size(1198, 439);
            this.splitContainer1.SplitterDistance = 212;
            this.splitContainer1.TabIndex = 64;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.all_slips, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Instruments, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroButton3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.send_to_repair_instruments, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.clients_btn, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.metroButton2, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.metroButton4, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.metroButton5, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.client_tax, 0, 8);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(212, 439);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // all_slips
            // 
            this.all_slips.Dock = System.Windows.Forms.DockStyle.Fill;
            this.all_slips.Location = new System.Drawing.Point(3, 3);
            this.all_slips.Name = "all_slips";
            this.all_slips.Size = new System.Drawing.Size(206, 37);
            this.all_slips.TabIndex = 2;
            this.all_slips.Text = "VIEW SLIP";
            this.all_slips.UseSelectable = true;
            this.all_slips.Click += new System.EventHandler(this.all_slips_Click);
            // 
            // Instruments
            // 
            this.Instruments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Instruments.Location = new System.Drawing.Point(3, 46);
            this.Instruments.Name = "Instruments";
            this.Instruments.Size = new System.Drawing.Size(206, 37);
            this.Instruments.TabIndex = 3;
            this.Instruments.Text = "INSTRUMENTS";
            this.Instruments.UseSelectable = true;
            this.Instruments.Click += new System.EventHandler(this.Instruments_Click);
            // 
            // metroButton3
            // 
            this.metroButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroButton3.Location = new System.Drawing.Point(3, 89);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(206, 37);
            this.metroButton3.TabIndex = 5;
            this.metroButton3.Text = "ON SITE DAMAGED LIST";
            this.metroButton3.UseSelectable = true;
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // send_to_repair_instruments
            // 
            this.send_to_repair_instruments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.send_to_repair_instruments.Location = new System.Drawing.Point(3, 132);
            this.send_to_repair_instruments.Name = "send_to_repair_instruments";
            this.send_to_repair_instruments.Size = new System.Drawing.Size(206, 37);
            this.send_to_repair_instruments.TabIndex = 6;
            this.send_to_repair_instruments.Text = "SEND TO REPAIR INSTRUMENTS";
            this.send_to_repair_instruments.UseSelectable = true;
            this.send_to_repair_instruments.Click += new System.EventHandler(this.send_to_repair_instruments_Click);
            // 
            // clients_btn
            // 
            this.clients_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clients_btn.Location = new System.Drawing.Point(3, 175);
            this.clients_btn.Name = "clients_btn";
            this.clients_btn.Size = new System.Drawing.Size(206, 37);
            this.clients_btn.TabIndex = 7;
            this.clients_btn.Text = "CLIENTS";
            this.clients_btn.UseSelectable = true;
            this.clients_btn.Click += new System.EventHandler(this.clients_btn_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroButton2.Location = new System.Drawing.Point(3, 218);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(206, 37);
            this.metroButton2.TabIndex = 8;
            this.metroButton2.Text = "USERS";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.Users_btn_Click);
            // 
            // metroButton4
            // 
            this.metroButton4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroButton4.Location = new System.Drawing.Point(3, 261);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(206, 37);
            this.metroButton4.TabIndex = 9;
            this.metroButton4.Text = "TEAM";
            this.metroButton4.UseSelectable = true;
            this.metroButton4.Click += new System.EventHandler(this.Teams_btn_Click);
            // 
            // metroButton5
            // 
            this.metroButton5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroButton5.Location = new System.Drawing.Point(3, 304);
            this.metroButton5.Name = "metroButton5";
            this.metroButton5.Size = new System.Drawing.Size(206, 37);
            this.metroButton5.TabIndex = 10;
            this.metroButton5.Text = "BRANDS";
            this.metroButton5.UseSelectable = true;
            this.metroButton5.Click += new System.EventHandler(this.Brands_btn_Click);
            // 
            // client_tax
            // 
            this.client_tax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.client_tax.Location = new System.Drawing.Point(3, 347);
            this.client_tax.Name = "client_tax";
            this.client_tax.Size = new System.Drawing.Size(206, 37);
            this.client_tax.TabIndex = 11;
            this.client_tax.Text = "CLIENT TAX";
            this.client_tax.UseSelectable = true;
            this.client_tax.Click += new System.EventHandler(this.client_tax_Click);
            // 
            // view_slips_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1198, 509);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.groupBox6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "view_slips_form";
            this.Text = "view_slips";
            this.Load += new System.EventHandler(this.view_slips_form_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroButton all_slips;
        private MetroFramework.Controls.MetroButton Instruments;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroButton send_to_repair_instruments;
        private MetroFramework.Controls.MetroButton clients_btn;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton metroButton4;
        private MetroFramework.Controls.MetroButton metroButton5;
        private MetroFramework.Controls.MetroButton client_tax;
    }
}