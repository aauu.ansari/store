﻿namespace StoreManagement.Views
{
    partial class assigment_slip_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(assigment_slip_form));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.completion_date = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.mobilization_date = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.issue_to = new System.Windows.Forms.ComboBox();
            this.issue_by = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.refresh_client = new MetroFramework.Controls.MetroButton();
            this.address = new System.Windows.Forms.RichTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.client_id = new System.Windows.Forms.ComboBox();
            this.client_new_btn = new MetroFramework.Controls.MetroButton();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.supervisor_id = new System.Windows.Forms.ComboBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.contact_person = new MetroFramework.Controls.MetroTextBox();
            this.name = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.quotation_ref = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.purchased_order_no = new MetroFramework.Controls.MetroTextBox();
            this.task = new System.Windows.Forms.RichTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.barCodeCatcher_btn = new MetroFramework.Controls.MetroButton();
            this.label8 = new System.Windows.Forms.Label();
            this.barCodeCatcher = new System.Windows.Forms.TextBox();
            this.instrumnets_selection_update = new MetroFramework.Controls.MetroButton();
            this.instrument_remove_all_btn = new MetroFramework.Controls.MetroButton();
            this.instrument_plus_btn = new MetroFramework.Controls.MetroButton();
            this.equipment_selection = new System.Windows.Forms.ComboBox();
            this.equipment_selection_list = new System.Windows.Forms.ListBox();
            this.instrument_remove_btn = new MetroFramework.Controls.MetroButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.team_selection_update = new MetroFramework.Controls.MetroButton();
            this.team_remove_all_btn = new MetroFramework.Controls.MetroButton();
            this.team_plus_btn = new MetroFramework.Controls.MetroButton();
            this.team_remove_btn = new MetroFramework.Controls.MetroButton();
            this.team_selection_list = new System.Windows.Forms.ListBox();
            this.team_selection = new System.Windows.Forms.ComboBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.site_save = new MetroFramework.Controls.MetroButton();
            this.equipment_issued_update = new MetroFramework.Controls.MetroButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.search = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.from = new MetroFramework.Controls.MetroDateTime();
            this.to = new MetroFramework.Controls.MetroDateTime();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.created_at = new MetroFramework.Controls.MetroDateTime();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.instrument_type = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.barCodeGridCatcher_btn = new MetroFramework.Controls.MetroButton();
            this.barCodeGridCatcher = new System.Windows.Forms.TextBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.instrument_quantity = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.damage_instrument_description = new System.Windows.Forms.RichTextBox();
            this.instrument_life = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.instrument_save = new MetroFramework.Controls.MetroButton();
            this.instrument_refresh = new MetroFramework.Controls.MetroButton();
            this.instrument_delete = new MetroFramework.Controls.MetroButton();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.instrument_name = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.instrument_status = new System.Windows.Forms.ComboBox();
            this.instrument_list = new System.Windows.Forms.DataGridView();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.site_update = new MetroFramework.Controls.MetroButton();
            this.print = new MetroFramework.Controls.MetroButton();
            this.fron_to_btn = new MetroFramework.Controls.MetroButton();
            this.open = new MetroFramework.Controls.MetroButton();
            this.refresh_form = new MetroFramework.Controls.MetroButton();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.consumable_equipment_selection_list = new System.Windows.Forms.ListBox();
            this.consumable_equipment_selection = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.quantity = new System.Windows.Forms.TextBox();
            this.consumable_barCodeCatcher_btn = new MetroFramework.Controls.MetroButton();
            this.label10 = new System.Windows.Forms.Label();
            this.consumable_barCodeCatcher = new System.Windows.Forms.TextBox();
            this.consumable_instrumnets_selection_update = new MetroFramework.Controls.MetroButton();
            this.consumable_instrument_remove_all_btn = new MetroFramework.Controls.MetroButton();
            this.consumable_instrument_plus_btn = new MetroFramework.Controls.MetroButton();
            this.consumable_instrument_remove_btn = new MetroFramework.Controls.MetroButton();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.instrument_list)).BeginInit();
            this.groupBox13.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.LightGray;
            this.groupBox2.Controls.Add(this.metroLabel9);
            this.groupBox2.Controls.Add(this.completion_date);
            this.groupBox2.Controls.Add(this.metroLabel16);
            this.groupBox2.Controls.Add(this.mobilization_date);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(767, 193);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(305, 135);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(16, 23);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(126, 19);
            this.metroLabel9.TabIndex = 18;
            this.metroLabel9.Text = "* Mobilization Date ";
            // 
            // completion_date
            // 
            this.completion_date.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.completion_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.completion_date.Location = new System.Drawing.Point(16, 95);
            this.completion_date.MinimumSize = new System.Drawing.Size(0, 25);
            this.completion_date.Name = "completion_date";
            this.completion_date.Size = new System.Drawing.Size(239, 25);
            this.completion_date.TabIndex = 21;
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(16, 73);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(117, 19);
            this.metroLabel16.TabIndex = 30;
            this.metroLabel16.Text = " Completion Date ";
            // 
            // mobilization_date
            // 
            this.mobilization_date.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.mobilization_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.mobilization_date.Location = new System.Drawing.Point(16, 45);
            this.mobilization_date.MinimumSize = new System.Drawing.Size(0, 25);
            this.mobilization_date.Name = "mobilization_date";
            this.mobilization_date.Size = new System.Drawing.Size(239, 25);
            this.mobilization_date.TabIndex = 20;
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(16, 68);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(142, 19);
            this.metroLabel15.TabIndex = 28;
            this.metroLabel15.Text = "* Equipment Issued To ";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(16, 15);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(143, 19);
            this.metroLabel14.TabIndex = 27;
            this.metroLabel14.Text = "* Equipment Issued By ";
            // 
            // issue_to
            // 
            this.issue_to.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.issue_to.FormattingEnabled = true;
            this.issue_to.Location = new System.Drawing.Point(16, 90);
            this.issue_to.Name = "issue_to";
            this.issue_to.Size = new System.Drawing.Size(239, 28);
            this.issue_to.TabIndex = 25;
            // 
            // issue_by
            // 
            this.issue_by.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.issue_by.FormattingEnabled = true;
            this.issue_by.Location = new System.Drawing.Point(16, 37);
            this.issue_by.Name = "issue_by";
            this.issue_by.Size = new System.Drawing.Size(239, 28);
            this.issue_by.TabIndex = 24;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.LightGray;
            this.groupBox3.Controls.Add(this.refresh_client);
            this.groupBox3.Controls.Add(this.address);
            this.groupBox3.Controls.Add(this.metroLabel2);
            this.groupBox3.Controls.Add(this.client_id);
            this.groupBox3.Controls.Add(this.client_new_btn);
            this.groupBox3.Controls.Add(this.metroLabel7);
            this.groupBox3.Controls.Add(this.supervisor_id);
            this.groupBox3.Controls.Add(this.metroLabel10);
            this.groupBox3.Controls.Add(this.metroLabel1);
            this.groupBox3.Controls.Add(this.contact_person);
            this.groupBox3.Controls.Add(this.name);
            this.groupBox3.Controls.Add(this.metroLabel6);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(27, 193);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(329, 345);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            // 
            // refresh_client
            // 
            this.refresh_client.BackgroundImage = global::StoreManagement.Properties.Resources.Refresh_2_icon;
            this.refresh_client.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.refresh_client.Location = new System.Drawing.Point(266, 85);
            this.refresh_client.Name = "refresh_client";
            this.refresh_client.Size = new System.Drawing.Size(22, 28);
            this.refresh_client.TabIndex = 10;
            this.refresh_client.UseSelectable = true;
            this.refresh_client.Click += new System.EventHandler(this.refresh_client_Click);
            // 
            // address
            // 
            this.address.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.address.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.address.Location = new System.Drawing.Point(21, 240);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(239, 84);
            this.address.TabIndex = 14;
            this.address.Text = "";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(21, 63);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(96, 19);
            this.metroLabel2.TabIndex = 9;
            this.metroLabel2.Text = "* Client Name ";
            // 
            // client_id
            // 
            this.client_id.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.client_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_id.FormattingEnabled = true;
            this.client_id.Location = new System.Drawing.Point(21, 85);
            this.client_id.Name = "client_id";
            this.client_id.Size = new System.Drawing.Size(239, 28);
            this.client_id.TabIndex = 9;
            // 
            // client_new_btn
            // 
            this.client_new_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Add_user_icon;
            this.client_new_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.client_new_btn.Location = new System.Drawing.Point(294, 85);
            this.client_new_btn.Name = "client_new_btn";
            this.client_new_btn.Size = new System.Drawing.Size(22, 28);
            this.client_new_btn.TabIndex = 11;
            this.client_new_btn.UseSelectable = true;
            this.client_new_btn.Click += new System.EventHandler(this.client_new_btn_Click);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(21, 217);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(95, 19);
            this.metroLabel7.TabIndex = 11;
            this.metroLabel7.Text = "* Site Address ";
            // 
            // supervisor_id
            // 
            this.supervisor_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervisor_id.FormattingEnabled = true;
            this.supervisor_id.Location = new System.Drawing.Point(21, 138);
            this.supervisor_id.Name = "supervisor_id";
            this.supervisor_id.Size = new System.Drawing.Size(239, 28);
            this.supervisor_id.TabIndex = 12;
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(21, 116);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(110, 19);
            this.metroLabel10.TabIndex = 54;
            this.metroLabel10.Text = "* Site Supervisor ";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(21, 16);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(104, 19);
            this.metroLabel1.TabIndex = 10;
            this.metroLabel1.Text = "* Project Name ";
            // 
            // contact_person
            // 
            // 
            // 
            // 
            this.contact_person.CustomButton.Image = null;
            this.contact_person.CustomButton.Location = new System.Drawing.Point(217, 1);
            this.contact_person.CustomButton.Name = "";
            this.contact_person.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.contact_person.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.contact_person.CustomButton.TabIndex = 1;
            this.contact_person.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.contact_person.CustomButton.UseSelectable = true;
            this.contact_person.CustomButton.Visible = false;
            this.contact_person.Lines = new string[0];
            this.contact_person.Location = new System.Drawing.Point(21, 191);
            this.contact_person.MaxLength = 32767;
            this.contact_person.Name = "contact_person";
            this.contact_person.PasswordChar = '\0';
            this.contact_person.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.contact_person.SelectedText = "";
            this.contact_person.SelectionLength = 0;
            this.contact_person.SelectionStart = 0;
            this.contact_person.ShortcutsEnabled = true;
            this.contact_person.Size = new System.Drawing.Size(239, 23);
            this.contact_person.TabIndex = 13;
            this.contact_person.UseSelectable = true;
            this.contact_person.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.contact_person.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // name
            // 
            // 
            // 
            // 
            this.name.CustomButton.Image = null;
            this.name.CustomButton.Location = new System.Drawing.Point(217, 1);
            this.name.CustomButton.Name = "";
            this.name.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.name.CustomButton.TabIndex = 1;
            this.name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.name.CustomButton.UseSelectable = true;
            this.name.CustomButton.Visible = false;
            this.name.Lines = new string[0];
            this.name.Location = new System.Drawing.Point(21, 38);
            this.name.MaxLength = 32767;
            this.name.Name = "name";
            this.name.PasswordChar = '\0';
            this.name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.name.SelectedText = "";
            this.name.SelectionLength = 0;
            this.name.SelectionStart = 0;
            this.name.ShortcutsEnabled = true;
            this.name.Size = new System.Drawing.Size(239, 23);
            this.name.TabIndex = 8;
            this.name.UseSelectable = true;
            this.name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.name.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(21, 169);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(111, 19);
            this.metroLabel6.TabIndex = 17;
            this.metroLabel6.Text = "* Contact Person ";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.LightGray;
            this.groupBox4.Controls.Add(this.metroLabel13);
            this.groupBox4.Controls.Add(this.metroLabel8);
            this.groupBox4.Controls.Add(this.quotation_ref);
            this.groupBox4.Controls.Add(this.metroLabel5);
            this.groupBox4.Controls.Add(this.purchased_order_no);
            this.groupBox4.Controls.Add(this.task);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(403, 193);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(311, 345);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(18, 23);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(123, 19);
            this.metroLabel13.TabIndex = 27;
            this.metroLabel13.Text = "* Purchased Order ";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(18, 185);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(76, 19);
            this.metroLabel8.TabIndex = 16;
            this.metroLabel8.Text = "* Site Tasks ";
            // 
            // quotation_ref
            // 
            // 
            // 
            // 
            this.quotation_ref.CustomButton.Image = null;
            this.quotation_ref.CustomButton.Location = new System.Drawing.Point(217, 1);
            this.quotation_ref.CustomButton.Name = "";
            this.quotation_ref.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.quotation_ref.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.quotation_ref.CustomButton.TabIndex = 1;
            this.quotation_ref.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.quotation_ref.CustomButton.UseSelectable = true;
            this.quotation_ref.CustomButton.Visible = false;
            this.quotation_ref.ForeColor = System.Drawing.Color.Black;
            this.quotation_ref.Lines = new string[0];
            this.quotation_ref.Location = new System.Drawing.Point(19, 95);
            this.quotation_ref.MaxLength = 32767;
            this.quotation_ref.Name = "quotation_ref";
            this.quotation_ref.PasswordChar = '\0';
            this.quotation_ref.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.quotation_ref.SelectedText = "";
            this.quotation_ref.SelectionLength = 0;
            this.quotation_ref.SelectionStart = 0;
            this.quotation_ref.ShortcutsEnabled = true;
            this.quotation_ref.Size = new System.Drawing.Size(239, 23);
            this.quotation_ref.TabIndex = 17;
            this.quotation_ref.UseSelectable = true;
            this.quotation_ref.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.quotation_ref.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(19, 73);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(107, 19);
            this.metroLabel5.TabIndex = 15;
            this.metroLabel5.Text = "* Quotation Ref. ";
            // 
            // purchased_order_no
            // 
            // 
            // 
            // 
            this.purchased_order_no.CustomButton.Image = null;
            this.purchased_order_no.CustomButton.Location = new System.Drawing.Point(217, 1);
            this.purchased_order_no.CustomButton.Name = "";
            this.purchased_order_no.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.purchased_order_no.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.purchased_order_no.CustomButton.TabIndex = 1;
            this.purchased_order_no.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.purchased_order_no.CustomButton.UseSelectable = true;
            this.purchased_order_no.CustomButton.Visible = false;
            this.purchased_order_no.Lines = new string[0];
            this.purchased_order_no.Location = new System.Drawing.Point(18, 45);
            this.purchased_order_no.MaxLength = 32767;
            this.purchased_order_no.Name = "purchased_order_no";
            this.purchased_order_no.PasswordChar = '\0';
            this.purchased_order_no.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.purchased_order_no.SelectedText = "";
            this.purchased_order_no.SelectionLength = 0;
            this.purchased_order_no.SelectionStart = 0;
            this.purchased_order_no.ShortcutsEnabled = true;
            this.purchased_order_no.Size = new System.Drawing.Size(239, 23);
            this.purchased_order_no.TabIndex = 16;
            this.purchased_order_no.UseSelectable = true;
            this.purchased_order_no.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.purchased_order_no.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // task
            // 
            this.task.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.task.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.task.Location = new System.Drawing.Point(18, 207);
            this.task.Name = "task";
            this.task.Size = new System.Drawing.Size(239, 117);
            this.task.TabIndex = 18;
            this.task.Text = "";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.LightGray;
            this.groupBox5.Controls.Add(this.barCodeCatcher_btn);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.barCodeCatcher);
            this.groupBox5.Controls.Add(this.instrumnets_selection_update);
            this.groupBox5.Controls.Add(this.instrument_remove_all_btn);
            this.groupBox5.Controls.Add(this.instrument_plus_btn);
            this.groupBox5.Controls.Add(this.equipment_selection);
            this.groupBox5.Controls.Add(this.equipment_selection_list);
            this.groupBox5.Controls.Add(this.instrument_remove_btn);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(366, 566);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(335, 348);
            this.groupBox5.TabIndex = 34;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "* Equipment Selection ";
            // 
            // barCodeCatcher_btn
            // 
            this.barCodeCatcher_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Plus_icon;
            this.barCodeCatcher_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.barCodeCatcher_btn.Location = new System.Drawing.Point(189, 39);
            this.barCodeCatcher_btn.Name = "barCodeCatcher_btn";
            this.barCodeCatcher_btn.Size = new System.Drawing.Size(23, 22);
            this.barCodeCatcher_btn.TabIndex = 36;
            this.barCodeCatcher_btn.UseSelectable = true;
            this.barCodeCatcher_btn.Click += new System.EventHandler(this.barCodeCatcher_btn_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(116, 17);
            this.label8.TabIndex = 42;
            this.label8.Text = "BarCode Catcher";
            // 
            // barCodeCatcher
            // 
            this.barCodeCatcher.Font = new System.Drawing.Font("Calibri", 9F);
            this.barCodeCatcher.Location = new System.Drawing.Point(19, 37);
            this.barCodeCatcher.Name = "barCodeCatcher";
            this.barCodeCatcher.Size = new System.Drawing.Size(164, 22);
            this.barCodeCatcher.TabIndex = 35;
            // 
            // instrumnets_selection_update
            // 
            this.instrumnets_selection_update.Enabled = false;
            this.instrumnets_selection_update.Location = new System.Drawing.Point(18, 306);
            this.instrumnets_selection_update.Name = "instrumnets_selection_update";
            this.instrumnets_selection_update.Size = new System.Drawing.Size(63, 25);
            this.instrumnets_selection_update.TabIndex = 42;
            this.instrumnets_selection_update.Text = "UPDATE";
            this.instrumnets_selection_update.UseSelectable = true;
            this.instrumnets_selection_update.Click += new System.EventHandler(this.instrumnets_selection_update_Click);
            // 
            // instrument_remove_all_btn
            // 
            this.instrument_remove_all_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Recycle_Bin_Empty_icon;
            this.instrument_remove_all_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.instrument_remove_all_btn.Location = new System.Drawing.Point(294, 251);
            this.instrument_remove_all_btn.Name = "instrument_remove_all_btn";
            this.instrument_remove_all_btn.Size = new System.Drawing.Size(35, 49);
            this.instrument_remove_all_btn.TabIndex = 41;
            this.instrument_remove_all_btn.UseSelectable = true;
            this.instrument_remove_all_btn.Click += new System.EventHandler(this.instrument_remove_all_btn_Click);
            // 
            // instrument_plus_btn
            // 
            this.instrument_plus_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Plus_icon;
            this.instrument_plus_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.instrument_plus_btn.Location = new System.Drawing.Point(297, 65);
            this.instrument_plus_btn.Name = "instrument_plus_btn";
            this.instrument_plus_btn.Size = new System.Drawing.Size(23, 22);
            this.instrument_plus_btn.TabIndex = 38;
            this.instrument_plus_btn.UseSelectable = true;
            this.instrument_plus_btn.Click += new System.EventHandler(this.instrument_plus_btn_Click);
            // 
            // equipment_selection
            // 
            this.equipment_selection.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipment_selection.FormattingEnabled = true;
            this.equipment_selection.Location = new System.Drawing.Point(18, 62);
            this.equipment_selection.Name = "equipment_selection";
            this.equipment_selection.Size = new System.Drawing.Size(273, 22);
            this.equipment_selection.TabIndex = 37;
            // 
            // equipment_selection_list
            // 
            this.equipment_selection_list.Font = new System.Drawing.Font("Microsoft JhengHei Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipment_selection_list.FormattingEnabled = true;
            this.equipment_selection_list.ItemHeight = 16;
            this.equipment_selection_list.Location = new System.Drawing.Point(18, 96);
            this.equipment_selection_list.Name = "equipment_selection_list";
            this.equipment_selection_list.Size = new System.Drawing.Size(273, 196);
            this.equipment_selection_list.TabIndex = 39;
            // 
            // instrument_remove_btn
            // 
            this.instrument_remove_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Science_Minus_Math_icon;
            this.instrument_remove_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.instrument_remove_btn.Location = new System.Drawing.Point(297, 96);
            this.instrument_remove_btn.Name = "instrument_remove_btn";
            this.instrument_remove_btn.Size = new System.Drawing.Size(23, 22);
            this.instrument_remove_btn.TabIndex = 40;
            this.instrument_remove_btn.UseSelectable = true;
            this.instrument_remove_btn.Click += new System.EventHandler(this.instrument_minus_btn_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.LightGray;
            this.groupBox7.Controls.Add(this.team_selection_update);
            this.groupBox7.Controls.Add(this.team_remove_all_btn);
            this.groupBox7.Controls.Add(this.team_plus_btn);
            this.groupBox7.Controls.Add(this.team_remove_btn);
            this.groupBox7.Controls.Add(this.team_selection_list);
            this.groupBox7.Controls.Add(this.team_selection);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(27, 566);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(329, 348);
            this.groupBox7.TabIndex = 27;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "* Team Selection ";
            // 
            // team_selection_update
            // 
            this.team_selection_update.Enabled = false;
            this.team_selection_update.Location = new System.Drawing.Point(14, 306);
            this.team_selection_update.Name = "team_selection_update";
            this.team_selection_update.Size = new System.Drawing.Size(63, 25);
            this.team_selection_update.TabIndex = 33;
            this.team_selection_update.Text = "UPDATE";
            this.team_selection_update.UseSelectable = true;
            this.team_selection_update.Click += new System.EventHandler(this.team_selection_update_Click);
            // 
            // team_remove_all_btn
            // 
            this.team_remove_all_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Recycle_Bin_Empty_icon;
            this.team_remove_all_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.team_remove_all_btn.Location = new System.Drawing.Point(264, 251);
            this.team_remove_all_btn.Name = "team_remove_all_btn";
            this.team_remove_all_btn.Size = new System.Drawing.Size(35, 49);
            this.team_remove_all_btn.TabIndex = 32;
            this.team_remove_all_btn.UseSelectable = true;
            this.team_remove_all_btn.Click += new System.EventHandler(this.team_remove_all_btn_Click);
            // 
            // team_plus_btn
            // 
            this.team_plus_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Plus_icon;
            this.team_plus_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.team_plus_btn.Location = new System.Drawing.Point(264, 42);
            this.team_plus_btn.Name = "team_plus_btn";
            this.team_plus_btn.Size = new System.Drawing.Size(23, 22);
            this.team_plus_btn.TabIndex = 29;
            this.team_plus_btn.UseSelectable = true;
            this.team_plus_btn.Click += new System.EventHandler(this.team_plus_btn_Click);
            // 
            // team_remove_btn
            // 
            this.team_remove_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Science_Minus_Math_icon;
            this.team_remove_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.team_remove_btn.Location = new System.Drawing.Point(264, 76);
            this.team_remove_btn.Name = "team_remove_btn";
            this.team_remove_btn.Size = new System.Drawing.Size(23, 22);
            this.team_remove_btn.TabIndex = 31;
            this.team_remove_btn.UseSelectable = true;
            this.team_remove_btn.Click += new System.EventHandler(this.team_remove_btn_Click);
            // 
            // team_selection_list
            // 
            this.team_selection_list.Font = new System.Drawing.Font("Microsoft JhengHei Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.team_selection_list.FormattingEnabled = true;
            this.team_selection_list.ItemHeight = 16;
            this.team_selection_list.Location = new System.Drawing.Point(14, 76);
            this.team_selection_list.Name = "team_selection_list";
            this.team_selection_list.Size = new System.Drawing.Size(244, 212);
            this.team_selection_list.TabIndex = 30;
            // 
            // team_selection
            // 
            this.team_selection.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.team_selection.FormattingEnabled = true;
            this.team_selection.Location = new System.Drawing.Point(16, 39);
            this.team_selection.Name = "team_selection";
            this.team_selection.Size = new System.Drawing.Size(242, 22);
            this.team_selection.TabIndex = 28;
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.LightGray;
            this.groupBox8.Controls.Add(this.tableLayoutPanel1);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(27, 923);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(1054, 57);
            this.groupBox8.TabIndex = 43;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = resources.GetString("groupBox8.Text");
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.site_save, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1048, 38);
            this.tableLayoutPanel1.TabIndex = 44;
            // 
            // site_save
            // 
            this.site_save.Dock = System.Windows.Forms.DockStyle.Right;
            this.site_save.Location = new System.Drawing.Point(931, 3);
            this.site_save.Name = "site_save";
            this.site_save.Size = new System.Drawing.Size(114, 32);
            this.site_save.TabIndex = 45;
            this.site_save.Text = "SAVE";
            this.site_save.UseSelectable = true;
            this.site_save.Click += new System.EventHandler(this.site_save_Click);
            // 
            // equipment_issued_update
            // 
            this.equipment_issued_update.Enabled = false;
            this.equipment_issued_update.Location = new System.Drawing.Point(1009, 513);
            this.equipment_issued_update.Name = "equipment_issued_update";
            this.equipment_issued_update.Size = new System.Drawing.Size(63, 25);
            this.equipment_issued_update.TabIndex = 26;
            this.equipment_issued_update.Text = "UPDATE";
            this.equipment_issued_update.UseSelectable = true;
            this.equipment_issued_update.Click += new System.EventHandler(this.equipment_issued_update_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.LightGray;
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1158, 48);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Impact", 15F);
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "ASSIGNMENT SLIP ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // search
            // 
            this.search.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search.FormattingEnabled = true;
            this.search.Location = new System.Drawing.Point(403, 88);
            this.search.Margin = new System.Windows.Forms.Padding(7);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(311, 23);
            this.search.TabIndex = 4;
            this.search.TextChanged += new System.EventHandler(this.search_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.LightGray;
            this.groupBox1.Controls.Add(this.metroLabel14);
            this.groupBox1.Controls.Add(this.issue_by);
            this.groupBox1.Controls.Add(this.issue_to);
            this.groupBox1.Controls.Add(this.metroLabel15);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(767, 366);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 141);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            // 
            // from
            // 
            this.from.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.from.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.from.Location = new System.Drawing.Point(27, 91);
            this.from.MinimumSize = new System.Drawing.Size(0, 25);
            this.from.Name = "from";
            this.from.Size = new System.Drawing.Size(146, 25);
            this.from.TabIndex = 1;
            // 
            // to
            // 
            this.to.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.to.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.to.Location = new System.Drawing.Point(179, 90);
            this.to.MinimumSize = new System.Drawing.Size(0, 25);
            this.to.Name = "to";
            this.to.Size = new System.Drawing.Size(149, 25);
            this.to.TabIndex = 2;
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.LightGray;
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(5, 121);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(1076, 13);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = resources.GetString("groupBox9.Text");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 66;
            this.label1.Text = "FROM";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(400, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 15);
            this.label3.TabIndex = 67;
            this.label3.Text = "SEARCH BY PROJECT CODE";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(176, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 15);
            this.label4.TabIndex = 68;
            this.label4.Text = "TO";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 15);
            this.label5.TabIndex = 70;
            this.label5.Text = "DATE";
            this.label5.Visible = false;
            // 
            // created_at
            // 
            this.created_at.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.created_at.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.created_at.Location = new System.Drawing.Point(27, 162);
            this.created_at.MinimumSize = new System.Drawing.Size(0, 25);
            this.created_at.Name = "created_at";
            this.created_at.Size = new System.Drawing.Size(172, 25);
            this.created_at.TabIndex = 69;
            this.created_at.Visible = false;
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.LightGray;
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(18, 1020);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(1054, 13);
            this.groupBox10.TabIndex = 47;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = resources.GetString("groupBox10.Text");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(-768, 1109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(283, 31);
            this.label6.TabIndex = 72;
            this.label6.Text = "ASSIGNMENT SLIP ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(21, 986);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(264, 31);
            this.label7.TabIndex = 46;
            this.label7.Text = "Return Instruments";
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.LightGray;
            this.groupBox11.Controls.Add(this.instrument_type);
            this.groupBox11.Controls.Add(this.label9);
            this.groupBox11.Controls.Add(this.barCodeGridCatcher_btn);
            this.groupBox11.Controls.Add(this.barCodeGridCatcher);
            this.groupBox11.Controls.Add(this.groupBox14);
            this.groupBox11.Controls.Add(this.instrument_list);
            this.groupBox11.Controls.Add(this.metroLabel3);
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(27, 1045);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(1054, 591);
            this.groupBox11.TabIndex = 48;
            this.groupBox11.TabStop = false;
            // 
            // instrument_type
            // 
            this.instrument_type.AutoSize = true;
            this.instrument_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instrument_type.Location = new System.Drawing.Point(740, 43);
            this.instrument_type.Name = "instrument_type";
            this.instrument_type.Size = new System.Drawing.Size(54, 17);
            this.instrument_type.TabIndex = 81;
            this.instrument_type.Text = "label12";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(534, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 17);
            this.label9.TabIndex = 55;
            this.label9.Text = "BarCode Catcher";
            // 
            // barCodeGridCatcher_btn
            // 
            this.barCodeGridCatcher_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Plus_icon;
            this.barCodeGridCatcher_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.barCodeGridCatcher_btn.Location = new System.Drawing.Point(711, 41);
            this.barCodeGridCatcher_btn.Name = "barCodeGridCatcher_btn";
            this.barCodeGridCatcher_btn.Size = new System.Drawing.Size(23, 22);
            this.barCodeGridCatcher_btn.TabIndex = 50;
            this.barCodeGridCatcher_btn.UseSelectable = true;
            this.barCodeGridCatcher_btn.Click += new System.EventHandler(this.barCodeGridCatcher_btn_Click);
            // 
            // barCodeGridCatcher
            // 
            this.barCodeGridCatcher.Location = new System.Drawing.Point(537, 42);
            this.barCodeGridCatcher.Name = "barCodeGridCatcher";
            this.barCodeGridCatcher.Size = new System.Drawing.Size(168, 20);
            this.barCodeGridCatcher.TabIndex = 49;
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.Color.Silver;
            this.groupBox14.Controls.Add(this.instrument_quantity);
            this.groupBox14.Controls.Add(this.metroLabel11);
            this.groupBox14.Controls.Add(this.metroLabel18);
            this.groupBox14.Controls.Add(this.damage_instrument_description);
            this.groupBox14.Controls.Add(this.instrument_life);
            this.groupBox14.Controls.Add(this.metroLabel17);
            this.groupBox14.Controls.Add(this.tableLayoutPanel2);
            this.groupBox14.Controls.Add(this.metroLabel12);
            this.groupBox14.Controls.Add(this.instrument_name);
            this.groupBox14.Controls.Add(this.metroLabel4);
            this.groupBox14.Controls.Add(this.instrument_status);
            this.groupBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox14.Location = new System.Drawing.Point(742, 68);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(297, 490);
            this.groupBox14.TabIndex = 52;
            this.groupBox14.TabStop = false;
            // 
            // instrument_quantity
            // 
            // 
            // 
            // 
            this.instrument_quantity.CustomButton.Image = null;
            this.instrument_quantity.CustomButton.Location = new System.Drawing.Point(112, 1);
            this.instrument_quantity.CustomButton.Name = "";
            this.instrument_quantity.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.instrument_quantity.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.instrument_quantity.CustomButton.TabIndex = 1;
            this.instrument_quantity.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.instrument_quantity.CustomButton.UseSelectable = true;
            this.instrument_quantity.CustomButton.Visible = false;
            this.instrument_quantity.Lines = new string[] {
        "0"};
            this.instrument_quantity.Location = new System.Drawing.Point(6, 152);
            this.instrument_quantity.MaxLength = 50;
            this.instrument_quantity.Name = "instrument_quantity";
            this.instrument_quantity.PasswordChar = '\0';
            this.instrument_quantity.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.instrument_quantity.SelectedText = "";
            this.instrument_quantity.SelectionLength = 0;
            this.instrument_quantity.SelectionStart = 0;
            this.instrument_quantity.ShortcutsEnabled = true;
            this.instrument_quantity.Size = new System.Drawing.Size(134, 23);
            this.instrument_quantity.TabIndex = 81;
            this.instrument_quantity.Text = "0";
            this.instrument_quantity.UseSelectable = true;
            this.instrument_quantity.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.instrument_quantity.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel11.Location = new System.Drawing.Point(6, 130);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(58, 19);
            this.metroLabel11.TabIndex = 82;
            this.metroLabel11.Text = "Quantity";
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel18.Location = new System.Drawing.Point(6, 188);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(201, 19);
            this.metroLabel18.TabIndex = 80;
            this.metroLabel18.Text = "Damaged Instrument Description";
            // 
            // damage_instrument_description
            // 
            this.damage_instrument_description.Font = new System.Drawing.Font("Calibri", 9F);
            this.damage_instrument_description.Location = new System.Drawing.Point(6, 210);
            this.damage_instrument_description.Name = "damage_instrument_description";
            this.damage_instrument_description.Size = new System.Drawing.Size(282, 79);
            this.damage_instrument_description.TabIndex = 56;
            this.damage_instrument_description.Text = "";
            // 
            // instrument_life
            // 
            // 
            // 
            // 
            this.instrument_life.CustomButton.Image = null;
            this.instrument_life.CustomButton.Location = new System.Drawing.Point(42, 1);
            this.instrument_life.CustomButton.Name = "";
            this.instrument_life.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.instrument_life.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.instrument_life.CustomButton.TabIndex = 1;
            this.instrument_life.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.instrument_life.CustomButton.UseSelectable = true;
            this.instrument_life.CustomButton.Visible = false;
            this.instrument_life.Lines = new string[0];
            this.instrument_life.Location = new System.Drawing.Point(194, 104);
            this.instrument_life.MaxLength = 50;
            this.instrument_life.Name = "instrument_life";
            this.instrument_life.PasswordChar = '\0';
            this.instrument_life.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.instrument_life.SelectedText = "";
            this.instrument_life.SelectionLength = 0;
            this.instrument_life.SelectionStart = 0;
            this.instrument_life.ShortcutsEnabled = true;
            this.instrument_life.Size = new System.Drawing.Size(64, 23);
            this.instrument_life.TabIndex = 55;
            this.instrument_life.UseSelectable = true;
            this.instrument_life.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.instrument_life.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.instrument_life.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.instrument_life_KeyPress);
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel17.Location = new System.Drawing.Point(194, 82);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(29, 19);
            this.metroLabel17.TabIndex = 78;
            this.metroLabel17.Text = "Life";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.instrument_save, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.instrument_refresh, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.instrument_delete, 2, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 295);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(285, 33);
            this.tableLayoutPanel2.TabIndex = 57;
            // 
            // instrument_save
            // 
            this.instrument_save.Dock = System.Windows.Forms.DockStyle.Fill;
            this.instrument_save.Location = new System.Drawing.Point(3, 3);
            this.instrument_save.Name = "instrument_save";
            this.instrument_save.Size = new System.Drawing.Size(89, 27);
            this.instrument_save.TabIndex = 58;
            this.instrument_save.Text = "SAVE";
            this.instrument_save.UseSelectable = true;
            this.instrument_save.Click += new System.EventHandler(this.instrument_save_Click);
            // 
            // instrument_refresh
            // 
            this.instrument_refresh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.instrument_refresh.Location = new System.Drawing.Point(98, 3);
            this.instrument_refresh.Name = "instrument_refresh";
            this.instrument_refresh.Size = new System.Drawing.Size(89, 27);
            this.instrument_refresh.TabIndex = 59;
            this.instrument_refresh.Text = "Refresh";
            this.instrument_refresh.UseSelectable = true;
            this.instrument_refresh.Click += new System.EventHandler(this.instrument_refresh_Click);
            // 
            // instrument_delete
            // 
            this.instrument_delete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.instrument_delete.Location = new System.Drawing.Point(193, 3);
            this.instrument_delete.Name = "instrument_delete";
            this.instrument_delete.Size = new System.Drawing.Size(89, 27);
            this.instrument_delete.TabIndex = 60;
            this.instrument_delete.Text = "Delete";
            this.instrument_delete.UseSelectable = true;
            this.instrument_delete.Click += new System.EventHandler(this.instrument_delete_Click);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(6, 77);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(43, 19);
            this.metroLabel12.TabIndex = 71;
            this.metroLabel12.Text = "Status";
            // 
            // instrument_name
            // 
            // 
            // 
            // 
            this.instrument_name.CustomButton.Image = null;
            this.instrument_name.CustomButton.Location = new System.Drawing.Point(230, 1);
            this.instrument_name.CustomButton.Name = "";
            this.instrument_name.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.instrument_name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.instrument_name.CustomButton.TabIndex = 1;
            this.instrument_name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.instrument_name.CustomButton.UseSelectable = true;
            this.instrument_name.CustomButton.Visible = false;
            this.instrument_name.ForeColor = System.Drawing.Color.Black;
            this.instrument_name.Lines = new string[0];
            this.instrument_name.Location = new System.Drawing.Point(6, 50);
            this.instrument_name.MaxLength = 32767;
            this.instrument_name.Name = "instrument_name";
            this.instrument_name.PasswordChar = '\0';
            this.instrument_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.instrument_name.SelectedText = "";
            this.instrument_name.SelectionLength = 0;
            this.instrument_name.SelectionStart = 0;
            this.instrument_name.ShortcutsEnabled = true;
            this.instrument_name.Size = new System.Drawing.Size(252, 23);
            this.instrument_name.TabIndex = 53;
            this.instrument_name.UseSelectable = true;
            this.instrument_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.instrument_name.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(6, 28);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(45, 19);
            this.metroLabel4.TabIndex = 70;
            this.metroLabel4.Text = "Name";
            // 
            // instrument_status
            // 
            this.instrument_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instrument_status.FormattingEnabled = true;
            this.instrument_status.Items.AddRange(new object[] {
            "OK",
            "Un-Serviceable",
            "Serviceable",
            "On Site"});
            this.instrument_status.Location = new System.Drawing.Point(6, 99);
            this.instrument_status.Name = "instrument_status";
            this.instrument_status.Size = new System.Drawing.Size(182, 28);
            this.instrument_status.TabIndex = 54;
            // 
            // instrument_list
            // 
            this.instrument_list.BackgroundColor = System.Drawing.Color.White;
            this.instrument_list.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 9.75F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.instrument_list.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.instrument_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.instrument_list.DefaultCellStyle = dataGridViewCellStyle2;
            this.instrument_list.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.instrument_list.Location = new System.Drawing.Point(6, 68);
            this.instrument_list.Name = "instrument_list";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.instrument_list.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.instrument_list.Size = new System.Drawing.Size(730, 490);
            this.instrument_list.TabIndex = 51;
            this.instrument_list.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.instrument_list_CellClick);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(6, 39);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(114, 19);
            this.metroLabel3.TabIndex = 36;
            this.metroLabel3.Text = "Issued Instruments";
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.LightGray;
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.Location = new System.Drawing.Point(24, 1637);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(1057, 25);
            this.groupBox12.TabIndex = 60;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = resources.GetString("groupBox12.Text");
            // 
            // site_update
            // 
            this.site_update.Enabled = false;
            this.site_update.Location = new System.Drawing.Point(1009, 335);
            this.site_update.Name = "site_update";
            this.site_update.Size = new System.Drawing.Size(63, 25);
            this.site_update.TabIndex = 22;
            this.site_update.Text = "UPDATE";
            this.site_update.UseSelectable = true;
            this.site_update.Click += new System.EventHandler(this.site_update_Click);
            // 
            // print
            // 
            this.print.Location = new System.Drawing.Point(1015, 90);
            this.print.Name = "print";
            this.print.Size = new System.Drawing.Size(57, 25);
            this.print.TabIndex = 75;
            this.print.Text = "Print";
            this.print.UseSelectable = true;
            this.print.Click += new System.EventHandler(this.print_Click);
            // 
            // fron_to_btn
            // 
            this.fron_to_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Refresh_2_icon;
            this.fron_to_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fron_to_btn.Location = new System.Drawing.Point(334, 88);
            this.fron_to_btn.Name = "fron_to_btn";
            this.fron_to_btn.Size = new System.Drawing.Size(22, 28);
            this.fron_to_btn.TabIndex = 3;
            this.fron_to_btn.UseSelectable = true;
            this.fron_to_btn.Click += new System.EventHandler(this.fron_to_btn_Click);
            // 
            // open
            // 
            this.open.BackgroundImage = global::StoreManagement.Properties.Resources.open_file_icon;
            this.open.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.open.Location = new System.Drawing.Point(720, 91);
            this.open.Name = "open";
            this.open.Size = new System.Drawing.Size(29, 21);
            this.open.TabIndex = 5;
            this.open.UseSelectable = true;
            this.open.Click += new System.EventHandler(this.open_Click);
            // 
            // refresh_form
            // 
            this.refresh_form.BackgroundImage = global::StoreManagement.Properties.Resources.new_icon;
            this.refresh_form.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.refresh_form.Location = new System.Drawing.Point(755, 91);
            this.refresh_form.Name = "refresh_form";
            this.refresh_form.Size = new System.Drawing.Size(29, 21);
            this.refresh_form.TabIndex = 6;
            this.refresh_form.UseSelectable = true;
            this.refresh_form.Click += new System.EventHandler(this.refresh_form_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.BackColor = System.Drawing.Color.LightGray;
            this.groupBox13.Controls.Add(this.consumable_equipment_selection_list);
            this.groupBox13.Controls.Add(this.consumable_equipment_selection);
            this.groupBox13.Controls.Add(this.label11);
            this.groupBox13.Controls.Add(this.quantity);
            this.groupBox13.Controls.Add(this.consumable_barCodeCatcher_btn);
            this.groupBox13.Controls.Add(this.label10);
            this.groupBox13.Controls.Add(this.consumable_barCodeCatcher);
            this.groupBox13.Controls.Add(this.consumable_instrumnets_selection_update);
            this.groupBox13.Controls.Add(this.consumable_instrument_remove_all_btn);
            this.groupBox13.Controls.Add(this.consumable_instrument_plus_btn);
            this.groupBox13.Controls.Add(this.consumable_instrument_remove_btn);
            this.groupBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.Location = new System.Drawing.Point(711, 566);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(361, 348);
            this.groupBox13.TabIndex = 43;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "* Consumable Equipment Selection ";
            // 
            // consumable_equipment_selection_list
            // 
            this.consumable_equipment_selection_list.Font = new System.Drawing.Font("Microsoft JhengHei Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consumable_equipment_selection_list.FormattingEnabled = true;
            this.consumable_equipment_selection_list.ItemHeight = 16;
            this.consumable_equipment_selection_list.Location = new System.Drawing.Point(18, 96);
            this.consumable_equipment_selection_list.Name = "consumable_equipment_selection_list";
            this.consumable_equipment_selection_list.Size = new System.Drawing.Size(299, 196);
            this.consumable_equipment_selection_list.TabIndex = 45;
            // 
            // consumable_equipment_selection
            // 
            this.consumable_equipment_selection.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consumable_equipment_selection.FormattingEnabled = true;
            this.consumable_equipment_selection.Location = new System.Drawing.Point(19, 62);
            this.consumable_equipment_selection.Name = "consumable_equipment_selection";
            this.consumable_equipment_selection.Size = new System.Drawing.Size(227, 22);
            this.consumable_equipment_selection.TabIndex = 43;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(250, 45);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 44;
            this.label11.Text = "Quantity";
            // 
            // quantity
            // 
            this.quantity.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantity.Location = new System.Drawing.Point(250, 62);
            this.quantity.Name = "quantity";
            this.quantity.Size = new System.Drawing.Size(67, 23);
            this.quantity.TabIndex = 43;
            this.quantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.quantity_KeyPress);
            // 
            // consumable_barCodeCatcher_btn
            // 
            this.consumable_barCodeCatcher_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Plus_icon;
            this.consumable_barCodeCatcher_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.consumable_barCodeCatcher_btn.Location = new System.Drawing.Point(189, 39);
            this.consumable_barCodeCatcher_btn.Name = "consumable_barCodeCatcher_btn";
            this.consumable_barCodeCatcher_btn.Size = new System.Drawing.Size(23, 22);
            this.consumable_barCodeCatcher_btn.TabIndex = 36;
            this.consumable_barCodeCatcher_btn.UseSelectable = true;
            this.consumable_barCodeCatcher_btn.Click += new System.EventHandler(this.consumable_barCodeCatcher_btn_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(17, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 17);
            this.label10.TabIndex = 42;
            this.label10.Text = "BarCode Catcher";
            // 
            // consumable_barCodeCatcher
            // 
            this.consumable_barCodeCatcher.Font = new System.Drawing.Font("Calibri", 9F);
            this.consumable_barCodeCatcher.Location = new System.Drawing.Point(19, 37);
            this.consumable_barCodeCatcher.Name = "consumable_barCodeCatcher";
            this.consumable_barCodeCatcher.Size = new System.Drawing.Size(164, 22);
            this.consumable_barCodeCatcher.TabIndex = 35;
            // 
            // consumable_instrumnets_selection_update
            // 
            this.consumable_instrumnets_selection_update.Enabled = false;
            this.consumable_instrumnets_selection_update.Location = new System.Drawing.Point(18, 306);
            this.consumable_instrumnets_selection_update.Name = "consumable_instrumnets_selection_update";
            this.consumable_instrumnets_selection_update.Size = new System.Drawing.Size(63, 25);
            this.consumable_instrumnets_selection_update.TabIndex = 42;
            this.consumable_instrumnets_selection_update.Text = "UPDATE";
            this.consumable_instrumnets_selection_update.UseSelectable = true;
            this.consumable_instrumnets_selection_update.Click += new System.EventHandler(this.consumable_instrumnets_selection_update_Click);
            // 
            // consumable_instrument_remove_all_btn
            // 
            this.consumable_instrument_remove_all_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Recycle_Bin_Empty_icon;
            this.consumable_instrument_remove_all_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.consumable_instrument_remove_all_btn.Location = new System.Drawing.Point(320, 251);
            this.consumable_instrument_remove_all_btn.Name = "consumable_instrument_remove_all_btn";
            this.consumable_instrument_remove_all_btn.Size = new System.Drawing.Size(35, 49);
            this.consumable_instrument_remove_all_btn.TabIndex = 41;
            this.consumable_instrument_remove_all_btn.UseSelectable = true;
            this.consumable_instrument_remove_all_btn.Click += new System.EventHandler(this.consumable_instrument_remove_all_btn_Click);
            // 
            // consumable_instrument_plus_btn
            // 
            this.consumable_instrument_plus_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Plus_icon;
            this.consumable_instrument_plus_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.consumable_instrument_plus_btn.Location = new System.Drawing.Point(320, 65);
            this.consumable_instrument_plus_btn.Name = "consumable_instrument_plus_btn";
            this.consumable_instrument_plus_btn.Size = new System.Drawing.Size(23, 22);
            this.consumable_instrument_plus_btn.TabIndex = 38;
            this.consumable_instrument_plus_btn.UseSelectable = true;
            this.consumable_instrument_plus_btn.Click += new System.EventHandler(this.consumable_instrument_plus_btn_Click);
            // 
            // consumable_instrument_remove_btn
            // 
            this.consumable_instrument_remove_btn.BackgroundImage = global::StoreManagement.Properties.Resources.Science_Minus_Math_icon;
            this.consumable_instrument_remove_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.consumable_instrument_remove_btn.Location = new System.Drawing.Point(323, 96);
            this.consumable_instrument_remove_btn.Name = "consumable_instrument_remove_btn";
            this.consumable_instrument_remove_btn.Size = new System.Drawing.Size(23, 22);
            this.consumable_instrument_remove_btn.TabIndex = 40;
            this.consumable_instrument_remove_btn.UseSelectable = true;
            this.consumable_instrument_remove_btn.Click += new System.EventHandler(this.consumable_instrument_remove_btn_Click);
            // 
            // assigment_slip_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1175, 920);
            this.Controls.Add(this.groupBox13);
            this.Controls.Add(this.print);
            this.Controls.Add(this.site_update);
            this.Controls.Add(this.equipment_issued_update);
            this.Controls.Add(this.groupBox12);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.created_at);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.fron_to_btn);
            this.Controls.Add(this.to);
            this.Controls.Add(this.from);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.search);
            this.Controls.Add(this.open);
            this.Controls.Add(this.refresh_form);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "assigment_slip_form";
            this.Text = "assigment_slip";
            this.Load += new System.EventHandler(this.assigment_slip_form_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.instrument_list)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroDateTime completion_date;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroDateTime mobilization_date;
        private System.Windows.Forms.ComboBox issue_to;
        private System.Windows.Forms.ComboBox issue_by;
        private System.Windows.Forms.GroupBox groupBox3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.ComboBox client_id;
        private MetroFramework.Controls.MetroButton client_new_btn;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox contact_person;
        private MetroFramework.Controls.MetroTextBox name;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private System.Windows.Forms.GroupBox groupBox4;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox quotation_ref;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox purchased_order_no;
        private System.Windows.Forms.ComboBox supervisor_id;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private System.Windows.Forms.RichTextBox task;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox equipment_selection;
        private System.Windows.Forms.ListBox equipment_selection_list;
        private MetroFramework.Controls.MetroButton instrument_remove_btn;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ListBox team_selection_list;
        private System.Windows.Forms.ComboBox team_selection;
        private System.Windows.Forms.GroupBox groupBox8;
        private MetroFramework.Controls.MetroButton site_save;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox search;
        private MetroFramework.Controls.MetroButton open;
        private MetroFramework.Controls.MetroButton refresh_form;
        private MetroFramework.Controls.MetroButton instrument_plus_btn;
        private MetroFramework.Controls.MetroButton team_plus_btn;
        private MetroFramework.Controls.MetroButton team_remove_btn;
        private MetroFramework.Controls.MetroButton instrument_remove_all_btn;
        private MetroFramework.Controls.MetroButton team_remove_all_btn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroButton equipment_issued_update;
        private System.Windows.Forms.RichTextBox address;
        private MetroFramework.Controls.MetroButton refresh_client;
        private MetroFramework.Controls.MetroDateTime from;
        private MetroFramework.Controls.MetroDateTime to;
        private MetroFramework.Controls.MetroButton fron_to_btn;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private MetroFramework.Controls.MetroDateTime created_at;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox11;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroButton instrument_refresh;
        private System.Windows.Forms.DataGridView instrument_list;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroButton instrument_save;
        private System.Windows.Forms.GroupBox groupBox14;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroTextBox instrument_name;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.ComboBox instrument_status;
        private MetroFramework.Controls.MetroTextBox instrument_life;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private MetroFramework.Controls.MetroButton instrumnets_selection_update;
        private MetroFramework.Controls.MetroButton team_selection_update;
        private MetroFramework.Controls.MetroButton site_update;
        private MetroFramework.Controls.MetroButton print;
        private System.Windows.Forms.RichTextBox damage_instrument_description;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private System.Windows.Forms.Label label8;
        private MetroFramework.Controls.MetroButton barCodeCatcher_btn;
        private System.Windows.Forms.Label label9;
        private MetroFramework.Controls.MetroButton barCodeGridCatcher_btn;
        private System.Windows.Forms.TextBox barCodeGridCatcher;
        private System.Windows.Forms.GroupBox groupBox13;
        private MetroFramework.Controls.MetroButton consumable_barCodeCatcher_btn;
        private System.Windows.Forms.Label label10;
        private MetroFramework.Controls.MetroButton consumable_instrumnets_selection_update;
        private MetroFramework.Controls.MetroButton consumable_instrument_remove_all_btn;
        private MetroFramework.Controls.MetroButton consumable_instrument_plus_btn;
        private MetroFramework.Controls.MetroButton consumable_instrument_remove_btn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox quantity;
        private System.Windows.Forms.ListBox consumable_equipment_selection_list;
        private System.Windows.Forms.ComboBox consumable_equipment_selection;
        private MetroFramework.Controls.MetroButton instrument_delete;
        private System.Windows.Forms.Label instrument_type;
        private MetroFramework.Controls.MetroTextBox instrument_quantity;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private System.Windows.Forms.TextBox barCodeCatcher;
        private System.Windows.Forms.TextBox consumable_barCodeCatcher;
    }
}