﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class store_form : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        string path = Application.StartupPath + "\\images\\Instruments";
        string pathWithFileName = "";
        string oldpath = "";
        DataView dv;
        Roles userRoles = new Roles();
        public store_form()
        {
            InitializeComponent();
        }
        private void store_form_Load(object sender, EventArgs e)
        {
            if (service.rolesUser(login.userCredentials.id, "store_form") != null)
            {
                userRoles = service.rolesUser(login.userCredentials.id, "store_form");
            }
            preRequisiets();
        }

        private void preRequisiets()
        {            
            refresh();
        }
        private void refresh()
        {
            dv = new DataView(service.instrumentsList_dataTable());
            categoryList();
            brandList();
            InstrumentList();
            instrument_name.Text = "";
            instrument_brand_id.Text = "";
            instrument_category.Text = "Parent";
            instrument_image.Text = "";
            instrument_code.Text = "";
            instrument_save.Enabled = true;
            instrument_update.Enabled = false;
            instrument_delete.Enabled = false;
            instrument_status.Text = "OK";
            instrument_image_box.ImageLocation = "";
            servicable_instrument_description.Enabled = false;
        }

        private void InstrumentList()
        {
            try
            {
                instrument_list.DataSource = dv;
                gridViewProperties();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void brandList()
        {
            instrument_brand_id.SelectedIndex = 0;
            instrument_brand_id.DataSource = service.brandList_dataTable();
            instrument_brand_id.DisplayMember = "Name";
            instrument_brand_id.ValueMember = "id";
        }

        private void categoryList()
        {
            DataTable dt = new DataTable();
            dt = service.instrumentsParentsList_dataTable();
            commonHelper.addNewRow(dt,"Parent");
            instrument_category.SelectedIndex = 0;
            instrument_category.DataSource = dt;
            instrument_category.DisplayMember = "name";
            instrument_category.ValueMember = "id";
        }

        private bool Validation()
        {
            string[] param = {
                instrument_category.Text,
                instrument_code.Text,
                instrument_name.Text,
                instrument_brand_id.Text
            };
            return commonHelper.Validator(param);
        }
        private void instrument_save_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_add == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    string distination = path + "\\" + DateTime.Now.Ticks.ToString() + "_" + pathWithFileName;
                    Instruments instruments = new BS_SERVICES.Instruments();
                    instruments.parent_id = Convert.ToInt32(instrument_category.SelectedValue.ToString());
                    instruments.instrument_code = instrument_code.Text;
                    instruments.life = Convert.ToInt32(instrument_life.Text);
                    instruments.brand_id = Convert.ToInt32(instrument_brand_id.SelectedValue.ToString());
                    instruments.name = instrument_name.Text;
                    instruments.image = distination;
                    instruments.status = instrument_status.Text;
                    instruments.created_at = DateTime.Now.ToShortDateString();
                    instruments.updated_at = DateTime.Now.ToShortDateString();
                    if (instrument_type.Text== "Consumable")
                    {
                        instruments.quantity = Convert.ToInt32(instrument_quantity.Text);
                    }
                    else
                    {
                        instruments.quantity = 0;
                    }
                    instruments.type = instrument_type.Text;
                    service.instrumentsSave(instruments);
                    if (pathWithFileName!= "")
                    {
                        File.Copy(instrument_image.Text, distination);
                    }                    
                    MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }
        private void instrument_category_TextChanged(object sender, EventArgs e)
        {
            
            
        }

        private void gridViewProperties()
        {
            instrument_list.Columns[0].Visible = false;
            instrument_list.Columns[1].Width = 90;
            instrument_list.Columns[2].Width = 90;
            instrument_list.Columns[3].Width = 150;
            instrument_list.Columns[4].Width = 150;
            instrument_list.Columns[5].Width = 70;
            instrument_list.Columns[6].Width = 70;
            instrument_list.Columns[7].Width = 120;
            instrument_list.Columns[8].Visible = false;
            instrument_list.Columns[9].Width = 120;
        }

        private void instrument_image_Click(object sender, EventArgs e)
        {
            OpenFileDialog op1 = new OpenFileDialog();
            op1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            op1.Title = "Please Select An Image For Instrument Or Tools";
            op1.ShowDialog();
            instrument_image.Text = op1.FileName;
            pathWithFileName = op1.SafeFileName;
            instrument_image_box.ImageLocation = op1.FileName;
            commonHelper.isFolderExist(path);            
            
        }
        private void brand_refresh_Click(object sender, EventArgs e)
        {
            refresh();
        }
        private void filterGrid(string filter,string filterColum= "Name")
        {
            try
            {
                if (filterColum == "Life>")
                {
                    dv.RowFilter = "Life >=  '" + filter + "' ";
                }
                else if (filterColum == "<Life")
                {
                    dv.RowFilter = "Life <=  '" + filter + "' ";
                }
                else
                {                    
                    dv.RowFilter = "" + filterColum + " Like '%" + filter + "%'";
                }

                instrument_list.DataSource = dv;
                gridViewProperties();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void instrument_search_TextChanged(object sender, EventArgs e)
        {
            filterGrid(instrument_search.Text,"Name");
        }
        int instrument_id=0;
        private void instrument_list_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ShowDataInTextbox(instrument_list.CurrentRow.Index);                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        string status = "";
        private void ShowDataInTextbox(int index)
        {
            if (index == -1) { return; }
            instrument_id = Convert.ToInt32(instrument_list.Rows[index].Cells["id"].Value.ToString());
            instrument_category.Text = instrument_list.Rows[index].Cells["Category"].Value.ToString();
            instrument_code.Text = instrument_list.Rows[index].Cells["Code"].Value.ToString();
            instrument_life.Text = instrument_list.Rows[index].Cells["Life"].Value.ToString();
            instrument_name.Text = instrument_list.Rows[index].Cells["Name"].Value.ToString();
            instrument_brand_id.Text = instrument_list.Rows[index].Cells["Brand"].Value.ToString();
            instrument_image.Text = instrument_list.Rows[index].Cells["Image"].Value.ToString();
            instrument_image_box.ImageLocation = instrument_list.Rows[index].Cells["Image"].Value.ToString();
            oldpath= instrument_list.Rows[index].Cells["Image"].Value.ToString();
            instrument_status.Text = instrument_list.Rows[index].Cells["Status"].Value.ToString();
            status = instrument_list.Rows[index].Cells["Status"].Value.ToString();
            instrument_type.Text = instrument_list.Rows[index].Cells["Type"].Value.ToString();
            instrument_quantity.Text = instrument_list.Rows[index].Cells["quantity"].Value.ToString();
            instrument_save.Enabled = false;
            instrument_update.Enabled = true;
            instrument_delete.Enabled = true;
            servicable_instrument_description.Enabled = true;
        }
        private bool saveRepairs()
        {
            if (servicable_instrument_description.Text != "")
            {
                Repairs repairs = new Repairs();
                repairs.instrument_id = instrument_id;
                repairs.shop = "";
                repairs.person = "";
                repairs.description = servicable_instrument_description.Text;
                repairs.site_id = 0;
                service.repairsSave(repairs);
                return true;
            }
            else
            {
                MessageBox.Show("Please Fill Serviceable Instrument Description Field", messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }
        private void instrument_update_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_update == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (Validation())
                {
                    if (status == "Serviceable" || status=="Send To Repair" || status == "On Site")
                    {
                        MessageBox.Show("This Instrument con't be update", messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (instrument_status.Text == "Serviceable")
                    {
                        if (saveRepairs() == false)
                        {
                            return;
                        }
                    }
                    
                    string distination = path + "\\" + DateTime.Now.Ticks.ToString() + "_" + pathWithFileName;
                    Instruments instruments = new BS_SERVICES.Instruments();
                    instruments.parent_id = Convert.ToInt32(instrument_category.SelectedValue.ToString());
                    instruments.instrument_code = instrument_code.Text;
                    instruments.life = Convert.ToInt32(instrument_life.Text);
                    instruments.brand_id = Convert.ToInt32(instrument_brand_id.SelectedValue.ToString());
                    instruments.name = instrument_name.Text;
                    instruments.image = distination;
                    instruments.status = instrument_status.Text;
                    instruments.updated_at = DateTime.Now.ToShortDateString();
                    instruments.id = instrument_id;                    
                    if (instrument_image.Text != oldpath)
                    {
                        File.Copy(instrument_image.Text, distination);
                        File.Delete(oldpath);
                        instruments.image = distination;
                    }else
                    {
                        instruments.image = instrument_image.Text;
                    }
                    if (instrument_type.Text == "Consumable")
                    {
                        instruments.quantity = Convert.ToInt32(instrument_quantity.Text);
                    }
                    else
                    {
                        instruments.quantity = 0;
                    }
                    instruments.type = instrument_type.Text;
                    service.instrumentsUpdate(instruments);
                    MessageBox.Show(messages.update, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refresh();
                }
                else
                {
                    MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }

        private void instrument_delete_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "User" && userRoles.roles_delete == 0)
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (MessageBox.Show("Are you sure! you want to delete this record", "Alert", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    if (Validation())
                    {
                        string distination = path + "\\" + DateTime.Now.Ticks.ToString() + "_" + pathWithFileName;
                        Instruments instruments = new BS_SERVICES.Instruments();
                        instruments.status = "Un-Serviceable";
                        instruments.updated_at = DateTime.Now.ToShortDateString();
                        instruments.id = instrument_id;
                        service.instrumentsDelete(instruments);
                        MessageBox.Show(messages.delete, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        refresh();
                    }
                    else
                    {
                        MessageBox.Show(messages.required, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            catch (Exception fex)
            {
                MessageBox.Show(fex.Message);
            }
        }

        private void barCodeCatchar_TextChanged(object sender, EventArgs e)
        {
                filterGrid(barCodeCatchar.Text, "Code");
                //barCodeCatchar.Focus();
                //barCodeCatchar.SelectionStart = 0;
                //barCodeCatchar.SelectionLength = barCodeCatchar.Text.Length;
        }
        

        private void instrument_type_TextChanged(object sender, EventArgs e)
        {
            if (instrument_type.Text== "Consumable")
            {
                instrument_quantity.Enabled = true;
            }
            else
            {
                instrument_quantity.Enabled = false;
            }
        }

        private void search_category_TextChanged(object sender, EventArgs e)
        {
            filterGrid(search_category.Text, "Category");
        }

        private void search_life_greater_TextChanged(object sender, EventArgs e)
        {
            filterGrid(search_life_greater.Text, "Life>");
        }

        private void search_lifelesser_TextChanged(object sender, EventArgs e)
        {
            filterGrid(search_lifelesser.Text, "<Life");
        }

        private void search_brand_TextChanged(object sender, EventArgs e)
        {
            filterGrid(search_brand.Text, "Brand");
        }

        private void search_type_TextChanged(object sender, EventArgs e)
        {
            filterGrid(search_type.Text, "Type");
        }
    }
}
