﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class bin_location_form : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        DateTimePicker dtp = new DateTimePicker();
        Rectangle _rectangle;
        string fileName = "";
        string fileSource = "";
        public bin_location_form()
        {
            InitializeComponent();

        }

        private void bin_location_form_Load(object sender, EventArgs e)
        {
            employeesList();
            bin_location_list.Controls.Add(dtp);
            dtp.Visible = false;
            dtp.Format = DateTimePickerFormat.Short;
            dtp.TextChanged+=new EventHandler(dtp_TexChange);
            binLocationList("","","");
        }
        private void employeesList()
        {
            DataTable client_dt = service.dropdown("clients", 0);//0 just for clients
            commonHelper.addNewRow(client_dt);
            client_id.DataSource = client_dt;
            client_id.DisplayMember = "name";
            client_id.ValueMember = "id";
            DataTable consultant_dt = service.dropdown("clients", 1);//0 just for consultants
            commonHelper.addNewRow(consultant_dt);
            consultant_id.DataSource = consultant_dt;
            consultant_id.DisplayMember = "name";
            consultant_id.ValueMember = "id";

            DataTable search_client_dt = service.dropdown("clients", 0);//0 just for clients
            commonHelper.addNewRow(search_client_dt);
            search_client.DataSource = search_client_dt;
            search_client.DisplayMember = "name";
            search_client.ValueMember = "id";
            DataTable search_consultant_dt = service.dropdown("clients", 1);//0 just for consultants
            commonHelper.addNewRow(search_consultant_dt);
            search_consultant.DataSource = search_consultant_dt;
            search_consultant.DisplayMember = "name";
            search_consultant.ValueMember = "id";
        }

        private void bin_location_list_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {               
                saveBin(e.RowIndex);
            }
        }
        private void saveBin(int index)
        {
            try
            {
                BinLocation binLocation = new BS_SERVICES.BinLocation();
                binLocation.id = bin_location_list.Rows[index].Cells["id"].Value != null ? Convert.ToInt32(bin_location_list.Rows[index].Cells["id"].Value.ToString()) : 0;
                binLocation.project = bin_location_list.Rows[index].Cells["project"].Value != null ? bin_location_list.Rows[index].Cells["project"].Value.ToString() : "";
                binLocation.client_id = bin_location_list.Rows[index].Cells["client_id"].Value != null ? Convert.ToInt32(bin_location_list.Rows[index].Cells["client_id"].Value.ToString()) : 0;
                binLocation.cell = bin_location_list.Rows[index].Cells["cell"].Value != null ? bin_location_list.Rows[index].Cells["cell"].Value.ToString() : "";
                binLocation.name = bin_location_list.Rows[index].Cells["name"].Value != null ? bin_location_list.Rows[index].Cells["name"].Value.ToString() : "";
                binLocation.consultant_id = bin_location_list.Rows[index].Cells["consultant_id"].Value != null ? Convert.ToInt32(bin_location_list.Rows[index].Cells["consultant_id"].Value.ToString()) : 0;
                binLocation.type = bin_location_list.Rows[index].Cells["type"].Value != null ? bin_location_list.Rows[index].Cells["type"].Value.ToString() : "";
                binLocation.year = bin_location_list.Rows[index].Cells["year"].Value != null ? bin_location_list.Rows[index].Cells["year"].Value.ToString() : "";
                binLocation.folder = bin_location_list.Rows[index].Cells["folder"].Value != null ? bin_location_list.Rows[index].Cells["folder"].Value.ToString() : "";
                binLocation.print_report = bin_location_list.Rows[index].Cells["print_report"].Value != null ? convertBoolToInt(Convert.ToBoolean(bin_location_list.Rows[index].Cells["print_report"].Value.ToString())) : 0;                
                binLocation.print_date = bin_location_list.Rows[index].Cells["print_date"].Value != null ? bin_location_list.Rows[index].Cells["print_date"].Value.ToString() : "";                
                binLocation.email = bin_location_list.Rows[index].Cells["email"].Value != null ? convertBoolToInt(Convert.ToBoolean(bin_location_list.Rows[index].Cells["email"].Value.ToString())) : 0;
                binLocation.email_date = bin_location_list.Rows[index].Cells["email_date"].Value != null ? bin_location_list.Rows[index].Cells["email_date"].Value.ToString() : "";
                binLocation.payments = bin_location_list.Rows[index].Cells["payments"].Value != null ? convertBoolToInt(Convert.ToBoolean(bin_location_list.Rows[index].Cells["payments"].Value.ToString())) : 0;                
                binLocation.payment_advance = bin_location_list.Rows[index].Cells["payment_advance"].Value != null ? convertBoolToInt(Convert.ToBoolean(bin_location_list.Rows[index].Cells["payment_advance"].Value.ToString())) : 0;                
                binLocation.payment_balance = bin_location_list.Rows[index].Cells["payment_balance"].Value != null ? convertBoolToInt(Convert.ToBoolean(bin_location_list.Rows[index].Cells["payment_balance"].Value.ToString())) : 0;
                binLocation.payment_date = bin_location_list.Rows[index].Cells["payment_date"].Value != null ? bin_location_list.Rows[index].Cells["payment_date"].Value.ToString() : "";                
                binLocation.delivery = bin_location_list.Rows[index].Cells["delivery"].Value != null ? convertBoolToInt(Convert.ToBoolean(bin_location_list.Rows[index].Cells["delivery"].Value.ToString())) : 0;
                binLocation.delivery_date = bin_location_list.Rows[index].Cells["delivery_date"].Value != null ? bin_location_list.Rows[index].Cells["delivery_date"].Value.ToString() : "";                
                binLocation.report = bin_location_list.Rows[index].Cells["report"].Value != null ? bin_location_list.Rows[index].Cells["report"].Value.ToString() : "";
                copyFiles(fileSource, binLocation.report);
                if (binLocation.id != 0)
                {
                    service.binLocationUpdate(binLocation);
                    MessageBox.Show("Record updated Successfully");
                }
                else {
                    service.binLocationSave(binLocation);
                    MessageBox.Show("New Record Saved Successfully");
                }
                binLocationList("","","");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }

        }
        BinLocation[] binLocation_list;
        private void binLocationList(string column, string search, string type)
        {
            try
            {
                binLocation_list = service.binLocationList(column, search, type);
                if (binLocation_list != null)
                {
                    bin_location_list.Rows.Clear();
                    bin_location_list.Refresh();
                    int i = 0;
                    foreach (var item in binLocation_list)
                    {
                        DataGridViewRow row = (DataGridViewRow)bin_location_list.Rows[0].Clone();
                        bin_location_list.Rows.Add(row);
                        bin_location_list.Rows[i].Cells["sr"].Value = i;
                        bin_location_list.Rows[i].Cells["id"].Value = item.id;
                        bin_location_list.Rows[i].Cells["project"].Value = item.project;
                        bin_location_list.Rows[i].Cells["client_id"].Value = item.client_id;
                        bin_location_list.Rows[i].Cells["cell"].Value = item.cell;
                        bin_location_list.Rows[i].Cells["name"].Value = item.name;
                        bin_location_list.Rows[i].Cells["consultant_id"].Value = item.consultant_id;
                        bin_location_list.Rows[i].Cells["type"].Value = item.type;
                        bin_location_list.Rows[i].Cells["year"].Value = item.year;
                        bin_location_list.Rows[i].Cells["folder"].Value = item.folder;
                        bin_location_list.Rows[i].Cells["print_report"].Value = item.print_report ==1 ? true : false;
                        bin_location_list.Rows[i].Cells["print_date"].Value = item.print_date;
                        bin_location_list.Rows[i].Cells["email"].Value = item.email == 1 ? true : false;
                        bin_location_list.Rows[i].Cells["email_date"].Value = item.email_date;
                        bin_location_list.Rows[i].Cells["payments"].Value = item.payments == 1 ? true : false;
                        bin_location_list.Rows[i].Cells["payment_advance"].Value = item.payment_advance == 1 ? true : false;
                        bin_location_list.Rows[i].Cells["payment_balance"].Value = item.payment_balance == 1 ? true : false;
                        bin_location_list.Rows[i].Cells["payment_date"].Value = item.payment_date;
                        bin_location_list.Rows[i].Cells["delivery"].Value = item.delivery == 1 ? true : false;
                        bin_location_list.Rows[i].Cells["delivery_date"].Value = item.delivery_date;
                        bin_location_list.Rows[i].Cells["report"].Value = item.report;
                        bin_location_list.Rows[i].Cells["save"].Value = "Save";
                        i = i + 1;
                    }
                }
                else
                {
                    MessageBox.Show("Record Not Found", messages.bs_worning, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private int convertBoolToInt(bool bo)
        {
            if (bo) {
                return 1;
            }
            else {
                return 0;
            }
        }

        private void bin_location_list_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex==-1) {
                return;
            }
            view_btn.Text = bin_location_list.Rows[e.RowIndex].Cells["report"].Value.ToString();
            switch (bin_location_list.Columns[e.ColumnIndex].Name) {
                case "print_date":
                    _rectangle = bin_location_list.GetCellDisplayRectangle(e.ColumnIndex,e.RowIndex, true);
                    dtp.Size = new Size(_rectangle.Width, _rectangle.Height);
                    dtp.Location = new Point(_rectangle.X,_rectangle.Y);
                    dtp.Visible = true;
                    break;
                case "email_date":
                    _rectangle = bin_location_list.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    dtp.Size = new Size(_rectangle.Width, _rectangle.Height);
                    dtp.Location = new Point(_rectangle.X, _rectangle.Y);
                    dtp.Visible = true;
                    break;
                case "payment_date":
                    _rectangle = bin_location_list.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    dtp.Size = new Size(_rectangle.Width, _rectangle.Height);
                    dtp.Location = new Point(_rectangle.X, _rectangle.Y);
                    dtp.Visible = true;
                    break;
                case "delivery_date":
                    _rectangle = bin_location_list.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    dtp.Size = new Size(_rectangle.Width, _rectangle.Height);
                    dtp.Location = new Point(_rectangle.X, _rectangle.Y);
                    dtp.Visible = true;
                    break;
                case "report":
                    OpenFileDialog op1 = new OpenFileDialog();
                    op1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                    op1.Title = "Please Select An Image For Instrument Or Tools";
                    op1.ShowDialog();
                    bin_location_list.CurrentCell.Value = System.Environment.MachineName + "-" + DateTime.Now.ToString("MM-dd-yyyy h-mm-tt") + op1.SafeFileName;
                    fileSource = op1.FileName;
                    fileName = op1.SafeFileName;
                    break;
            }
        }
        private void dtp_TexChange(object sender,EventArgs e)
        {
            bin_location_list.CurrentCell.Value = dtp.Text.ToString();
        }

        private void bin_location_list_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            dtp.Visible = false;
        }

        private void bin_location_list_Scroll(object sender, ScrollEventArgs e)
        {
            dtp.Visible = false;
        }

        private void column_TextChanged(object sender, EventArgs e)
        {
            if (column.Text == "client")
            {
                search_client.Visible = true;
                search.Visible = false;
                search_consultant.Visible = false;
                search_type.Visible = false;
                search_date.Visible = false;
            }
            else if (column.Text == "type")
            {
                search_type.Visible = true;
                search_client.Visible = false;
                search.Visible = false;
                search_consultant.Visible = false;
                search_date.Visible = false;
            }
            else if (column.Text == "consultant")
            {
                search_consultant.Visible = true;
                search_type.Visible = false;
                search_client.Visible = false;
                search.Visible = false;
                search_date.Visible = false;
            }
            else if (column.Text == "print_date" || column.Text == "email_date" || column.Text == "delivery_date")
            {
                search_date.Visible = true;
                search_consultant.Visible = false;
                search_type.Visible = false;
                search_client.Visible = false;
                search.Visible = false;                
            }
            else
            {
                search.Visible = true;
                search_consultant.Visible = false;
                search_type.Visible = false;
                search_client.Visible = false;
                search_date.Visible = false;
            }
        }

        private void search_btn_Click(object sender, EventArgs e)
        {
            if (column.Text == "client")
            {
                binLocationList(column.Text, search_client.SelectedValue.ToString(), "client");
            }
            else if (column.Text == "type")
            {
                binLocationList(column.Text, search_type.Text, "type");
            }
            else if (column.Text == "consultant")
            {
                binLocationList(column.Text, search_consultant.SelectedValue.ToString(), "consultant");
            }
            else if (column.Text == "print_date" || column.Text == "email_date" || column.Text == "delivery_date")
            {
                binLocationList(column.Text, search_date.Text, "date");
            }
            else
            {
                binLocationList(column.Text, search.Text, "");
            }
        }
        private void copyFiles(string source,string fileName)
        {
            if (source != null) {               
                File.Copy(@source, @"\\BS-SERVER/images/" + fileName , true);
            }
        }

        private void exportToExcel_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            worksheet = workbook.Sheets["Sheet1"];
            worksheet = workbook.ActiveSheet;
            worksheet.Name = "Exported from gridview";
            for (int i = 1; i < bin_location_list.Columns.Count + 1; i++)
            {
                worksheet.Cells[1, i] = bin_location_list.Columns[i - 1].HeaderText;
            }
            for (int i = 0; i < bin_location_list.Rows.Count - 1; i++)
            {
                for (int j = 0; j < bin_location_list.Columns.Count; j++)
                {
                    worksheet.Cells[i + 2, j + 1] = bin_location_list.Rows[i].Cells[j].Value.ToString();
                }
            }
            var saveFileDialoge = new SaveFileDialog();
            saveFileDialoge.FileName = "output";
            saveFileDialoge.DefaultExt = ".xlsx";
            if (saveFileDialoge.ShowDialog()==DialogResult.OK) {
                workbook.SaveAs(saveFileDialoge.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            }
            app.Quit();
        }

        private void view_btn_Click(object sender, EventArgs e)
        {
            if (File.Exists(@"\\BS-SERVER\images\" + view_btn.Text))
            {
                Process.Start(@"\\BS-SERVER\images\" + view_btn.Text);
            }
            else
            {
                MessageBox.Show("Record not found", messages.bs_alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
        }
    }
}
