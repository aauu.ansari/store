﻿namespace StoreManagement.Views
{
    partial class roles_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(roles_form));
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.user_txt = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabController = new MetroFramework.Controls.MetroTabControl();
            this.AssignmentSlip = new MetroFramework.Controls.MetroTabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.slip_update = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.slip_view = new System.Windows.Forms.CheckBox();
            this.slip_delete = new System.Windows.Forms.CheckBox();
            this.slip_add = new System.Windows.Forms.CheckBox();
            this.slip_btn = new MetroFramework.Controls.MetroButton();
            this.Store = new MetroFramework.Controls.MetroTabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.store_view = new System.Windows.Forms.CheckBox();
            this.store_delete = new System.Windows.Forms.CheckBox();
            this.store_add = new System.Windows.Forms.CheckBox();
            this.store_update = new System.Windows.Forms.CheckBox();
            this.store_btn = new MetroFramework.Controls.MetroButton();
            this.Reporting = new MetroFramework.Controls.MetroTabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.reporting_view = new System.Windows.Forms.CheckBox();
            this.reporting_delete = new System.Windows.Forms.CheckBox();
            this.reporting_add = new System.Windows.Forms.CheckBox();
            this.reporting_update = new System.Windows.Forms.CheckBox();
            this.reporting_btn = new MetroFramework.Controls.MetroButton();
            this.User = new MetroFramework.Controls.MetroTabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.user_view = new System.Windows.Forms.CheckBox();
            this.user_delete = new System.Windows.Forms.CheckBox();
            this.user_add = new System.Windows.Forms.CheckBox();
            this.user_update = new System.Windows.Forms.CheckBox();
            this.user_btn = new MetroFramework.Controls.MetroButton();
            this.Brand = new MetroFramework.Controls.MetroTabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.brand_view = new System.Windows.Forms.CheckBox();
            this.brand_delete = new System.Windows.Forms.CheckBox();
            this.brand_add = new System.Windows.Forms.CheckBox();
            this.brand_update = new System.Windows.Forms.CheckBox();
            this.brand_btn = new MetroFramework.Controls.MetroButton();
            this.Team = new MetroFramework.Controls.MetroTabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.team_view = new System.Windows.Forms.CheckBox();
            this.team_delete = new System.Windows.Forms.CheckBox();
            this.team_add = new System.Windows.Forms.CheckBox();
            this.team_update = new System.Windows.Forms.CheckBox();
            this.team_btn = new MetroFramework.Controls.MetroButton();
            this.Client = new MetroFramework.Controls.MetroTabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.client_view = new System.Windows.Forms.CheckBox();
            this.client_delete = new System.Windows.Forms.CheckBox();
            this.client_add = new System.Windows.Forms.CheckBox();
            this.client_update = new System.Windows.Forms.CheckBox();
            this.client__btn = new MetroFramework.Controls.MetroButton();
            this.Repair = new MetroFramework.Controls.MetroTabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.send_to_repair_view = new System.Windows.Forms.CheckBox();
            this.send_to_repair_delete = new System.Windows.Forms.CheckBox();
            this.send_to_repair_add = new System.Windows.Forms.CheckBox();
            this.send_to_repair_update = new System.Windows.Forms.CheckBox();
            this.send_to_repair_btn = new MetroFramework.Controls.MetroButton();
            this.Serviceable = new MetroFramework.Controls.MetroTabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.serviceable_view = new System.Windows.Forms.CheckBox();
            this.serviceable_delete = new System.Windows.Forms.CheckBox();
            this.serviceable_add = new System.Windows.Forms.CheckBox();
            this.serviceable_update = new System.Windows.Forms.CheckBox();
            this.serviceable_btn = new MetroFramework.Controls.MetroButton();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.billing_view = new System.Windows.Forms.CheckBox();
            this.billing_delete = new System.Windows.Forms.CheckBox();
            this.billing_add = new System.Windows.Forms.CheckBox();
            this.billing_update = new System.Windows.Forms.CheckBox();
            this.billing_btn = new MetroFramework.Controls.MetroButton();
            this.groupBox6.SuspendLayout();
            this.tabController.SuspendLayout();
            this.AssignmentSlip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Store.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.Reporting.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.User.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.Brand.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.Team.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.Client.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.Repair.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.Serviceable.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.LightGray;
            this.groupBox6.Controls.Add(this.user_txt);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1143, 96);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = resources.GetString("groupBox6.Text");
            // 
            // user_txt
            // 
            this.user_txt.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user_txt.FormattingEnabled = true;
            this.user_txt.Location = new System.Drawing.Point(9, 59);
            this.user_txt.Margin = new System.Windows.Forms.Padding(7);
            this.user_txt.Name = "user_txt";
            this.user_txt.Size = new System.Drawing.Size(218, 27);
            this.user_txt.TabIndex = 72;
            this.user_txt.TextChanged += new System.EventHandler(this.user_txt_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(4155, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = resources.GetString("label2.Text");
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tabController
            // 
            this.tabController.Controls.Add(this.AssignmentSlip);
            this.tabController.Controls.Add(this.Store);
            this.tabController.Controls.Add(this.Reporting);
            this.tabController.Controls.Add(this.User);
            this.tabController.Controls.Add(this.Brand);
            this.tabController.Controls.Add(this.Team);
            this.tabController.Controls.Add(this.Client);
            this.tabController.Controls.Add(this.Repair);
            this.tabController.Controls.Add(this.Serviceable);
            this.tabController.Controls.Add(this.metroTabPage1);
            this.tabController.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabController.Location = new System.Drawing.Point(0, 96);
            this.tabController.Name = "tabController";
            this.tabController.SelectedIndex = 9;
            this.tabController.Size = new System.Drawing.Size(1143, 367);
            this.tabController.TabIndex = 2;
            this.tabController.UseSelectable = true;
            // 
            // AssignmentSlip
            // 
            this.AssignmentSlip.Controls.Add(this.groupBox1);
            this.AssignmentSlip.Controls.Add(this.slip_btn);
            this.AssignmentSlip.HorizontalScrollbarBarColor = true;
            this.AssignmentSlip.HorizontalScrollbarHighlightOnWheel = false;
            this.AssignmentSlip.HorizontalScrollbarSize = 10;
            this.AssignmentSlip.Location = new System.Drawing.Point(4, 38);
            this.AssignmentSlip.Name = "AssignmentSlip";
            this.AssignmentSlip.Size = new System.Drawing.Size(1135, 325);
            this.AssignmentSlip.TabIndex = 0;
            this.AssignmentSlip.Text = "Assignment Slip";
            this.AssignmentSlip.VerticalScrollbarBarColor = true;
            this.AssignmentSlip.VerticalScrollbarHighlightOnWheel = false;
            this.AssignmentSlip.VerticalScrollbarSize = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.slip_update);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.slip_view);
            this.groupBox1.Controls.Add(this.slip_delete);
            this.groupBox1.Controls.Add(this.slip_add);
            this.groupBox1.Location = new System.Drawing.Point(40, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 266);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // slip_update
            // 
            this.slip_update.AutoSize = true;
            this.slip_update.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.slip_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slip_update.Location = new System.Drawing.Point(10, 122);
            this.slip_update.Name = "slip_update";
            this.slip_update.Size = new System.Drawing.Size(138, 30);
            this.slip_update.TabIndex = 3;
            this.slip_update.Text = "Update Slip";
            this.slip_update.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 23);
            this.label1.TabIndex = 5;
            this.label1.Text = "Assignment Slip Permission";
            // 
            // slip_view
            // 
            this.slip_view.AutoSize = true;
            this.slip_view.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.slip_view.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slip_view.Location = new System.Drawing.Point(10, 52);
            this.slip_view.Name = "slip_view";
            this.slip_view.Size = new System.Drawing.Size(118, 30);
            this.slip_view.TabIndex = 0;
            this.slip_view.Text = "View Slip";
            this.slip_view.UseVisualStyleBackColor = true;
            // 
            // slip_delete
            // 
            this.slip_delete.AutoSize = true;
            this.slip_delete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.slip_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slip_delete.Location = new System.Drawing.Point(10, 157);
            this.slip_delete.Name = "slip_delete";
            this.slip_delete.Size = new System.Drawing.Size(131, 30);
            this.slip_delete.TabIndex = 4;
            this.slip_delete.Text = "Delete Slip";
            this.slip_delete.UseVisualStyleBackColor = true;
            // 
            // slip_add
            // 
            this.slip_add.AutoSize = true;
            this.slip_add.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.slip_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slip_add.Location = new System.Drawing.Point(10, 87);
            this.slip_add.Name = "slip_add";
            this.slip_add.Size = new System.Drawing.Size(155, 30);
            this.slip_add.TabIndex = 2;
            this.slip_add.Text = "Add New Slip";
            this.slip_add.UseVisualStyleBackColor = true;
            // 
            // slip_btn
            // 
            this.slip_btn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.slip_btn.Location = new System.Drawing.Point(0, 289);
            this.slip_btn.Name = "slip_btn";
            this.slip_btn.Size = new System.Drawing.Size(1135, 36);
            this.slip_btn.TabIndex = 1;
            this.slip_btn.Text = "SAVE";
            this.slip_btn.UseSelectable = true;
            this.slip_btn.Click += new System.EventHandler(this.slip_btn_Click);
            // 
            // Store
            // 
            this.Store.Controls.Add(this.groupBox2);
            this.Store.Controls.Add(this.store_btn);
            this.Store.HorizontalScrollbarBarColor = true;
            this.Store.HorizontalScrollbarHighlightOnWheel = false;
            this.Store.HorizontalScrollbarSize = 10;
            this.Store.Location = new System.Drawing.Point(4, 38);
            this.Store.Name = "Store";
            this.Store.Size = new System.Drawing.Size(1135, 325);
            this.Store.TabIndex = 2;
            this.Store.Text = "Store";
            this.Store.VerticalScrollbarBarColor = true;
            this.Store.VerticalScrollbarHighlightOnWheel = false;
            this.Store.VerticalScrollbarSize = 10;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.store_view);
            this.groupBox2.Controls.Add(this.store_delete);
            this.groupBox2.Controls.Add(this.store_add);
            this.groupBox2.Controls.Add(this.store_update);
            this.groupBox2.Location = new System.Drawing.Point(40, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 266);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 23);
            this.label4.TabIndex = 15;
            this.label4.Text = "Instruments Permission";
            // 
            // store_view
            // 
            this.store_view.AutoSize = true;
            this.store_view.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.store_view.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.store_view.Location = new System.Drawing.Point(10, 52);
            this.store_view.Name = "store_view";
            this.store_view.Size = new System.Drawing.Size(221, 30);
            this.store_view.TabIndex = 11;
            this.store_view.Text = "View Instruments List";
            this.store_view.UseVisualStyleBackColor = true;
            // 
            // store_delete
            // 
            this.store_delete.AutoSize = true;
            this.store_delete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.store_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.store_delete.Location = new System.Drawing.Point(10, 157);
            this.store_delete.Name = "store_delete";
            this.store_delete.Size = new System.Drawing.Size(204, 30);
            this.store_delete.TabIndex = 14;
            this.store_delete.Text = "Delete Instruments ";
            this.store_delete.UseVisualStyleBackColor = true;
            // 
            // store_add
            // 
            this.store_add.AutoSize = true;
            this.store_add.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.store_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.store_add.Location = new System.Drawing.Point(10, 87);
            this.store_add.Name = "store_add";
            this.store_add.Size = new System.Drawing.Size(228, 30);
            this.store_add.TabIndex = 12;
            this.store_add.Text = "Add New Instruments ";
            this.store_add.UseVisualStyleBackColor = true;
            // 
            // store_update
            // 
            this.store_update.AutoSize = true;
            this.store_update.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.store_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.store_update.Location = new System.Drawing.Point(10, 122);
            this.store_update.Name = "store_update";
            this.store_update.Size = new System.Drawing.Size(211, 30);
            this.store_update.TabIndex = 13;
            this.store_update.Text = "Update Instruments ";
            this.store_update.UseVisualStyleBackColor = true;
            // 
            // store_btn
            // 
            this.store_btn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.store_btn.Location = new System.Drawing.Point(0, 289);
            this.store_btn.Name = "store_btn";
            this.store_btn.Size = new System.Drawing.Size(1135, 36);
            this.store_btn.TabIndex = 1;
            this.store_btn.Text = "SAVE";
            this.store_btn.UseSelectable = true;
            this.store_btn.Click += new System.EventHandler(this.store_btn_Click);
            // 
            // Reporting
            // 
            this.Reporting.Controls.Add(this.groupBox3);
            this.Reporting.Controls.Add(this.reporting_btn);
            this.Reporting.HorizontalScrollbarBarColor = true;
            this.Reporting.HorizontalScrollbarHighlightOnWheel = false;
            this.Reporting.HorizontalScrollbarSize = 10;
            this.Reporting.Location = new System.Drawing.Point(4, 38);
            this.Reporting.Name = "Reporting";
            this.Reporting.Size = new System.Drawing.Size(1135, 325);
            this.Reporting.TabIndex = 4;
            this.Reporting.Text = "Reporting";
            this.Reporting.VerticalScrollbarBarColor = true;
            this.Reporting.VerticalScrollbarHighlightOnWheel = false;
            this.Reporting.VerticalScrollbarSize = 10;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.reporting_view);
            this.groupBox3.Controls.Add(this.reporting_delete);
            this.groupBox3.Controls.Add(this.reporting_add);
            this.groupBox3.Controls.Add(this.reporting_update);
            this.groupBox3.Location = new System.Drawing.Point(40, 20);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(260, 266);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(173, 23);
            this.label6.TabIndex = 25;
            this.label6.Text = "Reporting Permission";
            // 
            // reporting_view
            // 
            this.reporting_view.AutoSize = true;
            this.reporting_view.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.reporting_view.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reporting_view.Location = new System.Drawing.Point(10, 52);
            this.reporting_view.Name = "reporting_view";
            this.reporting_view.Size = new System.Drawing.Size(203, 30);
            this.reporting_view.TabIndex = 21;
            this.reporting_view.Text = "View Reporting List";
            this.reporting_view.UseVisualStyleBackColor = true;
            // 
            // reporting_delete
            // 
            this.reporting_delete.AutoSize = true;
            this.reporting_delete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.reporting_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reporting_delete.Location = new System.Drawing.Point(10, 157);
            this.reporting_delete.Name = "reporting_delete";
            this.reporting_delete.Size = new System.Drawing.Size(181, 30);
            this.reporting_delete.TabIndex = 24;
            this.reporting_delete.Text = "Delete Reporting";
            this.reporting_delete.UseVisualStyleBackColor = true;
            // 
            // reporting_add
            // 
            this.reporting_add.AutoSize = true;
            this.reporting_add.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.reporting_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reporting_add.Location = new System.Drawing.Point(10, 87);
            this.reporting_add.Name = "reporting_add";
            this.reporting_add.Size = new System.Drawing.Size(205, 30);
            this.reporting_add.TabIndex = 22;
            this.reporting_add.Text = "Add New Reporting";
            this.reporting_add.UseVisualStyleBackColor = true;
            // 
            // reporting_update
            // 
            this.reporting_update.AutoSize = true;
            this.reporting_update.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.reporting_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reporting_update.Location = new System.Drawing.Point(10, 122);
            this.reporting_update.Name = "reporting_update";
            this.reporting_update.Size = new System.Drawing.Size(188, 30);
            this.reporting_update.TabIndex = 23;
            this.reporting_update.Text = "Update Reporting";
            this.reporting_update.UseVisualStyleBackColor = true;
            // 
            // reporting_btn
            // 
            this.reporting_btn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.reporting_btn.Location = new System.Drawing.Point(0, 289);
            this.reporting_btn.Name = "reporting_btn";
            this.reporting_btn.Size = new System.Drawing.Size(1135, 36);
            this.reporting_btn.TabIndex = 1;
            this.reporting_btn.Text = "SAVE";
            this.reporting_btn.UseSelectable = true;
            this.reporting_btn.Click += new System.EventHandler(this.reporting_btn_Click);
            // 
            // User
            // 
            this.User.Controls.Add(this.groupBox4);
            this.User.Controls.Add(this.user_btn);
            this.User.HorizontalScrollbarBarColor = true;
            this.User.HorizontalScrollbarHighlightOnWheel = false;
            this.User.HorizontalScrollbarSize = 10;
            this.User.Location = new System.Drawing.Point(4, 38);
            this.User.Name = "User";
            this.User.Size = new System.Drawing.Size(1135, 325);
            this.User.TabIndex = 8;
            this.User.Text = "User";
            this.User.VerticalScrollbarBarColor = true;
            this.User.VerticalScrollbarHighlightOnWheel = false;
            this.User.VerticalScrollbarSize = 10;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.user_view);
            this.groupBox4.Controls.Add(this.user_delete);
            this.groupBox4.Controls.Add(this.user_add);
            this.groupBox4.Controls.Add(this.user_update);
            this.groupBox4.Location = new System.Drawing.Point(40, 20);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(260, 266);
            this.groupBox4.TabIndex = 32;
            this.groupBox4.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 23);
            this.label10.TabIndex = 31;
            this.label10.Text = "User Permission";
            // 
            // user_view
            // 
            this.user_view.AutoSize = true;
            this.user_view.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.user_view.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user_view.Location = new System.Drawing.Point(10, 52);
            this.user_view.Name = "user_view";
            this.user_view.Size = new System.Drawing.Size(161, 30);
            this.user_view.TabIndex = 27;
            this.user_view.Text = "View User List";
            this.user_view.UseVisualStyleBackColor = true;
            // 
            // user_delete
            // 
            this.user_delete.AutoSize = true;
            this.user_delete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.user_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user_delete.Location = new System.Drawing.Point(10, 157);
            this.user_delete.Name = "user_delete";
            this.user_delete.Size = new System.Drawing.Size(139, 30);
            this.user_delete.TabIndex = 30;
            this.user_delete.Text = "Delete User";
            this.user_delete.UseVisualStyleBackColor = true;
            // 
            // user_add
            // 
            this.user_add.AutoSize = true;
            this.user_add.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.user_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user_add.Location = new System.Drawing.Point(10, 87);
            this.user_add.Name = "user_add";
            this.user_add.Size = new System.Drawing.Size(163, 30);
            this.user_add.TabIndex = 28;
            this.user_add.Text = "Add New User";
            this.user_add.UseVisualStyleBackColor = true;
            // 
            // user_update
            // 
            this.user_update.AutoSize = true;
            this.user_update.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.user_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user_update.Location = new System.Drawing.Point(10, 122);
            this.user_update.Name = "user_update";
            this.user_update.Size = new System.Drawing.Size(146, 30);
            this.user_update.TabIndex = 29;
            this.user_update.Text = "Update User";
            this.user_update.UseVisualStyleBackColor = true;
            // 
            // user_btn
            // 
            this.user_btn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.user_btn.Location = new System.Drawing.Point(0, 289);
            this.user_btn.Name = "user_btn";
            this.user_btn.Size = new System.Drawing.Size(1135, 36);
            this.user_btn.TabIndex = 26;
            this.user_btn.Text = "SAVE";
            this.user_btn.UseSelectable = true;
            this.user_btn.Click += new System.EventHandler(this.user_btn_Click);
            // 
            // Brand
            // 
            this.Brand.Controls.Add(this.groupBox5);
            this.Brand.Controls.Add(this.brand_btn);
            this.Brand.HorizontalScrollbarBarColor = true;
            this.Brand.HorizontalScrollbarHighlightOnWheel = false;
            this.Brand.HorizontalScrollbarSize = 10;
            this.Brand.Location = new System.Drawing.Point(4, 38);
            this.Brand.Name = "Brand";
            this.Brand.Size = new System.Drawing.Size(1135, 325);
            this.Brand.TabIndex = 1;
            this.Brand.Text = "Brand";
            this.Brand.VerticalScrollbarBarColor = true;
            this.Brand.VerticalScrollbarHighlightOnWheel = false;
            this.Brand.VerticalScrollbarSize = 10;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.brand_view);
            this.groupBox5.Controls.Add(this.brand_delete);
            this.groupBox5.Controls.Add(this.brand_add);
            this.groupBox5.Controls.Add(this.brand_update);
            this.groupBox5.Location = new System.Drawing.Point(40, 20);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(260, 266);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 23);
            this.label3.TabIndex = 10;
            this.label3.Text = "Brand Permission";
            // 
            // brand_view
            // 
            this.brand_view.AutoSize = true;
            this.brand_view.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.brand_view.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brand_view.Location = new System.Drawing.Point(10, 52);
            this.brand_view.Name = "brand_view";
            this.brand_view.Size = new System.Drawing.Size(172, 30);
            this.brand_view.TabIndex = 6;
            this.brand_view.Text = "View Brand List";
            this.brand_view.UseVisualStyleBackColor = true;
            // 
            // brand_delete
            // 
            this.brand_delete.AutoSize = true;
            this.brand_delete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.brand_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brand_delete.Location = new System.Drawing.Point(10, 157);
            this.brand_delete.Name = "brand_delete";
            this.brand_delete.Size = new System.Drawing.Size(150, 30);
            this.brand_delete.TabIndex = 9;
            this.brand_delete.Text = "Delete Brand";
            this.brand_delete.UseVisualStyleBackColor = true;
            // 
            // brand_add
            // 
            this.brand_add.AutoSize = true;
            this.brand_add.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.brand_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brand_add.Location = new System.Drawing.Point(10, 87);
            this.brand_add.Name = "brand_add";
            this.brand_add.Size = new System.Drawing.Size(174, 30);
            this.brand_add.TabIndex = 7;
            this.brand_add.Text = "Add New Brand";
            this.brand_add.UseVisualStyleBackColor = true;
            // 
            // brand_update
            // 
            this.brand_update.AutoSize = true;
            this.brand_update.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.brand_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brand_update.Location = new System.Drawing.Point(10, 122);
            this.brand_update.Name = "brand_update";
            this.brand_update.Size = new System.Drawing.Size(157, 30);
            this.brand_update.TabIndex = 8;
            this.brand_update.Text = "Update Brand";
            this.brand_update.UseVisualStyleBackColor = true;
            // 
            // brand_btn
            // 
            this.brand_btn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.brand_btn.Location = new System.Drawing.Point(0, 289);
            this.brand_btn.Name = "brand_btn";
            this.brand_btn.Size = new System.Drawing.Size(1135, 36);
            this.brand_btn.TabIndex = 1;
            this.brand_btn.Text = "SAVE";
            this.brand_btn.UseSelectable = true;
            this.brand_btn.Click += new System.EventHandler(this.brand_btn_Click);
            // 
            // Team
            // 
            this.Team.Controls.Add(this.groupBox7);
            this.Team.Controls.Add(this.team_btn);
            this.Team.HorizontalScrollbarBarColor = true;
            this.Team.HorizontalScrollbarHighlightOnWheel = false;
            this.Team.HorizontalScrollbarSize = 10;
            this.Team.Location = new System.Drawing.Point(4, 38);
            this.Team.Name = "Team";
            this.Team.Size = new System.Drawing.Size(1135, 325);
            this.Team.TabIndex = 3;
            this.Team.Text = "Team";
            this.Team.VerticalScrollbarBarColor = true;
            this.Team.VerticalScrollbarHighlightOnWheel = false;
            this.Team.VerticalScrollbarSize = 10;
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.team_view);
            this.groupBox7.Controls.Add(this.team_delete);
            this.groupBox7.Controls.Add(this.team_add);
            this.groupBox7.Controls.Add(this.team_update);
            this.groupBox7.Location = new System.Drawing.Point(40, 20);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(260, 266);
            this.groupBox7.TabIndex = 21;
            this.groupBox7.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 23);
            this.label5.TabIndex = 20;
            this.label5.Text = "Team Permission";
            // 
            // team_view
            // 
            this.team_view.AutoSize = true;
            this.team_view.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.team_view.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.team_view.Location = new System.Drawing.Point(10, 52);
            this.team_view.Name = "team_view";
            this.team_view.Size = new System.Drawing.Size(171, 30);
            this.team_view.TabIndex = 16;
            this.team_view.Text = "View Team List";
            this.team_view.UseVisualStyleBackColor = true;
            // 
            // team_delete
            // 
            this.team_delete.AutoSize = true;
            this.team_delete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.team_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.team_delete.Location = new System.Drawing.Point(10, 157);
            this.team_delete.Name = "team_delete";
            this.team_delete.Size = new System.Drawing.Size(149, 30);
            this.team_delete.TabIndex = 19;
            this.team_delete.Text = "Delete Team";
            this.team_delete.UseVisualStyleBackColor = true;
            // 
            // team_add
            // 
            this.team_add.AutoSize = true;
            this.team_add.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.team_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.team_add.Location = new System.Drawing.Point(10, 87);
            this.team_add.Name = "team_add";
            this.team_add.Size = new System.Drawing.Size(173, 30);
            this.team_add.TabIndex = 17;
            this.team_add.Text = "Add New Team";
            this.team_add.UseVisualStyleBackColor = true;
            // 
            // team_update
            // 
            this.team_update.AutoSize = true;
            this.team_update.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.team_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.team_update.Location = new System.Drawing.Point(10, 122);
            this.team_update.Name = "team_update";
            this.team_update.Size = new System.Drawing.Size(156, 30);
            this.team_update.TabIndex = 18;
            this.team_update.Text = "Update Team";
            this.team_update.UseVisualStyleBackColor = true;
            // 
            // team_btn
            // 
            this.team_btn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.team_btn.Location = new System.Drawing.Point(0, 289);
            this.team_btn.Name = "team_btn";
            this.team_btn.Size = new System.Drawing.Size(1135, 36);
            this.team_btn.TabIndex = 1;
            this.team_btn.Text = "SAVE";
            this.team_btn.UseSelectable = true;
            this.team_btn.Click += new System.EventHandler(this.team_btn_Click);
            // 
            // Client
            // 
            this.Client.Controls.Add(this.groupBox8);
            this.Client.Controls.Add(this.client__btn);
            this.Client.HorizontalScrollbarBarColor = true;
            this.Client.HorizontalScrollbarHighlightOnWheel = false;
            this.Client.HorizontalScrollbarSize = 10;
            this.Client.Location = new System.Drawing.Point(4, 38);
            this.Client.Name = "Client";
            this.Client.Size = new System.Drawing.Size(1135, 325);
            this.Client.TabIndex = 6;
            this.Client.Text = "Client";
            this.Client.VerticalScrollbarBarColor = true;
            this.Client.VerticalScrollbarHighlightOnWheel = false;
            this.Client.VerticalScrollbarSize = 10;
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox8.Controls.Add(this.label8);
            this.groupBox8.Controls.Add(this.client_view);
            this.groupBox8.Controls.Add(this.client_delete);
            this.groupBox8.Controls.Add(this.client_add);
            this.groupBox8.Controls.Add(this.client_update);
            this.groupBox8.Location = new System.Drawing.Point(40, 20);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(260, 266);
            this.groupBox8.TabIndex = 32;
            this.groupBox8.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 23);
            this.label8.TabIndex = 31;
            this.label8.Text = "Client Permission";
            // 
            // client_view
            // 
            this.client_view.AutoSize = true;
            this.client_view.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.client_view.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_view.Location = new System.Drawing.Point(10, 52);
            this.client_view.Name = "client_view";
            this.client_view.Size = new System.Drawing.Size(170, 30);
            this.client_view.TabIndex = 27;
            this.client_view.Text = "View Client List";
            this.client_view.UseVisualStyleBackColor = true;
            // 
            // client_delete
            // 
            this.client_delete.AutoSize = true;
            this.client_delete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.client_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_delete.Location = new System.Drawing.Point(10, 157);
            this.client_delete.Name = "client_delete";
            this.client_delete.Size = new System.Drawing.Size(148, 30);
            this.client_delete.TabIndex = 30;
            this.client_delete.Text = "Delete Client";
            this.client_delete.UseVisualStyleBackColor = true;
            // 
            // client_add
            // 
            this.client_add.AutoSize = true;
            this.client_add.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.client_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_add.Location = new System.Drawing.Point(10, 87);
            this.client_add.Name = "client_add";
            this.client_add.Size = new System.Drawing.Size(172, 30);
            this.client_add.TabIndex = 28;
            this.client_add.Text = "Add New Client";
            this.client_add.UseVisualStyleBackColor = true;
            // 
            // client_update
            // 
            this.client_update.AutoSize = true;
            this.client_update.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.client_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.client_update.Location = new System.Drawing.Point(10, 122);
            this.client_update.Name = "client_update";
            this.client_update.Size = new System.Drawing.Size(155, 30);
            this.client_update.TabIndex = 29;
            this.client_update.Text = "Update Client";
            this.client_update.UseVisualStyleBackColor = true;
            // 
            // client__btn
            // 
            this.client__btn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.client__btn.Location = new System.Drawing.Point(0, 289);
            this.client__btn.Name = "client__btn";
            this.client__btn.Size = new System.Drawing.Size(1135, 36);
            this.client__btn.TabIndex = 26;
            this.client__btn.Text = "SAVE";
            this.client__btn.UseSelectable = true;
            this.client__btn.Click += new System.EventHandler(this.client__btn_Click);
            // 
            // Repair
            // 
            this.Repair.Controls.Add(this.groupBox9);
            this.Repair.Controls.Add(this.send_to_repair_btn);
            this.Repair.HorizontalScrollbarBarColor = true;
            this.Repair.HorizontalScrollbarHighlightOnWheel = false;
            this.Repair.HorizontalScrollbarSize = 10;
            this.Repair.Location = new System.Drawing.Point(4, 38);
            this.Repair.Name = "Repair";
            this.Repair.Size = new System.Drawing.Size(1135, 325);
            this.Repair.TabIndex = 7;
            this.Repair.Text = "Send To Repair";
            this.Repair.VerticalScrollbarBarColor = true;
            this.Repair.VerticalScrollbarHighlightOnWheel = false;
            this.Repair.VerticalScrollbarSize = 10;
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox9.Controls.Add(this.label9);
            this.groupBox9.Controls.Add(this.send_to_repair_view);
            this.groupBox9.Controls.Add(this.send_to_repair_delete);
            this.groupBox9.Controls.Add(this.send_to_repair_add);
            this.groupBox9.Controls.Add(this.send_to_repair_update);
            this.groupBox9.Location = new System.Drawing.Point(40, 20);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(260, 266);
            this.groupBox9.TabIndex = 32;
            this.groupBox9.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(217, 23);
            this.label9.TabIndex = 31;
            this.label9.Text = "Send To Repair  Permission";
            // 
            // send_to_repair_view
            // 
            this.send_to_repair_view.AutoSize = true;
            this.send_to_repair_view.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.send_to_repair_view.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.send_to_repair_view.Location = new System.Drawing.Point(10, 52);
            this.send_to_repair_view.Name = "send_to_repair_view";
            this.send_to_repair_view.Size = new System.Drawing.Size(257, 30);
            this.send_to_repair_view.TabIndex = 27;
            this.send_to_repair_view.Text = "View Send To Repair List";
            this.send_to_repair_view.UseVisualStyleBackColor = true;
            // 
            // send_to_repair_delete
            // 
            this.send_to_repair_delete.AutoSize = true;
            this.send_to_repair_delete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.send_to_repair_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.send_to_repair_delete.Location = new System.Drawing.Point(10, 157);
            this.send_to_repair_delete.Name = "send_to_repair_delete";
            this.send_to_repair_delete.Size = new System.Drawing.Size(93, 30);
            this.send_to_repair_delete.TabIndex = 30;
            this.send_to_repair_delete.Text = "Delete";
            this.send_to_repair_delete.UseVisualStyleBackColor = true;
            // 
            // send_to_repair_add
            // 
            this.send_to_repair_add.AutoSize = true;
            this.send_to_repair_add.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.send_to_repair_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.send_to_repair_add.Location = new System.Drawing.Point(10, 87);
            this.send_to_repair_add.Name = "send_to_repair_add";
            this.send_to_repair_add.Size = new System.Drawing.Size(78, 30);
            this.send_to_repair_add.TabIndex = 28;
            this.send_to_repair_add.Text = "Add ";
            this.send_to_repair_add.UseVisualStyleBackColor = true;
            // 
            // send_to_repair_update
            // 
            this.send_to_repair_update.AutoSize = true;
            this.send_to_repair_update.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.send_to_repair_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.send_to_repair_update.Location = new System.Drawing.Point(10, 122);
            this.send_to_repair_update.Name = "send_to_repair_update";
            this.send_to_repair_update.Size = new System.Drawing.Size(105, 30);
            this.send_to_repair_update.TabIndex = 29;
            this.send_to_repair_update.Text = "Update ";
            this.send_to_repair_update.UseVisualStyleBackColor = true;
            // 
            // send_to_repair_btn
            // 
            this.send_to_repair_btn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.send_to_repair_btn.Location = new System.Drawing.Point(0, 289);
            this.send_to_repair_btn.Name = "send_to_repair_btn";
            this.send_to_repair_btn.Size = new System.Drawing.Size(1135, 36);
            this.send_to_repair_btn.TabIndex = 26;
            this.send_to_repair_btn.Text = "SAVE";
            this.send_to_repair_btn.UseSelectable = true;
            this.send_to_repair_btn.Click += new System.EventHandler(this.send_to_repair_btn_Click);
            // 
            // Serviceable
            // 
            this.Serviceable.Controls.Add(this.groupBox10);
            this.Serviceable.Controls.Add(this.serviceable_btn);
            this.Serviceable.HorizontalScrollbarBarColor = true;
            this.Serviceable.HorizontalScrollbarHighlightOnWheel = false;
            this.Serviceable.HorizontalScrollbarSize = 10;
            this.Serviceable.Location = new System.Drawing.Point(4, 38);
            this.Serviceable.Name = "Serviceable";
            this.Serviceable.Size = new System.Drawing.Size(1135, 325);
            this.Serviceable.TabIndex = 5;
            this.Serviceable.Text = "Serviceable";
            this.Serviceable.VerticalScrollbarBarColor = true;
            this.Serviceable.VerticalScrollbarHighlightOnWheel = false;
            this.Serviceable.VerticalScrollbarSize = 10;
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox10.Controls.Add(this.label7);
            this.groupBox10.Controls.Add(this.serviceable_view);
            this.groupBox10.Controls.Add(this.serviceable_delete);
            this.groupBox10.Controls.Add(this.serviceable_add);
            this.groupBox10.Controls.Add(this.serviceable_update);
            this.groupBox10.Location = new System.Drawing.Point(40, 20);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(260, 266);
            this.groupBox10.TabIndex = 26;
            this.groupBox10.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(185, 23);
            this.label7.TabIndex = 25;
            this.label7.Text = "Serviceable Permission";
            // 
            // serviceable_view
            // 
            this.serviceable_view.AutoSize = true;
            this.serviceable_view.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.serviceable_view.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serviceable_view.Location = new System.Drawing.Point(10, 52);
            this.serviceable_view.Name = "serviceable_view";
            this.serviceable_view.Size = new System.Drawing.Size(223, 30);
            this.serviceable_view.TabIndex = 21;
            this.serviceable_view.Text = "View Serviceable List";
            this.serviceable_view.UseVisualStyleBackColor = true;
            // 
            // serviceable_delete
            // 
            this.serviceable_delete.AutoSize = true;
            this.serviceable_delete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.serviceable_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serviceable_delete.Location = new System.Drawing.Point(10, 157);
            this.serviceable_delete.Name = "serviceable_delete";
            this.serviceable_delete.Size = new System.Drawing.Size(201, 30);
            this.serviceable_delete.TabIndex = 24;
            this.serviceable_delete.Text = "Delete Serviceable";
            this.serviceable_delete.UseVisualStyleBackColor = true;
            // 
            // serviceable_add
            // 
            this.serviceable_add.AutoSize = true;
            this.serviceable_add.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.serviceable_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serviceable_add.Location = new System.Drawing.Point(10, 87);
            this.serviceable_add.Name = "serviceable_add";
            this.serviceable_add.Size = new System.Drawing.Size(225, 30);
            this.serviceable_add.TabIndex = 22;
            this.serviceable_add.Text = "Add New Serviceable";
            this.serviceable_add.UseVisualStyleBackColor = true;
            // 
            // serviceable_update
            // 
            this.serviceable_update.AutoSize = true;
            this.serviceable_update.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.serviceable_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serviceable_update.Location = new System.Drawing.Point(10, 122);
            this.serviceable_update.Name = "serviceable_update";
            this.serviceable_update.Size = new System.Drawing.Size(208, 30);
            this.serviceable_update.TabIndex = 23;
            this.serviceable_update.Text = "Update Serviceable";
            this.serviceable_update.UseVisualStyleBackColor = true;
            // 
            // serviceable_btn
            // 
            this.serviceable_btn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.serviceable_btn.Location = new System.Drawing.Point(0, 289);
            this.serviceable_btn.Name = "serviceable_btn";
            this.serviceable_btn.Size = new System.Drawing.Size(1135, 36);
            this.serviceable_btn.TabIndex = 1;
            this.serviceable_btn.Text = "SAVE";
            this.serviceable_btn.UseSelectable = true;
            this.serviceable_btn.Click += new System.EventHandler(this.serviceable_btn_Click);
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.billing_btn);
            this.metroTabPage1.Controls.Add(this.groupBox11);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(1135, 325);
            this.metroTabPage1.TabIndex = 9;
            this.metroTabPage1.Text = "Billing";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox11.Controls.Add(this.label11);
            this.groupBox11.Controls.Add(this.billing_view);
            this.groupBox11.Controls.Add(this.billing_delete);
            this.groupBox11.Controls.Add(this.billing_add);
            this.groupBox11.Controls.Add(this.billing_update);
            this.groupBox11.Location = new System.Drawing.Point(40, 20);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(260, 266);
            this.groupBox11.TabIndex = 27;
            this.groupBox11.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(148, 23);
            this.label11.TabIndex = 25;
            this.label11.Text = "Billing Permission";
            // 
            // billing_view
            // 
            this.billing_view.AutoSize = true;
            this.billing_view.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.billing_view.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billing_view.Location = new System.Drawing.Point(10, 52);
            this.billing_view.Name = "billing_view";
            this.billing_view.Size = new System.Drawing.Size(171, 30);
            this.billing_view.TabIndex = 21;
            this.billing_view.Text = "View Billing List";
            this.billing_view.UseVisualStyleBackColor = true;
            // 
            // billing_delete
            // 
            this.billing_delete.AutoSize = true;
            this.billing_delete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.billing_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billing_delete.Location = new System.Drawing.Point(10, 157);
            this.billing_delete.Name = "billing_delete";
            this.billing_delete.Size = new System.Drawing.Size(149, 30);
            this.billing_delete.TabIndex = 24;
            this.billing_delete.Text = "Delete Billing";
            this.billing_delete.UseVisualStyleBackColor = true;
            // 
            // billing_add
            // 
            this.billing_add.AutoSize = true;
            this.billing_add.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.billing_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billing_add.Location = new System.Drawing.Point(10, 87);
            this.billing_add.Name = "billing_add";
            this.billing_add.Size = new System.Drawing.Size(173, 30);
            this.billing_add.TabIndex = 22;
            this.billing_add.Text = "Add New Billing";
            this.billing_add.UseVisualStyleBackColor = true;
            // 
            // billing_update
            // 
            this.billing_update.AutoSize = true;
            this.billing_update.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.billing_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billing_update.Location = new System.Drawing.Point(10, 122);
            this.billing_update.Name = "billing_update";
            this.billing_update.Size = new System.Drawing.Size(156, 30);
            this.billing_update.TabIndex = 23;
            this.billing_update.Text = "Update Billing";
            this.billing_update.UseVisualStyleBackColor = true;
            // 
            // billing_btn
            // 
            this.billing_btn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.billing_btn.Location = new System.Drawing.Point(0, 289);
            this.billing_btn.Name = "billing_btn";
            this.billing_btn.Size = new System.Drawing.Size(1135, 36);
            this.billing_btn.TabIndex = 28;
            this.billing_btn.Text = "SAVE";
            this.billing_btn.UseSelectable = true;
            this.billing_btn.Click += new System.EventHandler(this.billing_btn_Click);
            // 
            // roles_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 463);
            this.Controls.Add(this.tabController);
            this.Controls.Add(this.groupBox6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "roles_form";
            this.Text = "User Roles Management";
            this.Load += new System.EventHandler(this.roles_form_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabController.ResumeLayout(false);
            this.AssignmentSlip.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Store.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.Reporting.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.User.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.Brand.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.Team.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.Client.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.Repair.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.Serviceable.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.metroTabPage1.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroTabControl tabController;
        private MetroFramework.Controls.MetroTabPage AssignmentSlip;
        private MetroFramework.Controls.MetroTabPage Brand;
        private MetroFramework.Controls.MetroTabPage Store;
        private MetroFramework.Controls.MetroTabPage Team;
        private MetroFramework.Controls.MetroTabPage Reporting;
        private MetroFramework.Controls.MetroTabPage Serviceable;
        private MetroFramework.Controls.MetroTabPage Client;
        private MetroFramework.Controls.MetroTabPage Repair;
        private MetroFramework.Controls.MetroTabPage User;
        private MetroFramework.Controls.MetroButton slip_btn;
        private System.Windows.Forms.CheckBox slip_view;
        private MetroFramework.Controls.MetroButton brand_btn;
        private MetroFramework.Controls.MetroButton store_btn;
        private MetroFramework.Controls.MetroButton team_btn;
        private MetroFramework.Controls.MetroButton reporting_btn;
        private MetroFramework.Controls.MetroButton serviceable_btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox slip_delete;
        private System.Windows.Forms.CheckBox slip_update;
        private System.Windows.Forms.CheckBox slip_add;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox brand_delete;
        private System.Windows.Forms.CheckBox brand_update;
        private System.Windows.Forms.CheckBox brand_add;
        private System.Windows.Forms.CheckBox brand_view;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox store_delete;
        private System.Windows.Forms.CheckBox store_update;
        private System.Windows.Forms.CheckBox store_add;
        private System.Windows.Forms.CheckBox store_view;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox team_delete;
        private System.Windows.Forms.CheckBox team_update;
        private System.Windows.Forms.CheckBox team_add;
        private System.Windows.Forms.CheckBox team_view;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox reporting_delete;
        private System.Windows.Forms.CheckBox reporting_update;
        private System.Windows.Forms.CheckBox reporting_add;
        private System.Windows.Forms.CheckBox reporting_view;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox serviceable_delete;
        private System.Windows.Forms.CheckBox serviceable_update;
        private System.Windows.Forms.CheckBox serviceable_add;
        private System.Windows.Forms.CheckBox serviceable_view;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox client_delete;
        private System.Windows.Forms.CheckBox client_update;
        private System.Windows.Forms.CheckBox client_add;
        private System.Windows.Forms.CheckBox client_view;
        private MetroFramework.Controls.MetroButton client__btn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox send_to_repair_delete;
        private System.Windows.Forms.CheckBox send_to_repair_update;
        private System.Windows.Forms.CheckBox send_to_repair_add;
        private System.Windows.Forms.CheckBox send_to_repair_view;
        private MetroFramework.Controls.MetroButton send_to_repair_btn;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox user_delete;
        private System.Windows.Forms.CheckBox user_update;
        private System.Windows.Forms.CheckBox user_add;
        private System.Windows.Forms.CheckBox user_view;
        private MetroFramework.Controls.MetroButton user_btn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ComboBox user_txt;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroButton billing_btn;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox billing_view;
        private System.Windows.Forms.CheckBox billing_delete;
        private System.Windows.Forms.CheckBox billing_add;
        private System.Windows.Forms.CheckBox billing_update;
    }
}