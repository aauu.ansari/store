﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class roles_form : Form
    {
        public roles_form()
        {
            InitializeComponent();
        } 

        Roles roles = new BS_SERVICES.Roles();
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        Roles userRoles = new Roles();
        Messages messages = new Messages();
        Roles[] Uroles;
        private void slip_btn_Click(object sender, EventArgs e)
        {
            try
            {
                saveRoles(Convert.ToInt32(user_txt.SelectedValue), slip_add, slip_update, slip_delete, slip_view, "assigment_slip_form");
                MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void saveRoles(int user_id, CheckBox add, CheckBox update, CheckBox delete, CheckBox view, string form)
        {
            service.rolesDelete(Convert.ToInt32(user_txt.SelectedValue), form);
            Roles roles = new BS_SERVICES.Roles();
            roles.login_id = user_id;
            roles.roles_add = checkBoxValidation(add);
            roles.roles_update = checkBoxValidation(update);
            roles.roles_delete = checkBoxValidation(delete);
            roles.roles_view = checkBoxValidation(view);
            roles.form = form;
            roles.created_at = DateTime.Now.ToShortDateString();
            service.rolesSave(roles);
        }
        private int checkBoxValidation(CheckBox text)
        {
            if (text.Checked)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        private void userCredentials(int login_id, string form)
        {
            try
            {
                if (service.rolesUser(login_id, form) != null)
                {
                    userRoles = service.rolesUser(login_id, form);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void roles_form_Load(object sender, EventArgs e)
        {
            user_txt.DataSource = service.userList_dt();
            user_txt.DisplayMember = "username";
            user_txt.ValueMember = "id";
        }
        private void clear()
        {
            store_add.Checked = false;
            store_update.Checked = false;
            store_delete.Checked = false;
            reporting_add.Checked = false;
            reporting_update.Checked = false;
            reporting_view.Checked = false;
            reporting_delete.Checked = false;
            user_add.Checked = false;
            user_update.Checked = false;
            user_view.Checked = false;
            user_delete.Checked = false;
            brand_add.Checked = false;
            brand_update.Checked = false;
            brand_view.Checked = false;
            brand_delete.Checked = false;
            team_add.Checked = false;
            team_update.Checked = false;
            team_view.Checked = false;
            team_delete.Checked = false;
            client_add.Checked = false;
            client_update.Checked = false;
            client_view.Checked = false;
            client_delete.Checked = false;
            send_to_repair_add.Checked = false;
            send_to_repair_update.Checked = false;
            send_to_repair_view.Checked = false;
            send_to_repair_delete.Checked = false;
            serviceable_add.Checked = false;
            serviceable_update.Checked = false;
            serviceable_view.Checked = false;
            serviceable_delete.Checked = false;
            slip_add.Checked = false;
            slip_update.Checked = false;
            slip_view.Checked = false;
            slip_delete.Checked = false;
        }
        private void loadPermisions()
        {
            string user_id = user_txt.SelectedValue.ToString();
            if (user_id!= "System.Data.DataRowView")
            {
                Uroles = service.rolesList(Convert.ToInt32(user_id));
                findPermission();
            }
            
        }
        private void findPermission()
        {
            try
            {
                clear();
                if (roles != null)
                {
                    foreach (var item in Uroles)
                    {
                        if (item.form == "assigment_slip_form")
                        {
                            if (item.roles_add == 1)
                            {
                                slip_add.Checked = true;
                            }
                            if (item.roles_update == 1)
                            {
                                slip_update.Checked = true;
                            }
                            if (item.roles_view == 1)
                            {
                                slip_view.Checked = true;
                            }
                            if (item.roles_delete == 1)
                            {
                                slip_delete.Checked = true;
                            }
                        }
                        else if (item.form == "store_form")
                        {
                            if (item.roles_add == 1)
                            {
                                store_add.Checked = true;
                            }
                            if (item.roles_update == 1)
                            {
                                store_update.Checked = true;
                            }
                            if (item.roles_view == 1)
                            {
                                store_view.Checked = true;
                            }
                            if (item.roles_delete == 1)
                            {
                                store_delete.Checked = true;
                            }
                        }
                        else if (item.form == "view_slips_form")
                        {
                            if (item.roles_add == 1)
                            {
                                reporting_add.Checked = true;
                            }
                            if (item.roles_update == 1)
                            {
                                reporting_update.Checked = true;
                            }
                            if (item.roles_view == 1)
                            {
                                reporting_view.Checked = true;
                            }
                            if (item.roles_delete == 1)
                            {
                                reporting_delete.Checked = true;
                            }
                        }
                        else if (item.form == "user")
                        {
                            if (item.roles_add == 1)
                            {
                                user_add.Checked = true;
                            }
                            if (item.roles_update == 1)
                            {
                                user_update.Checked = true;
                            }
                            if (item.roles_view == 1)
                            {
                                user_view.Checked = true;
                            }
                            if (item.roles_delete == 1)
                            {
                                user_delete.Checked = true;
                            }
                        }
                        else if (item.form == "brands_form")
                        {
                            if (item.roles_add == 1)
                            {
                                brand_add.Checked = true;
                            }
                            if (item.roles_update == 1)
                            {
                                brand_update.Checked = true;
                            }
                            if (item.roles_view == 1)
                            {
                                brand_view.Checked = true;
                            }
                            if (item.roles_delete == 1)
                            {
                                brand_delete.Checked = true;
                            }
                        }
                        else if (item.form == "team_form")
                        {
                            if (item.roles_add == 1)
                            {
                                team_add.Checked = true;
                            }
                            if (item.roles_update == 1)
                            {
                                team_update.Checked = true;
                            }
                            if (item.roles_view == 1)
                            {
                                team_view.Checked = true;
                            }
                            if (item.roles_delete == 1)
                            {
                                team_delete.Checked = true;
                            }
                        }
                        else if (item.form == "client_form")
                        {
                            if (item.roles_add == 1)
                            {
                                client_add.Checked = true;
                            }
                            if (item.roles_update == 1)
                            {
                                client_update.Checked = true;
                            }
                            if (item.roles_view == 1)
                            {
                                client_view.Checked = true;
                            }
                            if (item.roles_delete == 1)
                            {
                                client_delete.Checked = true;
                            }
                        }
                        else if (item.form == "repairs_form")
                        {
                            if (item.roles_add == 1)
                            {
                                send_to_repair_add.Checked = true;
                            }
                            if (item.roles_update == 1)
                            {
                                send_to_repair_update.Checked = true;
                            }
                            if (item.roles_view == 1)
                            {
                                send_to_repair_view.Checked = true;
                            }
                            if (item.roles_delete == 1)
                            {
                                send_to_repair_delete.Checked = true;
                            }
                        }
                        else if (item.form == "serviceable_form")
                        {
                            if (item.roles_add == 1)
                            {
                                serviceable_add.Checked = true;
                            }
                            if (item.roles_update == 1)
                            {
                                serviceable_update.Checked = true;
                            }
                            if (item.roles_view == 1)
                            {
                                serviceable_view.Checked = true;
                            }
                            if (item.roles_delete == 1)
                            {
                                serviceable_delete.Checked = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void store_btn_Click(object sender, EventArgs e)
        {
            try
            {
                saveRoles(Convert.ToInt32(user_txt.SelectedValue), store_add, store_update, store_delete, store_view, "store_form");
                MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void reporting_btn_Click(object sender, EventArgs e)
        {
            try
            {
                saveRoles(
                    Convert.ToInt32(user_txt.SelectedValue),
                    reporting_add,
                    reporting_update,
                    reporting_delete,
                    reporting_view,
                    "view_slips_form");
                MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void user_btn_Click(object sender, EventArgs e)
        {
            try
            {
                saveRoles(
                    Convert.ToInt32(user_txt.SelectedValue),
                    user_add,
                    user_update,
                    user_delete,
                    user_view,
                    "user");
                MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void brand_btn_Click(object sender, EventArgs e)
        {
            try
            {
                saveRoles(
                    Convert.ToInt32(user_txt.SelectedValue),
                    brand_add,
                    brand_update,
                    brand_delete,
                    brand_view,
                    "brands_form");
                MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void team_btn_Click(object sender, EventArgs e)
        {
            try
            {
                saveRoles(
                    Convert.ToInt32(user_txt.SelectedValue),
                    team_add,
                    team_update,
                    team_delete,
                    team_view,
                    "team_form");
                MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void client__btn_Click(object sender, EventArgs e)
        {
            try
            {
                saveRoles(
                    Convert.ToInt32(user_txt.SelectedValue),
                    client_add,
                    client_update,
                    client_delete,
                    client_view,
                    "client_form");
                MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void send_to_repair_btn_Click(object sender, EventArgs e)
        {

            try
            {
                saveRoles(
                    Convert.ToInt32(user_txt.SelectedValue),
                    send_to_repair_add,
                    send_to_repair_update,
                    send_to_repair_delete,
                    send_to_repair_view,
                    "repairs_form");
                MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void serviceable_btn_Click(object sender, EventArgs e)
        {
            try
            {
                saveRoles(
                    Convert.ToInt32(user_txt.SelectedValue),
                    serviceable_add,
                    serviceable_update,
                    serviceable_delete,
                    serviceable_view,
                    "serviceable_form");
                MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void user_txt_TextChanged(object sender, EventArgs e)
        {
            loadPermisions();
        }

        private void billing_btn_Click(object sender, EventArgs e)
        {
            try
            {
                saveRoles(
                    Convert.ToInt32(user_txt.SelectedValue),
                    billing_add,
                    billing_update,
                    billing_delete,
                    billing_view,
                    "billing_form");
                MessageBox.Show(messages.successfull, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
