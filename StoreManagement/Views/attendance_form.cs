﻿using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using StoreManagement.Reports.view;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Views
{
    public partial class attendance_form : Form
    {
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        CommonHelper commonHelper = new CommonHelper();
        Messages messages = new Messages();
        //DataView dv;
        Attendance[] attendance_List;
        string date = DateTime.Now.ToShortDateString();
        public attendance_form()
        {
            InitializeComponent();
        }

        private void attendance_form_Load(object sender, EventArgs e)
        {
            refresh();
        }
        private void refresh()
        {
            attendanceSite_id.SelectedIndex = 0;
            // dv = new DataView(service.attendance_dataTable(date, date));
            employeesList();
            attendanceList();
            //attendance_status.Text = "";
            //attendance_save.Enabled = true;

        }
        private string isEditable(int index,int id)
        {
            if (id == 0)
            {
               // attendance_list.Rows[index].ReadOnly = true;
                //attendance_list.Rows[index].Cells["edit"].ReadOnly = true;
                return "Save It";
            }
            else
            {
                attendance_list.Rows[index].Cells["edit"].ReadOnly = false;
                return "Saved";
            }
        }
        private void attendanceList()
        {
            try
            {
                attendance_List = service.attendanceList(attendance_from.Text, attendance_to.Text, attendanceSite());
                if (attendance_List != null)
                {
                    attendance_list.Rows.Clear();
                    attendance_list.Refresh();
                    int i = 0;
                    foreach (var item in attendance_List)
                    {
                        DataGridViewRow row = (DataGridViewRow)attendance_list.Rows[0].Clone();
                        attendance_list.Rows.Add(row);
                        attendance_list.Rows[i].Cells["edit"].Value = isEditable(i, item.id);
                        attendance_list.Rows[i].Cells["id"].Value = item.id;
                        attendance_list.Rows[i].Cells["emp_id"].Value = item.emp_id;
                        attendance_list.Rows[i].Cells["name"].Value = item.name;
                        attendance_list.Rows[i].Cells["status"].Value = item.status;
                        attendance_list.Rows[i].Cells["description"].Value = item.description;
                        attendance_list.Rows[i].Cells["department"].Value = item.department;
                        i = i + 1;
                    }
                }
                else
                {
                    attendance_list.Rows.Clear();
                    attendance_list.Refresh();
                    MessageBox.Show("Something went wrong!");
                }
                gridViewProperties();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void employeesList()
        {
           // DataTable employees = service.dropdown("employee", 0);
           // commonHelper.addNewRow(employees);
           // emp_id.DataSource = employees;
           // emp_id.DisplayMember = "name";
           //emp_id.ValueMember = "id";
        }
        private void gridViewProperties()
        {
           // attendance_list.Columns["id"].Visible = true;
           // attendance_list.Columns["emp_id"].Visible = true;
            //attendance_list.Columns["attenDate"].Width = 200;
           // attendance_list.Columns["update_btn"].Visible = true;
           // attendance_list.Columns["status"].Visible = true;
        }
        private void attendance_search_ValueChanged(object sender, EventArgs e)
        {

        }

        private void attendance_update_Click(object sender, EventArgs e)
        {

        }
        private int attendanceSite()
        {
            if (attendanceSite_id.Text == "Office Attendance")
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        private void save(int index)
        {
            try
            {
                if (attendance_list.Rows[index].Cells["id"].Value.ToString() == "0")
                {
                    if (attendance_list.Rows[index].Cells["emp_id"].Value.ToString() != "0" &&
                        attendance_list.Rows[index].Cells["status"].Value.ToString() != "")
                    {
                        Attendance attendance = new BS_SERVICES.Attendance();
                        attendance.emp_id = Convert.ToInt32(attendance_list.Rows[index].Cells["emp_id"].Value.ToString());
                        attendance.date = attendance_from.Text;
                        attendance.status = attendance_list.Rows[index].Cells["status"].Value.ToString();
                        attendance.check_in = attendance_time.Text;
                        attendance.description = attendance_list.Rows[index].Cells["description"].Value.ToString();
                        attendance.department = attendance_list.Rows[index].Cells["department"].Value.ToString();
                        attendance.attendanceSite_id = attendanceSite();
                        attendance.updated_at = date;
                        service.attendanceSave(attendance);
                        info_lbl.Text = attendance_list.Rows[index].Cells["name"].Value.ToString()+" | "+ attendance.status;


                    }
                    else
                    {
                        MessageBox.Show(messages.required, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                else
                {
                    MessageBox.Show(messages.bs_alert, messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Doublicate Entry", messages.bs_successfull, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }           
        }
        private void attendance_save_Click(object sender, EventArgs e)
        {
           
        }

      //  private bool Validation()
       // {
           // string[] param = {
              //  attendance_status.Text
       // };
            //return commonHelper.Validator(param);            
     //   }

        private void Search_btn_Click(object sender, EventArgs e)
        {
            //dv = new DataView(service.attendance_dataTable(attendance_from.Text, attendance_to.Text));
            attendanceList();
        }

        private void summrized_report_btn_Click(object sender, EventArgs e)
        {
            attendanceSummarizedFROM form = new attendanceSummarizedFROM();
            form.ShowDialog();
        }

        private void attendance_list_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                save(e.RowIndex);
                //MessageBox.Show("Button Clicked");
            }
        }

        private void refresh_btn_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void detailed_report_btn_Click(object sender, EventArgs e)
        {
            attandanceDetailFORM from = new attandanceDetailFORM();
            from.ShowDialog();
        }

        private void attendanceSite_id_TextChanged(object sender, EventArgs e)
        {
            attendanceList();
        }
    }
}
