﻿namespace StoreManagement.Views
{
    partial class bin_location_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.exportToExcel = new System.Windows.Forms.Button();
            this.view_btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.search_date = new System.Windows.Forms.DateTimePicker();
            this.search_btn = new System.Windows.Forms.Button();
            this.search_type = new System.Windows.Forms.ComboBox();
            this.search_consultant = new System.Windows.Forms.ComboBox();
            this.search_client = new System.Windows.Forms.ComboBox();
            this.column = new System.Windows.Forms.ComboBox();
            this.search = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.bin_location_list = new System.Windows.Forms.DataGridView();
            this.sr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.project = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.client_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consultant_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.type = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.print_report = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.print_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.email_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.payments = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.payment_advance = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.payment_balance = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.payment_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.delivery = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.delivery_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.report = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.save = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bin_location_list)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.bin_location_list, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1714, 463);
            this.tableLayoutPanel1.TabIndex = 12;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.search_date);
            this.panel1.Controls.Add(this.search_btn);
            this.panel1.Controls.Add(this.search_type);
            this.panel1.Controls.Add(this.search_consultant);
            this.panel1.Controls.Add(this.search_client);
            this.panel1.Controls.Add(this.column);
            this.panel1.Controls.Add(this.search);
            this.panel1.Controls.Add(this.metroLabel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1708, 31);
            this.panel1.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.7929F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.71598F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.exportToExcel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.view_btn, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(863, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(845, 31);
            this.tableLayoutPanel2.TabIndex = 19;
            // 
            // exportToExcel
            // 
            this.exportToExcel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exportToExcel.Location = new System.Drawing.Point(3, 3);
            this.exportToExcel.Name = "exportToExcel";
            this.exportToExcel.Size = new System.Drawing.Size(119, 25);
            this.exportToExcel.TabIndex = 17;
            this.exportToExcel.Text = "Export To Excel";
            this.exportToExcel.UseVisualStyleBackColor = true;
            this.exportToExcel.Visible = false;
            this.exportToExcel.Click += new System.EventHandler(this.exportToExcel_Click);
            // 
            // view_btn
            // 
            this.view_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.view_btn.Location = new System.Drawing.Point(128, 3);
            this.view_btn.Name = "view_btn";
            this.view_btn.Size = new System.Drawing.Size(431, 25);
            this.view_btn.TabIndex = 18;
            this.view_btn.Text = "View Report";
            this.view_btn.UseVisualStyleBackColor = true;
            this.view_btn.Click += new System.EventHandler(this.view_btn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Impact", 15F);
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(565, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(277, 31);
            this.label2.TabIndex = 9;
            this.label2.Text = "Bin Location";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // search_date
            // 
            this.search_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.search_date.Location = new System.Drawing.Point(182, 3);
            this.search_date.Name = "search_date";
            this.search_date.Size = new System.Drawing.Size(297, 20);
            this.search_date.TabIndex = 16;
            this.search_date.Visible = false;
            // 
            // search_btn
            // 
            this.search_btn.Location = new System.Drawing.Point(485, 4);
            this.search_btn.Name = "search_btn";
            this.search_btn.Size = new System.Drawing.Size(74, 22);
            this.search_btn.TabIndex = 15;
            this.search_btn.Text = "Search";
            this.search_btn.UseVisualStyleBackColor = true;
            this.search_btn.Click += new System.EventHandler(this.search_btn_Click);
            // 
            // search_type
            // 
            this.search_type.FormattingEnabled = true;
            this.search_type.Items.AddRange(new object[] {
            "BS",
            "CP",
            "EIA",
            "EOI",
            "FS",
            "GT",
            "MD",
            "POC",
            "Propo",
            "RFP",
            "RH",
            "Tender",
            "Testing",
            "TG",
            "TP"});
            this.search_type.Location = new System.Drawing.Point(182, 3);
            this.search_type.Name = "search_type";
            this.search_type.Size = new System.Drawing.Size(208, 21);
            this.search_type.TabIndex = 14;
            this.search_type.Visible = false;
            // 
            // search_consultant
            // 
            this.search_consultant.FormattingEnabled = true;
            this.search_consultant.Location = new System.Drawing.Point(182, 3);
            this.search_consultant.Name = "search_consultant";
            this.search_consultant.Size = new System.Drawing.Size(297, 21);
            this.search_consultant.TabIndex = 13;
            this.search_consultant.Visible = false;
            // 
            // search_client
            // 
            this.search_client.FormattingEnabled = true;
            this.search_client.Location = new System.Drawing.Point(182, 3);
            this.search_client.Name = "search_client";
            this.search_client.Size = new System.Drawing.Size(297, 21);
            this.search_client.TabIndex = 12;
            this.search_client.Visible = false;
            // 
            // column
            // 
            this.column.FormattingEnabled = true;
            this.column.Items.AddRange(new object[] {
            "project",
            "client",
            "cell",
            "name",
            "consultant",
            "type",
            "year",
            "folder",
            "print",
            "print_date",
            "email",
            "email_date",
            "payments",
            "payments_advance",
            "payments_balance",
            "delivery",
            "delivery_date"});
            this.column.Location = new System.Drawing.Point(44, 4);
            this.column.Name = "column";
            this.column.Size = new System.Drawing.Size(136, 21);
            this.column.TabIndex = 10;
            this.column.TextChanged += new System.EventHandler(this.column_TextChanged);
            // 
            // search
            // 
            // 
            // 
            // 
            this.search.CustomButton.Image = null;
            this.search.CustomButton.Location = new System.Drawing.Point(160, 1);
            this.search.CustomButton.Name = "";
            this.search.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.search.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.search.CustomButton.TabIndex = 1;
            this.search.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.search.CustomButton.UseSelectable = true;
            this.search.CustomButton.Visible = false;
            this.search.Lines = new string[0];
            this.search.Location = new System.Drawing.Point(182, 3);
            this.search.MaxLength = 32767;
            this.search.Name = "search";
            this.search.PasswordChar = '\0';
            this.search.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.search.SelectedText = "";
            this.search.SelectionLength = 0;
            this.search.SelectionStart = 0;
            this.search.ShortcutsEnabled = true;
            this.search.Size = new System.Drawing.Size(297, 23);
            this.search.TabIndex = 3;
            this.search.UseSelectable = true;
            this.search.Visible = false;
            this.search.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.search.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(9, 5);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(34, 19);
            this.metroLabel2.TabIndex = 8;
            this.metroLabel2.Text = "By : ";
            // 
            // bin_location_list
            // 
            this.bin_location_list.BackgroundColor = System.Drawing.Color.White;
            this.bin_location_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bin_location_list.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sr,
            this.Id,
            this.project,
            this.client_id,
            this.cell,
            this.name,
            this.consultant_id,
            this.type,
            this.year,
            this.folder,
            this.print_report,
            this.print_date,
            this.email,
            this.email_date,
            this.payments,
            this.payment_advance,
            this.payment_balance,
            this.payment_date,
            this.delivery,
            this.delivery_date,
            this.report,
            this.save});
            this.bin_location_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bin_location_list.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.bin_location_list.Location = new System.Drawing.Point(3, 40);
            this.bin_location_list.Name = "bin_location_list";
            this.bin_location_list.Size = new System.Drawing.Size(1708, 420);
            this.bin_location_list.TabIndex = 5;
            this.bin_location_list.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.bin_location_list_CellClick);
            this.bin_location_list.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.bin_location_list_CellContentClick);
            this.bin_location_list.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.bin_location_list_ColumnWidthChanged);
            this.bin_location_list.Scroll += new System.Windows.Forms.ScrollEventHandler(this.bin_location_list_Scroll);
            // 
            // sr
            // 
            this.sr.HeaderText = "Sr";
            this.sr.Name = "sr";
            this.sr.Width = 50;
            // 
            // Id
            // 
            this.Id.HeaderText = "id";
            this.Id.Name = "Id";
            this.Id.Width = 50;
            // 
            // project
            // 
            this.project.FillWeight = 250F;
            this.project.HeaderText = "Project";
            this.project.Name = "project";
            this.project.ToolTipText = "Project";
            this.project.Width = 250;
            // 
            // client_id
            // 
            this.client_id.FillWeight = 250F;
            this.client_id.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.client_id.HeaderText = "Client";
            this.client_id.Name = "client_id";
            this.client_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.client_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.client_id.Width = 250;
            // 
            // cell
            // 
            this.cell.HeaderText = "Cell";
            this.cell.Name = "cell";
            // 
            // name
            // 
            this.name.HeaderText = "Name";
            this.name.Name = "name";
            // 
            // consultant_id
            // 
            this.consultant_id.FillWeight = 250F;
            this.consultant_id.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.consultant_id.HeaderText = "Consultant";
            this.consultant_id.Name = "consultant_id";
            this.consultant_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.consultant_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.consultant_id.Width = 250;
            // 
            // type
            // 
            this.type.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.type.HeaderText = "Type";
            this.type.Items.AddRange(new object[] {
            "BS",
            "CP",
            "EIA",
            "EOI",
            "FS",
            "GT",
            "MD",
            "POC",
            "Propo",
            "RFP",
            "RH",
            "Tender",
            "Testing",
            "TG",
            "TP"});
            this.type.Name = "type";
            this.type.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // year
            // 
            this.year.HeaderText = "Year";
            this.year.Name = "year";
            this.year.Width = 70;
            // 
            // folder
            // 
            this.folder.HeaderText = "Folder";
            this.folder.Name = "folder";
            this.folder.Width = 70;
            // 
            // print_report
            // 
            this.print_report.HeaderText = "Print";
            this.print_report.Name = "print_report";
            this.print_report.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.print_report.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // print_date
            // 
            this.print_date.HeaderText = "Print Date";
            this.print_date.Name = "print_date";
            // 
            // email
            // 
            this.email.HeaderText = "Email";
            this.email.Name = "email";
            // 
            // email_date
            // 
            this.email_date.HeaderText = "Email Date";
            this.email_date.Name = "email_date";
            // 
            // payments
            // 
            this.payments.HeaderText = "Payments";
            this.payments.Name = "payments";
            this.payments.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.payments.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // payment_advance
            // 
            this.payment_advance.HeaderText = "Payment Advance";
            this.payment_advance.Name = "payment_advance";
            this.payment_advance.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.payment_advance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // payment_balance
            // 
            this.payment_balance.HeaderText = "Payment Balance";
            this.payment_balance.Name = "payment_balance";
            this.payment_balance.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.payment_balance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // payment_date
            // 
            this.payment_date.HeaderText = "Payment Date";
            this.payment_date.Name = "payment_date";
            // 
            // delivery
            // 
            this.delivery.HeaderText = "Delivery";
            this.delivery.Name = "delivery";
            this.delivery.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.delivery.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // delivery_date
            // 
            this.delivery_date.HeaderText = "Delivery Date";
            this.delivery_date.Name = "delivery_date";
            // 
            // report
            // 
            this.report.HeaderText = "Report View";
            this.report.Name = "report";
            // 
            // save
            // 
            this.save.HeaderText = "Save";
            this.save.Name = "save";
            this.save.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.save.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.save.Text = "Save";
            this.save.Width = 50;
            // 
            // bin_location_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1714, 463);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "bin_location_form";
            this.Text = "BIN LOCATION ";
            this.Load += new System.EventHandler(this.bin_location_form_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bin_location_list)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroTextBox search;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.DataGridView bin_location_list;
        private System.Windows.Forms.ComboBox column;
        private System.Windows.Forms.ComboBox search_type;
        private System.Windows.Forms.ComboBox search_consultant;
        private System.Windows.Forms.ComboBox search_client;
        private System.Windows.Forms.DataGridViewTextBoxColumn sr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn project;
        private System.Windows.Forms.DataGridViewComboBoxColumn client_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn cell;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewComboBoxColumn consultant_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn type;
        private System.Windows.Forms.DataGridViewTextBoxColumn year;
        private System.Windows.Forms.DataGridViewTextBoxColumn folder;
        private System.Windows.Forms.DataGridViewCheckBoxColumn print_report;
        private System.Windows.Forms.DataGridViewTextBoxColumn print_date;
        private System.Windows.Forms.DataGridViewCheckBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn email_date;
        private System.Windows.Forms.DataGridViewCheckBoxColumn payments;
        private System.Windows.Forms.DataGridViewCheckBoxColumn payment_advance;
        private System.Windows.Forms.DataGridViewCheckBoxColumn payment_balance;
        private System.Windows.Forms.DataGridViewTextBoxColumn payment_date;
        private System.Windows.Forms.DataGridViewCheckBoxColumn delivery;
        private System.Windows.Forms.DataGridViewTextBoxColumn delivery_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn report;
        private System.Windows.Forms.DataGridViewButtonColumn save;
        private System.Windows.Forms.Button search_btn;
        private System.Windows.Forms.DateTimePicker search_date;
        private System.Windows.Forms.Button exportToExcel;
        private System.Windows.Forms.Button view_btn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    }
}