﻿using MetroFramework.Forms;
using StoreManagement.BS_SERVICES;
using StoreManagement.Common;
using StoreManagement.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement
{
    public partial class Home : Form
    {
        CommonHelper common = new CommonHelper();
        Messages messages = new Messages();
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        public Home()
        {
            InitializeComponent();
        }
        Roles[] roles;
        private bool findPermission(string form)
        {
            try
            {
                if (roles!=null)
                {
                    foreach (var item in roles)
                    {
                        if (login.userCredentials.roles == "Admin" || item.form == form && item.roles_view == 1)
                        {
                            return true;
                        }
                    }
                }                
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            return false;
        }
        private void Home_Load(object sender, EventArgs e)
        {
            username_txt.Text = login.userCredentials.username+" ( "+ login.userCredentials.roles + " )";
            roles = service.rolesList(login.userCredentials.id);
            common.tabControler(content_panel, "billing_form");
            //if (login.usercredentials.roles == "billing user")
            //{
            //    if (findpermission("billing_form"))
            //        common.tabcontroler(content_panel, "billing_form");
            //}
            //else
            //{
            //    if (findpermission("assigment_slip_form"))
            //        common.tabcontroler(content_panel, "assigment_slip_form");
            //}

        }

        private void Title_ASSIGNMENT_SLIP_Click(object sender, EventArgs e)
        {
            if (findPermission("assigment_slip_form"))
                common.tabControler(content_panel, "assigment_slip_form");
        }

        private void Title_VIEW_SLIPS_Click(object sender, EventArgs e)
        {
            if (findPermission("view_slips_form"))
                common.tabControler(content_panel, "view_slips_form");
        }

        private void Title_STORE_Click(object sender, EventArgs e)
        {
            if (findPermission("store_form"))
                common.tabControler(content_panel, "store_form");
        }

        private void Title_TEAM_Click(object sender, EventArgs e)
        {
            if (findPermission("team_form"))
                common.tabControler(content_panel, "team_form");
        }

        private void Title_BRANDS_Click(object sender, EventArgs e)
        {
            if (findPermission("brands_form"))
                common.tabControler(content_panel, "brands_form");
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            if (findPermission("serviceable_form"))
                common.tabControler(content_panel, "serviceable_form");
        }

        private void Title_ASSIGNMENT_CLIENT_Click(object sender, EventArgs e)
        {
            if (findPermission("client_form"))
                common.tabControler(content_panel, "client_form");
        }

        private void SEND_TO_REPAIR_Click(object sender, EventArgs e)
        {
            if (findPermission("repairs_form"))
                common.tabControler(content_panel, "repairs_form");
        }

        private void logout_Click(object sender, EventArgs e)
        {
            this.Close();
            login l = new login();
            l.Show();
        }

        private void user_Click(object sender, EventArgs e)
        {
            if (findPermission("user"))
                common.tabControler(content_panel, "user");
        }

        private void PERMISSION_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles == "Admin" || login.bs_username== "admin")
            {
                roles_form rolesform = new roles_form();
                rolesform.ShowDialog();
            }
            else
            {
                MessageBox.Show(messages.authrization, messages.bs_error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void billing_btn_Click(object sender, EventArgs e)
        {
            if (findPermission("billing_form"))
                common.tabControler(content_panel, "billing_form");            
        }

        private void setting_btn_Click(object sender, EventArgs e)
        {
            if (login.userCredentials.roles== "Admin")
            {
                common.tabControler(content_panel, "settings_form");
            }
        }

        private void rating_btn_Click(object sender, EventArgs e)
        {
            rateList_form frm = new Views.rateList_form();
            frm.ShowDialog();
        }

        private void employee_btn_Click(object sender, EventArgs e)
        {
            common.tabControler(content_panel, "employees_form");
        }

        private void attendance_btn_Click(object sender, EventArgs e)
        {            
            common.tabControler(content_panel, "attendance_form");
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            bin_location_form form = new Views.bin_location_form();
            form.ShowDialog();
        }
    }
}
