﻿using StoreManagement.Reports.view;
using StoreManagement.Views;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.Common
{
    class CommonHelper
    {
        Messages messages = new Messages();
        BS_SERVICES.IbsStoreClient service = new BS_SERVICES.IbsStoreClient();
        public dynamic findView(string view)
        {
            if (view == "assigment_slip_form")
            {
                assigment_slip_form objForm = new assigment_slip_form();
                return objForm;
            }
            else if (view == "brands_form")
            {
                brands_form objForm = new brands_form();
                return objForm;
            }
            else if (view == "store_form")
            {
                store_form objForm = new store_form();
                return objForm;
            }
            else if (view == "team_form")
            {
                team_form objForm = new team_form();
                return objForm;
            }
            else if (view == "view_slips_form")
            {
                view_slips_form objForm = new view_slips_form();
                return objForm;
            }
            else if (view == "serviceable_form")
            {
                serviceable_form objForm = new serviceable_form();
                return objForm;
            }
            else if (view == "client_form")
            {
                client_form objForm = new client_form();
                return objForm;
            }
            else if (view == "repairs_form")
            {
                repairs_form objForm = new repairs_form();
                return objForm;
            }
            else if (view == "AssignmentFORM")
            {
                AssignmentFORM objForm = new AssignmentFORM();
                return objForm;
            }
            else if (view == "user")
            {
                user objForm = new user();
                return objForm;
            }
            else if (view == "InstrumentsFORM")
            {
                InstrumentsFORM objForm = new InstrumentsFORM();
                return objForm;
            }
            else if (view == "SiteInstrumentsDamageFORM")
            {
                SiteInstrumentsDamageFORM objForm = new SiteInstrumentsDamageFORM();
                return objForm;
            }
            else if (view == "RepairsFORM")
            {
                RepairsFORM objForm = new RepairsFORM();
                return objForm;
            }
            else if (view == "roles_form")
            {
                roles_form objForm = new roles_form();
                return objForm;
            }
            else if (view == "ClientsFORM")
            {
                ClientsFORM objForm = new ClientsFORM();
                return objForm;
            }
            else if (view == "TeamsFORM")
            {
                TeamsFORM objForm = new TeamsFORM();
                return objForm;
            }
            else if (view == "UsersFORM")
            {
                UsersFORM objForm = new UsersFORM();
                return objForm;
            }
            else if (view == "BrandsFORM")
            {
                BrandsFORM objForm = new BrandsFORM();
                return objForm;
            }
            else if (view == "billing_form")
            {
                billing_form objForm = new billing_form();
                return objForm;
            }
            else if (view == "settings_form")
            {
                settings_form objForm = new settings_form();
                return objForm;
            }
            else if (view == "TAX_FORM")
            {
                TAX_FORM objForm = new TAX_FORM();
                return objForm;
            }
            else if (view == "rateList_form")
            {
                rateList_form objForm = new rateList_form();
                return objForm;
            }
            else if (view == "employees_form")
            {
                employees_form objForm = new employees_form();
                return objForm;
            }
            else if (view == "attendance_form")
            {
                attendance_form objForm = new attendance_form();
                return objForm;
            }
            
            MessageBox.Show("Please enter corect form name", "Store Management System - Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return false;

        }
        public dynamic tabControler(Panel panel, string formName)
        {
            try
            {
                panel.Refresh();
                panel.Controls.Clear();
                var obj = findView(formName);
                obj.TopLevel = false;
                panel.Controls.Add(obj);
                obj.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                obj.Dock = DockStyle.Fill;
                obj.Show();
                return panel;
            }
            catch (FaultException ex)
            {
                MessageBox.Show(ex.Message);
                return panel;
            }

        }
        public int status(bool active, bool deActive)
        {
            if (active)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public bool Validator(string[] param)
        {
            try
            {
                for (int i = 0; i < param.Length; i++)
                {
                    if (param[i] == "" || param[i] == null || param[i] == " " || param[i] == string.Empty || param[i] == "Select One")
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (FaultException ex)
            {
                throw ex;
            }

        }
        public void isFolderExist(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
        public void addNewRow(DataTable dt, string name = "Select One")
        {
            DataRow dr = dt.NewRow();
            dr["name"] = name;
            dr["id"] = 0;
            dt.Rows.InsertAt(dr, 0);
        }
        public void addRowInListBox(ListBox lb, ComboBox cb,int quantity=0)
        {
            bool isdoublicate = false;
            if (lb.Items.Count >= 0)
            {
                foreach (var item in lb.Items)
                {
                    if (item.ToString().Contains(cb.Text))
                    {
                        isdoublicate = true;
                    }
                }
            }
            if (!isdoublicate)
            {
                if (cb.Text == "Select One")
                {
                    MessageBox.Show("Please select One!", messages.bs_worning, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (quantity>0)
                    {
                        int Instrumnetquantity = service.getQuantityById(Convert.ToInt32(cb.SelectedValue));
                        if (quantity<=Instrumnetquantity)
                        {
                            string name = cb.Text + " | Quantity - " + quantity+" |";
                            lb.Items.Add(new { name = name, id = cb.SelectedValue });
                        }
                        else
                        {
                            MessageBox.Show("Available quantity ('"+Instrumnetquantity+ "') is less than from given quantity ('" + quantity + "')", messages.bs_worning, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        
                    }
                    else
                    {
                        lb.Items.Add(new { name = cb.Text, id = cb.SelectedValue });
                    }
                    
                }
            }
            else
            {
                MessageBox.Show("This name already exists!", messages.bs_worning, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void addRowInListBoxConsumableInstruments(ListBox lb1, ComboBox cb1, int quantity)
        {
            bool isdoublicate = false;
            if (lb1.Items.Count >= 0)
            {
                foreach (var item in lb1.Items)
                {
                    if (item.ToString().Contains(cb1.Text))
                    {
                        isdoublicate = true;
                    }
                }
            }
            if (!isdoublicate)
            {
                if (cb1.Text == "Select One")
                {
                    MessageBox.Show("Please select One!", messages.bs_worning, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string name = cb1.Text + " | Quantity - " + quantity;
                    lb1.Items.Add(new { name = cb1.Text, id = cb1.SelectedValue });
                }

            }
            else
            {
                MessageBox.Show("This name already exists!", messages.bs_worning, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public string getInvoiceNumber()
        {
            return DateTime.Now.Millisecond.ToString() +
                DateTime.Now.Hour.ToString() + "-" +
                DateTime.Now.Month.ToString() +
                DateTime.Now.Year.ToString() + "-" +
                DateTime.Now.Day.ToString();
        }
        public string getAccessCode()
        {
            return DateTime.Now.Month.ToString() +
                DateTime.Now.Millisecond.ToString() +
                DateTime.Now.Minute.ToString() + "-" +
                DateTime.Now.Day.ToString() +
                DateTime.Now.Year.ToString() + "-" +
                DateTime.Now.Second.ToString();
        }
    }
}
