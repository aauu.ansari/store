﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreManagement.Common
{
    class Messages
    {
        public string successfull = "Successfully Saved";
        public string delete = "Successfully Deleted";
        public string update = "Successfully Updated";
        public string required = "Please fill all rquired fields";
        public string bs_successfull = "Building Standards Store- Successfull";
        public string bs_error = "Building Standards Store- Error";
        public string bs_worning = "Building Standards Store- Worning";
        public string bs_alert = "Building Standards Store- This record already saved";
        public string invalidLogin = "Enter Correct Username Or Password";
        public string authrization = "Sorry you are not authrized for this action :(";
        public string InternetException = "There is no Internet connection";
    }
    class FormsNames
    {
        public string[] formsList = {
            "assigment_slip_form"
                ,"brands_form"
                ,"store_form"
                ,"team_form"
                ,"view_slips_form"
                ,"serviceable_form"
                ,"client_form"
                ,"repairs_form"
                ,"AssignmentFORM"
                ,"user"
                ,"InstrumentsFORM"
                ,"SiteInstrumentsDamageFORM"
                ,"RepairsFORM"
                ,"billing_form"
        };
        public string assigment_slip_form = "assigment_slip_form";
        public string brands_form = "brands_form";
        public string store_form = "store_form";
        public string team_form = "team_form";
        public string view_slips_form = "view_slips_form";
        public string serviceable_form = "serviceable_form";
        public string client_form = "client_form";
        public string repairs_form = "repairs_form";
        public string AssignmentFORM = "AssignmentFORM";
        public string user = "user";
        public string InstrumentsFORM = "InstrumentsFORM";
        public string SiteInstrumentsDamageFORM = "SiteInstrumentsDamageFORM";
        public string RepairsFORM = "RepairsFORM";
        public string billing_form = "billing_form"; 
    }
}
